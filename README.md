<div id="top"></div>

<!-- ABOUT THE PROJECT -->
## About The Project

This project encapsulates all guides, tutorials, video links and data required for installing GSI's APUs, as well as developing, compiling and running programs includng GSI's APU.

<!-- GETTING STARTED -->
## Getting Started

* Developers and programmers should start with downloading and installing the SDK. See GSI-installation/software for resources and [here](https://gsitechnology.sharepoint.com/sites/STC/Installation/Forms/AllItems.aspx) for guides.

Following the installation, developers should follow the course in the programming guide in the following order:

1. gal tutorils
2. gvml tutorials
3. gdl tutorials


### Installation
* hardware installation: installation guide and resources for APU hardware installation on a PC can be found in [here](https://gsitechnology.sharepoint.com/sites/STC/Installation/Forms/AllItems.aspx)

<p align="right">(<a href="#top">back to top</a>)</p>


### Doing labs on fractals-2 (f-2) server:
Use forticlient VPN to access the server.

## Connect to VPN using FortiClient App:
Download [FortiClient VPN](https://www.fortinet.com/tw/support/product-downloads#vpn).  
The credentials are as follows:
<div align="center">
  <a >
    <img src="images/forticlient.png" alt="forticlient_credentials" width="30" height="30">
  </a>
</div>

## Connect to VPN using terminal (Ubuntu):
Download the forticlientsslvpn_linux_4 tar:  
wget http://cdn.software-mirrors.com/forticlientsslvpn_linux_4.4.2328.tar.gz

Uncompress the downloaded file:  
tar -xzvf forticlientsslvpn_linux_4.4.2328.tar.gz

Install ppp (in case you don't have it):  
sudo apt-get install ppp

Go to the installer setup directory:  
cd ./forticlientsslvpn/64bit/helper

Run the setup file:  
sudo ./setup.linux.sh 

return to forticlientsslvpn/64bit/:  
cd ..

Finally you can connect whenever you want using this command:  
./forticlientsslvpn_cli --server 209.36.1.31:10443 --vpnuser username  
You will be asked for the password

Once connected to VPN- log into the server from the terminal.   
ssh username@192.168.99.32  

**For user names and passwords contact us. See contact information below.**

Once logged into the server - activate conda environment:  
Display list of environments:  
conda env list

Activate the environment:  
conda activate build_env_x  
Where x = 1,2,3,4 (Find correct x in list)

Run the following command:  
source /home/public/env_set.env

Next, open your own directory and enter it:  
mkdir <my_directory>  
cd <my_directory>

Next, clone the repository:  
git clone https://bitbucket.org/gsitech/course101.git

Enter the required lab directory in programming_guide/ and start experimenting with the APU!

<!-- CONTACT -->
## Contact

Eli Moise- emoise@gsitechnology.com

Project Link: [https://bitbucket.org/gsitech/course101/](https://bitbucket.org/gsitech/course101/)

<p align="right">(<a href="#top">back to top</a>)</p>

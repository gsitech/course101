from lpython import packed, dataclass, ccallable, u32, u64

GD_LAB_2_CMD_ADD_F16 : u32 = u32(0)
GD_LAB_2_CMD_EXERCISE_0 : u32 = u32(1)
GD_LAB_2_CMD_EXERCISE_1 : u32 = u32(2)

@ccallable
@packed
@dataclass
class gd_lab_2_add_f16_data:
    input_a: u64 = u64(0)
    input_b: u64 = u64(0)
    output: u64 = u64(0)

@ccallable
@packed
@dataclass
class gd_lab_2_exercise_data:
    input_a: u64 = u64(0)
    input_b: u64 = u64(0)
    input_c: u64 = u64(0)
    output: u64 = u64(0)

@ccallable
@packed
@dataclass
class gd_lab_2_cmd:
    cmd : u32
    add_f16_data : gd_lab_2_add_f16_data = gd_lab_2_add_f16_data()
    exercise_data : gd_lab_2_exercise_data = gd_lab_2_exercise_data()
from lpython import (
    u16, i32, u16,
    ccallback, CPtr, c_p_pointer, Pointer,
)

from common import gd_lab_2_exercise_data, gd_lab_2_add_f16_data, gd_lab_2_cmd, GD_LAB_2_CMD_ADD_F16, GD_LAB_2_CMD_EXERCISE_0, GD_LAB_2_CMD_EXERCISE_1
from gvml_defs import GVML_VR16_0, GVML_VR16_1, GVML_VR16_2, GVML_VM_0
from gchl.lib import gchl_init_once, gchl_load_16, gchl_store_16, gchl_add_f16, gchl_mul_f16

from gsi.gal import gal_mem_handle_to_apu_ptr
from gsi.arc_utils import direct_dma_l4_to_l1_32k, direct_dma_l1_to_l4_32k

def add_f16_demo(data : gd_lab_2_add_f16_data) -> i32:
    ptr_in_a : CPtr = gal_mem_handle_to_apu_ptr(data.input_a)
    ptr_in_b : CPtr = gal_mem_handle_to_apu_ptr(data.input_b)
    ptr_out : CPtr = gal_mem_handle_to_apu_ptr(data.output)

    vr_a : u16 = GVML_VR16_0
    vr_b : u16 = GVML_VR16_1

    direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_a)
    gchl_load_16(vr_a, GVML_VM_0)
    direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_b)
    gchl_load_16(vr_b, GVML_VM_0)
    gchl_add_f16(vr_a, vr_a, vr_b)
    gchl_store_16(GVML_VM_0, vr_a)
    direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0)
    return 0

def exercise_0(data : gd_lab_2_exercise_data) -> i32:
    return -1

def exercise_1(data : gd_lab_2_exercise_data) -> i32:
    return -1

@ccallback
def task(in_: CPtr, out: CPtr) -> i32:
    cmd: Pointer[gd_lab_2_cmd] = c_p_pointer(in_, gd_lab_2_cmd)

    if cmd.cmd == GD_LAB_2_CMD_ADD_F16:
        gchl_init_once()
        return add_f16_demo(cmd.add_f16_data)
    elif cmd.cmd == GD_LAB_2_CMD_EXERCISE_0:
        gchl_init_once()
        return exercise_0(cmd.exercise_data)
    elif cmd.cmd == GD_LAB_2_CMD_EXERCISE_1:
        gchl_init_once()
        return exercise_1(cmd.exercise_data)
    else:
        return -1
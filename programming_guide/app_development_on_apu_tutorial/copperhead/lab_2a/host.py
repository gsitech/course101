import sys
from lpython import i8, u16, i32, u32, i64, u64, ccall, CPtr, sizeof, Pointer, c_p_pointer
from common import gd_lab_2_cmd, GD_LAB_2_CMD_ADD_F16, GD_LAB_2_CMD_EXERCISE_0, GD_LAB_2_CMD_EXERCISE_1
from ref_gvml import refgvml_add_f16, refgvml_mul_f16
import numpy as np
import ctypes

from gsi.sys_apu import gsi_libsys_init_wrapper, gsi_libsys_exit_wrapper
from gsi.gdl import (gdl_init_wrapper, gdl_exit_wrapper, find_and_allocate_context, gdl_context_free_wrapper,
                     gdl_mem_alloc_aligned_wrapper, gdl_mem_handle_is_null_wrapper, gdl_mem_handle_to_host_ptr_wrapper,
                     gdl_run_task_timeout_wrapper, gdl_mem_free_wrapper, gdl_mem_cpy_to_dev_wrapper, gdl_mem_cpy_from_dev_wrapper,
                     gdl_add_to_mem_handle_wrapper)

# constants - start
_32K : i64 = i64(32768)
SIZE_F16 : i64 = i64(2)
GDL_MEM_HANDLE_NULL : u64 = u64(0)
GDL_CONST_MAPPED_POOL : i32 = i32(0)
GDL_ALIGN_32 : i32 = i32(4)
# constants - end

# Helper functions - start
def create_random_f16_array(length : i64) -> np.ndarray:
    return np.random.random(length).astype(np.float16)

def create_empty_f16_array(length : i64) -> np.ndarray:
    return np.empty(length, dtype=np.float16)

def convert_cptr_to_np_array(cptr: CPtr, size=_32K) -> np.ndarray:
    return np.ctypeslib.as_array(ctypes.cast(
        cptr, ctypes.POINTER(ctypes.c_uint16)), shape=(size,))

def convert_np_array_to_cptr(np_array: np.ndarray) -> CPtr:
    return np_array.ctypes.data_as(ctypes.POINTER(ctypes.c_uint16))
# Helper functions - end

def run_lab_2_cmd(
        ctx_id : i64,
        _32k_f16_elements_src_a : np.ndarray,
        _32k_f16_elements_src_b : np.ndarray) -> np.ndarray:
    
    ret : np.ndarray = create_empty_f16_array(2**15)

    dev_cmd_buf : u64 = GDL_MEM_HANDLE_NULL
    io_dev_bufs : u64 = GDL_MEM_HANDLE_NULL

    io_dev_buf_size : i64 = SIZE_F16 * _32K
    io_dev_bus_total_size : i64 = io_dev_buf_size * i64(3)

    try:
        io_dev_bufs = gdl_mem_alloc_aligned_wrapper(ctx_id, io_dev_bus_total_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
            raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', io_dev_bus_total_size, 'bytes')    

        cmd_buf_size : i64 = sizeof(gd_lab_2_cmd)
        dev_cmd_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
            raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', cmd_buf_size, 'bytes')
        
        cmd_ptr: CPtr = gdl_mem_handle_to_host_ptr_wrapper(dev_cmd_buf)
        cmd: Pointer[gd_lab_2_cmd] = c_p_pointer(cmd_ptr, gd_lab_2_cmd)
        cmd.cmd = GD_LAB_2_CMD_ADD_F16
        cmd.add_f16_data.input_a = io_dev_bufs
        cmd.add_f16_data.input_b = gdl_add_to_mem_handle_wrapper(cmd.add_f16_data.input_a, io_dev_buf_size)
        cmd.add_f16_data.output = gdl_add_to_mem_handle_wrapper(cmd.add_f16_data.input_b, io_dev_buf_size)

        sts = gdl_mem_cpy_to_dev_wrapper(
            cmd.add_f16_data.input_a,
            convert_np_array_to_cptr(_32k_f16_elements_src_a),
            io_dev_buf_size)
        if sts:
            raise MemoryError('gdl_mem_cpy_to_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')

        sts = gdl_mem_cpy_to_dev_wrapper(
            cmd.add_f16_data.input_b,
            convert_np_array_to_cptr(_32k_f16_elements_src_b),
            io_dev_buf_size)
        if sts:
            raise MemoryError('gdl_mem_cpy_to_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')

        sts = gdl_run_task_timeout_wrapper(ctx_id, 1234, dev_cmd_buf, GDL_MEM_HANDLE_NULL)
        if sts:
            raise MemoryError('gdl_run_task_timeout() failed:', sts)

        sts = gdl_mem_cpy_from_dev_wrapper(
            convert_np_array_to_cptr(ret),
            cmd.add_f16_data.output,
            io_dev_buf_size)
        if sts:
            raise MemoryError('gdl_mem_cpy_from_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')

    finally:
        gdl_mem_free_wrapper(dev_cmd_buf)
        gdl_mem_free_wrapper(io_dev_bufs)

    return ret

def run_lab_2_cmd_and_check(ctx_id : i64) -> None:
    a : np.ndarray = create_random_f16_array(_32K)
    b : np.ndarray = create_random_f16_array(_32K)

    dst : np.ndarray = run_lab_2_cmd(ctx_id, a, b)
    ref : np.ndarray = refgvml_add_f16(a, b)

    for i in range(_32K):
        ref_val : np.float16 = ref[i]
        res_val : np.float16 = dst[i]
        if ref_val != res_val:
            print('Mismatch for element', i, 'result =', res_val, 'expected value =', ref_val)
            raise RuntimeError('Mismatch')
        
def run_lab_2_exercise_cmd(
        ctx_id : i64,
        _32k_f16_elements_src_a : np.ndarray,
        _32k_f16_elements_src_b : np.ndarray,
        _32k_f16_elements_src_c : np.ndarray,
        exercise_type : u32) -> np.ndarray:
    
    ret : np.ndarray = create_empty_f16_array(2**15)

    dev_cmd_buf : u64 = GDL_MEM_HANDLE_NULL
    io_dev_bufs : u64 = GDL_MEM_HANDLE_NULL

    io_dev_buf_size : i64 = SIZE_F16 * _32K
    io_dev_bus_total_size : i64 = io_dev_buf_size * i64(4)

    try:
        io_dev_bufs = gdl_mem_alloc_aligned_wrapper(ctx_id, io_dev_bus_total_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
            raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', io_dev_bus_total_size, 'bytes')

        cmd_buf_size : i64 = sizeof(gd_lab_2_cmd)
        dev_cmd_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
            raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', cmd_buf_size, 'bytes')
        
        cmd_ptr: CPtr = gdl_mem_handle_to_host_ptr_wrapper(dev_cmd_buf)
        cmd: Pointer[gd_lab_2_cmd] = c_p_pointer(cmd_ptr, gd_lab_2_cmd)
        cmd.cmd = exercise_type
        cmd.exercise_data.input_a = io_dev_bufs  
        cmd.exercise_data.input_b = gdl_add_to_mem_handle_wrapper(cmd.exercise_data.input_a, io_dev_buf_size)
        cmd.exercise_data.input_c = gdl_add_to_mem_handle_wrapper(cmd.exercise_data.input_b, io_dev_buf_size)
        cmd.exercise_data.output = gdl_add_to_mem_handle_wrapper(cmd.exercise_data.input_c, io_dev_buf_size)

        sts = gdl_mem_cpy_to_dev_wrapper(
            cmd.exercise_data.input_a,
            convert_np_array_to_cptr(_32k_f16_elements_src_a),
            io_dev_buf_size)
        if sts:
            raise MemoryError('gdl_mem_cpy_to_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')
        
        sts = gdl_mem_cpy_to_dev_wrapper(
            cmd.exercise_data.input_b,
            convert_np_array_to_cptr(_32k_f16_elements_src_b),
            io_dev_buf_size)
        if sts:
            raise MemoryError('gdl_mem_cpy_to_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')
        
        sts = gdl_mem_cpy_to_dev_wrapper(
            cmd.exercise_data.input_c,
            convert_np_array_to_cptr(_32k_f16_elements_src_c),
            io_dev_buf_size)
        if sts:
            raise MemoryError('gdl_mem_cpy_to_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')

        sts = gdl_run_task_timeout_wrapper(ctx_id, 1234, dev_cmd_buf, GDL_MEM_HANDLE_NULL)
        if sts:
            raise RuntimeError('gdl_run_task_timeout() failed:', sts)

        sts = gdl_mem_cpy_from_dev_wrapper(
            convert_np_array_to_cptr(ret),
            cmd.exercise_data.output,
            io_dev_buf_size)
        if sts:
            raise MemoryError('gdl_mem_cpy_from_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')

    finally:
        gdl_mem_free_wrapper(dev_cmd_buf)
        gdl_mem_free_wrapper(io_dev_bufs)

    return ret

def run_lab_2_exercise_0_cmd_and_check(ctx_id : i64) -> None:
    a : np.ndarray = create_random_f16_array(_32K)
    b : np.ndarray = create_random_f16_array(_32K)
    c : np.ndarray = create_random_f16_array(_32K)       

    dst : np.ndarray = run_lab_2_exercise_cmd(ctx_id, a, b, c, GD_LAB_2_CMD_EXERCISE_0)
    # TODO: implement the calculation
    ref : np.ndarray = np.zeros_like(dst)

    for i in range(i32(_32K)):
        ref_val : np.float16 = ref[i]
        res_val : np.float16 = dst[i]
        if ref_val != res_val:
            print('Mismatch for element', i, 'result =', res_val, 'expected value =', ref_val)
            raise RuntimeError('Mismatch')
        
def run_lab_2_exercise_1_cmd_and_check(ctx_id : i64) -> None:
    a : np.ndarray = create_random_f16_array(_32K)
    b : np.ndarray = create_random_f16_array(_32K)
    c : np.ndarray = create_random_f16_array(_32K)       

    dst : np.ndarray = run_lab_2_exercise_cmd(ctx_id, a, b, c, GD_LAB_2_CMD_EXERCISE_1)
    # TODO: implement the calculation
    ref : np.ndarray = np.zeros_like(dst)

    for i in range(i32(_32K)):
        ref_val : np.float16 = ref[i]
        res_val : np.float16 = dst[i]
        if ref_val != res_val:
            print('Mismatch for element', i, 'result =', res_val, 'expected value =', ref_val)
            raise RuntimeError('Mismatch')        

def parse_args() -> i32:
    if len(sys.argv) != 2:
        raise ValueError('usage: ', sys.argv[0], '[0 | 1 | 2] (add_f16_demo, exercise_0, exercise_1)')
    ret : i32 = i32(int(sys.argv[1]))
    if ret != 0 and ret != 1 and ret != 2:
        raise ValueError('usage: ', sys.argv[0], '[0 | 1 | 2] (add_f16_demo, exercise_0, exercise_1)')
    return ret

def main() -> None :
    gsi_libsys_init_wrapper()
    choice : i32 = parse_args()
    gdl_init_wrapper()
    ctx : i64 = find_and_allocate_context()
    # run lab
    if choice == i32(GD_LAB_2_CMD_ADD_F16):
        print('\nadd_f16 demo')
        run_lab_2_cmd_and_check(ctx)
    elif choice == i32(GD_LAB_2_CMD_EXERCISE_0):
        print('Exercise 0, a * (a + b):')
        run_lab_2_exercise_0_cmd_and_check(ctx)
    elif choice == i32(GD_LAB_2_CMD_EXERCISE_1):
        print('Exercise 1, a * b + a * c:')
        run_lab_2_exercise_1_cmd_and_check(ctx)
    print('Success')
    gdl_context_free_wrapper(ctx)
    gdl_exit_wrapper()
    gsi_libsys_exit_wrapper()

if __name__ == "__main__":
    main()
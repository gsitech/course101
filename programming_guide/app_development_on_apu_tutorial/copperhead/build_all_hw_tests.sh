#!/bin/bash

# set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

pushd $SCRIPT_DIR

# Create the environment
mamba env create --force -f environment.yml

# Activate the environment
# There should be an env variable named CONDA_EXE with the form <conda root dir>/bin/conda
if [[ -z "${CONDA_EXE}" ]]; then
	echo "env variable CONDA_EXE not found"
	exit 1
fi

s=${CONDA_EXE}
CONDA_ROOT="${s%%/bin/*}"

source ${CONDA_ROOT}/etc/profile.d/conda.sh
conda activate course101-lpython

# Now we're ready to build
echo "----> Building lab 0 hw (1 out of 1) ..."
./lab_0/build_hw.sh

echo "----> Building lab 1 hw (1 out of 2) ..."
./lab_1a/build_hw.sh
echo "----> Building lab 1 hw (1 out of 2) ..."
./lab_1b/build_hw.sh

echo "----> Building lab 2 hw (1 out of 2) ..."
./lab_2a/build_hw.sh
echo "----> Building lab 2 hw (1 out of 2) ..."
./lab_2b/build_hw.sh

echo "----> Building lab 3 hw (1 out of 2) ..."
./lab_3a/build_hw.sh
echo "----> Building lab 3 hw (2 out of 2) ..."
./lab_3b/build_hw.sh

echo "----> Building lab 4 hw (1 out of 2) ..."
./lab_4a/build_hw.sh
echo "----> Building lab 4 hw (2 out of 2) ..."
./lab_4b/build_hw.sh

echo "----> Building lab 5 hw (1 out of 2) ..."
./lab_5a/build_hw.sh
echo "----> Building lab 5 hw (2 out of 2) ..."
./lab_5b/build_hw.sh

echo "----> Building lab 6 hw (1 out of 7) ..."
./lab_6a/build_hw.sh
echo "----> Building lab 6 hw (2 out of 7) ..."
./lab_6b/build_hw.sh
echo "----> Building lab 6 hw (3 out of 7) ..."
./lab_6c/build_hw.sh
echo "----> Building lab 6 hw (4 out of 7) ..."
./lab_6d/build_hw.sh
echo "----> Building lab 6 hw (5 out of 7) ..."
./lab_6e/build_hw.sh
echo "----> Building lab 6 hw (6 out of 7) ..."
./lab_6f/build_hw.sh
echo "----> Building lab 6 hw (7 out of 7) ..."
./lab_6g/build_hw.sh

popd +0

echo "Finished building all labs"
from lpython import u16
import numpy as np
from refgvml import add_f16_ew, mul_f16_ew, sub_f16_ew

refgvml_add_f16 = np.vectorize(lambda lhs, rhs : np.uint16(add_f16_ew(np.float16(lhs).view(np.uint16), np.float16(rhs).view(np.uint16))).view(np.float16))
refgvml_sub_f16 = np.vectorize(lambda lhs, rhs : np.uint16(sub_f16_ew(np.float16(lhs).view(np.uint16), np.float16(rhs).view(np.uint16))).view(np.float16))
refgvml_mul_f16 = np.vectorize(lambda lhs, rhs : np.uint16(mul_f16_ew(np.float16(lhs).view(np.uint16), np.float16(rhs).view(np.uint16))).view(np.float16))
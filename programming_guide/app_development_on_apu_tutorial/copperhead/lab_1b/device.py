from lpython import (
    u16, i32, u32, u16,
    ccallback, CPtr, c_p_pointer, Pointer, InOut
)

from common import (
    gd_lab_1_dma_demo_data, gd_lab_1_lookup_demo_data, gd_lab_1_gmd_demo_data, gd_lab_1_fizzbuzz_exercise_data, gd_lab_1_cmd,
    GD_LAB_1_CMD_DMA_DEMO, GD_LAB_1_CMD_LOOKUP_DEMO, GD_LAB_1_CMD_GMD_DEMO, GD_LAB_1_CMD_FIZZBUZZ_EXERCISE, 
    GD_LAB_1_FIZZ, GD_LAB_1_BUZZ, GD_LAB_1_FIZZBUZZ
)
from gvml_defs import (
    GVML_VR16_0, GVML_VR16_1, GVML_VR16_2, 
    GVML_VM_0, 
    GVML_MRK0, GVML_MRK1, GVML_MRK2
)

from gchl.lib import gchl_init_once, gchl_load_16, gchl_store_16

from gsi.gal import gal_mem_handle_to_apu_ptr
from gsi.gvml import (gvml_create_index_16,
                      gvml_cpy_imm_16, gvml_mod_u16, gvml_lookup_16, gvml_eq_imm_16_msk,
                      gvml_eq_imm_16, gvml_cpy_m, gvml_or_m, gvml_and_m, gvml_count_m_g32k_wrapper,
                      gvml_get_marked_data, gvml_cpy_imm_16_mrk)
from gsi.arc_utils import direct_dma_l4_to_l1_32k, direct_dma_l1_to_l4_32k

def dma_demo(data : InOut[gd_lab_1_dma_demo_data]) -> i32:
    ptr_in : CPtr = gal_mem_handle_to_apu_ptr(data.input)
    ptr_out : CPtr = gal_mem_handle_to_apu_ptr(data.output)

    direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in)
    direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0)
    
    return 0

def lookup_demo(data : gd_lab_1_lookup_demo_data) -> i32:
    ptr_out : CPtr = gal_mem_handle_to_apu_ptr(data.output)

    vr_idx : u16 = GVML_VR16_0
    vr_input : u16 = GVML_VR16_1

    gvml_create_index_16(vr_idx)
    gvml_cpy_imm_16(vr_input, u16(data.input_len))
    gvml_mod_u16(vr_idx, vr_idx, vr_input)

    p : Pointer[u16] = c_p_pointer(data.input, u16)
    gvml_lookup_16(vr_input, vr_idx, p, data.input_len)

    gchl_store_16(GVML_VM_0, vr_input)
    direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0)

    return 0

def gmd_demo(data : gd_lab_1_gmd_demo_data) -> i32:
    ptr_out : CPtr = gal_mem_handle_to_apu_ptr(data.output)

    vr_idx : u16 = GVML_VR16_0
    mrk_multiple_of_64 : u16 = GVML_MRK0

    gvml_create_index_16(vr_idx)
    gvml_eq_imm_16_msk(mrk_multiple_of_64, vr_idx, u16(0), u16(0x3f))
    gvml_get_marked_data(ptr_out, vr_idx, u32(1), mrk_multiple_of_64, u32(512))

    return 0

def fizzbuzz_exercise(data : InOut[gd_lab_1_fizzbuzz_exercise_data]) -> i32:
    ptr_in : CPtr = gal_mem_handle_to_apu_ptr(data.input)
    ptr_out : CPtr = gal_mem_handle_to_apu_ptr(data.output)

    vr_input : u16 = GVML_VR16_0
    vr_divisor : u16 = GVML_VR16_1
    vr_output : u16 = GVML_VR16_2

    vmr_tmp : u16 = GVML_VM_0

    mrk_fizz : u16 = GVML_MRK0
    mrk_buzz : u16 = GVML_MRK1
    mrk_output : u16 = GVML_MRK2

    direct_dma_l4_to_l1_32k(vmr_tmp, ptr_in)
    gchl_load_16(vr_input, vmr_tmp)

    gvml_cpy_imm_16(vr_divisor, u16(3))
    gvml_mod_u16(vr_divisor, vr_input, vr_divisor)
    gvml_eq_imm_16(mrk_fizz, vr_divisor, u16(0))
    gvml_cpy_imm_16_mrk(vr_output, GD_LAB_1_FIZZ, mrk_fizz)
    gvml_cpy_m(mrk_output, mrk_fizz)

    gvml_cpy_imm_16(vr_divisor, u16(5))
    gvml_mod_u16(vr_divisor, vr_input, vr_divisor)
    gvml_eq_imm_16(mrk_buzz, vr_divisor, u16(0))
    gvml_cpy_imm_16_mrk(vr_output, GD_LAB_1_BUZZ, mrk_buzz)
    gvml_or_m(mrk_output, mrk_output, mrk_buzz)

    gvml_and_m(mrk_fizz, mrk_fizz, mrk_buzz)
    gvml_cpy_imm_16_mrk(vr_output, GD_LAB_1_FIZZBUZZ, mrk_fizz)

    count : u32 = gvml_count_m_g32k_wrapper(mrk_output)
    gvml_create_index_16(vr_divisor)

    gvml_get_marked_data(ptr_out, vr_divisor, u32(2), mrk_output, count)

    data.output_len = count

    return 0


@ccallback
def task(in_: CPtr, out: CPtr) -> i32:
    cmd: Pointer[gd_lab_1_cmd] = c_p_pointer(in_, gd_lab_1_cmd)

    if cmd.cmd == GD_LAB_1_CMD_DMA_DEMO:
        gchl_init_once()
        return dma_demo(cmd.dma_demo_data)
    elif cmd.cmd == GD_LAB_1_CMD_LOOKUP_DEMO:
        gchl_init_once()
        return lookup_demo(cmd.lookup_demo_data)
    elif cmd.cmd == GD_LAB_1_CMD_GMD_DEMO:
        gchl_init_once()
        return gmd_demo(cmd.gmd_demo_data )
    elif cmd.cmd == GD_LAB_1_CMD_FIZZBUZZ_EXERCISE:
        gchl_init_once()
        return fizzbuzz_exercise(cmd.fizzbuzz_exercise_data)
    
    return -1
        
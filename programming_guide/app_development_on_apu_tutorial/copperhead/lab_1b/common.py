from lpython import (
    u16, u32, u64,
    packed, dataclass, ccallable
)

GD_LAB_1_CMD_DMA_DEMO : u32 = u32(0)
GD_LAB_1_CMD_LOOKUP_DEMO : u32 = u32(1)
GD_LAB_1_CMD_GMD_DEMO : u32 = u32(2)
GD_LAB_1_CMD_FIZZBUZZ_EXERCISE : u32 = u32(3)

GD_LAB_1_FIZZ : u16 = u16(0xf)
GD_LAB_1_BUZZ : u16 = u16(0xb)
GD_LAB_1_FIZZBUZZ : u16 = u16(0xfb)

@ccallable
@packed
@dataclass
class gd_lab_1_dma_demo_data:
    input: u64 = u64(0)
    output: u64 = u64(0)

@ccallable
@packed
@dataclass
class gd_lab_1_lookup_demo_data:
    input: u16[8] = u16(0)
    input_len: u32 = u32(0)
    output: u64 = u64(0)

@ccallable
@packed
@dataclass
class gd_lab_1_gmd_demo_data:   
    output: u64 = u64(0)

@ccallable
@packed
@dataclass
class gd_lab_1_fizzbuzz_exercise_data:  
    input: u64 = u64(0)
    output: u64 = u64(0)
    output_len: u32 = u32(0)

@ccallable
@packed
@dataclass
class gd_lab_1_cmd:
    cmd : u32
    dma_demo_data : gd_lab_1_dma_demo_data = gd_lab_1_dma_demo_data()
    lookup_demo_data : gd_lab_1_lookup_demo_data = gd_lab_1_lookup_demo_data()
    gmd_demo_data : gd_lab_1_gmd_demo_data = gd_lab_1_gmd_demo_data()
    fizzbuzz_exercise_data : gd_lab_1_fizzbuzz_exercise_data = gd_lab_1_fizzbuzz_exercise_data()

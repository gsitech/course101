#!/bin/bash

# set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export PYTHONPATH="$CONDA_PREFIX/share/lpython/lib/lpython:\
${SCRIPT_DIR}/..:\
${SCRIPT_DIR}/../declarations:\
${SCRIPT_DIR}/../declarations/gsi:\
${SCRIPT_DIR}/emulation"

export LPYTHON_PY_MOD_PATH=${SCRIPT_DIR}/build
export LPYTHON_PY_MOD_NAME=lab

python ${SCRIPT_DIR}/host.py $@
#!/bin/bash

# set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

pushd $SCRIPT_DIR

# Create the environment
mamba env create --force -f environment.yml

# Activate the environment
# There should be an env variable named CONDA_EXE with the form <conda root dir>/bin/conda
if [[ -z "${CONDA_EXE}" ]]; then
	echo "env variable CONDA_EXE not found"
	exit 1
fi

s=${CONDA_EXE}
CONDA_ROOT="${s%%/bin/*}"

source ${CONDA_ROOT}/etc/profile.d/conda.sh
conda activate course101-lpython

# Override lpython.py with our local copy because the environment lpython.py has a small bug
cp ./lpython.py $CONDA_PREFIX/share/lpython/lib/lpython/lpython.py

# Install refgvml python module
pip install ./refgvml

# Now we're ready to run tests
echo "----> Running lab 0 hw (1 out of 1) ..."
./lab_0/run_hw.sh

echo "----> Running lab 1 hw (1 out of 4) ..."
./lab_1b/run_hw.sh 0
echo "----> Running lab 1 hw (2 out of 4) ..."
./lab_1b/run_hw.sh 1
echo "----> Running lab 1 hw (3 out of 4) ..."
./lab_1b/run_hw.sh 2
echo "----> Running lab 1 hw (4 out of 4) ..."
./lab_1b/run_hw.sh 3

echo "----> Running lab 2 hw (1 out of 3) ..."
./lab_2b/run_hw.sh 0
echo "----> Running lab 2 hw (2 out of 3) ..."
./lab_2b/run_hw.sh 1
echo "----> Running lab 2 hw (3 out of 3) ..."
./lab_2b/run_hw.sh 2

echo "----> Running lab 3 hw (1 out of 1) ..."
./lab_3b/run_hw.sh 32768 12 3 123 2

echo "----> Running lab 4 hw (1 out of 1) ..."
./lab_4b/run_hw.sh 4096 80 3 2 123 2

echo "----> Running lab 5 hw (1 out of 1) ..."
./lab_5b/run_hw.sh 3 12 123 2

echo "----> Running lab 6 hw (1 out of 4) ..."
./lab_6a/run_hw.sh 3 12 123 2
echo "----> Running lab 6 hw (2 out of 4) ..."
./lab_6c/run_hw.sh 3 12 123 2
echo "----> Running lab 6 hw (3 out of 4) ..."
./lab_6e/run_hw.sh 3 12 123 2
echo "----> Running lab 6 hw (4 out of 4) ..."
./lab_6g/run_hw.sh 3 12 123 2

popd +0

echo "Finished running all HW tests"
from lpython import ccall

@ccall
def gsi_libsys_init_wrapper() -> None:
    pass

@ccall
def gsi_libsys_exit_wrapper() -> None:
    pass

from lpython import ccall, u16, i32, u32, CPtr, Pointer, Callable

@ccall
def gvml_init_once() -> None:
    pass

@ccall
def gvml_load_16(dest_vr: u16, source_vm_reg: u16) -> None:
    pass

@ccall
def gvml_add_f16(vdst : u16, vsrc1 : u16, vsrc2 : u16):
    pass

@ccall
def gvml_sub_f16(vdst : u16, vsrc1 : u16, vsrc2 : u16):
    pass

@ccall
def gvml_mul_f16(vdst : u16, vsrc1 : u16, vsrc2 : u16):
    pass

@ccall
def gvml_store_16(dest_vm_reg: u16, source_vr: u16) -> None:
    pass

@ccall
def gvml_create_index_16(vdst : u16) -> None:
    pass

@ccall
def gvml_lt_imm_u16(mdst : u16, vsrc : u16, val : u16) -> None:
    pass

@ccall
def gvml_reset_16(vreg : u16) -> None:
    pass

@ccall
def gvml_reset_16_msk(vreg : u16, msk : u16) -> None:
    pass

@ccall
def gvml_cpy_imm_16(vr_src : u16, val : u16)-> None:
    pass

@ccall
def gvml_monotonic_transformation_nan2max_u16_f16(vr_dst : u16, vr_src : u16)-> None:
    pass

@ccall
def gvml_mark_kmin_idxval_u16_mrk_g32k(mrk_dst : u16, vr_src : u16, vr_idx : u16, k : u32, mrk_src : u16, _2vtmp : u16)-> None:
    pass

@ccall
def gvml_get_marked_data_xv(output : CPtr, vr_idx : u16, vr_data : u16, mrk_src : u16, num_ent: u32) -> i32:
    pass

@ccall
def gvml_create_subgrp_index_u16(vr_dst : u16, grp_log : u16, sgrp_log : u16)-> None:
    pass

@ccall
def gvml_eq_imm_16(mrk_dst : u16, vr_src : u16, val : u16)-> None:
    pass

@ccall
def gvml_create_index_u16_grp_sgrp(vr_dst : u16, grp_log : u16, sgrp_log : u16, valid_subgrp_size : u32, vr_tmp : u16)-> None:
    pass

@ccall
def gvml_cpy_imm_subgrp_16_grp(vr_dst : u16, grp_log : u16, sgrp_log : u16, imm_data : CPtr, data_size : u32)-> None:
    pass

@ccall
def gvml_add_subgrps_f16_grp(vr_dst : u16, vr_src : u16, grp_log : u16, sgrp_log : u16, dst_subgrp_index : u32, vmtmp : u16, vtmp : u16)-> None:
    pass

@ccall
def gvml_set_m(mrk_src : u16) -> None:
    pass

@ccall
def gvml_cpy_m(mrk_dst : u16, mrk_src : u16) -> None:
    pass

@ccall
def gvml_cpy_from_mrk_16_msk(vr_dst : u16, mrk_src : u16, msk : u16)-> None:
    pass

@ccall
def gvml_cpy_16(vr_dst : u16, vr_src : u16)-> None:
    pass

@ccall
def gvml_merge_v15mrk1_g32k(v15mrk1_dst : u16, vidx_dst : u16, v15mrk1_src : u16)-> None:
    pass

@ccall
def gvml_cpy_to_mrk_16_msk(mrk_dst : u16, vr_src : u16, msk : u16)-> None:
    pass

@ccall
def gvml_xor_m(mrk_dst : u16, mrk_src1 : u16, mrk_src2 : u16)-> None:
    pass

@ccall
def gvml_and_m(mrk_dst : u16, mrk_src1 : u16, mrk_src2 : u16)-> None:
    pass

@ccall
def gvml_or_m(mrk_dst : u16, mrk_src1 : u16, mrk_src2 : u16)-> None:
    pass

@ccall
def gvml_cpy_16_mrk(vr_dst : u16, vr_src : u16, mrk_src : u16)-> None:
    pass

@ccall
def gvml_cpy_imm_16_msk_mrk(vr_dst : u16, val : u16, msk : u16, mrk_src : u16)-> None:
    pass

@ccall
def gvml_cpy_imm_16_mrk(vr_dst : u16, val : u16, mrk_src : u16)-> None:
    pass

@ccall
def gvml_get_marked_data(output : CPtr, start_vsrc : u16, num_vsrcs : u32, mrk_src : u16, num_ent: u32)-> u32:
    pass

@ccall
def gvml_mod_u16(vdst: u16, vsrc1: u16, vsrc2: u16) -> None:
    pass

@ccall
def gvml_lookup_16(vdst: u16, vsrc: u16, lut_addr: Pointer[u16], lut_size: u32) -> None:
    pass

@ccall
def gvml_eq_imm_16_msk(mrk_dst: u16, vsrc: u16, val: u16, msk: u16) -> None:
    pass

@ccall
def gvml_count_m_g32k_wrapper(mrk: u16) -> u32:
    pass

@ccall
def gvml_get_marked_data_sxvv_interval_wrapper(
    output: CPtr, 
    vr_merged_idx_lsb: u16,
    vr_merged_idx_msb: u16,
    vr_merged: u16,     
    val: u32,    
    mrk_src: u16,   
    num_ent: u32,
    interval_ent: u32,
    interval_func: Callable[[CPtr], i32],
    interval_params: CPtr) -> i32:
    pass
from lpython import i8, u16, i32, CPtr, Pointer, Const, ccall

@ccall
def increment_ptr(p : CPtr, offset : i32) -> CPtr:
    pass

@ccall
def dereference_u16_ptr(p : CPtr) -> u16:
    pass

@ccall
def direct_dma_l4_to_l1_32k(vmr_dst : u16, l4_src : CPtr) -> None:
    pass

@ccall
def direct_dma_l1_to_l4_32k(l4_dst : CPtr, vmr_src : u16) -> None:
    pass

@ccall
def c_strcpy(dst : Pointer[i8], src : Const[str])-> None:
    pass

@ccall
def i8_array_to_str(x : Pointer[i8]) -> str:
    pass

@ccall
def GSI_IS_ERR_PTR_OR_NULL(e : CPtr) -> bool:
    pass

@ccall
def direct_dma_l4_to_l2_32k_start(algn_p: CPtr) -> None:
    pass

@ccall
def dma_l2_sync() -> None:
    pass

@ccall
def direct_dma_l2_to_l1_32k(vmr: u16) -> None:
    pass
from lpython import ccall, i32, i64, CPtr, u64

@ccall
def gdl_init_wrapper() -> None:
    pass

@ccall
def gdl_exit_wrapper() -> None:
    pass

@ccall
def find_and_allocate_context() -> i64:
    pass

@ccall
def gdl_context_free_wrapper(ctx_id: i64) -> None:
    pass

@ccall
def gdl_mem_alloc_aligned_wrapper(ctx_id : i64, size: i64, pool : i32, alignment : i32) -> u64:
    pass

@ccall
def gdl_mem_free_wrapper(buffer : u64) -> None:
    pass

@ccall
def gdl_mem_handle_is_null_wrapper(hndl : u64) -> bool:
    pass

@ccall
def gdl_mem_cpy_to_dev_wrapper(
        dest_handle: u64,
        src_pointer: CPtr,
        size_in_bytes: i64) -> i32:
    pass

@ccall
def gdl_add_to_mem_handle_wrapper(inp : u64, addition : i64) -> u64:
    pass

@ccall
def gdl_mem_cpy_from_dev_wrapper(dest_pointer: CPtr, src_handle: u64, size_in_bytes: i64) -> i32:
    pass

@ccall
def gdl_run_task_timeout_wrapper(
        ctx_handle: i64,
        task_number_from_the_C_side: i32,
        in_handle: u64,
        out_handle: u64
) -> i32:
    pass

@ccall
def gdl_mem_handle_to_host_ptr_wrapper(handle : u64) -> CPtr:
    pass
from lpython import ccall, CPtr, u64, u32, i32, i64

@ccall
def gal_mem_handle_to_apu_ptr(hndl: u64) -> CPtr:
    pass

@ccall
def gal_fast_malloc_cache_aligned(size: u32, invalidate: bool) -> CPtr:
    pass

@ccall
def gal_fast_l2dma_async_memcpy_init(apuc_id: u32) -> None:
    pass

@ccall
def gal_fast_cache_dcache_flush_mlines_wrapper(start_addr: CPtr, size: u32) -> i32:
    pass

@ccall
def gal_fast_l2dma_mem_to_mem_512_wrapper(dst: CPtr, src: CPtr, apc_od: u32) -> None:
    pass

@ccall
def gal_fast_l2dma_async_memcpy_end(apuc_id: u32) -> None:
    pass

@ccall
def gal_fast_free_cache_aligned(algn_p: CPtr) -> None:
    pass

@ccall
def gal_init() -> None:
    pass

@ccall
def gal_pm_start() -> None:
    pass

@ccall
def gal_pm_stop() -> None:
    pass

@ccall
def gal_get_pm_cycle_count(a: bool) -> i64:
    pass
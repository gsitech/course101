from lpython import (
    i8, i32, i64, u64, 
    CPtr, sizeof, c_p_pointer, Pointer,
)
from common import (
    gd_lab_0_cmd, 
    GD_LAB_0_CMD_HELLO_WORLD_DEMO,
)

import ctypes

from gsi.sys_apu import gsi_libsys_init_wrapper, gsi_libsys_exit_wrapper
from gsi.gdl import (gdl_init_wrapper, gdl_exit_wrapper, find_and_allocate_context, gdl_context_free_wrapper,
                     gdl_mem_alloc_aligned_wrapper, gdl_mem_handle_is_null_wrapper, gdl_mem_handle_to_host_ptr_wrapper,
                     gdl_run_task_timeout_wrapper, gdl_mem_free_wrapper)

def cpy_str_into_i8_array(dst : Pointer[i8], the_str : str) -> None:
    b_array = ctypes.c_char_p(str.encode(the_str))
    x = ctypes.cast(b_array, ctypes.POINTER(ctypes.c_uint8))
    for i in range(len(the_str)):
        dst[i] = x[i]
    dst[i + 1] = 0

def i8_array_to_str(x : Pointer[i8]) -> str:
    i = 0
    b = bytearray()
    while x[i] != 0:
        b.append(x[i])
        i += 1
    b.append(0)
    ret = b.decode()
    return ret

# constants - start
GDL_MEM_HANDLE_NULL : u64 = u64(0)
GDL_CONST_MAPPED_POOL : i32 = i32(0)
GDL_ALIGN_32 : i32 = i32(4)

def run_lab_0_hello_world_demo_cmd(ctx_id : i64) -> None:

    dev_cmd_buf : u64 = GDL_MEM_HANDLE_NULL

    try:
        cmd_buf_size : i64 = sizeof(gd_lab_0_cmd)
        dev_cmd_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(dev_cmd_buf):
            raise MemoryError('gdl_mem_alloc_aligned() failed to allocate', cmd_buf_size, 'bytes')
        
        cmd_ptr: CPtr = gdl_mem_handle_to_host_ptr_wrapper(dev_cmd_buf)
        cmd: Pointer[gd_lab_0_cmd] = c_p_pointer(cmd_ptr, gd_lab_0_cmd)
        cmd.cmd = GD_LAB_0_CMD_HELLO_WORLD_DEMO
        cmd.hello_world_demo_data.an_int = 613
        cpy_str_into_i8_array(cmd.hello_world_demo_data.a_char_array , 'Hello Device!')
        cmd.hello_world_demo_data.a_mem_hndl = dev_cmd_buf

        print(f'BEFORE TASK: an_int = {cmd.hello_world_demo_data.an_int}, a_char_array = {i8_array_to_str(cmd.hello_world_demo_data.a_char_array)}')

        sts = gdl_run_task_timeout_wrapper(ctx_id, 1234, dev_cmd_buf, GDL_MEM_HANDLE_NULL)
        if sts:
            raise MemoryError('gdl_run_task_timeout() failed:', sts)

        print(f'AFTER TASK: an_int = {cmd.hello_world_demo_data.an_int}, a_char_array = {i8_array_to_str(cmd.hello_world_demo_data.a_char_array)}')

    finally:
        gdl_mem_free_wrapper(dev_cmd_buf)

def main() -> None :
    gsi_libsys_init_wrapper()
    gdl_init_wrapper()
    ctx : i64 = find_and_allocate_context()
    
    # run lab
    print('\nhello world demo')
    run_lab_0_hello_world_demo_cmd(ctx)

    print('Success')
    gdl_context_free_wrapper(ctx)
    gdl_exit_wrapper()
    gsi_libsys_exit_wrapper()

if __name__ == "__main__":
    main()
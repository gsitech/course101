from lpython import (
    i32, ccallback, CPtr, c_p_pointer, Pointer, InOut
)

from common import (
    gd_lab_0_cmd, 
    gd_lab_0_hello_world_demo_data, 
    GD_LAB_0_CMD_HELLO_WORLD_DEMO, 
)

from gsi.gal import gal_mem_handle_to_apu_ptr
from gsi.arc_utils import i8_array_to_str, c_strcpy

def hello_world_demo(data : InOut[gd_lab_0_hello_world_demo_data]) -> i32:
    print("an_int =", data.an_int)
    print("a_char_array =", i8_array_to_str(data.a_char_array))

    ptr : CPtr = gal_mem_handle_to_apu_ptr(data.a_mem_hndl)

    print("ptr =", ptr)

    data.an_int = i32(18)

    c_strcpy(data.a_char_array, "Hello Host!")
 
    return 0

@ccallback
def task(in_: CPtr, out: CPtr) -> i32:
    cmd: Pointer[gd_lab_0_cmd] = c_p_pointer(in_, gd_lab_0_cmd)

    if cmd.cmd == GD_LAB_0_CMD_HELLO_WORLD_DEMO:
        return hello_world_demo(cmd.hello_world_demo_data)
    
    return -1
   
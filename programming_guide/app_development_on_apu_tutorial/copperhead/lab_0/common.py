from lpython import (
    i32, u32, i8, u64,
    packed, dataclass, ccallable
)

GD_LAB_0_CMD_HELLO_WORLD_DEMO : u32 = u32(0)

@ccallable
@packed
@dataclass
class gd_lab_0_hello_world_demo_data:
    an_int: i32 = i32(0)
    a_char_array: i8[64] = i8(0)
    a_mem_hndl: u64 = u64(0)

@ccallable
@packed
@dataclass
class gd_lab_0_cmd:
    cmd : u32
    hello_world_demo_data : gd_lab_0_hello_world_demo_data = gd_lab_0_hello_world_demo_data()
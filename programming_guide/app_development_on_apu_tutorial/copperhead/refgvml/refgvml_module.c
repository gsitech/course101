#include <assert.h>
#include <Python.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#define PY_ARRAY_UNIQUE_SYMBOL REFGVML_ARRAY_API
#include <numpy/arrayobject.h>

#include <gsi/libsys/types.h>
#include <gsi/libgvml.h>
#include <gsi/refgvml.h>
#include <gsi/refgvml_element_wise.h>
#include <gsi/refgvml_min_max.h>
#include <gsi/refgvml_get_marked_data.h>
#include <gsi/refgvml_iv.h>

enum {
        _32K = 32 * 1024,
        _4K = 4 * 1024,
        _16 = 16,
};

static PyObject *refgvml_error;

static int check_np_1d_array(PyArrayObject *src, int dtype, uint32_t len)
{
        if (!PyArray_Check((PyObject *)src)) {
                PyErr_Format(refgvml_error, "expected a Numpy array");
                return -1;
        }

        const uint32_t src_ndim = PyArray_NDIM(src);
        if (src_ndim != 1) {
                PyErr_Format(refgvml_error, "array must be 1d but has %u dims", src_ndim);
                return -2;
        }

        const int src_dtype = PyArray_TYPE(src);
        if (src_dtype != dtype) {
                PyErr_Format(refgvml_error, "array type must be %d (np_type), not %d", dtype, src_dtype);
                return -3;
        }

        const uint32_t len_src = PyArray_DIMS(src)[0];
        if (len_src != len) {
                PyErr_Format(refgvml_error, "array len must be %d, not %d", len, len_src);
                return -4;
        }
        return 0;
}

static int check_vr(PyArrayObject *src)
{
        return check_np_1d_array(src, NPY_UINT16, _32K);
}

static int check_mrk(PyArrayObject *src)
{
        return check_np_1d_array(src, NPY_UINT8, _4K);
}

static int check_np_2d_array(PyArrayObject *srcs, int dtype, uint32_t max_num_rows, uint32_t num_expected_cols)
{
        if (!PyArray_Check((PyObject *)srcs)) {
                PyErr_Format(refgvml_error, "expected a Numpy array");
                return -1;
        }

        if (!PyArray_IS_C_CONTIGUOUS(srcs)) {
                PyErr_Format(refgvml_error, "array 2d must be c-contiguous");
                return -2;
        }

        const int src_dtype = PyArray_TYPE(srcs);
        if (src_dtype != dtype) {
                PyErr_Format(refgvml_error, "array 2d type must be %d (np_type), not %d", dtype, src_dtype);
                return -3;
        }

        const uint32_t src_ndim = PyArray_NDIM(srcs);
        if (src_ndim != 2) {
                PyErr_Format(refgvml_error, "array 2d must be 2d but has %u dims", src_ndim);
                return -4;
        }

        const uint32_t num_rows = PyArray_DIMS(srcs)[0];
        const uint32_t num_cols = PyArray_DIMS(srcs)[1];
        if (num_rows > max_num_rows) {
                PyErr_Format(refgvml_error, "num_rows (%u) must be no greater than %u", num_rows, max_num_rows);
                return -5;
        }
        
        if (num_cols != num_expected_cols) {
                PyErr_Format(refgvml_error, "num_cols (%u) must be %u", num_cols, num_expected_cols);
                return -6;
        }

        return 0;
}

static int check_vrs(PyArrayObject *srcs, uint32_t max_num_rows)
{
        return check_np_2d_array(srcs, NPY_UINT16, max_num_rows, _32K);
}

static PyObject *bindings_create_subgrp_index_u16(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *dst;
	uint32_t grp_log;
	uint32_t sgrp_log;

	static char *keywords[] = {"dst", "grp_log", "sgrp_log", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OII", keywords, &dst, &grp_log, &sgrp_log))
		return NULL;

        if (check_vr(dst))
                return NULL;

        refgvml_create_subgrp_index_u16(PyArray_DATA(dst), grp_log, sgrp_log);

	Py_RETURN_NONE;
}

static PyObject *bindings_create_index_u16_grp_sgrp(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *dst;
	uint32_t grp_log;
	uint32_t sgrp_log;
        uint32_t valid_sgrp_size;

	static char *keywords[] = {"dst", "grp_log", "sgrp_log", "valid_sgrp_size", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OIII", keywords, &dst, &grp_log, &sgrp_log, &valid_sgrp_size))
		return NULL;

        if (check_vr(dst))
                return NULL;

        refgvml_create_index_u16_grp_sgrp(PyArray_DATA(dst), grp_log, sgrp_log, valid_sgrp_size);

	Py_RETURN_NONE;
}


static PyObject *bindings_bin_op_out_16b_in_16b_16b_ew(PyObject *self, PyObject *args, PyObject *kwargs, uint16_t (*bin_op)(uint16_t, uint16_t))
{
        uint16_t src1;
        uint16_t src2; 

	static char *keywords[] = {"src1", "src2", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "HH", keywords, &src1, &src2))
		return NULL;

        uint16_t res = bin_op(src1, src2);

        return PyLong_FromLong(res);
}

static PyObject *bindings_bin_op_out_16b_in_16b_16b(PyObject *self, PyObject *args, PyObject *kwargs, uint16_t (*bin_op)(uint16_t, uint16_t))
{
        PyArrayObject *dst = NULL;
        PyArrayObject *src1 = NULL;
        PyArrayObject *src2 = NULL; 

	static char *keywords[] = {"dst", "src1", "src2", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OOO", keywords, &dst, &src1, &src2))
		return NULL;

        if (check_vr(src1))
		return NULL;
        if (check_vr(src2))
		return NULL;
        if (check_vr(dst))
		return NULL;

        uint16_t *dst_data = PyArray_DATA(dst);
        uint16_t *src_data1 = PyArray_DATA(src1);
        uint16_t *src_data2 = PyArray_DATA(src2);

        #pragma omp for
        for (uint32_t i = 0; i < _32K; ++i)
	        dst_data[i] = bin_op(src_data1[i], src_data2[i]);

	Py_RETURN_NONE;
}

static PyObject *bindings_add_f16(PyObject *self, PyObject *args, PyObject *kwargs)
{
        return bindings_bin_op_out_16b_in_16b_16b(self, args, kwargs, refgvml_add_f16);
}

static PyObject *bindings_add_f16_ew(PyObject *self, PyObject *args, PyObject *kwargs)
{
        return bindings_bin_op_out_16b_in_16b_16b_ew(self, args, kwargs, refgvml_add_f16);
}

static PyObject *bindings_sub_f16(PyObject *self, PyObject *args, PyObject *kwargs)
{
        return bindings_bin_op_out_16b_in_16b_16b(self, args, kwargs, refgvml_sub_f16);
}

static PyObject *bindings_sub_f16_ew(PyObject *self, PyObject *args, PyObject *kwargs)
{
        return bindings_bin_op_out_16b_in_16b_16b_ew(self, args, kwargs, refgvml_sub_f16);
}

static PyObject *bindings_mul_f16(PyObject *self, PyObject *args, PyObject *kwargs)
{
        return bindings_bin_op_out_16b_in_16b_16b(self, args, kwargs, refgvml_mul_f16);
}

static PyObject *bindings_mul_f16_ew(PyObject *self, PyObject *args, PyObject *kwargs)
{
        return bindings_bin_op_out_16b_in_16b_16b_ew(self, args, kwargs, refgvml_mul_f16);
}

static PyObject *bindings_monotonic_transformation_nan2max_u16_f16(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *dst = NULL;
        PyArrayObject *src = NULL;

	static char *keywords[] = {"dst", "src", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO", keywords, &dst, &src))
		return NULL;

        if (check_vr(src))
		return NULL;
        if (check_vr(dst))
		return NULL;

        uint16_t *dst_data = PyArray_DATA(dst);
        uint16_t *src_data = PyArray_DATA(src);

        #pragma omp for
        for (uint32_t i = 0; i < _32K; ++i)
	        dst_data[i] = refgvml_monotonic_transformation_nan2max_u16_f16(src_data[i]);
       
	Py_RETURN_NONE;
}

static PyObject *bindings_mark_kmin_idxval_u16_mrk_g32k(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *dst_mark = NULL;
        PyArrayObject *src_vals = NULL; 
        PyArrayObject *src_mark = NULL;
        unsigned int k = 0;
        PyArrayObject *src_indices = NULL;

	static char *keywords[] = {"dst_mark", "src_vals", "src_indices", "k", "src_mark", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OOOIO", keywords, &dst_mark, &src_vals, &src_indices, &k, &src_mark))
		return NULL;

        if (check_mrk(dst_mark))
		return NULL;
        if (check_vr(src_vals))
		return NULL;
        if (check_vr(src_indices))
		return NULL;
        if (check_mrk(src_mark))
		return NULL;

	refgvml_mark_kmin_idxval_u16_mrk_g32k(
                PyArray_DATA(dst_mark),
                PyArray_DATA(src_vals),
                PyArray_DATA(src_indices),
                k,
                PyArray_DATA(src_mark));
       
	Py_RETURN_NONE;
}

static PyObject *bindings_add_subgrps_u16_grp(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *dst = NULL;
        PyArrayObject *src = NULL;
        unsigned int grp_log;
	unsigned int sgrp_log;
        unsigned int dst_subgrp_index;

	static char *keywords[] = {"dst", "src", "grp_log", "sgrp_log", "dst_subgrp_index", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OOIII", keywords, &src, &dst, &grp_log, &sgrp_log, &dst_subgrp_index))
		return NULL;

        if (check_vr(src))
		return NULL;
        if (check_vr(dst))
		return NULL;

	refgvml_add_subgrps_f16_grp(PyArray_DATA(dst), PyArray_DATA(src), grp_log, sgrp_log, dst_subgrp_index);
       
	Py_RETURN_NONE;
}

static PyObject *bindings_add_subgrps_f16_grp(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *dst = NULL;
        PyArrayObject *src = NULL;
        unsigned int grp_log;
	unsigned int sgrp_log;
        unsigned int dst_subgrp_index;

	static char *keywords[] = {"dst", "src", "grp_log", "sgrp_log", "dst_subgrp_index", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OOIII", keywords, &dst, &src, &grp_log, &sgrp_log, &dst_subgrp_index))
		return NULL;

        if (check_vr(src))
		return NULL;
        if (check_vr(dst))
		return NULL;
        
	refgvml_add_subgrps_f16_grp(PyArray_DATA(dst), PyArray_DATA(src), grp_log, sgrp_log, dst_subgrp_index);
       
	Py_RETURN_NONE;
}

static PyObject *bindings_cpy_imm_subgrp_16_grp(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *dst;
	uint32_t grp_log;
	uint32_t sgrp_log;
        uintptr_t imm_data_p;
        uint32_t data_size;

	static char *keywords[] = {"dst", "grp_log", "sgrp_log", "imm_data_p","data_size", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OIIKI", keywords, &dst, &grp_log, &sgrp_log, &imm_data_p, &data_size))
		return NULL;

        if (check_vr(dst))
                return NULL;
        
        refgvml_cpy_imm_subgrp_16_grp(PyArray_DATA(dst), grp_log, sgrp_log, (uint16_t *)imm_data_p, data_size);
	
        Py_RETURN_NONE;
}

static PyObject *bindings_get_marked_data_xv(PyObject *self, PyObject *args, PyObject *kwargs)
{
        uintptr_t dst;
        PyArrayObject *_16b_src = NULL;
        PyArrayObject *_15b_src = NULL;
        PyArrayObject *mrk = NULL;
        unsigned int num_ent = 0;

	static char *keywords[] = {"dst", "_16b_src", "_15b_src", "mrk", "num_ent", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "KOOOI", keywords, &dst, &_16b_src, &_15b_src, &mrk, &num_ent))
		return NULL;

        if (check_vr(_15b_src))
		return NULL;
        if (check_vr(_16b_src))
		return NULL;
        if (check_mrk(mrk))
		return NULL;

	int ret = refgvml_get_marked_data_xv(
                (void *)dst,
                PyArray_DATA(_16b_src),
                PyArray_DATA(_15b_src),
                PyArray_DATA(mrk),
                num_ent);

	return PyLong_FromLong(ret);
}

static PyObject *bindings_get_marked_data_sxvv_interval(PyObject *self, PyObject *args, PyObject *kwargs)
{
        uintptr_t dst;
        PyArrayObject *_16b_src1 = NULL;
        PyArrayObject *_16b_src2 = NULL;
        PyArrayObject *_15b_src = NULL;
        uint16_t val = 0;
        PyArrayObject *mrk = NULL;
        unsigned int num_ent = 0;

	static char *keywords[] = {"dst", "_16b_src1", "_16b_src2", "_15b_src", "val", "mrk", "num_ent", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "KOOOHOI", keywords, &dst, &_16b_src1, &_16b_src2, &_15b_src, &val, &mrk, &num_ent))
		return NULL;

        if (check_vr(_15b_src))
		return NULL;
        if (check_vr(_16b_src1))
		return NULL;
        if (check_vr(_16b_src2))
		return NULL;
        if (check_mrk(mrk))
		return NULL;

	int ret = refgvml_get_marked_data_sxvv(
                (void *)dst,
                PyArray_DATA(_16b_src1),
                PyArray_DATA(_16b_src2),
                PyArray_DATA(_15b_src),
                val,
                PyArray_DATA(mrk),
                num_ent);
       
	return PyLong_FromLong(ret);
}

static PyObject *bindings_get_marked_data(PyObject *self, PyObject *args, PyObject *kwargs)
{
        uintptr_t dst;
        PyArrayObject *srcs = NULL;
        PyArrayObject *mrk = NULL;
        unsigned int num_ent = 0;

	static char *keywords[] = {"dst", "srcs", "mrk", "num_ent", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "KOOI", keywords, &dst, &srcs, &mrk, &num_ent))
		return NULL;
       
        if (check_vrs(srcs, _16))
                return NULL;

        const uint32_t num_srcs = PyArray_DIMS(srcs)[0];
	const uint32_t row_size = PyArray_DIMS(srcs)[1];
        uint16_t *srcs_data[_16];
        uint16_t *ptr = PyArray_DATA(srcs);
        for (uint32_t i = 0; i < num_srcs; i++, ptr += row_size) {
                srcs_data[i] = ptr;
        }
                
	int ret = refgvml_get_marked_data(
                (uint16_t *)dst,
                srcs_data,
                num_srcs,
                PyArray_DATA(mrk),
                num_ent);

	return PyLong_FromLong(ret);
}

static PyObject *bindings_merge_v15mrk1_g32k(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *dst_data15_mark1 = NULL;
        PyArrayObject *src_indices = NULL; 
        PyArrayObject *src_data15_mark1 = NULL;
        unsigned int merge_mode = 0;

	static char *keywords[] = {"dst_data15_mark1", "src_indices", "src_data15_mark1", "merge_mode", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OOOI", keywords, &dst_data15_mark1, &src_indices, &src_data15_mark1, &merge_mode))
		return NULL;

        if (check_vr(dst_data15_mark1))
		return NULL;
        if (check_vr(src_indices))
		return NULL;
        if (check_vr(src_data15_mark1))
		return NULL;

	refgvml_merge_v15mrk1_g32k(
                PyArray_DATA(dst_data15_mark1),
                PyArray_DATA(src_indices),
                PyArray_DATA(src_data15_mark1),
                merge_mode);
       
	Py_RETURN_NONE;
}

static PyObject *bindings_get_max_u16_mrk_g32k(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *src = NULL;
        PyArrayObject *mrk = NULL; 

	static char *keywords[] = {"src", "mrk", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO", keywords, &src, &mrk))
		return NULL;

        if (check_vr(src))
		return NULL;
        if (check_mrk(mrk))
		return NULL;

        uint16_t out_val;
	int sts = refgvml_get_max_u16_mrk_g32k(
                &out_val,
                PyArray_DATA(src),
                PyArray_DATA(mrk));

        if (sts) {
                PyErr_Format(refgvml_error, "mrk must have at least one entry marked");
                return NULL;
        }
       
	return PyLong_FromLong(out_val);
}

static PyObject *bindings_is_reset_m_g32k(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *src = NULL; 

	static char *keywords[] = {"src", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O", keywords, &src))
		return NULL;

        if (check_mrk(src))
		return NULL;
       
	return PyLong_FromLong(refgvml_is_reset_m_g32k(PyArray_DATA(src)));
}

static PyObject *bindings_vmark_kmin_nxu16_vmrk_g32k(PyObject *self, PyObject *args, PyObject *kwargs)
{
        PyArrayObject *vmrk = NULL; 
        PyArrayObject *srcs = NULL;
        uint32_t k; 

	static char *keywords[] = {"vmrk", "srcs", "k", NULL};

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OOI", keywords, &vmrk, &srcs, &k))
		return NULL;

        if (check_vr(vmrk))
		return NULL;

        if (check_vrs(srcs, 8))
		return NULL;

        const uint32_t num_srcs = PyArray_DIMS(srcs)[0];
        const uint32_t row_size = PyArray_DIMS(srcs)[1];
        uint16_t *srcs_data[8];
        uint16_t *ptr = PyArray_DATA(srcs);
        for (uint32_t i = 0; i < num_srcs; i++, ptr += row_size) {
                srcs_data[i] = ptr;
        }

        refgvml_vmark_kmin_nxu16_vmrk_g32k(
                PyArray_DATA(vmrk),
                srcs_data,
                num_srcs,
                k);
       
	Py_RETURN_NONE;
}

static PyMethodDef refgvml_methods[] = {
	{"create_subgrp_index_u16",                     (PyCFunction)bindings_create_subgrp_index_u16,                          METH_VARARGS | METH_KEYWORDS,   "create_subgrp_index_u16"                 },
	{"create_index_u16_grp_sgrp",                   (PyCFunction)bindings_create_index_u16_grp_sgrp,                        METH_VARARGS | METH_KEYWORDS,   "create_index_u16_grp_sgrp"               },
        {"cpy_imm_subgrp_16_grp",                       (PyCFunction)bindings_cpy_imm_subgrp_16_grp,                            METH_VARARGS | METH_KEYWORDS,   "cpy_imm_subgrp_16_grp"                   },
        {"add_subgrps_f16_grp",                         (PyCFunction)bindings_add_subgrps_f16_grp,                              METH_VARARGS | METH_KEYWORDS,   "add_subgrps_f16_grp"                     },
        {"add_f16",                                     (PyCFunction)bindings_add_f16,                                          METH_VARARGS | METH_KEYWORDS,   "add_f16"                                 },
        {"add_f16_ew",                                  (PyCFunction)bindings_add_f16_ew,                                       METH_VARARGS | METH_KEYWORDS,   "add_f16 element-wise"                    },
        {"sub_f16",                                     (PyCFunction)bindings_sub_f16,                                          METH_VARARGS | METH_KEYWORDS,   "sub_f16"                                 },
        {"sub_f16_ew",                                  (PyCFunction)bindings_sub_f16_ew,                                       METH_VARARGS | METH_KEYWORDS,   "sub_f16 element-wise"                    },
        {"mul_f16",                                     (PyCFunction)bindings_mul_f16,                                          METH_VARARGS | METH_KEYWORDS,   "mul_f16"                                 },
        {"mul_f16_ew",                                  (PyCFunction)bindings_mul_f16_ew,                                       METH_VARARGS | METH_KEYWORDS,   "mul_f16 element-wise"                    },
        {"monotonic_transformation_nan2max_u16_f16",    (PyCFunction)bindings_monotonic_transformation_nan2max_u16_f16,         METH_VARARGS | METH_KEYWORDS,   "monotonic_transformation_nan2max_u16_f16"},
        {"mark_kmin_idxval_u16_mrk_g32k",               (PyCFunction)bindings_mark_kmin_idxval_u16_mrk_g32k,                    METH_VARARGS | METH_KEYWORDS,   "mark_kmin_idxval_u16_mrk_g32k"           },
        {"add_subgrps_u16_grp",                         (PyCFunction)bindings_add_subgrps_u16_grp,                              METH_VARARGS | METH_KEYWORDS,   "add_subgrps_u16_grp"                     },
        {"get_marked_data_xv",                          (PyCFunction)bindings_get_marked_data_xv,                               METH_VARARGS | METH_KEYWORDS,   "get_marked_data_xv"                      },
        {"get_marked_data_sxvv_interval",               (PyCFunction)bindings_get_marked_data_sxvv_interval,                    METH_VARARGS | METH_KEYWORDS,   "get_marked_data_sxvv_interval"           },
        {"get_marked_data",                             (PyCFunction)bindings_get_marked_data,                                  METH_VARARGS | METH_KEYWORDS,   "get_marked_data"                         },
        {"merge_v15mrk1_g32k",                          (PyCFunction)bindings_merge_v15mrk1_g32k,                               METH_VARARGS | METH_KEYWORDS,   "merge_v15mrk1_g32k"                      },
        {"get_max_u16_mrk_g32k",                        (PyCFunction)bindings_get_max_u16_mrk_g32k,                             METH_VARARGS | METH_KEYWORDS,   "get_max_u16_mrk_g32k"                    },
        {"is_reset_m_g32k",                             (PyCFunction)bindings_is_reset_m_g32k,                                  METH_VARARGS | METH_KEYWORDS,   "is_reset_m_g32k"                         },
        {"vmark_kmin_nxu16_vmrk_g32k",                  (PyCFunction)bindings_vmark_kmin_nxu16_vmrk_g32k,                       METH_VARARGS | METH_KEYWORDS,   "vmark_kmin_nxu16_vmrk_g32k"              },
        {NULL, NULL, 0, NULL}        /* Sentinel */
};
 
static struct PyModuleDef refgvml_module = {
	PyModuleDef_HEAD_INIT,
	"refgvml",	/* name of module */
	NULL,		/* module documentation, may be NULL */
	-1,		/* size of per-interpreter state of the module,
			or -1 if the module keeps state in global variables. */
	refgvml_methods
};

static int add_constants_to_module(PyObject *m)
{        
	int res = PyModule_AddIntMacro(m, REFGVML_VR_SIZE);
	if (res)
		return res;
	res = PyModule_AddIntMacro(m, REFGVML_MRK_NUM_BITS);
	if (res)
		return res;
	res = PyModule_AddIntMacro(m, REFGVML_MRK_SIZE);
        if (res)
		return res;
	res = PyModule_AddIntMacro(m, REFGVML_DEFAULT_LAYOUT);
        if (res)
		return res;
	res = PyModule_AddIntMacro(m, REFGVML_M1_LAYOUT);
        if (res)
		return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_1);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_2);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_4);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_8);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_16);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_32);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_64);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_128);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_256);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_512);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_1K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_2K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_4K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_8K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_16K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_32K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_MERGE_FEW);
        if (res)
                return res;
         res = PyModule_AddIntMacro(m, REFGVML_MERGE_MANY);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_0);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_1);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_2);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_3);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_4);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_5);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_6);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_7);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_8);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_9);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_10);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_11);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_12);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_13);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_14);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VR16_IDX);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_MRK0);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_MRK1);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_MRK2);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_MRK3);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_MRK4);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_MRK5);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_MRK6);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_MRK7);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_0);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_1);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_2);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_3);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_4);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_5);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_6);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_7);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_8);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_9);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_10);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_11);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_12);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_13);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_14);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_15);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_16);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_17);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_18);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_19);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_20);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_21);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_22);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_23);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_24);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_25);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_26);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_27);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_28);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_29);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_30);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_31);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_32);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_33);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_34);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_35);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_36);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_37);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_38);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_39);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_40);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_41);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_42);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_43);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_44);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_45);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_46);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, GVML_VM_47);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_2);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_4);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_8);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_16);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_32);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_64);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_128);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_256);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_512);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_1K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_2K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_1);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_4K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_8K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_16K);
        if (res)
                return res;
        res = PyModule_AddIntMacro(m, REFGVML_P2_32K);
        return res;
}

PyMODINIT_FUNC PyInit_refgvml(void)
{
	PyObject *m = PyModule_Create(&refgvml_module);
	if (m == NULL)
		return NULL;

	import_array();
	add_constants_to_module(m);

	refgvml_error = PyErr_NewException("refgvml.error", NULL, NULL);
	Py_INCREF(refgvml_error);
	PyModule_AddObject(m, "error", refgvml_error);
	return m;
}
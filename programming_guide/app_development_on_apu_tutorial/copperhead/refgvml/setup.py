from distutils.core import setup, Extension
import numpy

import os

hw_dir = os.environ['CONDA_PREFIX']

module1 = Extension('refgvml',
                    sources = ['refgvml_module.c'],
                    library_dirs =[f'{hw_dir}/lib/gsi/'],
                    extra_objects = [f'{hw_dir}/lib/gsi/libgsigvml_ref.a'],
                    extra_compile_args=['-fopenmp'],
                    extra_link_args = ['-fopenmp', '-Wl,--whole-archive', '-lgsisys_x86_64', '-Wl,--no-whole-archive'],
                    include_dirs = [numpy.get_include(), f'{hw_dir}/include'])

setup (name = 'refgvml',
       version = '1.0',
       description = 'This is a demo package',
       ext_modules = [module1])

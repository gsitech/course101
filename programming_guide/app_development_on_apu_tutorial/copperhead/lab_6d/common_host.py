import numpy as np
from lpython import u16, u32, u64
from common import GD_LAB_6_MAX_NUM_FEATURES, GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK

def gd_lab_6_get_preprocessed_db_size(num_features : u32, num_chunks : u32) -> int:
    if u32(num_features) > GD_LAB_6_MAX_NUM_FEATURES:
        raise RuntimeError(f'num_features, {num_features}, must not exceed {GD_LAB_6_MAX_NUM_FEATURES}')
    size_16b : u64 = 2
    return size_16b * num_features * num_chunks * int(GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK)

def gd_lab_6_preprocess_db(src_f16 : np.ndarray) -> np.ndarray:
    assert src_f16.dtype == np.float16
    assert src_f16.ndim == 2
    num_records, num_features = src_f16.shape
    assert (num_records % int(GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK)) == 0
    if num_records % int(GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK):
        raise RuntimeError(f'num_records, {num_records}, must be a multiple of GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK, {GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK}')
    if u32(num_features) > GD_LAB_6_MAX_NUM_FEATURES:
        raise RuntimeError(f'num_features, {num_features}, must not exceed {GD_LAB_6_MAX_NUM_FEATURES}')

    num_chunks = num_records // int(GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK)

    ret = np.empty((num_chunks, num_features, GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK), dtype=np.float16)
    chunkified_src = src_f16.reshape(num_chunks, GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK, num_features)

    for i, chunk in enumerate(chunkified_src):
        ret[i] = np.transpose(chunk)

    return ret.ravel()
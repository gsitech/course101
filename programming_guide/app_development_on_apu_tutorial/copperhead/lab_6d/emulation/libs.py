import emulation.gsi
import lpython
from types import FunctionType

lpython.CTypes.emulations = {k: v for k, v in emulation.gsi.__dict__.items()
        if isinstance(v, FunctionType)}
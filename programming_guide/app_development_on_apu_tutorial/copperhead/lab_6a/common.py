from lpython import packed, dataclass, ccallable, u16, u32, u64

GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK : u32 = u32(32 * 1024)
GD_LAB_6_MAX_NUM_FEATURES : u32 = u32(48)
GD_LAB_6_MAX_K : u32 = u32(GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK // u32(2))

GD_LAB_6_CMD_LOAD_DB : u32 = u32(0)
GD_LAB_6_CMD_SEARCH : u32 = u32(1)

@ccallable
@packed
@dataclass
class gd_lab_6_idx_val:
    idx: u32
    val: u16

@ccallable
@packed
@dataclass
class gd_lab_6_load_db:
    db: u64 = u64(0)
    num_features: u32 = u32(0)
    num_32k_chunks_records: u32 = u32(0)

@ccallable
@packed
@dataclass
class gd_lab_6_search:
    output: u64 = u64(0)
    query: u64 = u64(0)
    k: u32 = u32(0)

@ccallable
@packed
@dataclass
class gd_lab_6_cmd:
    cmd : u32 = u32(0)
    load_db_data : gd_lab_6_load_db = gd_lab_6_load_db()
    search_data : gd_lab_6_search = gd_lab_6_search()
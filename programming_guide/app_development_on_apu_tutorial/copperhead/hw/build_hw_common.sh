#!/bin/bash

# set -ex

if [[ "$#" -lt 2 ]]; then
	echo "usage: ./build_hw.sh build_dir src_files_dir"
	exit -1
fi

# Get licence
if ! command -v ccac &> /dev/null
then
    echo "ccac could not be found"
    echo "ensure you have access to synopsis toolchain, e.g source /efs/data/public/synopsis/ARC-2018.06/env_set.env"
    exit -1
fi

build_dir=$1
src_files_dir=$2

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

rm -rf $build_dir
mkdir $build_dir

lpython_source_file=("common" "device")

for sc in ${lpython_source_file[@]}; do
	# compile with LPython to c
	lpython --show-c -I $CONDA_PREFIX/share/lpython/lib/ -I ${src_files_dir}/../ -I ${SCRIPT_DIR}/../declarations ${src_files_dir}/${sc}.py > ${build_dir}/${sc}.c
	# compile resulting c file with gcc to ensure lpython output is correct
	gcc -I $CONDA_PREFIX/share/lpython/lib/impure/ ${build_dir}/${sc}.c -c -o ${build_dir}/${sc}.o
done

# Refactor code (lots of workarounds ...)
src_file=${build_dir}/device.c
processed_src_file=${build_dir}/device_processed.c

$CONDA_PREFIX/bin/python ${SCRIPT_DIR}/../process_lpython_output.py ${processed_src_file} ${src_file}

# create apl file
# update the PYTHONPATH before calling belex-aot
export PYTHONPATH=$(find $CONDA_PREFIX -type d -path "*/site-packages/belex" -exec dirname {} \; -quit)
belex-aot -O1 --explicit-frags-only --source-file ${build_dir}/fragments.apl --no-uniquify-nyms ${SCRIPT_DIR}/../gchl/fragments.py

# preprocess APL
hw_dir=$CONDA_PREFIX
preproc=${hw_dir}/bin/apl_preproc
dst=${build_dir}/device-funcs.c
aux_prefix=${build_dir}/

${preproc} -o ${dst} \
	--frag-section --aux-prefix=${aux_prefix} \
	--aux-same-dir \
	--cpp-opts='-DGSI_LIBSYS_ARCHS36 -D__APL_PREPROC__ -DAPUC_TYPE_hw -MMD -MMD -MP -MF ./device-funcs.c.d.in -MT ./device-funcs.c' \
	${processed_src_file}

# workaround: move the resulting device-funcs.c.d.in to the build dir
mv ./device-funcs.c.d.in ${build_dir}

host_file=${SCRIPT_DIR}/host_utils.c
lib_name=lab
apu_file=${build_dir}/device-funcs.c

gdl_a=${hw_dir}/lib/gsi/libgsidevice.a
gal_a=${hw_dir}/lib/gsi/libgsiapu_arc.a
libsys_a=${hw_dir}/lib/gsi/libgsisys_archs36.a
libgvml_a=${hw_dir}/lib/gsi/libgsigvml.a
apu_cc=ccac
lcf_dir=${hw_dir}/common/products/wnc-l4
tcf_dir=${hw_dir}/common/products
apu_cc_args="-tcf=${tcf_dir}/arc.tcf -tcf_apex -tcf_core_config ${lcf_dir}/arc_module.lcf -Hnocopyr -Hnosdata -Hpictable"
apu_link_args=""
apu_syslib_args="-DGSI_LIBSYS_ARCHS36 -DAPUC_TYPE_hw"
elf2bin="elf2bin  -Pload -q"
OBJCOPY="objcopy -I binary -O elf64-x86-64 -B i386"

# Compile application module task
$apu_cc $apu_cc_args $apu_syslib_args \
	-I. -I ${hw_dir}/include \
	-I ${hw_dir}/include/gsi/aarch64 \
	-I ${hw_dir}/include/gsi/archs36 \
	-I ${hw_dir}/include/gsi/x86_32 \
	-I ${hw_dir}/include/gsi/x86_64 -c $apu_file \
	-I ${SCRIPT_DIR} \
	-o $build_dir/$lib_name-module.o

# Build application module
$apu_cc $apu_cc_args $apu_link_args\
	$build_dir/$lib_name-module.o \
	-Wl,--whole-archive \
	$gal_a \
	-Wl,--no-whole-archive \
	$libsys_a \
	$libgvml_a \
	-o $build_dir/$lib_name.mod

# Use modtool to get module info
${hw_dir}/bin/modtool -c $build_dir/$lib_name-defs.c -i $build_dir/$lib_name-defs.h -b $build_dir/$lib_name.mod

# Convert module to bin
$elf2bin $build_dir/$lib_name.mod $build_dir/$lib_name.bin

# Create object file from module bin
obj_name=$(echo $build_dir/$lib_name.bin | sed 's@[/.-]@\_@g')
${OBJCOPY} \
	--redefine-sym _binary_${obj_name}_start=gsi_module_dump_start \
	--redefine-sym _binary_${obj_name}_end=gsi_module_dump_end \
	--redefine-sym _binary_${obj_name}_size=gsi_module_dump_size \
	$build_dir/$lib_name.bin \
	$build_dir/$lib_name-apuc-code.o

gcc -DGSI_LIBSYS_X86_64 \
	-I ${hw_dir}/include \
	-I ${hw_dir}/include/gsi/aarch64 \
	-I ${hw_dir}/include/gsi/x86_64 \
	-I $build_dir \
	$host_file \
	-c \
	-o $build_dir/$lib_name.o \

gcc -DGSI_LIBSYS_X86_64 \
	-I ${hw_dir}/include \
	-I ${hw_dir}/include/gsi/aarch64 \
	-I ${hw_dir}/include/gsi/x86_64 \
	-I $build_dir \
	$build_dir/$lib_name-defs.c \
	-c \
	-o $build_dir/$lib_name-defs.o \
	
gcc -L$hw_dir/lib/gsi -shared \
	-o $build_dir/lib$lib_name.so \
	$build_dir/$lib_name.o \
	$build_dir/$lib_name-defs.o \
	$build_dir/$lib_name-apuc-code.o \
	-Wl,--whole-archive -lgsisys_x86_64 -lgsidevice -Wl,--no-whole-archive
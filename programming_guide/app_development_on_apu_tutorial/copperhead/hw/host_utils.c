#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <gsi/libgdl.h>

#include <gsi/libsys.h>
#include <gsi/libsys/error.h>

GDL_TASK_DECLARE(gd_lab);

void gsi_libsys_init_wrapper(void)
{
	if (gsi_libsys_init("", false))
		gsi_fatal("gsi_libsys_init() failed");
}

void gsi_libsys_exit_wrapper(void)
{
	gsi_libsys_exit();
}

void gdl_init_wrapper(void)
{
	if (gdl_init())
		gsi_fatal("gdl_init() failed");
}

void gdl_exit_wrapper(void)
{
	if (gdl_exit())
		gsi_fatal("gdl_exit() failed");
}

gdl_mem_handle_t find_and_allocate_context(void)
{
	uint32_t num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	printf("*************************************\n");
	printf("Num Contexts = %u\n", num_ctxs);
	/* Use first available context */
	gdl_context_handle_t valid_ctx_id;
	uint32_t ctx;
	for (ctx = 0; ctx < num_ctxs; ++ctx) {
		if (contexts_desc[ctx].status == GDL_CONTEXT_READY) {
			valid_ctx_id = contexts_desc[ctx].ctx_id;
			printf("Memory Size = %0.1fG\n", (float)contexts_desc[ctx].mem_size / 1024L / 1024L / 1024L);
			printf("Num Apucs = %d\n", contexts_desc[ctx].num_apucs);
			break;
		}
	}

	if (ctx == num_ctxs) {
		gsi_fatal("Failed to find valid context");
	}
	printf("*************************************\n");

	const long long unsigned int const_mapped_size_req = 3L * 1024L * 1024L * 1024L;
	long long unsigned int const_mapped_size_recv, dynamic_mapped_size_recv;

	int sts = gdl_context_alloc(valid_ctx_id, const_mapped_size_req, &const_mapped_size_recv, &dynamic_mapped_size_recv);
	if (sts) {
		gsi_fatal("gdl_context_alloc failed: %s", gsi_status_errorstr(sts));
	}

	return valid_ctx_id;
}

void gdl_context_free_wrapper(uint64_t ctx)
{
	gdl_context_free(ctx);
}

gdl_mem_handle_t gdl_mem_alloc_aligned_wrapper(
	gdl_context_handle_t ctx_handler,
	unsigned long long size,
	gdl_mem_pools pool,
	gdl_alloc_alignment alignment)
{
	return gdl_mem_alloc_aligned(ctx_handler, size, pool, alignment);
}

int gdl_mem_handle_is_null_wrapper(gdl_mem_handle_t handle)
{
	return gdl_mem_handle_is_null(handle);
}

int gdl_mem_cpy_to_dev_wrapper(gdl_mem_handle_t dst, const void *src, unsigned long long size)
{
	return gdl_mem_cpy_to_dev(dst, src, size);
}

gdl_mem_handle_t gdl_add_to_mem_handle_wrapper(gdl_mem_handle_t inp, unsigned long long addition)
{
	gdl_mem_handle_t ret;
	if (gdl_add_to_mem_handle(&ret, inp, addition))
		gsi_fatal("gdl_add_to_mem_handle() failed");
	return ret;
}

int gdl_mem_cpy_from_dev_wrapper(void *dst, gdl_mem_handle_t src, unsigned long long size)
{
	return gdl_mem_cpy_from_dev(dst, src, size);
}

void gdl_mem_free_wrapper(gdl_mem_handle_t buffer)
{
	gdl_mem_free(buffer);
}

int gdl_run_task_timeout_wrapper(gdl_context_handle_t ctx_hndl,
	GSI_UNUSED(int32_t task_number_from_the_C_side),
	gdl_mem_handle_t in_handle,
	gdl_mem_handle_t out_handle)
{
	return gdl_run_task_timeout(
		ctx_hndl,              /* @ctx_handler - the id of a hardware context previously allocated */
		GDL_TASK(gd_lab),  /* @code_offset - the code offset of the function that the task should execute */
		in_handle,         /* @inp - input memory handle */
		out_handle, /* @outp - output memory handle */
		GDL_TEMPORARY_DEFAULT_MEM_BUF,      /* @mem_buf - an array of previously allocated memory handles and their sizes */
		GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE, /* @buf_size - the length of the mem_buf array */
		GDL_TEMPORARY_DEFAULT_CORE_INDEX,   /* @apuc_idx - the apuc that the task should be executed on */
		NULL,              /* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
		0,                 /* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
		GDL_USER_MAPPING); /* @map_type - determine the mapping type for the specific task */
}

void *gdl_mem_handle_to_host_ptr_wrapper(gdl_mem_handle_t handle)
{
	return gdl_mem_handle_to_host_ptr(handle);
}
#ifndef __ARC_UTILS_H__
#define __ARC_UTILS_H__

#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>
#include <gsi/libgvml.h>
#include <gsi/libgvml_element_wise.h>
#include <gsi/libgvml_get_marked_data.h>
#include <gsi/libgvml_min_max.h>
#include <gsi/libgvml_iv.h>

static inline void direct_dma_l4_to_l2_16k(int32_t apc_id, const void *l4_src, uint64_t num_512b_chunks)
{
        struct gal_fast_l2dma_l4_l2_transaction txn = {
		.num_steps = 1,
		.step_size_num_512b_chunk = num_512b_chunks,
		.l4_addr = (void *)l4_src,
		.l2_mode = GAL_L2T_MODE_64,
		.l2_col_group = 0,
	};
	gal_fast_l2dma_mem_to_l2_start(apc_id, 1, &txn, GAL_L2DMA_L2_READY_SET);
}

static inline void dma_l2_to_l1_32k(enum gvml_vm_reg vmr)
{
	gal_fast_l2dma_l2_ready_rst_all();
	for (uint8_t bank = 0; bank < 4; bank++)
		gvml_load_vmr_16(vmr, bank, bank == (4 - 1), bank * 2);
}

static inline void dma_l2_sync(void)
{
	gal_fast_l2dma_async_memcpy_end(GAL_L2DMA_APC_ID_0);
	gal_fast_l2dma_async_memcpy_end(GAL_L2DMA_APC_ID_1);
}

static inline void direct_dma_l4_to_l1_32k(enum gvml_vm_reg vmr, void *l4_src)
{
	const uint64_t num_512b_chunks_in_32k_16b_elements = sizeof(uint16_t) * 32 * 1024 / 512;
	direct_dma_l4_to_l2_16k(GAL_L2DMA_APC_ID_0, l4_src, num_512b_chunks_in_32k_16b_elements / 2);
	direct_dma_l4_to_l2_16k(GAL_L2DMA_APC_ID_1, l4_src + 32 * 1024, num_512b_chunks_in_32k_16b_elements / 2);
	dma_l2_sync();
	dma_l2_to_l1_32k(vmr);
}

static inline void direct_dma_l2_to_l4_16k(int32_t apc_id, void *l4_dst, uint64_t num_512b_chunks)
{
        struct gal_fast_l2dma_l4_l2_transaction txn = {
		.num_steps = 1,
		.step_size_num_512b_chunk = num_512b_chunks,
		.l4_addr = l4_dst,
		.l2_mode = GAL_L2T_MODE_64,
		.l2_col_group = 0,
	};
	gal_fast_l2dma_l2_to_mem_start(apc_id, 1, &txn, GAL_L2DMA_L2_READY_SET);
}

static inline void dma_l1_to_l2_32k(enum gvml_vm_reg vmr)
{
        gal_fast_l2dma_l2_ready_rst_all();
        for (uint8_t bank = 0; bank < 4; bank++)
                gvml_store_vmr_16(vmr, bank, bank == (4 - 1), bank * 2);
}

static inline void direct_dma_l1_to_l4_32k(void *l4_dst, enum gvml_vm_reg vmr)
{
	dma_l1_to_l2_32k(vmr);
	const uint64_t num_512b_chunks_in_32k_16b_elements = sizeof(uint16_t) * 32 * 1024 / 512;
	direct_dma_l2_to_l4_16k(GAL_L2DMA_APC_ID_0, l4_dst, num_512b_chunks_in_32k_16b_elements / 2);
	direct_dma_l2_to_l4_16k(GAL_L2DMA_APC_ID_1, l4_dst + 32 * 1024 , num_512b_chunks_in_32k_16b_elements / 2);
	dma_l2_sync();
}

static inline void direct_dma_l4_to_l2_32k_start(const void *l4_src)
{
	const uint64_t num_512b_chunks_in_32k_16b_elements = sizeof(uint16_t) * 32 * 1024 / 512;
	direct_dma_l4_to_l2_16k(GAL_L2DMA_APC_ID_0, l4_src, num_512b_chunks_in_32k_16b_elements / 2);
	direct_dma_l4_to_l2_16k(GAL_L2DMA_APC_ID_1, l4_src + 32 * 1024, num_512b_chunks_in_32k_16b_elements / 2);
}

static inline void direct_dma_l2_to_l1_32k(enum gvml_vm_reg vmr)
{
	dma_l2_to_l1_32k(vmr);
}

static inline uint16_t dereference_u16_ptr(void *p)
{
	return *(uint16_t *)p;
}

static inline void *increment_ptr(void *p, int32_t size)
{
	return p + size;
}

static inline uint32_t gvml_count_m_g32k_wrapper(enum gvml_mrks_n_flgs msrc)
{
	uint32_t ret;
	gvml_count_m_g32k(&ret, msrc);
	return ret;
}

static inline const char *i8_array_to_str(const int8_t *arr)
{
	return (const char *)arr;
}

static inline void c_strcpy(int8_t *dst, const char *src)
{
	strcpy((char *)dst, src);
}

static inline int gal_fast_cache_dcache_flush_mlines_wrapper(void *start_addr, uint32_t size)
{
	return gal_fast_cache_dcache_flush_mlines((uintptr_t)start_addr, size);
}

static inline __attribute__((always_inline)) void gal_fast_l2dma_mem_to_mem_512_wrapper(void *dst, void *src, uint32_t apc_id)
{
	gal_fast_l2dma_mem_to_mem_512((uint8_t *)dst, (uint8_t *)src, apc_id);
}

/* workaround... */
static inline int gvml_get_marked_data_sxvv_interval_wrapper(
	void *buff,
	enum gvml_vr16 v16src1,
	enum gvml_vr16 v16src2,
	enum gvml_vr16 v15src,
	uint16_t val,
	enum gvml_mrks_n_flgs msrc,
	unsigned int num_ent,
	unsigned int interval_ent,
	int (*interval_func)(void *),
	void *interval_params)
{
	return gvml_get_marked_data_sxvv_interval(buff, v16src1, v16src2, v15src, val, msrc, num_ent, interval_ent, (interval_func_t)interval_func, interval_params);
}				       

#endif /* __ARC_UTILS_H__ */

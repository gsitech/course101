import os
import sys

conda_prefix = os.environ['CONDA_PREFIX']
belex_subpath = 'site-packages/belex'

def find_parent_directory_with_subpath(root_dir, subpath):
    for root, dirs, files in os.walk(root_dir):
        if subpath in root:
            parent_dir = os.path.abspath(os.path.join(root, os.pardir))
            return parent_dir

packages_dir = find_parent_directory_with_subpath(conda_prefix, belex_subpath)

lab_path = os.environ['C101_LAB_PATH']
sys.path.append(f'{conda_prefix}/share/lpython/lib/lpython')
sys.path.append(packages_dir)
sys.path.append(f'{packages_dir}/gsi_emulation/')
sys.path.append(lab_path)
sys.path.append(f'{lab_path}/../declarations')
sys.path.append(f'{lab_path}/../emulation')
sys.path.append(f'{lab_path}/../emulation/gsi')
sys.path.append(f'{lab_path}/../gchl')
sys.path.append(f'{lab_path}/emulation')

import emulation.libs
from host import main

main()

import numpy as np
from lpython import u16, u32, u64
from common import GD_LAB_3_MAX_NUM_FEATURES, GD_LAB_3_MAX_NUM_RECORDS_IN_DB

def gd_lab_3_get_preprocessed_db_size(num_features : u32) -> u64:
    if num_features > GD_LAB_3_MAX_NUM_FEATURES:
        raise RuntimeError(f'num_features, {num_features}, must not exceed {GD_LAB_3_MAX_NUM_FEATURES}')
    size_16b : u64 = u64(2)
    return size_16b * u64(num_features) * u64(GD_LAB_3_MAX_NUM_RECORDS_IN_DB)

def gd_lab_3_preprocess_db(src_f16 : np.ndarray) -> np.ndarray:
    assert src_f16.dtype == np.float16
    assert src_f16.ndim == 2
    num_records, num_features = src_f16.shape
    if num_records > int(GD_LAB_3_MAX_NUM_RECORDS_IN_DB):
        raise RuntimeError(f'num_records, {num_records}, must not exceed GD_LAB_3_MAX_NUM_RECORDS_IN_DB, {GD_LAB_3_MAX_NUM_RECORDS_IN_DB}')
    if num_features > int(GD_LAB_3_MAX_NUM_FEATURES):
        raise RuntimeError(f'num_features, {num_features}, must not exceed {GD_LAB_3_MAX_NUM_FEATURES}')
    
    padded_src = np.pad(src_f16, [(0, int(GD_LAB_3_MAX_NUM_RECORDS_IN_DB) - num_records), (0,0)], mode='empty')
    return np.ascontiguousarray(np.transpose(padded_src))
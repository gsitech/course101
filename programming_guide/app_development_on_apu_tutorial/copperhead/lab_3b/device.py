from lpython import (
    u16, i32, u32, u16,
    ccallback, CPtr, c_p_pointer, Pointer, InOut,
    dataclass, sizeof,
)

from common import GD_LAB_3_MAX_NUM_FEATURES, GD_LAB_3_MAX_NUM_RECORDS_IN_DB, GD_LAB_3_CMD_LOAD_DB, GD_LAB_3_CMD_SEARCH, gd_lab_3_load_db, gd_lab_3_search, gd_lab_3_cmd, gd_lab_3_idx_val
from gvml_defs import GVML_MRK0, GVML_MRK1, GVML_VR16_0, GVML_VR16_1, GVML_VR16_2, GVML_VR16_3
from gchl.lib import gchl_init_once, gchl_reset_16, gchl_reset_16_msk, gchl_load_16, gchl_add_f16, gchl_sub_f16, gchl_cpy_imm_16, gchl_monotonic_transformation_nan2max_u16_f16

from gsi.arc_utils import increment_ptr, dereference_u16_ptr, direct_dma_l4_to_l1_32k
from gsi.gal import gal_mem_handle_to_apu_ptr

from gsi.gvml import (gvml_create_index_16, gvml_lt_imm_u16, gvml_mark_kmin_idxval_u16_mrk_g32k, gvml_get_marked_data_xv)

@dataclass
class db_data:
    num_records : u32 = u32(0)
    num_features : u32 = u32(0)
    mrk_num_records : u16 = GVML_MRK0
    vr_idx : u16 = GVML_VR16_0

g_db_data : db_data = db_data()

# workaround to write to a global user type
def init_db_data(d : InOut[db_data], num_records : u32, num_features : u32) -> None:
    d.num_records = num_records
    d.num_features = num_features

def load_db(load_db_data : gd_lab_3_load_db) -> i32:
    global g_db_data
    
    if load_db_data.num_features > GD_LAB_3_MAX_NUM_FEATURES:
        print('number of features,', load_db_data.num_features, ', must not exceed', GD_LAB_3_MAX_NUM_FEATURES)
        return -1
    
    if load_db_data.num_records > GD_LAB_3_MAX_NUM_RECORDS_IN_DB:
        print('number of records,', load_db_data.num_records, ', must not exceed', GD_LAB_3_MAX_NUM_RECORDS_IN_DB)
        return -1
    
    init_db_data(g_db_data, load_db_data.num_records, load_db_data.num_features)

    gvml_create_index_16(g_db_data.vr_idx)
    gvml_lt_imm_u16(g_db_data.mrk_num_records, g_db_data.vr_idx, u16(i32(g_db_data.num_records)))
    
    ptr_in : CPtr = gal_mem_handle_to_apu_ptr(load_db_data.db)

    f : i32
    for f in range(i32(load_db_data.num_features)):
        direct_dma_l4_to_l1_32k(u16(f), increment_ptr(ptr_in, f * i32(GD_LAB_3_MAX_NUM_RECORDS_IN_DB) * i32(sizeof(u16))))

    return 0

def abs_f16(vr : u16) -> None:
    gchl_reset_16_msk(vr, u16(0x8000))

def do_search(search_data : gd_lab_3_search) -> i32:
    global g_db_data

    if search_data.k > g_db_data.num_records:
        print('k,', search_data.k, ', must not exceed number of records', g_db_data.num_records)
        return -1
    
    vr_distances : u16 = GVML_VR16_1
    vr_records : u16 = GVML_VR16_2
    vr_query : u16 = GVML_VR16_3
    mrk_kmin : u16 = GVML_MRK1

    p_q : CPtr = gal_mem_handle_to_apu_ptr(search_data.queries)
    p_iv : CPtr = gal_mem_handle_to_apu_ptr(search_data.output)

    q : i32
    for q in range(i32(search_data.num_queries)):
        gchl_reset_16(vr_distances)
        f : i32
        for f in range(i32(g_db_data.num_features)):
            f_val : u16 = dereference_u16_ptr(increment_ptr(p_q, f * i32(sizeof(u16))))
            gchl_cpy_imm_16(vr_query, f_val)
            gchl_load_16(vr_records, u16(f))
            gchl_sub_f16(vr_records, vr_records, vr_query)
            abs_f16(vr_records)
            gchl_add_f16(vr_distances, vr_distances, vr_records)

        gchl_monotonic_transformation_nan2max_u16_f16(vr_records, vr_distances)

        gvml_mark_kmin_idxval_u16_mrk_g32k(
            mrk_kmin,
            vr_records,
            g_db_data.vr_idx,
            search_data.k,
            g_db_data.mrk_num_records,
            vr_query)
        
        gvml_get_marked_data_xv(
            p_iv,
            g_db_data.vr_idx,
            vr_distances,
            mrk_kmin,
            search_data.k)

        p_q = increment_ptr(p_q, i32(g_db_data.num_features) * i32(sizeof(u16)))
        p_iv = increment_ptr(p_iv, i32(search_data.k) * i32(sizeof(gd_lab_3_idx_val)))        

    return 0

@ccallback
def task(in_: CPtr, out: CPtr) -> i32:
    cmd: Pointer[gd_lab_3_cmd] = c_p_pointer(in_, gd_lab_3_cmd)

    if cmd.cmd == GD_LAB_3_CMD_LOAD_DB:
        gchl_init_once()
        return load_db(cmd.load_db_data)
    elif cmd.cmd == GD_LAB_3_CMD_SEARCH:
        return do_search(cmd.search_data)
    else:
        return -1
#!/bin/bash

# set -ex

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export C101_LAB_PATH=$SCRIPT_DIR

python ${SCRIPT_DIR}/../run_emulation.py $@
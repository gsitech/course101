import sys
import re
import os

def convert_run_frag_async(str_file : str) -> str:

    re_run_frag_async_params = re.compile(r', "(\w+)", (\w+)')


    def call_run_frag_async(match: re.Match) -> str:
        indent = match.group(1)
        frag_nym = match.group(2)
        params_str = match.group(3)
        frag_kwargs = []
        frag_params = []

        if params_str is not None:
            for param_match in re_run_frag_async_params.finditer(params_str):
                param_nym = param_match.group(1)
                param_val = param_match.group(2)
                frag_kwargs.append(f"{param_nym}={param_val}")
                frag_params.append(param_val)

        frag_kwargs = ", ".join(frag_kwargs)
        frag_params = ", ".join(frag_params)

        return "\n".join([
            f"#ifdef APUC_TYPE_hw",
            f"{indent}RUN_FRAG_ASYNC({frag_nym}({frag_kwargs}));",
            f"#else",
            f"{indent}{frag_nym}({frag_params});",
            f"#endif",
        ])

    re_call_ch_run_frag_async = \
        re.compile(r'([ \t]*)__lpython_overloaded_\d+__CH_RUN_FRAG_ASYNC\("(\w+)"((?:, "\w+", \w+)+)?\);')
    return re_call_ch_run_frag_async.sub(call_run_frag_async, str_file)

def compile_with_gcc_aux_info(c_file_path : str) -> None:
    gcc_res = os.system(f'gcc -I $CONDA_PREFIX/share/lpython/lib/impure/ {c_file_path} -c -o {c_file_path}.o -aux-info {c_file_path}.aux.h')
    if gcc_res:
        raise RuntimeError(f'Failed to compile {c_file_path} with gcc')
    
def get_function_metadata(line: str) -> tuple():
    s = line.split()
    assert s[0] == '/*'
    assert s[2] == '*/'
    source = s[1].split(':')
    assert len(source) == 3

    filename = source[0]
    line_no = int(source[1])
    assert len(source[2]) == 2
    function_type = source[2][1]
    assert function_type == 'C' or function_type == 'F'

    start_function_args_index = -1
    for i, frag in enumerate(s):
        if '(' in frag:
            start_function_args_index = i
            break

    assert start_function_args_index > 0

    function_name = s[start_function_args_index - 1]

    # function might be preceeded by a '*' if it retruns a pointer
    if function_name[0] == '*':
        function_name = function_name[1:]

    assert re.match('^[A-Za-z0-9_-]*$', function_name)

    return filename, line_no, function_type, function_name

def flatten_comprehension(list_of_lists):
    return [item for row in list_of_lists for item in row]
    
def get_function_declarations_line_nos(c_file_path : str) -> list:
    compile_with_gcc_aux_info(c_file_path)

    function_declarations = {}
    aux_file = f'{c_file_path}.aux.h'
    with open(aux_file, 'r') as info:
        for line in info.readlines()[1:]:
            filename, line_no, function_type, function_name = get_function_metadata(line)
            if filename == c_file_path:
                if function_name in function_declarations:
                    if function_type == 'F':
                        # found the definition of a previous declaration
                        del function_declarations[function_name]
                    else:
                        # found an additional declaration
                        function_declarations[function_name].append(line_no)
                elif function_type == 'C':
                    function_declarations[function_name] = [line_no]

    return flatten_comprehension(function_declarations.values())

def remove_unwanted_lines(lines : list) -> list:
    unwanted_lines = [
        '#include <complex.h>',
        '#include <inttypes.h>',
        '#include <stdlib.h>',
        '#include <stdbool.h>',
        '#include <stdio.h>',
        '#include <string.h>',
        '#include <lfortran_intrinsics.h>',
        'uint16_t APL_INVAL_ROWNUM',
        'uint16_t B_FLAG',
        'uint16_t C_FLAG',
        'uint16_t FIRST_GP0_MRK',
        'uint16_t FIRST_TMP0_MRK',
        'uint16_t GCHL_VM_1',
        'uint16_t GCHL_VM_10',
        'uint16_t GCHL_VM_11',
        'uint16_t GCHL_VM_12',
        'uint16_t GCHL_VM_13',
        'uint16_t GCHL_VM_14',
        'uint16_t GCHL_VM_15',
        'uint16_t GCHL_VM_16',
        'uint16_t GCHL_VM_17',
        'uint16_t GCHL_VM_18',
        'uint16_t GCHL_VM_19',
        'uint16_t GCHL_VM_2',
        'uint16_t GCHL_VM_20',
        'uint16_t GCHL_VM_21',
        'uint16_t GCHL_VM_22',
        'uint16_t GCHL_VM_23',
        'uint16_t GCHL_VM_24',
        'uint16_t GCHL_VM_25',
        'uint16_t GCHL_VM_26',
        'uint16_t GCHL_VM_27',
        'uint16_t GCHL_VM_28',
        'uint16_t GCHL_VM_29',
        'uint16_t GCHL_VM_3',
        'uint16_t GCHL_VM_30',
        'uint16_t GCHL_VM_31',
        'uint16_t GCHL_VM_32',
        'uint16_t GCHL_VM_33',
        'uint16_t GCHL_VM_34',
        'uint16_t GCHL_VM_35',
        'uint16_t GCHL_VM_36',
        'uint16_t GCHL_VM_37',
        'uint16_t GCHL_VM_38',
        'uint16_t GCHL_VM_39',
        'uint16_t GCHL_VM_4',
        'uint16_t GCHL_VM_40',
        'uint16_t GCHL_VM_41',
        'uint16_t GCHL_VM_42',
        'uint16_t GCHL_VM_43',
        'uint16_t GCHL_VM_44',
        'uint16_t GCHL_VM_45',
        'uint16_t GCHL_VM_46',
        'uint16_t GCHL_VM_47',
        'uint16_t GCHL_VM_5',
        'uint16_t GCHL_VM_6',
        'uint16_t GCHL_VM_7',
        'uint16_t GCHL_VM_8',
        'uint16_t GCHL_VM_9',
        'uint16_t GCHL_VR16_0',
        'uint16_t GCHL_VR16_1',
        'uint16_t GCHL_VR16_10',
        'uint16_t GCHL_VR16_11',
        'uint16_t GCHL_VR16_12',
        'uint16_t GCHL_VR16_13',
        'uint16_t GCHL_VR16_14',
        'uint16_t GCHL_VR16_2',
        'uint16_t GCHL_VR16_3',
        'uint16_t GCHL_VR16_4',
        'uint16_t GCHL_VR16_5',
        'uint16_t GCHL_VR16_6',
        'uint16_t GCHL_VR16_7',
        'uint16_t GCHL_VR16_8',
        'uint16_t GCHL_VR16_9',
        'uint16_t GP0_MRK',
        'uint16_t GP1_MRK',
        'uint16_t GP2_MRK',
        'uint16_t GP3_MRK',
        'uint16_t GP4_MRK',
        'uint16_t GP5_MRK',
        'uint16_t GP6_MRK',
        'uint16_t GP7_MRK',
        'uint16_t GSI_APC_NUM_HALF_BANKS',
        'uint16_t GSI_APUC_NUM_APCS',
        'uint16_t GSI_MMB_NUM_BANKS',
        'uint16_t L1_ADDR_REG0',
        'uint16_t L1_ADDR_REG1',
        'uint16_t L1_ADDR_REG2',
        'uint16_t L1_ADDR_REG3',
        'uint16_t L2_ADDR_REG0',
        'uint16_t LAST_GP7_MRK',
        'uint16_t LAST_TMP3_MRK',
        'uint16_t MMB_B_FLAG',
        'uint16_t MMB_C_FLAG',
        'uint16_t MMB_MRK0',
        'uint16_t MMB_MRK1',
        'uint16_t MMB_MRK2',
        'uint16_t MMB_MRK3',
        'uint16_t MMB_MRK4',
        'uint16_t MMB_MRK5',
        'uint16_t MMB_MRK6',
        'uint16_t MMB_MRK7',
        'uint16_t MMB_OF_FLAG',
        'uint16_t MMB_PE_FLAG',
        'uint16_t MMB_TMP0_MRK',
        'uint16_t MMB_TMP1_MRK',
        'uint16_t MMB_TMP2_MRK',
        'uint16_t MMB_TMP3_MRK',
        'uint16_t OF_FLAG',
        'uint16_t PE_FLAG',
        'uint16_t RN_REG_FLAGS',
        'uint16_t RN_REG_G0',
        'uint16_t RN_REG_G1',
        'uint16_t RN_REG_G2',
        'uint16_t RN_REG_G3',
        'uint16_t RN_REG_G4',
        'uint16_t RN_REG_G5',
        'uint16_t RN_REG_G6',
        'uint16_t RN_REG_G7',
        'uint16_t RN_REG_T0',
        'uint16_t RN_REG_T1',
        'uint16_t RN_REG_T2',
        'uint16_t RN_REG_T3',
        'uint16_t RN_REG_T4',
        'uint16_t RN_REG_T5',
        'uint16_t RN_REG_T6',
        'uint16_t SB_0',
        'uint16_t SB_1',
        'uint16_t SB_10',
        'uint16_t SB_11',
        'uint16_t SB_12',
        'uint16_t SB_13',
        'uint16_t SB_14',
        'uint16_t SB_15',
        'uint16_t SB_16',
        'uint16_t SB_17',
        'uint16_t SB_18',
        'uint16_t SB_19',
        'uint16_t SB_2',
        'uint16_t SB_20',
        'uint16_t SB_21',
        'uint16_t SB_22',
        'uint16_t SB_23',
        'uint16_t SB_3',
        'uint16_t SB_4',
        'uint16_t SB_5',
        'uint16_t SB_6',
        'uint16_t SB_7',
        'uint16_t SB_8',
        'uint16_t SB_9',
        'uint16_t SM_0X0001',
        'uint16_t SM_0X000F',
        'uint16_t SM_0X001F',
        'uint16_t SM_0X003F',
        'uint16_t SM_0X00FF',
        'uint16_t SM_0X0101',
        'uint16_t SM_0X0707',
        'uint16_t SM_0X0F0F',
        'uint16_t SM_0X1111',
        'uint16_t SM_0X3333',
        'uint16_t SM_0X5555',
        'uint16_t SM_0XFFFF',
        'uint16_t SM_BIT_0',
        'uint16_t SM_BIT_1',
        'uint16_t SM_BIT_10',
        'uint16_t SM_BIT_11',
        'uint16_t SM_BIT_12',
        'uint16_t SM_BIT_13',
        'uint16_t SM_BIT_14',
        'uint16_t SM_BIT_15',
        'uint16_t SM_BIT_2',
        'uint16_t SM_BIT_3',
        'uint16_t SM_BIT_4',
        'uint16_t SM_BIT_5',
        'uint16_t SM_BIT_6',
        'uint16_t SM_BIT_7',
        'uint16_t SM_BIT_8',
        'uint16_t SM_BIT_9',
        'uint16_t SM_REG0',
        'uint16_t SM_REG1',
        'uint16_t SM_REG2',
        'uint16_t SM_REG3',
        'uint16_t TMP0_MRK;',
        'uint16_t TMP1_MRK;',
        'uint16_t TMP2_MRK;',
        'uint16_t TMP3_MRK;',
        'uint16_t VR16_G1',
        'uint16_t VR16_G10',
        'uint16_t VR16_G11',
        'uint16_t VR16_G12',
        'uint16_t VR16_G13',
        'uint16_t VR16_G14',
        'uint16_t VR16_G2',
        'uint16_t VR16_G3',
        'uint16_t VR16_G4',
        'uint16_t VR16_G5',
        'uint16_t VR16_G6',
        'uint16_t VR16_G7',
        'uint16_t VR16_G8',
        'uint16_t VR16_G9',
        'uint16_t VR16_G_FIRST',
        'uint16_t VR16_G_LAST',
        'uint16_t VR16_M4_IDX',
        'uint16_t VR16_RO_LAST',
        'uint16_t VR16_T_FIRST',
        'uint16_t VR16_T_LAST',
        'uint16_t _2VR16_G0_1',
        'uint16_t _2VR16_G10_11',
        'uint16_t _2VR16_G11_12',
        'uint16_t _2VR16_G12_13',
        'uint16_t _2VR16_G13_14',
        'uint16_t _2VR16_G1_2',
        'uint16_t _2VR16_G2_3',
        'uint16_t _2VR16_G3_4',
        'uint16_t _2VR16_G4_5',
        'uint16_t _2VR16_G5_6',
        'uint16_t _2VR16_G6_7',
        'uint16_t _2VR16_G7_8',
        'uint16_t _2VR16_G8_9',
        'uint16_t _2VR16_G9_10',
        'uint16_t _2VR16_G_FIRST',
        'uint16_t _2VR16_G_LAST',
        'uint16_t _2VR16_T0_1',
        'uint16_t _2VR16_T1_2',
        'uint16_t _2VR16_T2_3',
        'uint16_t _2VR16_T3_4',
        'uint16_t _2VR16_T4_5',
        'uint16_t _2VR16_T5_6',
        'uint16_t _2VR16_T_FIRST',
        'uint16_t _2VR16_T_LAST',
        'uint16_t GVML_MRK0',
        'uint16_t GVML_MRK1',
        'uint16_t GVML_MRK2',
        'uint16_t GVML_MRK3',
        'uint16_t GVML_MRK4',
        'uint16_t GVML_MRK5',
        'uint16_t GVML_MRK6',
        'uint16_t GVML_MRK7',
        'uint16_t GVML_P2_1',
        'uint16_t GVML_P2_128',
        'uint16_t GVML_P2_16',
        'uint16_t GVML_P2_16K',
        'uint16_t GVML_P2_1K',
        'uint16_t GVML_P2_2',
        'uint16_t GVML_P2_256',
        'uint16_t GVML_P2_2K',
        'uint16_t GVML_P2_32',
        'uint16_t GVML_P2_32K',
        'uint16_t GVML_P2_4',
        'uint16_t GVML_P2_4K',
        'uint16_t GVML_P2_512',
        'uint16_t GVML_P2_64',
        'uint16_t GVML_P2_8',
        'uint16_t GVML_P2_8K',
        'uint16_t GVML_VM_0',
        'uint16_t GVML_VM_1',
        'uint16_t GVML_VM_10',
        'uint16_t GVML_VM_11',
        'uint16_t GVML_VM_12',
        'uint16_t GVML_VM_13',
        'uint16_t GVML_VM_14',
        'uint16_t GVML_VM_15',
        'uint16_t GVML_VM_16',
        'uint16_t GVML_VM_17',
        'uint16_t GVML_VM_18',
        'uint16_t GVML_VM_19',
        'uint16_t GVML_VM_2',
        'uint16_t GVML_VM_20',
        'uint16_t GVML_VM_21',
        'uint16_t GVML_VM_22',
        'uint16_t GVML_VM_23',
        'uint16_t GVML_VM_24',
        'uint16_t GVML_VM_25',
        'uint16_t GVML_VM_26',
        'uint16_t GVML_VM_27',
        'uint16_t GVML_VM_28',
        'uint16_t GVML_VM_29',
        'uint16_t GVML_VM_3',
        'uint16_t GVML_VM_30',
        'uint16_t GVML_VM_31',
        'uint16_t GVML_VM_32',
        'uint16_t GVML_VM_33',
        'uint16_t GVML_VM_34',
        'uint16_t GVML_VM_35',
        'uint16_t GVML_VM_36',
        'uint16_t GVML_VM_37',
        'uint16_t GVML_VM_38',
        'uint16_t GVML_VM_39',
        'uint16_t GVML_VM_4',
        'uint16_t GVML_VM_40',
        'uint16_t GVML_VM_41',
        'uint16_t GVML_VM_42',
        'uint16_t GVML_VM_43',
        'uint16_t GVML_VM_44',
        'uint16_t GVML_VM_45',
        'uint16_t GVML_VM_46',
        'uint16_t GVML_VM_47',
        'uint16_t GVML_VM_5',
        'uint16_t GVML_VM_6',
        'uint16_t GVML_VM_7',
        'uint16_t GVML_VM_8',
        'uint16_t GVML_VM_9',
        'uint16_t GVML_VR16_0',
        'uint16_t GVML_VR16_1',
        'uint16_t GVML_VR16_10',
        'uint16_t GVML_VR16_11',
        'uint16_t GVML_VR16_12',
        'uint16_t GVML_VR16_13',
        'uint16_t GVML_VR16_14',
        'uint16_t GVML_VR16_2',
        'uint16_t GVML_VR16_3',
        'uint16_t GVML_VR16_4',
        'uint16_t GVML_VR16_5',
        'uint16_t GVML_VR16_6',
        'uint16_t GVML_VR16_7',
        'uint16_t GVML_VR16_8',
        'uint16_t GVML_VR16_9',
        'uint16_t GVML_VR16_IDX',
        'uint16_t GSI_L1_VA_SET_ADDR_ROWS',
        'uint32_t GAL_L2DMA_APC_ID_0',
        'uint32_t GAL_L2DMA_APC_ID_1',
    ]

    ret = []
    for line in lines:
        unwanted_line_present = False
        for unwanted in unwanted_lines:
            if unwanted in line:
                unwanted_line_present = True
                break
        if not unwanted_line_present:
            ret.append(line)
    return ret

def refactor_main(lines: list, entry_point_name: str) -> list:

    ret = []
    task_line = -1
    in_main = False
    init_done = False
    for i, line in enumerate(lines):
        if line.startswith('int32_t task(void* in_, void* out)'):
            task_line = i
            ret.append(f'GAL_TASK_ENTRY_POINT({entry_point_name}, in_, out)\n')
        elif line.startswith('int main('):
            in_main = True
        elif in_main and line.startswith('}'):
            in_main = False
        elif in_main and ('__main____global_initializer();' in line or '__main____global_statements();' in line):
            assert task_line != -1
            if not init_done:
                init_done = True
                ret.insert(task_line + 2, '\tstatic int is_globals_init_done = 0;\n') # +2 b/c we wish to skip the '{'
                ret.insert(task_line + 3, '\tif (!is_globals_init_done) {\n')
                ret.insert(task_line + 4, '\t\tis_globals_init_done = 1;\n')
                task_line += 5
            ret.insert(task_line, line)
            task_line += 1 # increment the task line to maintain order
        else:
            if not in_main:
                ret.append(line)

    if init_done:
        ret.insert(task_line, '\t}\n')
    return ret

def remove_unwanted_newlines(str_file : str) -> str:
    pattern = re.compile(r'\n{2,}')
    modified_content = pattern.sub('\n', str_file)
    return modified_content

def prepend_headers(str_file : str) -> str:
    headers = '''
/*
 * Copyright (C) 2020-2023 GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

APL_C_INCLUDE <gsi/libsys/assert.h>
APL_C_INCLUDE <gsi/libsys.h>
APL_C_INCLUDE <string.h>
APL_C_INCLUDE <gsi/libapl.h>
APL_C_INCLUDE <gsi/gal-fast-funcs.h>
APL_C_INCLUDE <arc_utils.h>

#ifndef APL_DEFS_H
#define APL_DEFS_H

// #ifdef __APL_PREPROC__

/* rownum registers used for function parameters */
#define RN_REG_G0 RN_REG_0
#define RN_REG_G1 RN_REG_1
#define RN_REG_G2 RN_REG_2
#define RN_REG_G3 RN_REG_3
#define RN_REG_G4 RN_REG_4
#define RN_REG_G5 RN_REG_5
#define RN_REG_G6 RN_REG_6
#define RN_REG_G7 RN_REG_7

/* rownum registers to hold predefined temporary vector-registers.
 * These rownum registers shouldn't change during program execution.
 */
#define RN_REG_T0 RN_REG_8
#define RN_REG_T1 RN_REG_9
#define RN_REG_T2 RN_REG_10
#define RN_REG_T3 RN_REG_11
#define RN_REG_T4 RN_REG_12
#define RN_REG_T5 RN_REG_13
#define RN_REG_T6 RN_REG_14

/* rownum register to hold pre-defined flags vector-registers.
 * This rownum register shouldn't change during program execution.
 */
#define RN_REG_FLAGS RN_REG_15


/* l1_addr registers used for function parameters */
#define L1_ADDR_REG0 L1_ADDR_REG_0
#define L1_ADDR_REG1 L1_ADDR_REG_1
#define L1_ADDR_REG2 L1_ADDR_REG_2
#define L1_ADDR_REG3 L1_ADDR_REG_3

#define L2_ADDR_REG0 L2_ADDR_REG_0

#define RE_REG_G0 RE_REG_0
#define RE_REG_G1 RE_REG_1
#define RE_REG_T0 RE_REG_2
#define RE_REG_NO_RE RE_REG_3

#define EWE_REG_G0 EWE_REG_0
#define EWE_REG_G1 EWE_REG_1
#define EWE_REG_T0 EWE_REG_2
#define EWE_REG_NO_EWE EWE_REG_3


/* general purpose smaps registers */
#define SM_REG0 SM_REG_0
#define SM_REG1 SM_REG_1
#define SM_REG2 SM_REG_2
#define SM_REG3 SM_REG_3

/* smaps registers to hold predefined section maps.
 * These smaps registers shouldn't change during program execution.
 */
#define SM_0XFFFF SM_REG_4
#define SM_0X0001 SM_REG_5
#define SM_0X1111 SM_REG_6
#define SM_0X0101 SM_REG_7
#define SM_0X000F SM_REG_8
#define SM_0X0F0F SM_REG_9
#define SM_0X0707 SM_REG_10
#define SM_0X5555 SM_REG_11
#define SM_0X3333 SM_REG_12
#define SM_0X00FF SM_REG_13
#define SM_0X001F SM_REG_14
#define SM_0X003F SM_REG_15


/* Flag IDs (in flags Vector Register) */
/* pre-defined flags for the flags vector-register */
#define C_FLAG SM_BIT_0		/* Carry in/out flag */
#define B_FLAG SM_BIT_1		/* Borrow in/out flag */
#define OF_FLAG SM_BIT_2	/* Overflow flag */
#define PE_FLAG SM_BIT_3	/* Parity error */

/* Markers for general purpose usage Callee must preserve */
#define GP0_MRK SM_BIT_4
#define GP1_MRK SM_BIT_5
#define GP2_MRK SM_BIT_6
#define GP3_MRK SM_BIT_7
#define GP4_MRK SM_BIT_8
#define GP5_MRK SM_BIT_9
#define GP6_MRK SM_BIT_10
#define GP7_MRK SM_BIT_11

/* Markers for general purpose usage Callee may overwrite preserve */
#define TMP0_MRK SM_BIT_12
#define TMP1_MRK SM_BIT_13
#define TMP2_MRK SM_BIT_14
#define TMP3_MRK SM_BIT_15

// #endif /* __APL_PREPROC__ */

#endif /* APL_DEFS_H */

enum smaps_vals {
	/* pre-defined section maps vector-registers */
	SM_0XFFFF_VAL = 0xFFFF,
	SM_0X0001_VAL = 0x0001,
	SM_0X1111_VAL = 0x1111,
	SM_0X0101_VAL = 0x0101,
	SM_0X000F_VAL = 0x000F,
	SM_0X0F0F_VAL = 0x0F0F,
	SM_0X0707_VAL = 0x0707,
	SM_0X5555_VAL = 0x5555,
	SM_0X3333_VAL = 0x3333,
	SM_0X00FF_VAL = 0x00FF,
	SM_0X001F_VAL = 0x001F,
	SM_0X003F_VAL = 0x003F
};

#include "fragments.apl.h"
'''
    return headers + str_file

def replace_prints_with_gsi_info(s :str) -> str:
    return s.replace('printf', 'gsi_info')

def correct_long_printing(s :str) -> str:
    return s.replace('%li', '%lli')

def refactor_code(c_dst_file: str, c_src_file: str) -> None:
    
    with open(c_src_file, 'r') as src_file:
        src = src_file.read()
        with open(c_dst_file, 'w+') as dst:
            src = convert_run_frag_async(src)
            dst.write(src)
            dst.flush()
            line_nos_to_delete = get_function_declarations_line_nos(dst.name)
            dst.seek(0, 0)
            lines = dst.readlines()
            dst.truncate(0)
            redacted_lines = [l for i, l in enumerate(lines) if i + 1 not in line_nos_to_delete]
            redacted_lines = remove_unwanted_lines(redacted_lines)
            redacted_lines = refactor_main(redacted_lines, 'gd_lab')
            dst.seek(0, 0)
            s = prepend_headers(''.join(redacted_lines))
            s = remove_unwanted_newlines(s)
            s = replace_prints_with_gsi_info(s)
            s = correct_long_printing(s)
            dst.write(s)

if __name__ == '__main__':
    refactor_code(sys.argv[1], sys.argv[2])
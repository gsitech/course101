import numpy as np
import random

GD_LAB_4_MAX_NUM_FEATURES = 48

def gd_lab_4_get_preprocessed_db_size(num_features, num_cols_per_rec_pow_2) -> int:
    _32k = 2**15
    size_16b = 2
    return size_16b * (num_features >> num_cols_per_rec_pow_2) * _32k

def gd_lab_4_preprocess_db(src_f16 : np.ndarray, num_cols_per_rec_pow_2) -> np.ndarray:
    assert src_f16.itemsize == 2
    assert src_f16.ndim == 2

    num_records, num_features = src_f16.shape

    if num_cols_per_rec_pow_2 > 3:
        raise ValueError(f'num_cols_per_rec_pow_2 {num_cols_per_rec_pow_2} must be <= 3')
    
    if num_records & 3:
        raise ValueError(f'num_records must be a multiple of 4')
    
    num_feature_grps = num_features >> num_cols_per_rec_pow_2
    if num_features != num_feature_grps << num_cols_per_rec_pow_2:
        raise ValueError(f'num_features {num_features} must be a multiple of (2 ^ num_cols_per_rec_pow_2), (num_cols_per_rec_pow_2 = {num_cols_per_rec_pow_2})')
    
    if (num_features >> num_cols_per_rec_pow_2) > GD_LAB_4_MAX_NUM_FEATURES:
        raise ValueError(f'The combination of num_features = {num_features} and num_cols_per_rec_pow_2 = {num_cols_per_rec_pow_2} exceeds available L1 memory')

    num_records = src_f16.shape[0]
    num_elms_in_vmr = 2**15

    src_view = np.lib.stride_tricks.as_strided(
        src_f16.ravel(),
        shape=(num_feature_grps, num_records // 4, 1 << num_cols_per_rec_pow_2, 4),
        strides=(src_f16.itemsize << num_cols_per_rec_pow_2,
                 4 * num_features * src_f16.itemsize,
                 src_f16.itemsize, num_features * src_f16.itemsize))
    
    dst_view_shape=(src_view.shape[0], (num_elms_in_vmr >> num_cols_per_rec_pow_2) // 4, src_view.shape[2], src_view.shape[3])

    num_16b_elms = gd_lab_4_get_preprocessed_db_size(num_features, num_cols_per_rec_pow_2) // src_f16.itemsize
    ret = np.zeros(num_16b_elms, dtype=src_f16.dtype)
    dst_view = ret.reshape(dst_view_shape)
    dst_view[:, :src_view.shape[1], :, :] = src_view

    return ret

def ref(src_f16 : np.ndarray, num_cols_per_rec_pow_2):
    num_records, num_features = src_f16.shape
    num_feature_grps = num_features >> num_cols_per_rec_pow_2
    num_quad_recs = num_records // 4
    _32k = 32 * 1024
    num_cols_per_rec = 1 << num_cols_per_rec_pow_2
    num_16b_elms = gd_lab_4_get_preprocessed_db_size(num_features, num_cols_per_rec_pow_2) // src_f16.itemsize
    ret = np.zeros(num_16b_elms, dtype=src_f16.dtype)

    src_ravel = src_f16.ravel()
    for qr in range(num_quad_recs):
        for fg in range(num_feature_grps):
            for c in range(num_cols_per_rec):
                for i in range(4):
                    ret[_32k * fg + qr * num_cols_per_rec * 4 + c * 4 + i] = src_ravel[(qr * 4  + i) * num_features + fg * num_cols_per_rec + c]

    return ret

num_folds = 0
num_features = 48
num_records = (random.randint(0, 2**15) >> num_folds) & 0xfffc
print(f'num_records = {num_records}')
src0 = np.arange(num_records * num_features, dtype=np.uint16).reshape(num_records, num_features)
dst0 = gd_lab_4_preprocess_db(src0, num_folds)
ref0 = ref(src0, num_folds)

print(np.array_equal(dst0, ref0))
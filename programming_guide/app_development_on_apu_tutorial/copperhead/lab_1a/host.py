import sys
from lpython import (
    u16, i32, u32, i64, u64, 
    CPtr, sizeof, c_p_pointer, Pointer
)
from common import (
    gd_lab_1_cmd, 
    GD_LAB_1_CMD_DMA_DEMO, GD_LAB_1_CMD_LOOKUP_DEMO, GD_LAB_1_CMD_GMD_DEMO, GD_LAB_1_CMD_FIZZBUZZ_EXERCISE, 
    GD_LAB_1_FIZZ, GD_LAB_1_BUZZ, GD_LAB_1_FIZZBUZZ
)

from gsi.sys_apu import gsi_libsys_init_wrapper, gsi_libsys_exit_wrapper
from gsi.gdl import (gdl_init_wrapper, gdl_exit_wrapper, find_and_allocate_context, gdl_context_free_wrapper,
                     gdl_mem_alloc_aligned_wrapper, gdl_mem_handle_is_null_wrapper, gdl_mem_handle_to_host_ptr_wrapper,
                     gdl_run_task_timeout_wrapper, gdl_mem_free_wrapper, gdl_mem_cpy_to_dev_wrapper, gdl_mem_cpy_from_dev_wrapper,
                     gdl_add_to_mem_handle_wrapper)

import numpy as np
import ctypes

# Helper functions - start
def convert_cptr_to_np_array(cptr: CPtr, size: u16) -> CPtr:
    return np.ctypeslib.as_array(ctypes.cast(cptr, ctypes.POINTER(ctypes.c_uint16)), shape=(size,))

def convert_np_array_to_cptr(np_array: np.ndarray) -> CPtr:
    return np_array.ctypes.data_as(ctypes.POINTER(ctypes.c_uint16))

def create_random_f16_array(length : i64) -> np.ndarray:
    return np.random.random(length).astype(np.float16)

def create_empty_f16_array(length : i64) -> np.ndarray:
    return np.empty(length, dtype=np.float16)

def create_empty_u16_array(length : i64) -> np.ndarray:
    return np.empty(length, dtype=np.uint16)

def create_random_u16_array(length : i64) -> np.ndarray:
    return np.random.randint(low=0, high=0xffff+1, size=length, dtype=np.uint16)

# types - start
idx_val : np.dtype = np.dtype({'names': ['idx','val'], 'formats': [np.uint16, np.uint16]})

# constants - start
_32K : i64 = i64(32768)
SIZE_F16 : i64 = i64(2)
SIZE_U16 : i64 = i64(2)
SIZE_U64 : i64 = i64(8)
GDL_MEM_HANDLE_NULL : u64 = u64(0)
GDL_CONST_MAPPED_POOL : i32 = i32(0)
GDL_ALIGN_32 : i32 = i32(4)
SIZE_IDX_VAL : u16 = idx_val.itemsize

def parse_args() -> i32:
    if len(sys.argv) != 2:
        raise ValueError('usage: ', sys.argv[0], '[0 | 1 | 2 | 3] (dma_demo, exercise_0, exercise_1, exercise_2)')
    ret : i32 = i32(int(sys.argv[1]))
    if ret != 0 and ret != 1 and ret != 2 and ret != 3:
        raise ValueError('usage: ', sys.argv[0], '[0 | 1 | 2 | 3] (dma_demo, exercise_0, exercise_1, exercise_2)')
    return ret

def run_lab_1_dma_demo_cmd(
        ctx_id : i64,
        dst : np.ndarray,
        src : np.ndarray) -> i32:
    
    ret : i32 = i32(0)

    dev_cmd_buf : u64 = GDL_MEM_HANDLE_NULL
    io_dev_bufs : u64 = GDL_MEM_HANDLE_NULL

    io_dev_buf_size : i64 = SIZE_F16 * _32K
    io_dev_bus_total_size : i64 = io_dev_buf_size * i64(2)

    io_dev_bufs = gdl_mem_alloc_aligned_wrapper(ctx_id, io_dev_bus_total_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
    if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
        raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', io_dev_bus_total_size, 'bytes')
    
    ret = gdl_mem_cpy_to_dev_wrapper(io_dev_bufs, convert_np_array_to_cptr(src), io_dev_buf_size)
    if ret:
        raise MemoryError('gdl_mem_cpy_to_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')
    
    cmd_buf_size : i64 = sizeof(gd_lab_1_cmd)
    dev_cmd_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
    if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
        raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', cmd_buf_size, 'bytes')
    
    cmd_ptr: CPtr = gdl_mem_handle_to_host_ptr_wrapper(dev_cmd_buf)
    cmd: Pointer[gd_lab_1_cmd] = c_p_pointer(cmd_ptr, gd_lab_1_cmd)    
    cmd.cmd = GD_LAB_1_CMD_DMA_DEMO
    cmd.dma_demo_data.input = io_dev_bufs
    cmd.dma_demo_data.output = gdl_add_to_mem_handle_wrapper(cmd.dma_demo_data.input, io_dev_buf_size)

    ret = gdl_run_task_timeout_wrapper(ctx_id, 1234, dev_cmd_buf, GDL_MEM_HANDLE_NULL)
    if ret:
        raise MemoryError('gdl_run_task_timeout() failed:', ret)

    ret = gdl_mem_cpy_from_dev_wrapper(convert_np_array_to_cptr(dst), cmd.dma_demo_data.output, io_dev_buf_size)
    if ret:
        raise MemoryError('gdl_mem_cpy_from_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')

    gdl_mem_free_wrapper(dev_cmd_buf)
    gdl_mem_free_wrapper(io_dev_bufs)

    return ret

def run_lab_1_dma_demo_cmd_and_check(ctx_id : i64) -> None:
    src_arr : np.nd_array = create_random_f16_array(_32K)
    dst_arr : np.nd_array = create_empty_f16_array(_32K)

    run_lab_1_dma_demo_cmd(ctx_id, dst_arr, src_arr)

    i : i32
    for i in range(i32(_32K)):
        if src_arr[i] != dst_arr[i]:
            print('Mismatch for element', i, 'result =', dst_arr[i], 'expected value =', src_arr[i])
            raise RuntimeError('Mismatch')

def run_lab_1_lookup_demo_cmd(
        ctx_id : i64,
        dst : np.ndarray,
        src_arr : u16[8],
        lut_len : u32) -> i32:
    ret : i32 = i32(0)

    dev_cmd_buf : u64 = GDL_MEM_HANDLE_NULL
    io_dev_bufs : u64 = GDL_MEM_HANDLE_NULL

    io_dev_buf_size : i64 = SIZE_U16 * _32K

    io_dev_bufs = gdl_mem_alloc_aligned_wrapper(ctx_id, io_dev_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
    if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
        raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', io_dev_buf_size, 'bytes')

    cmd_buf_size : i64 = sizeof(gd_lab_1_cmd)
    dev_cmd_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
    if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
        raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', cmd_buf_size, 'bytes')

    cmd_ptr: CPtr = gdl_mem_handle_to_host_ptr_wrapper(dev_cmd_buf)
    cmd: Pointer[gd_lab_1_cmd] = c_p_pointer(cmd_ptr, gd_lab_1_cmd)    
    cmd.cmd = GD_LAB_1_CMD_LOOKUP_DEMO
    cmd.lookup_demo_data.input = src_arr
    cmd.lookup_demo_data.input_len = lut_len
    cmd.lookup_demo_data.output = io_dev_bufs

    ret = gdl_run_task_timeout_wrapper(ctx_id, 1234, dev_cmd_buf, GDL_MEM_HANDLE_NULL)
    if ret:
        raise RuntimeError('gdl_run_task_timeout() failed:', ret)

    ret = gdl_mem_cpy_from_dev_wrapper(convert_np_array_to_cptr(dst), cmd.lookup_demo_data.output, io_dev_buf_size)
    if ret:
        raise MemoryError('gdl_mem_cpy_from_dev_wrapper() failed to copy', io_dev_buf_size, 'bytes')

    gdl_mem_free_wrapper(dev_cmd_buf)
    gdl_mem_free_wrapper(io_dev_bufs)

    return ret

def run_lab_1_lookup_demo_cmd_and_check(ctx_id : i64) -> None:
    src_arr : u16[8] = (ctypes.c_uint16 * 8)(18, 5, 7)
    dst_arr : np.ndarray = create_empty_u16_array(_32K)

    run_lab_1_lookup_demo_cmd(ctx_id, dst_arr, src_arr, 3)

    i : i32
    for i in range(i32(_32K)):
        if src_arr[i % 3] != dst_arr[i]:
            print('Mismatch for element', i, 'result =', dst_arr[i], 'expected value =', src_arr[i])
            raise RuntimeError('Mismatch')

def run_lab_1_gmd_demo_cmd(
        ctx_id : i64,
        dst : np.ndarray) -> i32:
    ret : i32 = i32(0)

    dev_cmd_buf : u64 = GDL_MEM_HANDLE_NULL
    o_dev_bufs : u64 = GDL_MEM_HANDLE_NULL

    o_dev_buf_size : i64 = SIZE_U16 * 512

    o_dev_bufs = gdl_mem_alloc_aligned_wrapper(ctx_id, o_dev_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
    if gdl_mem_handle_is_null_wrapper(o_dev_bufs):
        raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', o_dev_buf_size, 'bytes')
    
    cmd : gd_lab_1_cmd = gd_lab_1_cmd(GD_LAB_1_CMD_GMD_DEMO)
    cmd.gmd_demo_data.output = o_dev_bufs

    cmd_buf_size : i64 = sizeof(gd_lab_1_cmd)
    dev_cmd_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
    if gdl_mem_handle_is_null_wrapper(o_dev_bufs):
        raise MemoryError('gdl_mem_alloc_aligned_wrapper() failed to allocate', cmd_buf_size, 'bytes')
    
    cmd_ptr: CPtr = gdl_mem_handle_to_host_ptr_wrapper(dev_cmd_buf)
    cmd: Pointer[gd_lab_1_cmd] = c_p_pointer(cmd_ptr, gd_lab_1_cmd)    
    cmd.cmd = GD_LAB_1_CMD_GMD_DEMO
    cmd.gmd_demo_data.output = o_dev_bufs

    ret = gdl_run_task_timeout_wrapper(ctx_id, 1234, dev_cmd_buf, GDL_MEM_HANDLE_NULL)
    if ret:
        raise MemoryError('gdl_run_task_timeout() failed:', ret)

    ret = gdl_mem_cpy_from_dev_wrapper(convert_np_array_to_cptr(dst), cmd.gmd_demo_data.output, o_dev_buf_size)
    if ret:
        raise MemoryError('gdl_mem_cpy_from_dev_wrapper() failed to copy', o_dev_buf_size, 'bytes')

    gdl_mem_free_wrapper(dev_cmd_buf)
    gdl_mem_free_wrapper(o_dev_bufs)

    return ret

def run_lab_1_gmd_demo_cmd_and_check(ctx_id : i64) -> None:
    dst_size : i32 = i32(512)
    dst_arr : np.ndarray = create_empty_u16_array(dst_size)

    run_lab_1_gmd_demo_cmd(ctx_id, dst_arr)

    dst_arr.sort()

    i : i32
    for i in range(0, dst_size):
        if dst_arr[i] != i * 64:
            print('Mismatch for element', i, 'result =', dst_arr[int(i / 64)], 'expected value =', i)
            raise RuntimeError('Mismatch')

def run_lab_1_fizzbuzz_exercise_cmd(
        ctx_id : i64,
        dst : np.ndarray,
        src : np.ndarray) -> u32:
	raise NotImplementedError('Need to send a GD_LAB_1_CMD_FIZZBUZZ_EXERCISE command to the device')

def run_lab_1_fizzbuzz_exercise_cmd_and_check(ctx_id : i64) -> None:
    src_arr : np.ndarray = create_random_u16_array(_32K)
    dst_arr : np.ndarray = np.empty(_32K, dtype=idx_val)

    dst_len : u32 = run_lab_1_fizzbuzz_exercise_cmd(ctx_id, dst_arr, src_arr)

    dst_arr[:dst_len].sort(order='idx')

    dst_ctr : i32 = 0

    i : i32
    for i in range(i32(_32K)):
        if (u16(src_arr[i]) % u16(15)) == 0:
            if dst_ctr == dst_len:
                print('FIZZBUZZ missing: i =', i, ': val =', src_arr[i])
                raise RuntimeError('Missing')         
            iv : idx_val = dst_arr[dst_ctr]
            if (i32(iv['idx']) != i) or (u16(iv['val']) != GD_LAB_1_FIZZBUZZ):
                print('FIZZBUZZ mismatch: i =', i, ': val =', src_arr[i], ', dev result =', '0x' + u16(iv['val']))
                raise RuntimeError('Mismatch')          
            dst_ctr += 1
        elif (u16(src_arr[i]) % u16(3)) == 0:
            if dst_ctr == dst_len:
                print('FIZZ missing: i =', i, ': val =', src_arr[i])
                raise RuntimeError('Missing')          
            iv : idx_val = dst_arr[dst_ctr]
            if (i32(iv['idx']) != i) or (u16(iv['val']) != GD_LAB_1_FIZZ):
                print('FIZZ mismatch: i =', i, ': val =', src_arr[i], ', dev result =', '0x' + u16(iv['val']))
                raise RuntimeError('Mismatch')           
            dst_ctr += 1
        elif (u16(src_arr[i]) % u16(5)) == 0:
            if dst_ctr == dst_len:
                print('BUZZ missing: i =', i, ': val =', src_arr[i])
                raise RuntimeError('Missing')           
            iv : idx_val = dst_arr[dst_ctr]
            if (i32(iv['idx']) != i) or (u16(iv['val']) != GD_LAB_1_BUZZ):
                print('BUZZ mismatch: i =', i, ': val =', src_arr[i], ', dev result =', '0x' + u16(iv['val']))
                raise RuntimeError('Mismatch')          
            dst_ctr += 1

def main() -> None :
    gsi_libsys_init_wrapper()
    choice : i32 = parse_args()
    gdl_init_wrapper()
    ctx : i64 = find_and_allocate_context()
    
    # run lab
    if choice == i32(GD_LAB_1_CMD_DMA_DEMO):
        print('\ndma demo')
        run_lab_1_dma_demo_cmd_and_check(ctx)
    elif choice == i32(GD_LAB_1_CMD_LOOKUP_DEMO):
        print('\nlookup demo')
        run_lab_1_lookup_demo_cmd_and_check(ctx)
    elif choice == i32(GD_LAB_1_CMD_GMD_DEMO):
        print('\ngmd demo')
        run_lab_1_gmd_demo_cmd_and_check(ctx)
    elif choice == i32(GD_LAB_1_CMD_FIZZBUZZ_EXERCISE):
        print('\nfizzbuzz exercise')
        run_lab_1_fizzbuzz_exercise_cmd_and_check(ctx)
    print('Success')
    
    gdl_context_free_wrapper(ctx)
    gdl_exit_wrapper()
    gsi_libsys_exit_wrapper()

if __name__ == "__main__":
    main()
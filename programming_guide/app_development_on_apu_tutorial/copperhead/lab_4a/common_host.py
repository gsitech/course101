import numpy as np
from lpython import u16, u32, u64
from common import GD_LAB_4_MAX_NUM_FEATURES

def gd_lab_4_get_preprocessed_db_size(num_features : u32, num_cols_per_rec_pow_2: u32) -> u64:
    _32k : u64 = u64(2**15)
    size_16b : u64 = u64(2)
    return size_16b * u64(num_features >> num_cols_per_rec_pow_2) * _32k

def gd_lab_4_preprocess_db(src_16b : np.ndarray, num_cols_per_rec_pow_2 : u32) -> np.ndarray:
    assert src_16b.itemsize == 2
    assert src_16b.ndim == 2

    num_records, num_features = src_16b.shape
    num_features = u32(num_features)

    if num_cols_per_rec_pow_2 > u32(3):
        raise ValueError(f'num_cols_per_rec_pow_2 {num_cols_per_rec_pow_2} must be <= 3')
    
    if num_records & 3:
        raise ValueError(f'num_records must be a multiple of 4')
    
    num_feature_grps = num_features >> num_cols_per_rec_pow_2
    if num_features != u32(num_feature_grps) << num_cols_per_rec_pow_2:
        raise ValueError(f'num_features {num_features} must be a multiple of (2 ^ num_cols_per_rec_pow_2), (num_cols_per_rec_pow_2 = {num_cols_per_rec_pow_2})')
    
    if (num_features >> num_cols_per_rec_pow_2) > GD_LAB_4_MAX_NUM_FEATURES:
        raise ValueError(f'The combination of num_features = {num_features} and num_cols_per_rec_pow_2 = {num_cols_per_rec_pow_2} exceeds available L1 memory')

    num_records = src_16b.shape[0]
    num_elms_in_vmr = 2**15

    src_view = np.lib.stride_tricks.as_strided(
        src_16b.ravel(),
        shape=(num_feature_grps, num_records // 4, u32(1) << num_cols_per_rec_pow_2, 4),
        strides=(u32(src_16b.itemsize) << num_cols_per_rec_pow_2,
                 4 * int(num_features) * src_16b.itemsize,
                 src_16b.itemsize, int(num_features) * src_16b.itemsize))
    
    dst_view_shape=(src_view.shape[0], (num_elms_in_vmr >> int(num_cols_per_rec_pow_2)) // 4, src_view.shape[2], src_view.shape[3])

    num_16b_elms = int(gd_lab_4_get_preprocessed_db_size(num_features, num_cols_per_rec_pow_2)) // src_16b.itemsize
    ret = np.empty(num_16b_elms, dtype=src_16b.dtype)
    dst_view = ret.reshape(dst_view_shape)
    dst_view[:, :src_view.shape[1], :, :] = src_view

    return ret 

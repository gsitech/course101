import sys
from dataclasses import dataclass
from lpython import i32, u32, i64, u64, CPtr, sizeof, Pointer, c_p_pointer
from common import gd_lab_4_cmd, GD_LAB_4_CMD_LOAD_DB, GD_LAB_4_CMD_SEARCH
from common_host import gd_lab_4_get_preprocessed_db_size, gd_lab_4_preprocess_db
from ref_gvml import refgvml_add_f16, refgvml_sub_f16, refgvml_mul_f16
import numpy as np
import ctypes

from gsi.sys_apu import gsi_libsys_init_wrapper, gsi_libsys_exit_wrapper
from gsi.gdl import (gdl_init_wrapper, gdl_exit_wrapper, find_and_allocate_context, gdl_context_free_wrapper,
                     gdl_mem_alloc_aligned_wrapper, gdl_mem_handle_is_null_wrapper, gdl_mem_handle_to_host_ptr_wrapper,
                     gdl_run_task_timeout_wrapper, gdl_mem_free_wrapper, gdl_mem_cpy_to_dev_wrapper, gdl_mem_cpy_from_dev_wrapper,
                     gdl_add_to_mem_handle_wrapper)

# constants - start
_32K : i64 = i64(32768)
SIZE_F16 : i64 = i64(2)
GDL_MEM_HANDLE_NULL : u64 = u64(0)
GDL_CONST_MAPPED_POOL : i32 = i32(0)
GDL_ALIGN_32 : i32 = i32(4)
# constants - end

# Helper functions - start
def create_random_f16_array(num_rows : u32, num_cols : u32) -> np.ndarray:
    return np.random.random((num_rows, num_cols)).astype(np.float16)

idx_val = np.dtype({'names': ['idx','val'], 'formats': [np.uint16, np.float16]})

def create_empty_idx_val_array(num_rows : u32, num_cols : u32) -> np.ndarray:
    return np.empty((num_rows, num_cols), dtype=idx_val)

def convert_cptr_to_np_array(cptr: CPtr, size=_32K) -> np.ndarray:
    return np.ctypeslib.as_array(ctypes.cast(
        cptr, ctypes.POINTER(ctypes.c_uint16)), shape=(size,))

def convert_np_array_to_cptr(np_array: np.ndarray) -> CPtr:
    return np_array.ctypes.data_as(ctypes.POINTER(ctypes.c_uint16))
# Helper functions - end

def calc_l1_distance(records : np.ndarray, query : np.ndarray, num_cols_per_rec_pow_2 : u32) -> np.ndarray:

    num_records, num_features = records.shape
    num_feature_grps = u32(num_features) >> num_cols_per_rec_pow_2
    num_cols_per_rec = u32(1) << num_cols_per_rec_pow_2
    intermediate_sums = np.zeros((num_records, num_feature_grps), dtype=np.float16)

    diff = refgvml_sub_f16(records, query)
    abs_diff = np.abs(diff).reshape(num_records, num_feature_grps, num_cols_per_rec)  

    for fg in range(num_feature_grps):
        for i in range(num_cols_per_rec):
            intermediate_sums[:, i] = refgvml_add_f16(intermediate_sums[:, i], abs_diff[:, fg, i])

    # pyrmid sum
    offset = 1
    for l2 in range(num_cols_per_rec_pow_2, 0, -1):
        num_sums = 1 << (l2 - 1)
        for sum in range(num_sums):
            intermediate_sums[:, 2 * sum * offset] = refgvml_add_f16(
                intermediate_sums[:, 2 * sum * offset],
                intermediate_sums[:, 2 * sum * offset + offset])
        offset <<= 1
        
    return intermediate_sums[:, 0]

def calc_ref_iv(records : np.ndarray, queries : np.ndarray, num_cols_per_rec_pow_2 : u32) -> np.ndarray:
    ret = create_empty_idx_val_array(queries.shape[0], records.shape[0])
    for i, q in enumerate(queries):
        ret[i]['val'] = calc_l1_distance(records, q, num_cols_per_rec_pow_2)
        ret[i]['idx'] = np.arange(records.shape[0], dtype=np.uint16)
    return ret

def calc_sorted_min_k(iv : np.ndarray, k : u32) -> np.ndarray:
    partition = np.argpartition(iv, int(k) - 1, order = ['val', 'idx'])
    return np.sort(iv[partition][:int(k)], order = ['val', 'idx'])

def check_results(iv_res : np.ndarray, iv_ref : np.ndarray, k : u32) -> None:
    assert iv_res.shape[0] == iv_ref.shape[0]
    num_records = iv_ref.shape[1]

    # sanity check
    for q, q_res in enumerate(iv_res):
        for e, x in enumerate(q_res):
            if x['idx'] >= num_records:
                raise RuntimeError(f'q = {q}, e = {e}: result has index >= num_records ({x}), num_records = {num_records}')
            ref_val = iv_ref[q, x['idx']]['val']
            if ref_val != x['val']:
                raise RuntimeError(f'q = {q}, e = {e}: mismatch in distances, idx = {x["idx"]}, res = {x["val"]}, ref_val = {ref_val}')
            
    for q, (q_res, q_ref) in enumerate(zip(iv_res, iv_ref)):
        q_res = calc_sorted_min_k(q_res, k)
        q_ref = calc_sorted_min_k(q_ref, k)

        for e, (res, ref) in enumerate(zip(q_res, q_ref)):
            if res != ref:
                raise RuntimeError(f'q = {q}, e = {e}: mismatch in results, res = {res}, ref = {ref}')

def load_db(ctx_id : u64, records : np.ndarray, num_cols_per_rec_pow_2 : u32) -> None:
    dev_cmd_buf : u64 = GDL_MEM_HANDLE_NULL
    dev_records_buf : u64 = GDL_MEM_HANDLE_NULL

    pre_processed_records_size : u64 = gd_lab_4_get_preprocessed_db_size(u32(records.shape[1]), num_cols_per_rec_pow_2)
    pre_processed_records : np.ndarray = gd_lab_4_preprocess_db(records, num_cols_per_rec_pow_2)

    assert pre_processed_records_size == pre_processed_records.nbytes

    try:
        dev_records_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, pre_processed_records_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(dev_records_buf):
            raise MemoryError(f'gdl_mem_alloc_aligned_wrapper failed to allocate {pre_processed_records_size} bytes')
        
        sts : i32 = gdl_mem_cpy_to_dev_wrapper(
            dev_records_buf,
            convert_np_array_to_cptr(pre_processed_records),
            pre_processed_records_size)
        if sts:
            raise RuntimeError(f'gdl_mem_cpy_to_dev_wrapper failed with {sts}')

        cmd_buf_size : u64 = sizeof(gd_lab_4_cmd)
        dev_cmd_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(dev_cmd_buf):
            raise MemoryError(f'gdl_mem_alloc_aligned_wrapper() failed to allocate {cmd_buf_size} bytes')
        
        cmd_ptr: CPtr = gdl_mem_handle_to_host_ptr_wrapper(dev_cmd_buf)
        cmd: Pointer[gd_lab_4_cmd] = c_p_pointer(cmd_ptr, gd_lab_4_cmd)    
        cmd.cmd = GD_LAB_4_CMD_LOAD_DB
        cmd.load_db_data.db = dev_records_buf
        cmd.load_db_data.num_records = records.shape[0]
        cmd.load_db_data.num_features = records.shape[1]
        cmd.load_db_data.num_cols_per_rec_pow_2 = num_cols_per_rec_pow_2
        
        sts = gdl_run_task_timeout_wrapper(ctx_id, 1234, dev_cmd_buf, GDL_MEM_HANDLE_NULL)
        if sts:
            raise RuntimeError(f'gdl_run_task_timeout() failed: {sts}')
    
    finally:
        gdl_mem_free_wrapper(dev_cmd_buf)
        gdl_mem_free_wrapper(dev_records_buf)

def do_search(ctx_id : u64, queries : np.ndarray, k : u32) -> np.ndarray:
    ret : np.ndarray = create_empty_idx_val_array(queries.shape[0], k)

    dev_cmd_buf : u64 = GDL_MEM_HANDLE_NULL
    io_dev_bufs : u64 = GDL_MEM_HANDLE_NULL

    try:
        io_dev_buf_size : u64 = queries.nbytes + ret.nbytes
        io_dev_bufs = gdl_mem_alloc_aligned_wrapper(ctx_id, io_dev_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(io_dev_bufs):
            raise MemoryError(f'gdl_mem_alloc_aligned_wrapper failed to allocate {io_dev_buf_size} bytes')
        
        sts : i32 = gdl_mem_cpy_to_dev_wrapper(
            io_dev_bufs,
            convert_np_array_to_cptr(queries),
            queries.nbytes)
        if sts:
            raise RuntimeError(f'gdl_mem_cpy_to_dev_wrapper failed with {sts}')

        cmd_buf_size : u64 = sizeof(gd_lab_4_cmd)
        dev_cmd_buf = gdl_mem_alloc_aligned_wrapper(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32)
        if gdl_mem_handle_is_null_wrapper(dev_cmd_buf):
            raise MemoryError(f'gdl_mem_alloc_aligned_wrapper() failed to allocate {cmd_buf_size} bytes')
        
        cmd_ptr: CPtr = gdl_mem_handle_to_host_ptr_wrapper(dev_cmd_buf)
        cmd: Pointer[gd_lab_4_cmd] = c_p_pointer(cmd_ptr, gd_lab_4_cmd)    
        cmd.cmd = GD_LAB_4_CMD_SEARCH
        cmd.search_data.queries = io_dev_bufs
        cmd.search_data.num_queries = queries.shape[0]
        cmd.search_data.k = k
        cmd.search_data.output = gdl_add_to_mem_handle_wrapper(cmd.search_data.queries, queries.nbytes)
        
        sts = gdl_run_task_timeout_wrapper(ctx_id, 1234, dev_cmd_buf, GDL_MEM_HANDLE_NULL)
        if sts:
            raise RuntimeError(f'gdl_run_task_timeout() failed: {sts}')
        
        sts = gdl_mem_cpy_from_dev_wrapper(
            convert_np_array_to_cptr(ret),
            cmd.search_data.output,
            ret.nbytes)
        if sts:
            raise RuntimeError(f'gdl_mem_cpy_from_dev_wrapper() failed to copy {ret.nbytes} bytes')
    
    finally:
        gdl_mem_free_wrapper(dev_cmd_buf)
        gdl_mem_free_wrapper(io_dev_bufs)

    return ret

@dataclass
class lab_4_args:
    num_records : u32 = u32(0)
    num_features : u32 = u32(0)
    num_cols_per_rec_pow_2 : u32 = u32(0)
    num_queries : u32 = u32(0)
    k : u32 = u32(0)
    num_searches : u32 = u32(0)

def parse_args() -> lab_4_args:
    if len(sys.argv) != 7:
        raise ValueError(f'usage: {sys.argv[0]} num_records num_features num_cols_per_rec_pow_2 num_queries k num_searches')
    
    args : lab_4_args = lab_4_args()
    args.num_records = u32(int(sys.argv[1]))
    args.num_features = u32(int(sys.argv[2]))
    args.num_cols_per_rec_pow_2 = u32(int(sys.argv[3]))
    args.num_queries = u32(int(sys.argv[4]))
    args.k = u32(int(sys.argv[5]))
    args.num_searches = u32(int(sys.argv[6]))

    print('****************** ARGS ******************')
    print(f'num_records = {args.num_records}')
    print(f'num_features = {args.num_features}')
    print(f'num_cols_per_rec_pow_2 = {args.num_cols_per_rec_pow_2}')
    print(f'num_queries = {args.num_queries}')
    print(f'k = {args.k}')
    print(f'num_searches = {args.num_searches}')
    print('******************************************')
    return args

def main() -> None :
    np.random.seed(1)
    gsi_libsys_init_wrapper()
    args : lab_4_args = parse_args()
    gdl_init_wrapper()
    ctx : i64 = find_and_allocate_context()

    records : np.ndarray = create_random_f16_array(args.num_records, args.num_features)

    print('Loading database ...')
    load_db(ctx, records, args.num_cols_per_rec_pow_2)

    for s in range(args.num_searches):
        queries : np.ndarray = create_random_f16_array(args.num_queries, args.num_features)
        print(f'Performing search {s + 1} of {args.num_searches} ...')
        iv_res : np.ndarray = do_search(ctx, queries, args.k)
        print(f'Finished searching')
        
        print(f'Checking results ...')
        iv_ref : np.ndarray = calc_ref_iv(records, queries, args.num_cols_per_rec_pow_2)
        check_results(iv_res, iv_ref, args.k)
        print(f'Finished checking results')
    
    print('Success')
    gdl_context_free_wrapper(ctx)
    gdl_exit_wrapper()
    gsi_libsys_exit_wrapper()

if __name__ == "__main__":
    main()
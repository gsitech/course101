from lpython import packed, dataclass, ccallable, u16, u32, u64

GD_LAB_4_MAX_NUM_RECORDS_IN_DB : u32 = u32(32 * 1024)
GD_LAB_4_MAX_NUM_FEATURES : u32 = u32(48)

GD_LAB_4_CMD_LOAD_DB : u32 = u32(0)
GD_LAB_4_CMD_SEARCH : u32 = u32(1)

@ccallable
@packed
@dataclass
class gd_lab_4_idx_val:
    idx: u16
    val: u16

@ccallable
@packed
@dataclass
class gd_lab_4_load_db:
    db: u64 = u64(0)
    num_features: u32 = u32(0)
    num_records: u32 = u32(0)
    num_cols_per_rec_pow_2: u32 = u32(0)

@ccallable
@packed
@dataclass
class gd_lab_4_search:
    output: u64 = u64(0)
    queries: u64 = u64(0)
    num_queries: u32 = u32(0)
    k: u32 = u32(0)

@ccallable
@packed
@dataclass
class gd_lab_4_cmd:
    cmd : u32 = u32(0)
    load_db_data : gd_lab_4_load_db = gd_lab_4_load_db()
    search_data : gd_lab_4_search = gd_lab_4_search()
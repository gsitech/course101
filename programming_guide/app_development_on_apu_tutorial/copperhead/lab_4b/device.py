from lpython import (
    u16, i32, u32, u16,
    ccallback, CPtr, c_p_pointer, Pointer, InOut,
    dataclass, sizeof,
)

from common import GD_LAB_4_MAX_NUM_FEATURES, GD_LAB_4_MAX_NUM_RECORDS_IN_DB, GD_LAB_4_CMD_LOAD_DB, GD_LAB_4_CMD_SEARCH, gd_lab_4_load_db, gd_lab_4_search, gd_lab_4_cmd, gd_lab_4_idx_val
from gvml_defs import (GVML_P2_4, GVML_MRK0, GVML_MRK1, GVML_VR16_0, GVML_VR16_1, GVML_VR16_2, GVML_VR16_3, GVML_VM_0)
from gchl.lib import gchl_init_once, gchl_reset_16, gchl_reset_16_msk, gchl_load_16, gchl_add_f16, gchl_sub_f16, gchl_create_subgrp_index_u16, gchl_monotonic_transformation_nan2max_u16_f16

from gsi.arc_utils import increment_ptr, direct_dma_l4_to_l1_32k
from gsi.gal import gal_mem_handle_to_apu_ptr
from gsi.gvml import (gvml_mark_kmin_idxval_u16_mrk_g32k, gvml_get_marked_data_xv, gvml_eq_imm_16,
                      gvml_create_index_u16_grp_sgrp, gvml_cpy_imm_subgrp_16_grp, gvml_add_subgrps_f16_grp)

@dataclass
class db_data:
    num_records : u32 = u32(0)
    num_features : u32 = u32(0)
    num_cols_per_rec_pow_2 : u32 = u32(0)
    mrk_valid_records : u16 = GVML_MRK0
    vr_idx : u16 = GVML_VR16_0

g_db_data : db_data = db_data()

# workaround to write to a global user type
def init_db_data(d : InOut[db_data], num_records : u32, num_features : u32, num_cols_per_rec_pow_2 : u32) -> None:
    d.num_records = num_records
    d.num_features = num_features
    d.num_cols_per_rec_pow_2 = num_cols_per_rec_pow_2

def create_mrk(mrk_dst : u16, num_cols_per_rec_pow_2 : u32, vr_tmp : u16) -> None:
    gchl_create_subgrp_index_u16(vr_tmp, u16(i32(num_cols_per_rec_pow_2) + i32(GVML_P2_4)), GVML_P2_4)
    gvml_eq_imm_16(mrk_dst, vr_tmp, u16(0))

def load_db(load_db_data : gd_lab_4_load_db) -> i32:
    global g_db_data
    
    if u32((load_db_data.num_features >> load_db_data.num_cols_per_rec_pow_2)) > GD_LAB_4_MAX_NUM_FEATURES:
        print('The combination of num_features =', load_db_data.num_features, 'and num_cols_per_rec_pow_2 =', load_db_data.num_cols_per_rec_pow_2, 'exceeds available L1 memory')
        return -1
    
    if u32((load_db_data.num_records << load_db_data.num_cols_per_rec_pow_2)) > GD_LAB_4_MAX_NUM_RECORDS_IN_DB:
        print('The combination of num_features =', load_db_data.num_features, 'and num_cols_per_rec_pow_2 =', load_db_data.num_cols_per_rec_pow_2, 'exceeds available L1 memory')
        return -1
    
    init_db_data(g_db_data, load_db_data.num_records, load_db_data.num_features, load_db_data.num_cols_per_rec_pow_2)

    gvml_create_index_u16_grp_sgrp(
        g_db_data.vr_idx,
        u16(i32(g_db_data.num_cols_per_rec_pow_2) + i32(GVML_P2_4)),
        GVML_P2_4,
        u32(i32(1) << i32(GVML_P2_4)),
        GVML_VR16_1)
    create_mrk(g_db_data.mrk_valid_records, g_db_data.num_cols_per_rec_pow_2, GVML_VR16_1)
    
    ptr_in : CPtr = gal_mem_handle_to_apu_ptr(load_db_data.db)
    num_feature_grps : u32 = g_db_data.num_features >> g_db_data.num_cols_per_rec_pow_2
    fg : i32
    for fg in range(i32(num_feature_grps)):
        direct_dma_l4_to_l1_32k(u16(fg), increment_ptr(ptr_in, fg * i32(GD_LAB_4_MAX_NUM_RECORDS_IN_DB) * i32(sizeof(u16))))       

    return 0

def abs_f16(vr : u16) -> None:
    gchl_reset_16_msk(vr, u16(0x8000))

def do_search(search_data : gd_lab_4_search) -> i32:
    global g_db_data

    if u32(search_data.k) > g_db_data.num_records:
        print('k,', search_data.k, ', must not exceed number of records', g_db_data.num_records)
        return -1
    
    vr_distances : u16 = GVML_VR16_1
    vr_records : u16 = GVML_VR16_2
    vr_query : u16 = GVML_VR16_3
    mrk_kmin : u16 = GVML_MRK1

    p_q : CPtr = gal_mem_handle_to_apu_ptr(search_data.queries)
    p_iv : CPtr = gal_mem_handle_to_apu_ptr(search_data.output)
    num_feature_grps : u32 = g_db_data.num_features >> g_db_data.num_cols_per_rec_pow_2

    q : i32
    for q in range(i32(search_data.num_queries)):
        gchl_reset_16(vr_distances)
        fg : i32
        for fg in range(i32(num_feature_grps)):
            gvml_cpy_imm_subgrp_16_grp(
                vr_query,
                u16(i32(g_db_data.num_cols_per_rec_pow_2) + i32(GVML_P2_4)),
                GVML_P2_4,
                increment_ptr(p_q, i32(sizeof(u16)) * (fg << i32(g_db_data.num_cols_per_rec_pow_2))),
                u32(1) << g_db_data.num_cols_per_rec_pow_2)
            gchl_load_16(vr_records, u16(fg))
            gchl_sub_f16(vr_records, vr_records, vr_query)
            abs_f16(vr_records)
            gchl_add_f16(vr_distances, vr_distances, vr_records)
        
        gvml_add_subgrps_f16_grp(
            vr_distances,
            vr_distances,
            u16(i32(g_db_data.num_cols_per_rec_pow_2) + i32(GVML_P2_4)),
            GVML_P2_4,
            u32(0),
            GVML_VM_0,
            vr_query)         

        gchl_monotonic_transformation_nan2max_u16_f16(vr_records, vr_distances)

        gvml_mark_kmin_idxval_u16_mrk_g32k(
            mrk_kmin,
            vr_records,
            g_db_data.vr_idx,
            search_data.k,
            g_db_data.mrk_valid_records,
            vr_query)
        
        gvml_get_marked_data_xv(
            p_iv,
            g_db_data.vr_idx,
            vr_distances,
            mrk_kmin,
            search_data.k)

        p_q = increment_ptr(p_q, i32(g_db_data.num_features) * i32(sizeof(u16)))
        p_iv = increment_ptr(p_iv, i32(search_data.k) * i32(sizeof(gd_lab_4_idx_val)))        

    return 0

@ccallback
def task(in_: CPtr, out: CPtr) -> i32:
    cmd: Pointer[gd_lab_4_cmd] = c_p_pointer(in_, gd_lab_4_cmd)

    if cmd.cmd == GD_LAB_4_CMD_LOAD_DB:
        gchl_init_once()
        return load_db(cmd.load_db_data)
    elif cmd.cmd == GD_LAB_4_CMD_SEARCH:
        return do_search(cmd.search_data)
    else:
        return -1

from lpython import (
    u16, i32, u32, u16, i64,
    ccallback, CPtr, c_p_pointer, Pointer, InOut, Const,
    dataclass, sizeof, empty_c_void_p, ccallable, pointer
)

from common import GD_LAB_6_MAX_NUM_FEATURES, GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK, GD_LAB_6_MAX_K, GD_LAB_6_CMD_LOAD_DB, GD_LAB_6_CMD_SEARCH, gd_lab_6_load_db, gd_lab_6_search, gd_lab_6_cmd, gd_lab_6_idx_val
from gvml_defs import GVML_MRK0, GVML_MRK1, GVML_MRK2, GVML_MRK3, GVML_VR16_0, GVML_VR16_1, GVML_VR16_2, GVML_VR16_3, GVML_VR16_4, GVML_VR16_5, GVML_VR16_6
from gchl.lib import gchl_init_once, gchl_cpy_16, gchl_reset_16, gchl_reset_16_msk, gchl_load_16, gchl_add_f16, gchl_sub_f16, gchl_cpy_imm_16

from gsi.arc_utils import (increment_ptr, dereference_u16_ptr, direct_dma_l4_to_l1_32k,
                           GSI_IS_ERR_PTR_OR_NULL, direct_dma_l4_to_l2_32k_start,
                           dma_l2_sync, direct_dma_l2_to_l1_32k)
from gsi.gal import (gal_mem_handle_to_apu_ptr, gal_fast_malloc_cache_aligned,
                     gal_fast_l2dma_async_memcpy_init, gal_fast_cache_dcache_flush_mlines_wrapper,
                     gal_fast_l2dma_mem_to_mem_512_wrapper, gal_fast_l2dma_async_memcpy_end, gal_fast_free_cache_aligned,
                     gal_pm_start, gal_get_pm_cycle_count, gal_pm_stop)
from gsi.gvml import (gvml_create_index_16, gvml_mark_kmin_idxval_u16_mrk_g32k, gvml_set_m,
                      gvml_cpy_m, gvml_cpy_from_mrk_16_msk, gvml_merge_v15mrk1_g32k,
                      gvml_cpy_to_mrk_16_msk, gvml_xor_m, gvml_and_m, gvml_cpy_16_mrk,
                      gvml_cpy_imm_16_msk_mrk, gvml_cpy_imm_16_mrk, gvml_get_marked_data_sxvv_interval_wrapper)

@ccallable
@dataclass
class prof_data:
    cycles_start : i64 = i64(0)
    cycles_acc : i64 = i64(0)
    hits : i64 = i64(0)

def PROF_START(prof : InOut[prof_data]) -> None:
    prof.hits += i64(1)
    prof.cycles_start = gal_get_pm_cycle_count(True)

def PROF_STOP(prof : InOut[prof_data]) -> None:
    prof.cycles_acc += (gal_get_pm_cycle_count(True) - prof.cycles_start)

def PROF_PRINT(prof : InOut[prof_data], name : Const[str]) -> None:
    print('Event:', name, 'hits:', prof.hits, 'cycles:', prof.cycles_acc)

@dataclass
class db_data:
    num_32k_chunks_records : u32 = u32(0)
    num_features : u32 = u32(0)
    p_data : CPtr = empty_c_void_p()

g_db_data : db_data = db_data()
prof_search : prof_data = prof_data()
prof_l4_to_l1 : prof_data = prof_data()
prof_calc : prof_data = prof_data()
prof_extract : prof_data = prof_data()     

GAL_L2DMA_APC_ID_0 : u32 = u32(0)
NUM_64_BIT_OUTPUT_ELEMENTS_IN_512B : u32 = u32(512) // u32(sizeof(gd_lab_6_idx_val))

@ccallable
@dataclass
class _64_bit_output_out_data:
    l3: CPtr = empty_c_void_p()
    l4: CPtr = empty_c_void_p()

def div_round_up(a : u32, b : u32) -> u32:
    return (a + b - u32(1)) // b

# workaround to write to a global user type
def init_db_data(d : InOut[db_data], num_features : u32, num_32k_chunks_records : u32, p_data : CPtr) -> None:
    d.num_features = num_features
    d.num_32k_chunks_records = num_32k_chunks_records
    d.p_data = p_data

def load_db(load_db_data : gd_lab_6_load_db) -> i32:
    global g_db_data

    if (load_db_data.num_32k_chunks_records < u32(2)):
        print('number of chunks,', load_db_data.num_32k_chunks_records, ', must be greater than 1')
        return -1
    
    if load_db_data.num_features > GD_LAB_6_MAX_NUM_FEATURES:
        print('number of features,', load_db_data.num_features, ', must not exceed', GD_LAB_6_MAX_NUM_FEATURES)
        return -1
    
    init_db_data(g_db_data, load_db_data.num_features, load_db_data.num_32k_chunks_records, gal_mem_handle_to_apu_ptr(load_db_data.db))

    # Load the first chunk
    f : i32
    for f in range(i32(g_db_data.num_features)):
        direct_dma_l4_to_l1_32k(u16(f), increment_ptr(g_db_data.p_data, f * i32(GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK) * i32(sizeof(u16))))

    gal_pm_start()
    
    return 0

def _64_bit_output_copy0_512_bytes_from_l3_to_l4(params: CPtr) -> i32:
    out_data_ptr: Pointer[_64_bit_output_out_data] = c_p_pointer(params, _64_bit_output_out_data)

    gal_fast_cache_dcache_flush_mlines_wrapper(out_data_ptr.l3, u32(512))
    gal_fast_l2dma_mem_to_mem_512_wrapper(out_data_ptr.l4, out_data_ptr.l3, GAL_L2DMA_APC_ID_0)

    out_data_ptr.l3 = increment_ptr(out_data_ptr.l3, i32(512))
    out_data_ptr.l4 = increment_ptr(out_data_ptr.l4, i32(512))
    return 0

def abs_f16(vr : u16) -> None:
    gchl_reset_16_msk(vr, u16(0x8000))

def do_search(search_data : gd_lab_6_search) -> i32:
    global g_db_data
    global prof_search
    global prof_l4_to_l1
    global prof_calc
    global prof_extract

    PROF_START(prof_search)

    if u32(search_data.k) > GD_LAB_6_MAX_K:
        print('k,', search_data.k, ', must not exceed', GD_LAB_6_MAX_K)
        return -1
    
    num_512b_txns : u32 = div_round_up(u32(sizeof(gd_lab_6_idx_val)) * u32(search_data.k), u32(512))
    l3_buff_size : u32 = u32(num_512b_txns * u32(512))
    l3_buff : CPtr = gal_fast_malloc_cache_aligned(l3_buff_size, False)
    if GSI_IS_ERR_PTR_OR_NULL(l3_buff):
        print('gal_fast_malloc_cache_aligned() failed to allocate', l3_buff_size, 'bytes')
        return -2

    vr_merged_idx_lsb : u16 = GVML_VR16_0
    vr_merged_idx_msb : u16 = GVML_VR16_1
    vr_merged : u16 = GVML_VR16_2
    vr_idx : u16 = GVML_VR16_3
    vr_distances : u16 = GVML_VR16_4
    vr_records : u16 = GVML_VR16_5
    vr_query : u16 = GVML_VR16_6
    mrk_merged : u16 = GVML_MRK0
    mrk_in : u16 = GVML_MRK1
    mrk_kmin : u16 = GVML_MRK2
    mrk_added_entries : u16 = GVML_MRK3

    gvml_create_index_16(vr_merged_idx_lsb)
    gchl_reset_16(vr_merged_idx_msb)
    gvml_create_index_16(vr_idx)
    gvml_set_m(mrk_in)

    num_bytes_in_vr : i32 = i32(GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK) * i32(sizeof(u16))
    num_bytes_in_chunk : i32 = num_bytes_in_vr * i32(g_db_data.num_features)

    p_q : CPtr = gal_mem_handle_to_apu_ptr(search_data.query)
    p_iv : CPtr = gal_mem_handle_to_apu_ptr(search_data.output)
    p_next_chunk : CPtr = increment_ptr(g_db_data.p_data, num_bytes_in_chunk)

    c : i32
    for c in range(i32(g_db_data.num_32k_chunks_records)):
        # Load next chunk (or the 1st chunk if we're done)
        if c == (i32(g_db_data.num_32k_chunks_records) - i32(1)):
            p_next_chunk = g_db_data.p_data
        direct_dma_l4_to_l2_32k_start(p_next_chunk)

        PROF_START(prof_calc)
        gchl_reset_16(vr_distances)
        f : i32
        for f in range(i32(g_db_data.num_features)):
            f_val : u16 = dereference_u16_ptr(increment_ptr(p_q, f * i32(sizeof(u16))))
            gchl_cpy_imm_16(vr_query, f_val)
            gchl_load_16(vr_records, u16(f))
            gchl_sub_f16(vr_records, vr_records, vr_query)
            abs_f16(vr_records)
            gchl_add_f16(vr_distances, vr_distances, vr_records)

        gvml_mark_kmin_idxval_u16_mrk_g32k(mrk_kmin, vr_distances, vr_idx, search_data.k, mrk_in, vr_query)

        if c == 0:
            gvml_cpy_m(mrk_merged, mrk_kmin)
            gvml_cpy_from_mrk_16_msk(vr_distances, mrk_merged, u16(0x8000))
            gchl_cpy_16(vr_merged, vr_distances)
        else:
            # merge results
            gvml_cpy_from_mrk_16_msk(vr_distances, mrk_kmin, u16(0x8000))
            gvml_cpy_from_mrk_16_msk(vr_merged, mrk_merged, u16(0x8000))
            gvml_merge_v15mrk1_g32k(vr_merged, vr_records, vr_distances)

            # determine the added entries
            gvml_cpy_m(mrk_added_entries, mrk_merged)
            gvml_cpy_to_mrk_16_msk(mrk_merged, vr_merged, u16(0x8000))
            gvml_xor_m(mrk_added_entries, mrk_added_entries, mrk_merged)

            gvml_mark_kmin_idxval_u16_mrk_g32k(mrk_merged, vr_merged, vr_idx, search_data.k, mrk_merged, vr_query)

            # determine the added entries that survived the cut
            gvml_and_m(mrk_added_entries, mrk_added_entries, mrk_merged)

            # update the indices of the added entries
            gvml_cpy_16_mrk(vr_merged_idx_lsb, vr_records, mrk_added_entries)
            if c & 1:
                gvml_cpy_imm_16_msk_mrk(vr_merged_idx_lsb, u16(0x8000), u16(0x8000), mrk_added_entries)
            gvml_cpy_imm_16_mrk(vr_merged_idx_msb, u16(c >> 1), mrk_added_entries)
        PROF_STOP(prof_calc)

        PROF_START(prof_l4_to_l1)
        dma_l2_sync()
        direct_dma_l2_to_l1_32k(u16(0))
        for f in range(i32(1), i32(g_db_data.num_features)):
            direct_dma_l4_to_l1_32k(u16(f), increment_ptr(p_next_chunk, f * num_bytes_in_vr))
        PROF_STOP(prof_l4_to_l1)

        p_next_chunk = increment_ptr(p_next_chunk, num_bytes_in_chunk) 

    gchl_reset_16_msk(vr_merged, u16(0x8000))

    PROF_START(prof_extract)
    out_data : _64_bit_output_out_data = _64_bit_output_out_data()
    out_data.l3 = l3_buff
    out_data.l4 = p_iv

    gal_fast_l2dma_async_memcpy_init(GAL_L2DMA_APC_ID_0)
    arg : CPtr = c_p_pointer(pointer(out_data), CPtr)
    gvml_get_marked_data_sxvv_interval_wrapper(
        l3_buff,
        vr_merged_idx_lsb,
        vr_merged_idx_msb,
        vr_merged,
        u32(0),
        mrk_merged,
        search_data.k,
        NUM_64_BIT_OUTPUT_ELEMENTS_IN_512B,
        _64_bit_output_copy0_512_bytes_from_l3_to_l4,
        arg)
    if u32(search_data.k) & (NUM_64_BIT_OUTPUT_ELEMENTS_IN_512B - u32(1)):
        gal_fast_cache_dcache_flush_mlines_wrapper(out_data.l3, u32(512))
        gal_fast_l2dma_mem_to_mem_512_wrapper(out_data.l4, out_data.l3, GAL_L2DMA_APC_ID_0)

    gal_fast_l2dma_async_memcpy_end(GAL_L2DMA_APC_ID_0)
    gal_fast_free_cache_aligned(l3_buff)
    PROF_STOP(prof_extract)
    PROF_STOP(prof_search)

    PROF_PRINT(prof_search, 'search')
    PROF_PRINT(prof_l4_to_l1, 'l4_to_l1')
    PROF_PRINT(prof_calc, 'calc')
    PROF_PRINT(prof_extract, 'extract')

    return 0

@ccallback
def task(in_: CPtr, out: CPtr) -> i32:
    cmd: Pointer[gd_lab_6_cmd] = c_p_pointer(in_, gd_lab_6_cmd)

    if cmd.cmd == GD_LAB_6_CMD_LOAD_DB:
        gchl_init_once()
        return load_db(cmd.load_db_data )
    elif cmd.cmd == GD_LAB_6_CMD_SEARCH:
        return do_search(cmd.search_data)
    else:
        return -1

from numpy import array

from lpython import (
    i16, i32, u32, u16, i64, CPtr,
    ccallback, ccall, overload, Pointer, c_p_pointer
)

from gchl_defs import (SM_0XFFFF, SM_0X0001, SM_0X1111, SM_0X0101, SM_0X000F, SM_0X0F0F, SM_0X0707, SM_0X5555, SM_0X3333, SM_0X00FF, SM_0X001F, SM_0X003F,
                       SM_0XFFFF_VAL, SM_0X0001_VAL, SM_0X1111_VAL, SM_0X0101_VAL, SM_0X000F_VAL, SM_0X0F0F_VAL, SM_0X0707_VAL, SM_0X5555_VAL, SM_0X3333_VAL, SM_0X00FF_VAL, SM_0X001F_VAL, SM_0X003F_VAL,
                       RN_REG_T0, RN_REG_T1, RN_REG_T2, RN_REG_T3, RN_REG_T4, RN_REG_T5, RN_REG_T6, RN_REG_FLAGS,
                       VR16_T0, VR16_T1, VR16_T2, VR16_T3, VR16_T4, VR16_T5, VR16_T6, VR16_FLAGS)
from gchl_defs import (RN_REG_G0, RN_REG_G1, RN_REG_G2, RN_REG_G3, SM_REG0, SM_REG1, SM_REG2, SM_REG3, GCHL_VR16_IDX, GCHL_VM_0, L1_ADDR_REG0, L1_ADDR_REG1, VR16_G0, TMP0_MRK)

# from system_emulation.sys_apu_device import (apl_set_rn_reg, apl_set_sm_reg, apl_set_l1_reg, CH_RUN_FRAG_ASYNC)

# from declarations.gsi.gal import gal_init, gal_fast_malloc_cache_aligned, gal_fast_free_cache_aligned, gal_fast_cache_dcache_flush_mlines_wrapper
# from declarations.gsi.arc_utils import direct_dma_l4_to_l1_32k

@ccall
def direct_dma_l4_to_l1_32k(vmr_dst : u16, l4_src : CPtr) -> None:
    pass

@ccall
def gal_fast_cache_dcache_flush_mlines_wrapper(start_addr: CPtr, size: u32) -> i32:
    pass

@ccall
def gal_fast_malloc_cache_aligned(size: u32, invalidate: bool) -> CPtr:
    pass

@ccall
def gal_fast_free_cache_aligned(algn_p: CPtr) -> None:
    pass

@ccall
def gal_init() -> None:
    pass

@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str) -> None:
    pass


@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str,
                      a1: str, b1: u16,
                      ) -> None:
    pass


@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str,
                      a1: str, b1: u16,
                      a2: str, b2: u16,
                      ) -> None:
    pass


@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str,
                      a1: str, b1: u16,
                      a2: str, b2: u16,
                      a3: str, b3: u16,
                      ) -> None:
    pass


@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str,
                      a1: str, b1: u16,
                      a2: str, b2: u16,
                      a3: str, b3: u16,
                      a4: str, b4: u16,
                      ) -> None:
    pass

@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str,
                      a1: str, b1: u16,
                      a2: str, b2: u16,
                      a3: str, b3: u16,
                      a4: str, b4: u16,
                      a5: str, b5: u16,
                      ) -> None:
    pass

@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str,
                      a1: str, b1: u16,
                      a2: str, b2: u16,
                      a3: str, b3: u16,
                      a4: str, b4: u16,
                      a5: str, b5: u16,
                      a6: str, b6: u16,
                      ) -> None:
    pass

@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str,
                      a1: str, b1: u16,
                      a2: str, b2: u16,
                      a3: str, b3: u16,
                      a4: str, b4: u16,
                      a5: str, b5: u16,
                      a6: str, b6: u16,
                      a7: str, b7: u16
                      ) -> None:
    pass

@ccall
@overload
def CH_RUN_FRAG_ASYNC(fn: str,
                      a1: str, b1: u16,
                      a2: str, b2: u16,
                      a3: str, b3: u16,
                      a4: str, b4: u16,
                      a5: str, b5: u16,
                      a6: str, b6: u16,
                      a7: str, b7: u16,
                      a8: str, b8: u16
                      ) -> None:
    pass


@ccall
def apl_set_rn_reg(a: u16, b: u16) -> None:
    pass


@ccall
def apl_set_sm_reg(a: u16, b: u16) -> None:
    pass

@ccall
def apl_set_l1_reg(a: u16, b: u16) -> None:
    pass

@ccall
def apl_set_l1_reg_ext(a: u16, b: u16, c: u16, d: u16) -> None:
    pass

@ccall
def apl_set_l2_reg(a: u16, b: u16) -> None:
    pass

VR_SIZE:                u32 = u32(32768)
GSI_L1_VA_SET_ADDR_ROWS:            u16 = u16(16)
APL_VM_ROWS_PER_U16_T:                u16 = u16(4)

def chl_io_store_16_t0(vmdst: u16, vsrc: u16) -> None:
    # Internal level
    l1_parity_grp: u16
    l1_parity_row: u16
    l1_row: u16
    parity_mask: u16

    # Implement: l1_row =  my_gal_vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
    l1_parity_grp = u16(vmdst & u16(1))  # 1 or 0 (True or False)
    l1_row = (vmdst >> u16(1)) * GSI_L1_VA_SET_ADDR_ROWS
    l1_parity_row = u16(l1_row + (u16(2) * APL_VM_ROWS_PER_U16_T))
    l1_row += u16(APL_VM_ROWS_PER_U16_T * u16(l1_parity_grp))
    parity_mask = (u16)(u16(0x0001) << u16(u16(4) * u16(l1_parity_grp)))

    apl_set_rn_reg(RN_REG_G0, vsrc)
    apl_set_l1_reg(L1_ADDR_REG0, l1_row)
    apl_set_l1_reg(L1_ADDR_REG1, l1_parity_row)
    apl_set_sm_reg(SM_REG0, parity_mask)

    # Call to the fragment
    CH_RUN_FRAG_ASYNC("store_16_gchl", "dst", L1_ADDR_REG0, "parity_dst", L1_ADDR_REG1, "parity_mask", SM_REG0, "src",
                      RN_REG_G0)

def apl_init_vals() -> None:
    indx_vr: u16 = GCHL_VR16_IDX
    indx_vmr: u16 = GCHL_VM_0

    VEC_SIZE_IN_BYTES_u32:      u32 = u32(65536)

    p: CPtr = gal_fast_malloc_cache_aligned(VEC_SIZE_IN_BYTES_u32, False)

    VR_SIZE_i16 : i16 = i16(VR_SIZE)
    p_p1 : Pointer[i16[:]] = c_p_pointer(p, i16[:], array([VR_SIZE_i16]))

    ii: i32
    for ii in range(i32(VR_SIZE)):
        p_p1[i32(ii)] = i16(ii)
        
    gal_fast_cache_dcache_flush_mlines_wrapper(p, VEC_SIZE_IN_BYTES_u32);

    direct_dma_l4_to_l1_32k(indx_vmr, p)
    gchl_load_16(indx_vr, indx_vmr)

    gal_fast_free_cache_aligned(p)

    apl_set_rn_reg(RN_REG_G0, VR16_FLAGS)
    apl_set_sm_reg(SM_REG0, u16(0))
    CH_RUN_FRAG_ASYNC("my_cpy_imm_16", "tgt", RN_REG_G0, "val", SM_REG0)

    apl_set_rn_reg(RN_REG_G0, VR16_G0)
    apl_set_sm_reg(SM_REG0, u16(0))
    CH_RUN_FRAG_ASYNC("my_cpy_imm_16", "tgt", RN_REG_G0, "val", SM_REG0)

    vm_reg: i64
    for vm_reg in range(i64(48)):
        chl_io_store_16_t0(u16(vm_reg), u16(0))

def gchl_apl_init() -> None:
    apl_set_sm_reg(SM_0XFFFF, SM_0XFFFF_VAL)
    apl_set_sm_reg(SM_0X0001, SM_0X0001_VAL)
    apl_set_sm_reg(SM_0X1111, SM_0X1111_VAL)
    apl_set_sm_reg(SM_0X0101, SM_0X0101_VAL)
    apl_set_sm_reg(SM_0X000F, SM_0X000F_VAL)
    apl_set_sm_reg(SM_0X0F0F, SM_0X0F0F_VAL)
    apl_set_sm_reg(SM_0X0707, SM_0X0707_VAL)
    apl_set_sm_reg(SM_0X5555, SM_0X5555_VAL)
    apl_set_sm_reg(SM_0X3333, SM_0X3333_VAL)
    apl_set_sm_reg(SM_0X00FF, SM_0X00FF_VAL)
    apl_set_sm_reg(SM_0X001F, SM_0X001F_VAL)
    apl_set_sm_reg(SM_0X003F, SM_0X003F_VAL)

    apl_set_rn_reg(RN_REG_T0, VR16_T0)
    apl_set_rn_reg(RN_REG_T1, VR16_T1)
    apl_set_rn_reg(RN_REG_T2, VR16_T2)
    apl_set_rn_reg(RN_REG_T3, VR16_T3)
    apl_set_rn_reg(RN_REG_T4, VR16_T4)
    apl_set_rn_reg(RN_REG_T5, VR16_T5)
    apl_set_rn_reg(RN_REG_T6, VR16_T6)
    apl_set_rn_reg(RN_REG_FLAGS, VR16_FLAGS)

    apl_init_vals()

init_done: bool = False

def gchl_init() -> None:
    global init_done
    init_done = True
    gchl_apl_init()
    gal_init()

def chl_io_load_16_t0(vdst: u16, vmsrc: u16) -> None:
    # Internal level
    l1_parity_grp: u16
    l1_parity_row: u16
    l1_row: u16
    parity_mask: u16

    # Implement: l1_row =  my_gal_vm_reg_to_set_ext(vm_reg, &parity_set, &row_in_set, &l1_parity_grp, &l1_parity_row);
    l1_parity_grp = u16(vmsrc & u16(1))  # 1 or 0 (True or False)
    l1_row = (vmsrc >> u16(1)) * GSI_L1_VA_SET_ADDR_ROWS
    l1_parity_row = u16(l1_row + (u16(2) * APL_VM_ROWS_PER_U16_T))
    l1_row += u16(APL_VM_ROWS_PER_U16_T * u16(l1_parity_grp))
    parity_mask = (u16)(u16(0x0808) << l1_parity_grp)

    apl_set_rn_reg(RN_REG_G0, vdst)
    apl_set_l1_reg(L1_ADDR_REG0, l1_row)
    apl_set_l1_reg(L1_ADDR_REG1, l1_parity_row)
    apl_set_sm_reg(SM_REG0, parity_mask)

    # Call to the fragment
    CH_RUN_FRAG_ASYNC("load_16_gchl", "dst", RN_REG_G0, "src", L1_ADDR_REG0, "parity_src", L1_ADDR_REG1, "parity_mask2",
                      SM_REG0)

@ccallback
def gchl_load_16(vdst: u16, vmsrc: u16):
    # Internal level
    chl_io_load_16_t0(vdst, vmsrc)

# External level
@ccallback
def gchl_init_once() -> None:
    global init_done
    if init_done == False:
        gchl_init()

def chl_csrc_cpy_16_t0(vdst: u16, vsrc: u16) -> None:
    # Internal level
    apl_set_rn_reg(RN_REG_G0, vdst)
    apl_set_rn_reg(RN_REG_G1, vsrc)
    apl_set_sm_reg(SM_REG0, u16(0xFFFF))
    # Call to the fragment
    CH_RUN_FRAG_ASYNC("chf_cmn_cpy_16_msk", "vdst", RN_REG_G0, "vsrc", RN_REG_G1, "msk", SM_REG0)

@ccallback
def gchl_cpy_16(vdst: u16, vsrc: u16):
    # Internal level
    chl_csrc_cpy_16_t0(vdst, vsrc)

def chl_csrc_reset_16_t0(vreg: u16) -> None:
    # Internal level
    apl_set_rn_reg(RN_REG_G0, vreg)
    apl_set_sm_reg(SM_REG0, u16(0xFFFF))
    # Call to the fragment
    CH_RUN_FRAG_ASYNC("chf_csrc_reset_16_msk", "vreg", RN_REG_G0, "msk", SM_REG0)

@ccallback
def gchl_reset_16(vreg: u16):
    # Internal level
    chl_csrc_reset_16_t0(vreg)

# Actual Copperhead code:
def chl_csrc_reset_16_msk_t0(vreg: u16, msk: u16) -> None:
    # Internal level
    apl_set_rn_reg(RN_REG_G0, vreg)
    apl_set_sm_reg(SM_REG0, u16(msk))
    # Call to the fragment
    CH_RUN_FRAG_ASYNC("chf_csrc_reset_16_msk", "vreg", RN_REG_G0, "msk", SM_REG0)    

@ccallback
def gchl_reset_16_msk(vreg: u16, msk: u16):
    # Internal level
    chl_csrc_reset_16_msk_t0(vreg, msk)

def inl_add_f16_t7(res: u16, x: u16, y: u16) -> None:
    # x_rn_reg:          u16 = RN_REG_G0
    # y_rn_reg:          u16 = RN_REG_G1
    # res_rn_reg:        u16 = RN_REG_G2
    # res_mant_rn_reg:   u16 = RN_REG_T6
    # res_exp_rn_reg:    u16 = RN_REG_T5
    # small_mant_rn_reg: u16 = RN_REG_T4
    # result_sign:       u16 = RN_REG_T4
    # tmp_rn_reg:        u16 = RN_REG_T3
    # swap_sm_reg:       u16 = SM_REG2
    # _0x0071_sm:       u16 = SM_REG1
    # swap_mrk:          u16 = TMP0_MRK

    #--------------------------------------------

    apl_set_rn_reg(RN_REG_G0, x)
    apl_set_rn_reg(RN_REG_G1, y)
    apl_set_rn_reg(RN_REG_G2, res)

    #CH_RUN_FRAG_ASYNC("add_f16_dbg")
    if(x != y):
        #RUN_IMM_FRAG_ASYNC(swap_if_x_bigger_than_y(RN_REG x = RN_REG_G0, RN_REG y = RN_REG_G1)
        CH_RUN_FRAG_ASYNC("swap_if_x_bigger_than_y", "x", RN_REG_G0, "y", RN_REG_G1)
        #RUN_FRAG_ASYNC(get_exp_and_shifted3_mant(f16 = RN_REG_T0, exp = RN_REG_T3, shifted_mant = RN_REG_T4)); // 6 instructions
        CH_RUN_FRAG_ASYNC("get_exp_and_shifted3_mant", "f16", RN_REG_T0, "exp", RN_REG_T3, "shifted_mant" , RN_REG_T4)
        #RUN_FRAG_ASYNC(get_exp_and_shifted3_mant(f16 = RN_REG_T1, exp = RN_REG_T5, shifted_mant = RN_REG_T6)); // 6 instructions
        CH_RUN_FRAG_ASYNC("get_exp_and_shifted3_mant", "f16", RN_REG_T1, "exp", RN_REG_T5, "shifted_mant" , RN_REG_T6)
    else:
        #RUN_FRAG_ASYNC(get_exp_and_shifted3_mant(f16 = RN_REG_G0, exp = RN_REG_T3, shifted_mant = RN_REG_T4));
        CH_RUN_FRAG_ASYNC("get_exp_and_shifted3_mant", "f16" , RN_REG_G0, "exp", RN_REG_T3, "shifted_mant" , RN_REG_T4)
        #RUN_FRAG_ASYNC(get_exp_and_shifted3_mant(f16 = RN_REG_G1, exp = RN_REG_T5, shifted_mant = RN_REG_T6));
        CH_RUN_FRAG_ASYNC("get_exp_and_shifted3_mant", "f16", RN_REG_G1, "exp", RN_REG_T5, "shifted_mant", RN_REG_T6)

    #RUN_FRAG_ASYNC(simple_sub_6msb_frag_calc_sgn_data_t2(res = RN_REG_T3, x = RN_REG_T5, y = RN_REG_T3, sgn_data = RN_REG_T2, small_mant_vr = RN_REG_T4, big_mant_vr = RN_REG_T6));
    CH_RUN_FRAG_ASYNC("simple_sub_6msb_frag_calc_sgn_data_t2", "res", RN_REG_T3, "x", RN_REG_T5, "y", RN_REG_T3, "sgn_data", RN_REG_T2, "small_mant_vr", RN_REG_T4, "big_mant_vr", RN_REG_T6)
    #RUN_IMM_FRAG_ASYNC(move_small_shift_to_5msb_and_limit_it_to_16(RN_REG shift = RN_REG_T3) // 3 instructions
    CH_RUN_FRAG_ASYNC("move_small_shift_to_5msb_and_limit_it_to_16", "shift", RN_REG_T3)

    apl_set_sm_reg(SM_REG1, u16(0x0071))
    #RUN_FRAG_ASYNC(sr_sticky_by_vec_t2(in_out = RN_REG_T4, shift = RN_REG_T3, sm_0x0071 = SM_REG1)); // 36 instructions
    CH_RUN_FRAG_ASYNC("sr_sticky_by_vec_t2", "in_out", RN_REG_T4, "shift", RN_REG_T3, "sm_0x0071", SM_REG1)
    #RUN_FRAG_ASYNC(negate_mant_16_msb_sign_t1(mant_vr = RN_REG_T4)); // 10 instructions
    CH_RUN_FRAG_ASYNC("negate_mant_16_msb_sign_t1", "mant_vr", RN_REG_T4)
    #RUN_FRAG_ASYNC(addsub_frag_add_no_  co_u16(x = RN_REG_T6, y = RN_REG_T4, res = RN_REG_T6, x_xor_y = RN_REG_T0, cout1 = RN_REG_T1));
    CH_RUN_FRAG_ASYNC("addsub_frag_add_no_co_u16", "x", RN_REG_T6, "y", RN_REG_T4, "res", RN_REG_T6, "x_xor_y", RN_REG_T0, "cout1", RN_REG_T1)
    #RUN_IMM_FRAG_ASYNC(calc_res_sgn(RN_REG sgn_res = RN_REG_T4, RN_REG mant_res = RN_REG_T6, RN_REG sgn_data = RN_REG_T2)
    CH_RUN_FRAG_ASYNC("calc_res_sgn", "sgn_res", RN_REG_T4, "mant_res", RN_REG_T6, "sgn_data", RN_REG_T2)
    #RUN_FRAG_ASYNC(negate_mant_16_t1(mant_vr = RN_REG_T6, msb_sign = RN_REG_T6)); // 9 instructions
    CH_RUN_FRAG_ASYNC("negate_mant_16_t1", "mant_vr", RN_REG_T6, "msb_sign", RN_REG_T6)
    #RUN_FRAG_ASYNC(_5b_on_10_to_14b_left_mask_t3(mask_vr = RN_REG_T3, _5b_vr = RN_REG_T5)); // 7 instructions
    CH_RUN_FRAG_ASYNC("_5b_on_10_to_14b_left_mask_t3", "mask_vr", RN_REG_T3, "_5b_vr", RN_REG_T5)

    apl_set_sm_reg(SM_REG2, u16(u16(1) << TMP0_MRK))
    #RUN_IMM_FRAG_ASYNC(add_mask_to_mant(RN_REG mant = RN_REG_T6, RN_REG mask = RN_REG_T3, SM_REG mrk = SM_REG2)
    CH_RUN_FRAG_ASYNC("add_mask_to_mant", "mant", RN_REG_T6, "mask", RN_REG_T3, "mrk", SM_REG2)

    #RUN_FRAG_ASYNC(shift_left_data_size15_count_lsb10_t1(data = RN_REG_T6, count = RN_REG_T3)); // (19 + 6) = 25 instructions !!!!
    CH_RUN_FRAG_ASYNC("shift_left_data_size15_count_lsb10_t1", "data", RN_REG_T6, "count", RN_REG_T3)
    #RUN_FRAG_ASYNC(simple_sub_6msb_frag_t2(res = RN_REG_T5, x = RN_REG_T5, y = RN_REG_T3)); // 9 instructions
    CH_RUN_FRAG_ASYNC("simple_sub_6msb_frag_t2", "res", RN_REG_T5, "x", RN_REG_T5, "y", RN_REG_T3)

    #RUN_FRAG_ASYNC(_5b_on_10_to_14b_left_mask_t3(mask_vr = RN_REG_T3, _5b_vr = RN_REG_T5)); // 7 instructions
    CH_RUN_FRAG_ASYNC("_5b_on_10_to_14b_left_mask_t3", "mask_vr", RN_REG_T3, "_5b_vr", RN_REG_T5)
    #RUN_IMM_FRAG_ASYNC(remove_mask_from_mant(RN_REG mant = RN_REG_T6, RN_REG mask = RN_REG_T3, SM_REG mrk = SM_REG2)
    CH_RUN_FRAG_ASYNC("remove_mask_from_mant", "mant", RN_REG_T6, "mask", RN_REG_T3, "mrk", SM_REG2)
    #RUN_IMM_FRAG_ASYNC(round_mant_and_inc_exp(RN_REG sgn_res = RN_REG_T4, RN_REG mant = RN_REG_T6, RN_REG exp = RN_REG_T5)
    CH_RUN_FRAG_ASYNC("round_mant_and_inc_exp", "sgn_res", RN_REG_T4, "mant", RN_REG_T6, "exp", RN_REG_T5)
    #RUN_IMM_FRAG_ASYNC(build_res(RN_REG res = RN_REG_G2, RN_REG f16_x = RN_REG_G0, RN_REG f16_y = RN_REG_G1, RN_REG mant = RN_REG_T6, RN_REG exp = RN_REG_T5)
    CH_RUN_FRAG_ASYNC("build_res", "res", RN_REG_G2, "f16_x", RN_REG_G0, "f16_y", RN_REG_G1, "mant", RN_REG_T6, "exp", RN_REG_T5)

def add_f16_t7(res: u16, x: u16, y: u16) -> None:
    inl_add_f16_t7(res, x, y)

def gchl_add_f16(res: u16, x: u16, y: u16) -> None:
    inl_add_f16_t7(res, x, y)

def inl_sub_f16_t7(res: u16, x: u16, y: u16) -> None:
    sub_f16_t7_x_rn_reg:          u16 = RN_REG_G0
    sub_f16_t7_y_rn_reg:          u16 = RN_REG_G1
    sub_f16_t7_res_rn_reg:          u16 = RN_REG_G2
    if x != y:
        apl_set_rn_reg(RN_REG_G1, y);
        #RUN_IMM_FRAG_ASYNC(negate_y0(RN_REG y = RN_REG_G1)
        CH_RUN_FRAG_ASYNC("negate_y0", "y", RN_REG_G1)
        add_f16_t7(res, x, y)
        if res != y:
            apl_set_rn_reg(RN_REG_G1, y);
            #RUN_IMM_FRAG_ASYNC(negate_y1(RN_REG y = RN_REG_G1)
            CH_RUN_FRAG_ASYNC("negate_y1", "y", RN_REG_G1)
    else:
        apl_set_rn_reg(RN_REG_G0, x);
        apl_set_rn_reg(RN_REG_G2, res);
        #RUN_IMM_FRAG_ASYNC(handle_x_minus_x(RN_REG x = RN_REG_G0, RN_REG res = RN_REG_G2)
        CH_RUN_FRAG_ASYNC("handle_x_minus_x", "x", RN_REG_G0, "res", RN_REG_G2)

def sub_f16_t7(res: u16, x: u16, y: u16) -> None:
    inl_sub_f16_t7(res, x, y)

def gchl_sub_f16(res: u16, x: u16, y: u16) -> None:
    inl_sub_f16_t7(res, x, y)

def ch_csrc_cpy_imm_16_t0(vreg: u16, val: u16) -> None:
    apl_set_rn_reg(RN_REG_G3, vreg)
    apl_set_sm_reg(SM_REG3, val)
    CH_RUN_FRAG_ASYNC("my_cpy_imm_16", "tgt", RN_REG_G3, "val", SM_REG3)

def gchl_cpy_imm_16(vreg: u16, val: u16):
    ch_csrc_cpy_imm_16_t0(vreg, val)

def bwc_sr_16_t0(dst: u16, src: u16, size: u16) -> None:
    #FIXME: BUG
    #u32 start_msk = 1 << size;
    #start_msk: u32 = u32(u16(1) << u16(1))
    start_msk: u16 = u16(i16(1) << i16(size))

    #FIXME: BUG
    # u32 msk = (1 << (16 - size)) - 1;
    shift: i16 = i16(16) - i16(size)
    msk: u16 = u16((i16(1) << shift) - i16(1))

    # print("start_msk =", start_msk)
    # print("msk =", msk)
    # print("dst =", dst)
    # print("src =", src)
    apl_set_sm_reg(SM_REG0, start_msk)
    apl_set_sm_reg(SM_REG1, msk)
    apl_set_rn_reg(RN_REG_G1, dst)
    apl_set_rn_reg(RN_REG_G0, src)
    CH_RUN_FRAG_ASYNC("sr_16_t0_imm_frag", "dst", RN_REG_G1, "src", RN_REG_G0, "msk_src", SM_REG0, "msk_dst", SM_REG1)    

def io_calc_subgrp_index_grp_t0(dst: u16, base_idx: u16, grp: u16, subgrp: u16) -> None:
    #FIXME: BUG
    #u16 msk = (u16)(~((1 << grp) - 1));
    #msk: u16 = u16(~((u16(1) << grp) - u16(1)))

    # msk = (u16(1) << grp)
    # msk = msk - u16(1)
    # msk = ~msk
    msk: u16 = u16((i16(1) << i16(grp)) - i16(1))
    #print("io_calc_subgrp_index_grp_t0 001 msk", msk)
    #print("io_calc_subgrp_index_grp_t0 001 msk", hex(msk))
    #msk = u16(u32(0x10000) - u32(msk)) - u16(1)

    msk = u16(0xffff) - msk
    #print("msk =", msk)
    #print("io_calc_subgrp_index_grp_t0 001 msk2", msk)
    #print("io_calc_subgrp_index_grp_t0 001 msk2", hex(msk))

    chl_csrc_reset_16_msk_t0(dst, msk)
    bwc_sr_16_t0(dst, base_idx, subgrp)

# Actual Copperhead code:
def io_calc_subgrp_index_m4_grp_t0(vdst: u16, grp: u16, subgrp: u16) -> None:
    # Internal level
    vr_m4_idx:  u16 = vdst

    chl_csrc_cpy_16_t0(vr_m4_idx, GCHL_VR16_IDX)
    io_calc_subgrp_index_grp_t0(vdst, vr_m4_idx, grp, subgrp)

# External level
@ccallback
def gchl_create_subgrp_index_u16(vdst: u16, grp: u16, subgrp: u16) -> None:
    # Internal level
    io_calc_subgrp_index_m4_grp_t0(vdst, grp, subgrp)

def monotonic_transformation_nan2max_u16_f16_t1(u16_out: u16, f16_in: u16) -> None:
    apl_set_rn_reg(RN_REG_G0, u16_out)
    apl_set_rn_reg(RN_REG_G1, f16_in)
    CH_RUN_FRAG_ASYNC("monotonic_transformation_f16_u16_nan2max_frag", "u16_out", RN_REG_G0, "f16_in", RN_REG_G1, "tmp_exp_non_zero", RN_REG_T0)

def gchl_monotonic_transformation_nan2max_u16_f16(vdst: u16, vsrc: u16) -> None:
    monotonic_transformation_nan2max_u16_f16_t1(vdst, vsrc)

##########################################################
def mant_mul_t7() -> None:
	#RUN_FRAG_ASYNC(init_mul_11_from_f16(f16_y = RN_REG_G1, mnt_x = RN_REG_T3, s0 = RN_REG_T4,
	#				    s1 = RN_REG_T0, _2x_sticky = RN_REG_T1, m0 = RN_REG_T2, c_xor_s = RN_REG_T5));
    CH_RUN_FRAG_ASYNC("init_mul_11_from_f16", "f16_y", RN_REG_G1, "mnt_x", RN_REG_T3, "s0", RN_REG_T4, \
					    "s1", RN_REG_T0, "_2x_sticky", RN_REG_T1, "m0", RN_REG_T2, "c_xor_s", RN_REG_T5)

    i: i32
    for i in range(i32(1), i32(7), i32(2)):
        apl_set_sm_reg(SM_REG0, u16(1) << u16(i))
        #RUN_FRAG_ASYNC(_3to2_mul_f16(c = RN_REG_T3, s0 = RN_REG_T4, s1 = RN_REG_T0,
		#			     _2x_sticky = RN_REG_T1, m0 = RN_REG_T2,
		#                             c_xor_s = RN_REG_T5, f16_y = RN_REG_G1,
		#                             i_msk= SM_REG0));
        CH_RUN_FRAG_ASYNC("_3to2_mul_f16", "c", RN_REG_T3, "s0", RN_REG_T4, "s1", RN_REG_T0, \
					      "_2x_sticky", RN_REG_T1, "m0", RN_REG_T2, \
		                  "c_xor_s", RN_REG_T5, "f16_y", RN_REG_G1, "i_msk", SM_REG0)
        apl_set_sm_reg(SM_REG1, u16(2) << u16(i))
        #RUN_FRAG_ASYNC(_3to2_mul_f16(c = RN_REG_T3, s1 = RN_REG_T4, s0 = RN_REG_T0,
		#			     _2x_sticky = RN_REG_T1, m0 = RN_REG_T2,
		#                             c_xor_s = RN_REG_T5, f16_y = RN_REG_G1,
		#                             i_msk= SM_REG1));
        CH_RUN_FRAG_ASYNC("_3to2_mul_f16", "c", RN_REG_T3, "s1", RN_REG_T4, "s0", RN_REG_T0, \
					      "_2x_sticky", RN_REG_T1, "m0", RN_REG_T2, \
		                  "c_xor_s", RN_REG_T5, "f16_y", RN_REG_G1, "i_msk" , SM_REG1)

    apl_set_sm_reg(SM_REG0, u16(1) << u16(7))
	# RUN_FRAG_ASYNC(_3to2_mul_f16(c = RN_REG_T3, s0 = RN_REG_T4, s1 = RN_REG_T0,
	# 			     _2x_sticky = RN_REG_T1, m0 = RN_REG_T2,
	# 			     c_xor_s = RN_REG_T5, f16_y = RN_REG_G1,
	# 	                     i_msk= SM_REG0));
    CH_RUN_FRAG_ASYNC("_3to2_mul_f16", "c", RN_REG_T3, "s0", RN_REG_T4, "s1", RN_REG_T0, \
					  "_2x_sticky", RN_REG_T1, "m0", RN_REG_T2, \
		              "c_xor_s", RN_REG_T5, "f16_y", RN_REG_G1, "i_msk", SM_REG0)


	# RUN_FRAG_ASYNC(_3to2_mul_f16_before_last(c = RN_REG_T3, s1 = RN_REG_T4, s0 = RN_REG_T0,
	# 					 _2x_sticky = RN_REG_T1, m0 = RN_REG_T2,
	# 					 c_xor_s = RN_REG_T5, f16_y = RN_REG_G1));
    CH_RUN_FRAG_ASYNC("_3to2_mul_f16_before_last", "c", RN_REG_T3, "s1", RN_REG_T4, "s0", RN_REG_T0, \
	 				  "_2x_sticky", RN_REG_T1, "m0", RN_REG_T2,
	 				  "c_xor_s", RN_REG_T5, "f16_y", RN_REG_G1)
	# RUN_FRAG_ASYNC(_3to2_mul_f16_last(c = RN_REG_T3, s0 = RN_REG_T4, s1 = RN_REG_T0,
	# 				  _2x_sticky = RN_REG_T1, m0 = RN_REG_T2,
	# 	                          c_xor_s = RN_REG_T5));
    CH_RUN_FRAG_ASYNC("_3to2_mul_f16_last", "c", RN_REG_T3, "s0", RN_REG_T4, "s1", RN_REG_T0,
	 				  "_2x_sticky", RN_REG_T1, "m0", RN_REG_T2, "c_xor_s", RN_REG_T5)

	#RUN_FRAG_ASYNC(addsub_carry_prefix(x = RN_REG_T3, y = RN_REG_T0, t_xory_ci = RN_REG_T4, t_ci0 = RN_REG_T1));
    CH_RUN_FRAG_ASYNC("addsub_carry_prefix", "x", RN_REG_T3, "y", RN_REG_T0, "t_xory_ci", RN_REG_T4, "t_ci0", RN_REG_T1)
	#RUN_FRAG_ASYNC(addsub_carry_in_out_u12_suffix(res = RN_REG_T5, x = RN_REG_T3, y = RN_REG_T0, t_xory_ci = RN_REG_T4, t_ci0 = RN_REG_T1, t_ci1 = RN_REG_T2));
    CH_RUN_FRAG_ASYNC("addsub_carry_in_out_u12_suffix", "res", RN_REG_T5, "x", RN_REG_T3, "y", RN_REG_T0, "t_xory_ci", RN_REG_T4, "t_ci0", RN_REG_T1, "t_ci1", RN_REG_T2)

def inl_mul_f16_t6(res: u16, x: u16, y: u16) -> None:
    swap_mrk:   u16 = u16(1) << TMP0_MRK

    apl_set_rn_reg(RN_REG_G0, x);
    apl_set_rn_reg(RN_REG_G1, y);
    apl_set_sm_reg(SM_REG2, swap_mrk);

    #CH_RUN_FRAG_ASYNC("add_f16_dbg")
    #CH_RUN_FRAG_ASYNC("mul_f16_dbg")
    if (x != y):
        # swap small y #
		#RUN_FRAG_ASYNC(is_small_f16(f16 = RN_REG_G1));
        CH_RUN_FRAG_ASYNC("is_small_f16", "f16",  RN_REG_G1)
		#RUN_FRAG_ASYNC(swap_and_set_mrk_t1(x = RN_REG_G0, y = RN_REG_G1, mrk = SM_REG2));
        CH_RUN_FRAG_ASYNC("swap_and_set_mrk_t1", "x", RN_REG_G0, "y", RN_REG_G1, "mrk", SM_REG2)

    # create_11b of mant_x and_shift it to the left #
	#RUN_FRAG_ASYNC(create_and_shift_left_tmpx_t1(f16_x = RN_REG_G0, mant_x_and_shift = RN_REG_T3));		// 20
    CH_RUN_FRAG_ASYNC("create_and_shift_left_tmpx_t1", "f16_x", RN_REG_G0, "mant_x_and_shift", RN_REG_T3)

	# Multiply mantissas and round half to even #
    mant_mul_t7();			# 65

	# Inputs: T3 - 11b mant_x
	#         G1 - f16_y
	# Output: T5 - Mant res 13b (last bit is the sticky_bit)
	# Add exponents #

	# exp_res = y.exponent + MAX(x.exponent, 1) - mant_x_shift_left - 15;
	#RUN_FRAG_ASYNC(add_f16exp(res = RN_REG_T3, f16_x = RN_REG_G0, f16_y = RN_REG_G1, x = RN_REG_T1, t_xory = RN_REG_T2));
    CH_RUN_FRAG_ASYNC("add_f16exp", "res", RN_REG_T3, "f16_x", RN_REG_G0, "f16_y", RN_REG_G1, "x", RN_REG_T1, "t_xory", RN_REG_T2)
	#RUN_FRAG_ASYNC(add_f16biad_shift(res = RN_REG_T4, shift = RN_REG_T5));
    CH_RUN_FRAG_ASYNC("add_f16biad_shift", "res", RN_REG_T4, "shift", RN_REG_T5)

	#arith_sub_16_t3(VR16_T5, VR16_T5, VR16_T4, 1, 0);
	#RUN_FRAG_ASYNC(simple_sub_7msb_frag_t2(res = RN_REG_T3, x = RN_REG_T3, y = RN_REG_T4));
    CH_RUN_FRAG_ASYNC("simple_sub_7msb_frag_t2", "res", RN_REG_T3, "x", RN_REG_T3, "y", RN_REG_T4)

	#arith_sub_16_t3(VR16_T4, VR16_T5, VR16_T4, 1, 0);
	#RUN_FRAG_ASYNC(simple_dec_7msb_frag(res = RN_REG_T4, x = RN_REG_T3));
    CH_RUN_FRAG_ASYNC("simple_dec_7msb_frag", "res", RN_REG_T4, "x", RN_REG_T3)

    #RUN_IMM_FRAG_ASYNC(min_zero_and_negate(RN_REG dst = RN_REG_T4, RN_REG src = RN_REG_T4, RN_REG tmp = RN_REG_T0)
    CH_RUN_FRAG_ASYNC("min_zero_and_negate", "dst", RN_REG_T4, "src", RN_REG_T4, "tmp", RN_REG_T0)

    #RUN_IMM_FRAG_ASYNC(max_0xF_on_7msb_and_sr3(RN_REG dst = RN_REG_T4, RN_REG src = RN_REG_T4)
    CH_RUN_FRAG_ASYNC("max_0xF_on_7msb_and_sr3", "dst", RN_REG_T4, "src", RN_REG_T4)

	# shift right when exp is too small
	#RUN_FRAG_ASYNC(sr_16_by_vec_t1(in_out = RN_REG_T5, shift = RN_REG_T4));
    CH_RUN_FRAG_ASYNC("sr_16_by_vec_t1", "in_out", RN_REG_T5, "shift", RN_REG_T4)

    #RUN_IMM_FRAG_ASYNC(shift_right_one_when_mant_is_too_big(RN_REG mant = RN_REG_T5, RN_REG exp = RN_REG_T3, RN_REG shift = RN_REG_T4, RN_REG tmp = RN_REG_T0)
    CH_RUN_FRAG_ASYNC("shift_right_one_when_mant_is_too_big", "mant", RN_REG_T5, "exp", RN_REG_T3, "shift", RN_REG_T4, "tmp", RN_REG_T0)

	# restore x, y swap #
    if (x != y):
		#swap back x and y #
        #RUN_FRAG_ASYNC(is_mrk(mrk = SM_REG2));
        CH_RUN_FRAG_ASYNC("is_mrk", "mrk", SM_REG2)
        #RUN_FRAG_ASYNC(swap_and_set_mrk_t1(x = RN_REG_G0, y = RN_REG_G1, mrk = SM_REG2));
        CH_RUN_FRAG_ASYNC("swap_and_set_mrk_t1", "x", RN_REG_G0, "y", RN_REG_G1, "mrk", SM_REG2)

	# build_result #
    apl_set_rn_reg(RN_REG_G2, res);
    #RUN_IMM_FRAG_ASYNC(build_f16_res(RN_REG res = RN_REG_G2, RN_REG f16_x = RN_REG_G0, RN_REG f16_y = RN_REG_G1)
    CH_RUN_FRAG_ASYNC("build_f16_res", "res", RN_REG_G2, "f16_x", RN_REG_G0, "f16_y", RN_REG_G1)

def mul_f16_t6(res: u16, x: u16, y: u16) -> None:
    inl_mul_f16_t6(res, x, y)

def gchl_mul_f16(res: u16, x: u16, y: u16) -> None:
    inl_mul_f16_t6(res, x, y)

# External level
@ccallback
def gchl_store_16(vmdst: u16, vsrc: u16):
    # Internal level
    chl_io_store_16_t0(vmdst, vsrc)
from belex.literal import (NOOP, RL, RSP2K, RSP16, RSP32K, RSP256, RSP_END, VR,
                           apl_commands, belex_apl, u16, SM_0XFFFF,
                           RN_REG_FLAGS, B_FLAG, TMP0_MRK, GL, INV_GL, Mask,
                           RN_REG_T0, RN_REG_T1, RN_REG_T2, RN_REG_T3, RN_REG_T4, RN_REG_T5, 
			               SM_0X0001, SM_0X0707, SM_0X3333, SM_0X1111, SM_0X000F, SM_0X00FF, SM_0X0101, SM_0X001F, SM_0X0F0F, SM_0X3333, SM_0X5555,
                           INV_RSP16, SRL, INV_SRL, INV_NRL, INV_RL, INV_GGL, GGL, NRL,
                           PE_FLAG, C_FLAG,
                           L1,
                           L2, L2_END, LGL)


@belex_apl
def my_cpy_imm_16(Belex, tgt: VR, val: u16) -> None:
    with apl_commands():
        tgt[val] <= INV_RSP16()
        tgt[~val] <= RSP16()


@belex_apl
def chf_cmn_cpy_16_msk(Belex, vdst: VR, vsrc: VR, msk: u16) -> None:
    with apl_commands():
        RL[msk] <= vsrc()
    with apl_commands():
        vdst[msk] <= RL()

@belex_apl
def my_rd_rsp2k(Belex, src: VR) -> None:
    RL[::] <= src()
    RSP16[::] <= RL()
    RSP256() <= RSP16()
    RSP2K() <= RSP256()
    RSP32K() <= RSP2K()
    NOOP()
    NOOP()
    RSP_END()

############################################ DIV U16 ############################################
@belex_apl
def init_div_u16(Belex, dividend: VR, divisor: VR, result: VR, modulo: VR, t_divisor: VR, t_rot_divisor: VR, t0: VR):

    with apl_commands(" init vregs"):
        RL[SM_0XFFFF] <= 0

    with apl_commands("SM_0X0001:SB[result] = INV_RL; Set res lsb = 1"):
        t_divisor[SM_0XFFFF] <= RL()
        RL[SM_0XFFFF] <= dividend()

    with apl_commands():
        t0[SM_0XFFFF] <= RL()

    with apl_commands():
        RL[SM_0XFFFF] <= divisor()

    with apl_commands():
        result[SM_0X0001] <= INV_NRL()

    with apl_commands():
        RL[SM_0XFFFF] <= INV_RL()
        GL[SM_0XFFFF << 1] <= RL()

    with apl_commands("FOR (U8 iter = 0 TO 14 STEP 1) { { ((U16)SM_0XFFFF <(< 1) << iter):GL = RL;   15 - iter:SB[result] = GL; } }"):
        result[SM_0X0001 << 15] <= GL()
        GL[SM_0XFFFF << 2] <= RL()

    with apl_commands():
        result[SM_0X0001 << 14] <= GL()
        GL[SM_0XFFFF << 3] <= RL()

    with apl_commands():
        result[SM_0X0001 << 13] <= GL()
        GL[SM_0XFFFF << 4] <= RL()

    with apl_commands():
        result[SM_0X0001 << 12] <= GL()
        GL[SM_0XFFFF << 5] <= RL()

    with apl_commands():
        result[SM_0X0001 << 11] <= GL()
        GL[SM_0XFFFF << 6] <= RL()

    with apl_commands():
        result[SM_0X0001 << 10] <= GL()
        GL[SM_0XFFFF << 7] <= RL()

    with apl_commands():
        result[SM_0X0001 << 9] <= GL()
        GL[SM_0XFFFF << 8] <= RL()

    with apl_commands():
        result[SM_0X0001 << 8] <= GL()
        GL[SM_0XFFFF << 9] <= RL()

    with apl_commands():
        result[SM_0X0001 << 7] <= GL()
        GL[SM_0XFFFF << 10] <= RL()

    with apl_commands():
        result[SM_0X0001 << 6] <= GL()
        GL[SM_0XFFFF << 11] <= RL()

    with apl_commands():
        result[SM_0X0001 << 5] <= GL()
        GL[SM_0XFFFF << 12] <= RL()

    with apl_commands():
        result[SM_0X0001 << 4] <= GL()
        GL[SM_0XFFFF << 13] <= RL()

    with apl_commands():
        result[SM_0X0001 << 3] <= GL()
        GL[SM_0XFFFF << 14] <= RL()

    with apl_commands():
        result[SM_0X0001 << 2] <= GL()
        GL[SM_0XFFFF << 15] <= RL()

    with apl_commands():
        result[SM_0X0001 << 1] <= GL()
        GL[SM_0X0001] <= RL()

    with apl_commands():
        (SM_0X0001 << 15)[t_rot_divisor,t_divisor] <= INV_GL()
        t_rot_divisor[~(SM_0X0001 << 15)] <= INV_SRL()
        RL[SM_0XFFFF] <= t0()

    with apl_commands():
        modulo[SM_0XFFFF] <= RL()
@belex_apl
def div_0(Belex) -> None:
    with apl_commands():
        RL[SM_0X0001 << B_FLAG] <= RN_REG_FLAGS()
        GL[SM_0X0001 << B_FLAG] <= RL()

    with apl_commands():
        RN_REG_FLAGS[SM_0X0001 << TMP0_MRK] <= GL()

@belex_apl
def div_iter0(Belex, result: VR, iter_msk: Mask) -> None:

    with apl_commands():
        RL[SM_0X0001 << B_FLAG] <= RN_REG_FLAGS()
        GL[SM_0X0001 << B_FLAG] <= RL()

    with apl_commands():
        RL[iter_msk] <= result() & INV_GL()

    with apl_commands():
        result[iter_msk] <= RL()

@belex_apl
def div_iter1(Belex, t_divisor: VR, t_rot_divisor: VR) -> None:

    with apl_commands():
        RL[SM_0XFFFF] <= t_rot_divisor()
        GL[SM_0X0001] <= RL()

    with apl_commands():
        t_rot_divisor[SM_0X0001 << 15] <= GL()
        t_rot_divisor[~(SM_0X0001 << 15)] <= SRL()
        RL[SM_0XFFFF << 1] <= t_divisor()
        GL[SM_0X0001] <= RL()

    with apl_commands():
        t_divisor[SM_0X0001 << 15] <= GL()
        t_divisor[~(SM_0X0001 << 15)] <= SRL()

@belex_apl
def div_1(Belex) -> None:

    with apl_commands():
        RL[SM_0X0001 << TMP0_MRK] <= RN_REG_FLAGS()
        GL[SM_0X0001 << TMP0_MRK] <= RL()

    with apl_commands():
        RN_REG_FLAGS[SM_0X0001 << B_FLAG] <= GL()


####################################################################################################
@belex_apl
def div_cpy_16_vmrk_t0(Belex, dst: VR, src: VR, reg_mrk: VR, mrk_flag: Mask) -> None:

    with apl_commands():
        RL[mrk_flag] <= reg_mrk()
        GL[mrk_flag] <= RL()

    with apl_commands():
        RL[SM_0XFFFF] <= dst() & INV_GL()

    with apl_commands():
        RL[SM_0XFFFF] |= src() & GL()

    with apl_commands():
        dst[SM_0XFFFF] <= RL()

@belex_apl
def chf_sub_u16_t3(Belex, res: VR, x: VR, y: VR, x_xor_noty: VR, cout1: VR, noty: VR) -> None:

    with apl_commands():
        #1
        RL[SM_0XFFFF] <= y()

    with apl_commands():
        noty[SM_0XFFFF] <= INV_RL()
        RL[SM_0XFFFF] ^= x()

    with apl_commands():
        x_xor_noty[SM_0XFFFF] <= INV_RL()
        RL[SM_0X3333] <= INV_RL()
        GGL[SM_0X3333] <= RL()

    with apl_commands():
        cout1[SM_0X1111] <= RL()
        cout1[SM_0X1111 << 1] <= GGL()
        RL[SM_0X1111 << 2] <= x_xor_noty() & GGL()
        RL[SM_0X3333] <= x() & noty()

    with apl_commands():
        cout1[SM_0X1111 << 2] <= RL()
        RL[SM_0X1111 << 3] <= x_xor_noty() & NRL()
        RL[SM_0X1111 << 1] |= x_xor_noty() & NRL()
        RL[SM_0X1111 << 2] <= x() & noty()

    with apl_commands():
        cout1[SM_0X1111 << 3] <= RL()
        RL[SM_0X1111 << 3] <= x() & noty()
        RL[SM_0X1111 << 2] |= x_xor_noty() & NRL()

    with apl_commands():
        RL[SM_0X1111 << 3] |= x_xor_noty() & NRL()

    with apl_commands():
        RL[SM_0X000F] |= cout1()
        GGL[SM_0X0001 << 1] <= RL()
        GL[SM_0X0001 << 3] <= RL()

    with apl_commands():
        RL[SM_0X0001] <= ~x_xor_noty() & INV_RSP16()
        RL[SM_0X0001 << 1] <= x_xor_noty() ^ NRL()
        RL[SM_0X000F << 4] |= cout1() & GL()
        GL[SM_0X0001 << 7] <= RL()

    with apl_commands():
        res[~(SM_0XFFFF << 2)] <= RL()
        RL[SM_0X000F << 8] |= cout1() & GL()
        GL[SM_0X0001 << 11] <= RL()

    with apl_commands():
        RL[SM_0X0001 << 1] <= GGL()
        RL[SM_0X000F << 12] |= cout1() & GL()
        GL[SM_0X0001 << 15] <= RL()

    with apl_commands():
        RL[SM_0XFFFF << 2] <= x_xor_noty() ^ NRL()
        RN_REG_FLAGS[SM_0X0001 << B_FLAG] <= INV_GL()
        RL[SM_0X0001 << B_FLAG] <= INV_GL()
        GL[SM_0X0001 << B_FLAG] <= RL()

    with apl_commands():
        res[SM_0XFFFF << 2] <= RL()

##########################################################

@belex_apl
def load_16_gchl(Belex, dst: VR, src: L1, parity_src: L1, parity_mask2: Mask):

	with apl_commands():
		RL[SM_0X0001 << PE_FLAG] <= RN_REG_FLAGS()
		GL[SM_0X0001 << PE_FLAG] <= RL()
		GGL() <= src() + 0

	with apl_commands():
		dst[SM_0X1111 << 0] <= GGL()
		RL[SM_0X3333 << 3] <= GGL()
		RL[SM_0X0001 << 1] <= INV_GL()
		GGL() <= src() + 1

	with apl_commands():
		dst[SM_0X1111 << 1] <= GGL()
		RL[SM_0X3333 << 3] ^= GGL()
		GGL() <= src() + 2

	with apl_commands():
		dst[SM_0X1111 << 2] <= GGL()
		RL[SM_0X3333 << 3] ^= GGL()
		GGL() <= src() + 3

	with apl_commands():
		dst[SM_0X1111 << 3] <= GGL()
		RL[SM_0X3333 << 3] ^= GGL()
		GGL() <= parity_src()

	with apl_commands():
		RL[parity_mask2] ^= GGL()

	with apl_commands():
		#use GL to compute new_parity = ~(~p0 & ~p1 & ~old_parity)
		RL[SM_0X0101 << 3] ^= INV_SRL()
		RL[SM_0X0101 << 4] ^= INV_NRL()
		GL[parity_mask2] <= RL()
		GL[SM_0X0001 << 1] <= RL()

	with apl_commands():
		RN_REG_FLAGS[SM_0X0001 << PE_FLAG] <= INV_GL()        
                
##########################################################                

@belex_apl
def store_16_gchl(Belex, dst: L1, parity_dst: L1, parity_mask: Mask, src: VR):

	with apl_commands():
		RL[SM_0XFFFF] <= src()
		GGL[SM_0X1111 << 0] <= RL()

	with apl_commands():
		dst() + 0 <= GGL()
		GGL[SM_0X1111 << 1] <= RL()

	with apl_commands():
		dst() + 1 <= GGL()
		RL[SM_0X1111 << 1] ^= NRL()
		GGL[SM_0X1111 << 2] <= RL()

	with apl_commands():
		dst() + 2 <= GGL()
		RL[SM_0X1111 << 2] ^= NRL()
		GGL[SM_0X1111 << 3] <= RL()

	with apl_commands():
		dst() + 3 <= GGL()
		RL[SM_0X1111 << 3] ^= NRL()
		GL[SM_0X0001 << 11] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 15] ^= GL()
		GL[SM_0X0001 << 3] <= RL()
		GGL() <= parity_dst()

	with apl_commands():
		RL[SM_0X0001 << 7] ^= GL()
		GL[SM_0X0001 << 7] <= RL()
		RL[SM_0X1111 << 0] <= GGL()

	with apl_commands():
		RL[parity_mask << 0] <= GL()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[parity_mask << 8] <= GL()
		GGL[SM_0X1111 << 0] <= RL()

	with apl_commands():
		parity_dst() <= GGL()
                
##########################################################

@belex_apl
def l2_end_load_vmr(Belex):

	with apl_commands():
		L2_END()

@belex_apl
def copy_l2_to_l1_byte_gchl(Belex, dst: L1, parity_dst: L1, src: L2):

	with apl_commands():
		LGL() <= src() + 0

	with apl_commands():
		dst() + 0 <= LGL()
		LGL() <= src() + 1

	with apl_commands():
		dst() + 1 <= LGL()
		LGL() <= src() + 2

	with apl_commands():
		dst() + 2 <= LGL()
		LGL() <= src() + 3

	with apl_commands():
		dst() + 3 <= LGL()
		LGL() <= src() + 4

	with apl_commands():
		dst() + (1,0) <= LGL()
		LGL() <= src() + 5

	with apl_commands():
		dst() + (1,1) <= LGL()
		LGL() <= src() + 6

	with apl_commands():
		dst() + (1,2) <= LGL()
		LGL() <= src() + 7

	with apl_commands():
		dst() + (1,3) <= LGL()
		LGL() <= src() + 8

	with apl_commands():
		parity_dst() <= LGL()

##########################################################                

@belex_apl
def l2_end_store_vmr(Belex):

	with apl_commands():
		L2_END()
		
@belex_apl
def copy_l1_to_l2_byte_gchl(Belex, dst: L2, src: L1, parity_src: L1):

	with apl_commands():
		LGL() <= src() + 0

	with apl_commands():
		dst() + 0 <= LGL()
		LGL() <= src() + 1

	with apl_commands():
		dst() + 1 <= LGL()
		LGL() <= src() + 2

	with apl_commands():
		dst() + 2 <= LGL()
		LGL() <= src() + 3

	with apl_commands():
		dst() + 3 <= LGL()
		LGL() <= src() + (1,0)

	with apl_commands():
		dst() + 4 <= LGL()
		LGL() <= src() + (1,1)

	with apl_commands():
		dst() + 5 <= LGL()
		LGL() <= src() + (1,2)

	with apl_commands():
		dst() + 6 <= LGL()
		LGL() <= src() + (1,3)

	with apl_commands():
		dst() + 7 <= LGL()
		LGL() <= parity_src()

	with apl_commands():
		dst() + 8 <= LGL()

##########################################################                

@belex_apl
def chf_and_m(Belex, mdst: Mask, 
                     msrc0: Mask, 
                     msrc1: Mask):
    with apl_commands():
        RL[msrc0] <= RN_REG_FLAGS()    
        RL[msrc1] <= RN_REG_FLAGS()    
        GL[msrc0] <= RL()
        GL[msrc1] <= RL()
    with apl_commands():
        RN_REG_FLAGS[mdst] <= GL()

@belex_apl
def chf_and_m_1(Belex, vdst: VR, mdst: Mask, 
                     vsrc0: VR, msrc0: Mask, 
                     vsrc1: VR, msrc1: Mask):
    with apl_commands():
        RL[msrc0] <= vsrc0()
        RL[msrc1] <= vsrc1()
        GL[msrc0] <= RL()
        GL[msrc1] <= RL()
    with apl_commands():
        vdst[mdst] <= GL()

##########################################################                

@belex_apl
def chf_csrc_reset_16_msk(Belex, vreg: VR, msk: u16):
    with apl_commands():
        vreg[msk] <= RSP16()

##################################################

@belex_apl
def eq_imm_to_flag(Belex, flag: Mask, x: VR, imm_val: Mask):

	with apl_commands():
		RL[imm_val] <= x()
		RL[~imm_val] <= ~x() & INV_RSP16()
		GL[SM_0XFFFF] <= RL()

	with apl_commands():
		RN_REG_FLAGS[flag] <= GL()        
                
##################################################
@belex_apl
def sr_16_t0_imm_frag(Belex, dst: VR, src: VR, msk_src: Mask, msk_dst: Mask):
	with apl_commands("1"):
		RL[SM_0XFFFF] <= src()
		GL[msk_src] <= RL()                
	with apl_commands("2"):
		dst[~SM_0X0001] = RSP16()
		RL[SM_0X0001] <= GL()
		GL[msk_src << 1] <= RL()
	with apl_commands("3"):
		dst[SM_0X0001] <= RSP16()
		RL[SM_0X0001 << 1] <= GL()
		GL[msk_src << 2] <= RL()
	with apl_commands("4"):
		RL[SM_0X0001 << 2] <= GL()
		GL[msk_src << 3] <= RL()
	with apl_commands("5"):
		RL[SM_0X0001 << 3] <= GL()
		GL[msk_src << 4] <= RL()
	with apl_commands("6"):
		RL[SM_0X0001 << 4] <= GL()
		GL[msk_src << 5] <= RL()
	with apl_commands("7"):
		RL[SM_0X0001 << 5] <= GL()
		GL[msk_src << 6] <= RL()
	with apl_commands("8"):
		RL[SM_0X0001 << 6] <= GL()
		GL[msk_src << 7] <= RL()
	with apl_commands("9"):
		RL[SM_0X0001 << 7] <= GL()
		GL[msk_src << 8] <= RL()
		GGL[msk_src << 12] <= RL()
	with apl_commands("10"):
		dst[SM_0X0001 << 12] <= GGL()
		RL[SM_0X0001 << 8] <= GL()
		GL[msk_src << 9] <= RL()
		GGL[msk_src << 13] <= RL()
	with apl_commands("11"):
		dst[SM_0X0001 << 13] <= GGL()
		RL[SM_0X0001 << 9] <= GL()
		GL[msk_src << 10] <= RL()
		GGL[msk_src << 14] <= RL()
	with apl_commands("12"):
		dst[SM_0X0001 << 14] <= GGL()                
		RL[SM_0X0001 << 10] <= GL()
		GL[msk_src << 11] <= RL()
	with apl_commands("13"):
		RL[SM_0X0001 << 11] <= GL()
		RL[SM_0X0707 << 12] <= dst()
	with apl_commands("14"):
		dst[msk_dst] <= RL()
		dst[~msk_dst] <= RSP16()

##################################################
# inl_add_f16_t7 fragments

@belex_apl
def get_exp_and_shifted3_mant(Belex, f16: VR, exp: VR, shifted_mant: VR):

	with apl_commands():
		RL[~(SM_0XFFFF << 10)] <= f16()
		RL[SM_0XFFFF << 10] <= ~f16() & INV_RSP16()
		GL[SM_0X001F << 10] <= RL()
		exp[~(SM_0XFFFF << 10)] <= RSP16()

	with apl_commands():
		exp[SM_0X001F << 10] <= INV_RL()
		exp[SM_0X0001 << 15] <= RSP16()

	with apl_commands():
		RL[~(SM_0XFFFF << 13)] <= NRL()
		shifted_mant[SM_0X0001 << 14] <= RSP16()
		shifted_mant[SM_0X0001 << 15] <= INV_RL()

	with apl_commands():
		RL[~(SM_0XFFFF << 13)] <= NRL()
		RL[SM_0X0001 << 13] <= INV_GL()
		shifted_mant[SM_0X0001 << 13] <= INV_GL()

	with apl_commands():
		shifted_mant[~(SM_0XFFFF << 13)] <= NRL()

	with apl_commands():
		exp[SM_0X0001 << 10] |= GL()

@belex_apl
def simple_sub_6msb_frag_calc_sgn_data_t2(Belex, res: VR, x: VR, y: VR, sgn_data: VR, small_mant_vr: VR, big_mant_vr: VR):

	with apl_commands():
		RL[SM_0X001F << 10] <= x()
		RL[SM_0X0001 << 15] <= big_mant_vr()
		GGL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0X001F << 10] ^= y()
		RN_REG_T0[SM_0X001F << 10] <= INV_RL()
		RL[SM_0X0001 << 15] <= small_mant_vr()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RN_REG_T1[SM_0X001F << 10] <= INV_RL()
		RL[SM_0X001F << 10] <= RN_REG_T0() & y()
		big_mant_vr[SM_0X0001 << 15] <= RSP16()

	with apl_commands():
		RL[SM_0X0001 << 15] ^= GGL()
		sgn_data[SM_0X0001 << 15] <= GGL()
		sgn_data[SM_0X3333 << 13] <= INV_GGL()

	with apl_commands():
		RL[SM_0X0001 << 11] |= RN_REG_T1() & NRL()
		small_mant_vr[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 12] |= RN_REG_T1() & NRL()
		RL[SM_0X0001 << 15] <= INV_GL()
		GGL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 13] |= RN_REG_T1() & NRL()
		sgn_data[SM_0X0001 << 14] |= GL()
		RL[SM_0X0001 << 15] <= RSP16()

	with apl_commands():
		RL[SM_0X0001 << 14] |= RN_REG_T1() & NRL()
		sgn_data[SM_0X5555 << 13] |= GGL()

	with apl_commands():
		RL[SM_0XFFFF << 11] <= RN_REG_T1() ^ INV_NRL()
		RL[SM_0X0001 << 10] <= RN_REG_T1() ^ INV_RSP16()

	with apl_commands():
		res[SM_0XFFFF << 10] <= RL()

@belex_apl
def sr_sticky_by_vec_t2(Belex, in_out: VR, shift: VR, sm_0x0071: Mask):

	with apl_commands(" small_mant |= (s16)((!!(small_mant & ((1 << small_shift) - 1))) << small_shift);  create filled mask to calc sticky bit  RL = (1 << small_shift) - 1"):
		RL[SM_0X001F << 11+0] <= ~shift() & INV_RSP16()
		GL[SM_0X0001 << 11+4] <= RL()
		GL[SM_0X0001 << 11+0] <= RL()

	with apl_commands():
		RN_REG_T0[SM_0X1111 << 1] <= GL()
		RN_REG_T0[SM_0X1111 << 3] <= INV_GL()
		GL[SM_0X0001 << 11+4] <= RL()
		GL[SM_0X0001 << 11+1] <= RL()

	with apl_commands():
		RN_REG_T1[SM_0X1111 << 1] <= GL()
		RN_REG_T1[SM_0X3333 << 2] <= INV_GL()
		GL[SM_0X0001 << 11+4] <= RL()
		GL[SM_0X0001 << 11+3] <= RL()

	with apl_commands():
		RL[~(SM_0X5555 << 11+2)] <= INV_GL()
		GL[SM_0X0001 << 11+4] <= RL()
		GL[SM_0X0001 << 11+2] <= RL()

	with apl_commands():
		GGL[~(SM_0X5555 << 11+2)] <= RL()
		RL[SM_0X0001] <= 1

	with apl_commands():
		RL[SM_0X0101 << 1] <= ~(RN_REG_T0() & RN_REG_T1()) & INV_RSP16()
		RL[SM_0X0101 << 5] <= ~(RN_REG_T0() & RN_REG_T1()) & INV_GL()
		RL[SM_0X0101 << 3] <= (RN_REG_T0() & RN_REG_T1()) | INV_GL()
		RL[SM_0X0101 << 7] <= (RN_REG_T0() & RN_REG_T1()) & INV_GL()

	with apl_commands():
		RL[SM_0X0101 << 1] |= INV_GL()
		RL[SM_0X0101 << 2] <= RN_REG_T1() | INV_GL()
		RL[SM_0X0101 << 6] <= RN_REG_T1() & INV_GL()
		RL[SM_0X1111 << 4] <= INV_GL()

	with apl_commands():
		RL[SM_0X00FF] |= GGL()
		RL[SM_0X0001 << 8] <= GGL()
		RL[SM_0X00FF << 9] &= GGL()

	with apl_commands(" RL = small_mant & ((1 << small_shift) - 1);"):
		RL[~(SM_0XFFFF << 14)] <= in_out() & RL()

	with apl_commands(" GL = !(small_mant & ((1 << small_shift) - 1));"):
		RL[~(SM_0XFFFF << 14)] <= INV_RL()
		GL[~(SM_0XFFFF << 14)] <= RL()

	with apl_commands(" T1[0] = sticky_bit = !!(small_mant & ((1 << small_shift) - 1))"):
		RN_REG_T1[SM_0X0001] <= INV_GL()

	with apl_commands(" small_mant |= (s16)((!!(small_mant & ((1 << small_shift) - 1))) << small_shift);  small_mant = (s16)((u16)small_mant >> small_shift);"):
		RL[SM_0X0001 << 11+3] <= shift()
		GL[SM_0X0001 << 11+3] <= RL()

	with apl_commands():
		RL[~(SM_0XFFFF << 14)] <= in_out() & ~INV_GL()
		RN_REG_T0[~(SM_0XFFFF << 14)] <= INV_GL()
		RL[SM_0XFFFF << 14] <= RSP16()
		GGL[SM_0X0001 << 11] <= RL()
		GL[SM_0X0001 << 9] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 8] <= GGL()
		RL[~(SM_0X0707 << 8)] <= SRL()
		GGL[SM_0X0001 << 7] <= RL()
		GGL[SM_0X0001 << 11] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 4] <= GGL()
		RL[SM_0X0001 << 8] <= GGL()
		RL[~(sm_0x0071 << 4)] <= SRL()
		GGL[SM_0X0001 << 11] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 8] <= GGL()
		RL[SM_0X0001 << 4] <= GL()
		RL[~(sm_0x0071 << 4)] <= SRL()
		GGL[SM_0X0001 << 11] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 8] <= GGL()
		RL[~(SM_0X0707 << 8)] <= SRL()
		GGL[SM_0X0001 << 11] <= RL()
		GL[SM_0X0001 << 10] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 8] <= GGL()
		RL[~(SM_0XFFFF << 8)] <= SRL()
		RN_REG_T0[SM_0XFFFF << 14] <= RSP16()

	with apl_commands():
		RL[SM_0X001F << 3] <= (in_out() & RN_REG_T0()) | SRL()
		RL[SM_0X0001 << 2] <= (in_out() & RN_REG_T0()) | GL()
		RL[~(SM_0XFFFF << 2)] <= (in_out() & RN_REG_T0()) | SRL()
		RL[SM_0XFFFF << 8] <= in_out() & RN_REG_T0()

	with apl_commands():
		in_out[~(SM_0XFFFF << 14)] <= RL()
		RL[SM_0X0001 << 11+2] <= shift()
		GL[SM_0X0001 << 11+2] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 14)] <= INV_GL()
		RL[~(SM_0XFFFF << 14)] <= in_out() & ~INV_GL()

	with apl_commands():
		RL[~(SM_0XFFFF << 14-1)] <= SRL()

	with apl_commands():
		RL[~(SM_0XFFFF << 14-2)] <= SRL()

	with apl_commands():
		RL[~(SM_0XFFFF << 14-3)] <= SRL()

	with apl_commands():
		RL[~(SM_0XFFFF << 14-4)] <= (in_out() & RN_REG_T0()) | SRL()
		RL[SM_0X000F << 14-4] <= in_out() & RN_REG_T0()

	with apl_commands():
		in_out[~(SM_0XFFFF << 14)] <= RL()
		RL[SM_0X0001 << 11+1] <= shift()
		GL[SM_0X0001 << 11+1] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 14)] <= INV_GL()
		RL[~(SM_0XFFFF << 14)] <= in_out() & ~INV_GL()

	with apl_commands():
		RL[~(SM_0XFFFF << 14-1)] <= SRL()
		RL[SM_0X0001 << 14-1] <= in_out() & RN_REG_T0()

	with apl_commands():
		RL[~(SM_0XFFFF << 14-2)] <= (in_out() & RN_REG_T0()) | SRL()
		RL[SM_0X0001 << 14-2] <= in_out() & RN_REG_T0()

	with apl_commands():
		in_out[~(SM_0XFFFF << 14)] <= RL()
		RL[SM_0X0001 << 11+0] <= shift()
		GL[SM_0X0001 << 11+0] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 14)] <= INV_GL()
		RL[~(SM_0XFFFF << 14)] <= in_out() & ~INV_GL()

	with apl_commands():
		RL[~(SM_0XFFFF << 14-1)] <= (in_out() & RN_REG_T0()) | SRL()
		RL[SM_0X0001 << 14-1] <= in_out() & RN_REG_T0()

	with apl_commands():
		in_out[~(SM_0XFFFF << 14)] <= RL()
		RL[SM_0X0001 << 11+4] <= shift()
		GL[SM_0X0001 << 11+4] <= RL()

	with apl_commands():
		RL[~(SM_0XFFFF << 14)] <= in_out() & INV_GL()

	with apl_commands():
		in_out[~(SM_0XFFFF << 14)] <= RL()

	with apl_commands():
		RL[SM_0X0001] <= RN_REG_T1()

	with apl_commands():
		in_out[SM_0X0001] <= RL()

@belex_apl
def negate_mant_16_msb_sign_t1(Belex, mant_vr: VR):

	with apl_commands():
		RL[SM_0XFFFF] <= mant_vr()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[~(SM_0XFFFF << 15)] ^= GL()

	with apl_commands():
		RN_REG_T0[SM_0XFFFF] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 15] <= GL()
		RL[~(SM_0X0001 << 15)] &= GL()
		GGL[~(SM_0XFFFF << 3)] <= RL()
		GL[~(SM_0XFFFF << 11)] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 3] <= RN_REG_T0() & GGL()
		RL[SM_0X0001 << 11] <= RN_REG_T0() & GL()
		GL[~(SM_0XFFFF << 8)] <= RL()

	with apl_commands():
		RL[SM_0X0101 << 4] <= RN_REG_T0() & NRL()
		RL[SM_0X0001 << 8] <= RN_REG_T0() & GL()
		GL[~(SM_0XFFFF << 7)] <= RL()

	with apl_commands():
		RL[SM_0X1111 << 1] <= RN_REG_T0() & NRL()
		RL[SM_0X0001 << 7] <= RN_REG_T0() & GL()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0X1111 << 2] <= RN_REG_T0() & NRL()

	with apl_commands():
		RL[~SM_0X0001] <= RN_REG_T0() ^ NRL()
		RL[SM_0X0001] <= RN_REG_T0() ^ GL()

	with apl_commands():
		mant_vr[SM_0XFFFF] <= RL()

@belex_apl
def negate_mant_16_t1(Belex, mant_vr: VR, msb_sign: VR):

	with apl_commands():
		RL[SM_0X0001 << 15] <= msb_sign()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0XFFFF] <= mant_vr() ^ GL()

	with apl_commands():
		RN_REG_T0[SM_0XFFFF] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 15] <= GL()
		RL[~(SM_0X0001 << 15)] &= GL()
		GGL[~(SM_0XFFFF << 3)] <= RL()
		GL[~(SM_0XFFFF << 11)] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 3] <= RN_REG_T0() & GGL()
		RL[SM_0X0001 << 11] <= RN_REG_T0() & GL()
		GL[~(SM_0XFFFF << 8)] <= RL()

	with apl_commands():
		RL[SM_0X0101 << 4] <= RN_REG_T0() & NRL()
		RL[SM_0X0001 << 8] <= RN_REG_T0() & GL()
		GL[~(SM_0XFFFF << 7)] <= RL()

	with apl_commands():
		RL[SM_0X1111 << 1] <= RN_REG_T0() & NRL()
		RL[SM_0X0001 << 7] <= RN_REG_T0() & GL()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0X1111 << 2] <= RN_REG_T0() & NRL()

	with apl_commands():
		RL[~SM_0X0001] <= RN_REG_T0() ^ NRL()
		RL[SM_0X0001] <= RN_REG_T0() ^ GL()

	with apl_commands():
		mant_vr[SM_0XFFFF] <= RL()

@belex_apl
def _5b_on_10_to_14b_left_mask_t3(Belex, mask_vr: VR, _5b_vr: VR):

	with apl_commands():
		RL[SM_0X001F << 10+0] <= _5b_vr()
		GL[SM_0X0001 << 10+4] <= RL()

	with apl_commands():
		RL[SM_0X000F << 10+0] |= GL()
		GL[SM_0X0001 << 10+3] <= RL()

	with apl_commands():
		RN_REG_T0[SM_0X00FF] <= GL()
		RN_REG_T0[~SM_0X00FF] <= INV_GL()
		GL[SM_0X0001 << 10+2] <= RL()

	with apl_commands():
		RN_REG_T1[SM_0X0F0F] <= GL()
		RN_REG_T1[~SM_0X0F0F] <= INV_GL()
		GL[SM_0X0001 << 10+1] <= RL()

	with apl_commands():
		RN_REG_T2[SM_0X3333] <= GL()
		RN_REG_T2[~SM_0X3333] <= INV_GL()
		GL[SM_0X0001 << 10+0] <= RL()

	with apl_commands():
		RL[SM_0X5555] <= (RN_REG_T0() & RN_REG_T1() & RN_REG_T2()) & GL()
		RL[~SM_0X5555] <= (RN_REG_T0() & RN_REG_T1() & RN_REG_T2()) & INV_GL()

	with apl_commands():
		mask_vr[SM_0XFFFF] <= SRL()
		RL[SM_0XFFFF] <= SRL()

@belex_apl
def shift_left_data_size15_count_lsb10_t1(Belex, data: VR, count: VR):

	with apl_commands():
		RL[~(SM_0XFFFF << 15)] <= data()

	with apl_commands():
		data[~(SM_0XFFFF << 15)] <= INV_RL()
		RL[~(SM_0XFFFF << 15)] <= INV_RL()
		GL[SM_0X00FF << 15-8] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 15)] <= INV_GL()
		RL[~(SM_0XFFFF << 15-8)] &= ~INV_GL()
		count[SM_0X0001 << 10+3] <= INV_GL()
		GL[SM_0X0001] <= RL()

	with apl_commands():
		RL[SM_0X0001] <= ~RN_REG_T0() & INV_NRL()
		GL[SM_0X0001 << 1] <= RL()
		RL[SM_0X0001 << 8] <= (data() & RN_REG_T0()) | GL()

	with apl_commands():
		RL[SM_0X0001 << 1] <= NRL()
		GL[SM_0X0001 << 1+1] <= RL()
		RL[SM_0X0001 << 1+8] <= (data() & RN_REG_T0()) | GL()

	with apl_commands():
		RL[SM_0X0001 << 2] <= NRL()
		GL[SM_0X0001 << 2+1] <= RL()
		RL[SM_0X0001 << 2+8] <= (data() & RN_REG_T0()) | GL()

	with apl_commands():
		RL[SM_0X0001 << 3] <= NRL()
		GL[SM_0X0001 << 3+1] <= RL()
		RL[SM_0X0001 << 3+8] <= (data() & RN_REG_T0()) | GL()

	with apl_commands():
		RL[SM_0X0001 << 4] <= NRL()
		GL[SM_0X0001 << 4+1] <= RL()
		RL[SM_0X0001 << 4+8] <= (data() & RN_REG_T0()) | GL()

	with apl_commands():
		RL[SM_0X0001 << 5] <= NRL()
		GL[SM_0X0001 << 5+1] <= RL()
		RL[SM_0X0001 << 5+8] <= (data() & RN_REG_T0()) | GL()

	with apl_commands():
		RL[SM_0X0001 << 15-9] <= (data() & RN_REG_T0()) | NRL()
		RL[SM_0X0001 << 15-1] <= (data() & RN_REG_T0()) | GL()
		RL[~(SM_0XFFFF << 15-9)] <= (data() & RN_REG_T0()) | RL()
		GL[SM_0X000F << 15-4] <= RL()

	with apl_commands():
		data[~(SM_0XFFFF << 15)] <= RL()
		data[SM_0XFFFF << 15] <= RSP16()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 15)] <= INV_GL()
		count[SM_0X0001 << 10+2] <= INV_GL()
		RL[~(SM_0XFFFF << 15)] &= ~INV_GL()

	with apl_commands():
		RL[SM_0XFFFF << 1] <= NRL()
		RL[SM_0X0001] <= GL()

	with apl_commands():
		RL[SM_0XFFFF << 1] <= NRL()

	with apl_commands():
		RL[SM_0XFFFF << 2] <= NRL()

	with apl_commands():
		RL[~SM_0X0001] <= (data() & RN_REG_T0()) | NRL()
		RL[SM_0X0001] <= (data() & RN_REG_T0()) | GL()
		GL[SM_0X0001 << 15-2] <= RL()
		GL[SM_0X0001 << 15-1] <= RL()

	with apl_commands():
		data[~(SM_0XFFFF << 15)] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 15)] <= INV_GL()
		count[SM_0X0001 << 10+1] <= INV_GL()
		RL[~(SM_0XFFFF << 15)] &= ~INV_GL()

	with apl_commands():
		RL[~SM_0X0001] <= NRL()
		RL[SM_0X0001] <= GL()

	with apl_commands():
		RL[~SM_0X0001] <= (data() & RN_REG_T0()) | NRL()
		RL[SM_0X0001] <= (data() & RN_REG_T0()) | GL()
		GL[SM_0X0001 << 15-1] <= RL()

	with apl_commands():
		data[~(SM_0XFFFF << 15)] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 15)] <= INV_GL()
		count[SM_0X0001 << 10+0] <= INV_GL()
		RL[~(SM_0XFFFF << 15)] &= ~INV_GL()

	with apl_commands():
		RL[~SM_0X0001] <= (data() & RN_REG_T0()) | NRL()
		RL[SM_0X0001] <= (data() & RN_REG_T0()) | GL()

	with apl_commands():
		data[~(SM_0XFFFF << 15)] <= INV_RL()
		RL[SM_0X000F << 10] <= count()
		RL[~(SM_0X000F << 10)] <= 1

	with apl_commands():
		count[SM_0XFFFF] <= INV_RL()
		RL[SM_0XFFFF] <= INV_RL()

@belex_apl
def simple_sub_6msb_frag_t2(Belex, res: VR, x: VR, y: VR):

	with apl_commands():
		RL[SM_0XFFFF << 10] <= x()

	with apl_commands():
		RL[SM_0XFFFF << 10] ^= y()
		RN_REG_T0[SM_0XFFFF << 10] <= INV_RL()

	with apl_commands():
		RN_REG_T1[SM_0XFFFF << 10] <= INV_RL()
		RL[SM_0XFFFF << 10] <= RN_REG_T0() & y()

	with apl_commands():
		RL[SM_0X0001 << 11] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 12] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 13] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 14] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0XFFFF << 11] <= RN_REG_T1() ^ INV_NRL()
		RL[SM_0X0001 << 10] <= RN_REG_T1() ^ INV_RSP16()

	with apl_commands():
		res[SM_0XFFFF << 10] <= RL()

@belex_apl
def swap_if_x_bigger_than_y(Belex, x: VR, y: VR):

	with apl_commands(" set GL if x > y"):
		RL[SM_0X001F << 10] <= y()
		RL[~(SM_0X001F << 10)] <= 1

	with apl_commands():
		RL[SM_0X001F << 10] <= x() ^ INV_RL()
		GL[SM_0X0001 << 10+4] <= RL()

	with apl_commands():
		RL[SM_0X000F << 10] &= SRL()

	with apl_commands():
		RL[SM_0X000F << 10+1] &= SRL()
		RL[SM_0X0001 << 10] |= INV_GL()

	with apl_commands():
		RL[SM_0X001F << 10] |= INV_SRL()

	with apl_commands():
		RL[SM_0X001F << 10] |= x()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands(" swap when  GL == 1 dst_x,    dst_y"):
		RL[SM_0XFFFF] <= y() & GL()
		RN_REG_T1[SM_0XFFFF] <= GL()

	with apl_commands():
		RL[SM_0XFFFF] |= x() & INV_GL()

	with apl_commands():
		RN_REG_T0[SM_0XFFFF] <= RL()
		RL[SM_0XFFFF] <= RN_REG_T1() & x()

	with apl_commands():
		RL[SM_0XFFFF] |= y() & INV_GL()

	with apl_commands():
		RN_REG_T1[SM_0XFFFF] <= RL()

@belex_apl
def move_small_shift_to_5msb_and_limit_it_to_16(Belex, shift: VR):

	with apl_commands():
		RL[SM_0X001F << 10] <= shift()
		GL[SM_0X0001 << 10+4] <= RL()

	with apl_commands():
		RL[SM_0X000F << 10] &= INV_GL()

	with apl_commands():
		shift[SM_0X001F << 11] <= NRL()
		shift[SM_0X0001 << 10] <= RSP16()

@belex_apl
def calc_res_sgn(Belex, sgn_res: VR, mant_res: VR, sgn_data: VR):

	with apl_commands():
		RL[SM_0X0001 << 15] <= mant_res()
		GGL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 15] <= sgn_data() | INV_GGL()
		RL[SM_0X3333 << 13] <= sgn_data() | GGL()
		GGL[SM_0XFFFF << 13] <= RL()

	with apl_commands():
		sgn_res[SM_0X0001 << 15] <= INV_GGL()

@belex_apl
def add_mask_to_mant(Belex, mant: VR, mask: VR, mrk: Mask):

	with apl_commands():
		RL[SM_0XFFFF] <= mask()

	with apl_commands():
		RL[SM_0XFFFF] <= mant() | INV_RL()
		GL[SM_0XFFFF] <= RL()

	with apl_commands():
		RN_REG_FLAGS[mrk] <= INV_GL()
		RL[SM_0XFFFF] <= mask()

	with apl_commands():
		mant[SM_0XFFFF] |= RL()

@belex_apl
def remove_mask_from_mant(Belex, mant: VR, mask: VR, mrk: Mask):

	with apl_commands():
		RL[mrk] <= RN_REG_FLAGS()
		GL[mrk] <= RL()

	with apl_commands():
		RL[SM_0XFFFF] <= mask() & GL()

	with apl_commands():
		RL[SM_0XFFFF] ^= mant()

	with apl_commands():
		mant[SM_0XFFFF] <= RL()

#------------------------------------------------------------------
#FIXME: BUG
@belex_apl
def add_f16_dbg(Belex):
	with apl_commands():
		RL[SM_0XFFFF] <= RSP16()
	with apl_commands():		
		RL[SM_0X0001 << 13] <= INV_RSP16()
		RL[SM_0X0001 << 14] <= INV_RSP16()
	with apl_commands():		
		GGL[SM_0X0001 << 12] <= RL()
		GGL[SM_0X0001 << 13] <= RL()
	with apl_commands():		
		GL[SM_0X0001 << 12] <= RL()
		GL[SM_0X0001 << 13] <= RL()
	Belex.glass(RL, plats = range(0, 16, 1), order = "lsb")		
	Belex.glass(GGL, plats = range(0, 16, 1), order = "lsb")	
	Belex.glass(GL, plats = range(0, 16, 1), order = "lsb")	
	with apl_commands():
		NOOP()				

@belex_apl
def mul_f16_dbg(Belex):
	with apl_commands():
		RL[SM_0XFFFF] <= RSP16()
	with apl_commands():		
		RL[SM_0X0001 << 10] <= INV_RSP16()
		RL[SM_0X0001 << 11] <= INV_RSP16()	
	with apl_commands():		
		RN_REG_T0[SM_0XFFFF] <= RL() 
	with apl_commands():		
		RL[SM_0XFFFF << 9] <= RN_REG_T0()
		#FIXME: BUG
		GGL[SM_0X0001 << 9] <= RL()
		GGL[SM_0X0001 << 10] <= RL()
		GL[SM_0X000F << 9] <= RL()
	Belex.glass(RL, plats = range(0, 16, 1), order = "lsb")		
	Belex.glass(GGL, plats = range(0, 16, 1), order = "lsb")	
	with apl_commands():		
		GGL[SM_0X0001 << 9] <= RL()
		GGL[SM_0X0001 << 10] <= RL()
	Belex.glass(GGL, plats = range(0, 16, 1), order = "lsb")	
	with apl_commands():
		NOOP()				

#------------------------------------------------------------------

@belex_apl
def round_mant_and_inc_exp(Belex, sgn_res: VR, mant: VR, exp: VR):

	with apl_commands():
		RL[~(SM_0XFFFF << 3)] <= ~mant() & INV_RSP16()
		GL[~(SM_0XFFFF << 3)] <= RL()
		RL[SM_0X0001 << 4] <= mant()
	with apl_commands():
		RL[SM_0X0001 << 4] |= INV_GL()
		GL[SM_0X0001 << 4] <= RL()
		RN_REG_T0[SM_0X0001 << 3] <= SRL()
		RL[~(SM_0X0001 << 4)] <= mant()
	with apl_commands():
		RL[~(SM_0X0001 << 3)] <= SRL()
		RL[SM_0X0001 << 3] <= RN_REG_T0()
		RN_REG_T1[SM_0X0001 << 3] <= GL()
	
	with apl_commands():
		RL[SM_0XFFFF] <= SRL()

	with apl_commands():
		mant[SM_0XFFFF] <= SRL()

	with apl_commands():
		RL[~(SM_0X0001 << 15)] <= mant() & GL()
		RL[SM_0X0001 << 15] <= GL()
		GGL[~(SM_0XFFFF << 3)] <= RL()
		GL[~(SM_0XFFFF << 11)] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 3] <= mant() & GGL()
		RL[SM_0X0001 << 11] <= mant() & GL()
		GL[~(SM_0XFFFF << 8)] <= RL()

	with apl_commands():
		RL[SM_0X0101 << 4] <= mant() & NRL()
		RL[SM_0X0001 << 8] <= mant() & GL()
		GL[~(SM_0XFFFF << 7)] <= RL()

	with apl_commands():
		RL[SM_0X1111 << 1] <= mant() & NRL()
		RL[SM_0X0001 << 7] <= mant() & GL()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0X1111 << 2] <= mant() & NRL()

	with apl_commands():
		RL[~SM_0X0001] <= mant() ^ NRL()
		RL[SM_0X0001] <= mant() ^ GL()

	with apl_commands():
		mant[SM_0XFFFF] <= SRL()
		RL[SM_0XFFFF] <= exp()

	with apl_commands(" increment exp 6 bits"):
		RL[SM_0XFFFF << 10] <= exp()
		GL[SM_0X0001 << 10] <= RL()
		GL[SM_0X0001 << 11] <= RL()
		GL[SM_0X0001 << 12] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 11] <= exp() & NRL()
		RL[SM_0X0001 << 13] <= exp() & GL()

	with apl_commands():
		RL[SM_0X0001 << 12] <= exp() & NRL()
		RL[SM_0X0001 << 14] <= exp() & NRL()

	with apl_commands():
		RL[SM_0XFFFF << 11] <= exp() ^ NRL()
		RL[SM_0X0001 << 10] <= exp() ^ INV_RSP16()

	with apl_commands():
		exp[SM_0XFFFF << 10] <= RL()

	with apl_commands(" if (res_mant == 0x800) {"):
		RL[SM_0X0001 << 11] <= mant()
		RL[~(SM_0X0001 << 11)] <= ~mant() & INV_RSP16()
		GL[SM_0XFFFF] <= RL()

	with apl_commands(" res_mant = 0x400;"):
		RL[SM_0X0001 << 10] <= mant() | GL()
		RL[~(SM_0X0001 << 10)] <= mant() & INV_GL()

	with apl_commands():
		mant[SM_0XFFFF] <= RL()

	with apl_commands(" res_exp++;"):
		RL[SM_0XFFFF << 10] <= exp() & GL()
		GL[SM_0X0001 << 10] <= RL()
		GL[SM_0X0001 << 11] <= RL()
		RL[SM_0X0001 << 9] <= GL()

	with apl_commands():
		RL[SM_0X0001 << 11] <= exp() & NRL()
		RL[SM_0X0001 << 12] <= exp() & GL()
		GGL[SM_0X0001 << 12] <= RL()
		GGL[SM_0X0001 << 13] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 13] <= exp() & NRL()
		RL[SM_0X0001 << 14] <= exp() & GGL()

	with apl_commands():
		RL[SM_0XFFFF << 10] <= exp() ^ NRL()
		RL[SM_0X0001 << 9] <= RSP16()

	with apl_commands(" }"):
		exp[SM_0XFFFF << 10] <= RL()
		#if (!(res_mant & (1 << 10)))
# res_exp = 0;
		RL[SM_0X0001 << 10] <= mant()
		GL[SM_0X0001 << 10] <= RL()

	with apl_commands():
		RL[SM_0X001F << 10] <= exp() & GL()
		RL[SM_0X0001 << 15] <= sgn_res()

	with apl_commands():
		exp[SM_0X001F << 10] <= RL()
		#build result
		mant[SM_0X001F << 10] <= RL()
		mant[SM_0X0001 << 15] <= RL()

########################################################################	

# @belex_apl
# def round_mant_and_inc_exp_00(Belex, sgn_res: VR, mant: VR, exp: VR):
# 	Belex.glass(sgn_res, plats = range(0, 16, 1), order = "lsb")
# 	Belex.glass(mant, plats = range(0, 16, 1), order = "lsb")
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")
# 	with apl_commands():
# 		RL[~(SM_0XFFFF << 3)] <= ~mant() & INV_RSP16()
# 		GL[~(SM_0XFFFF << 3)] <= RL()
# 		RL[SM_0X0001 << 4] <= mant()
# 	#Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")
# 	with apl_commands():
# 		RL[SM_0X0001 << 4] |= INV_GL()
# 		GL[SM_0X0001 << 4] <= RL()
# 		RN_REG_T0[SM_0X0001 << 3] <= SRL()
# 		RL[~(SM_0X0001 << 4)] <= mant()
# 	#Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")
# 	with apl_commands():
# 		RL[~(SM_0X0001 << 3)] <= SRL()
# 		RL[SM_0X0001 << 3] <= RN_REG_T0()
# 		RN_REG_T1[SM_0X0001 << 3] <= GL()
# 	#Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")
# 	with apl_commands():
# 		RL[SM_0XFFFF] <= SRL()

# 	with apl_commands():
# 		mant[SM_0XFFFF] <= SRL()

# 	with apl_commands():
# 		RL[~(SM_0X0001 << 15)] <= mant() & GL()
# 		RL[SM_0X0001 << 15] <= GL()
# 		GGL[~(SM_0XFFFF << 3)] <= RL()
# 		GL[~(SM_0XFFFF << 11)] <= RL()

# 	with apl_commands():
# 		RL[SM_0X0001 << 3] <= mant() & GGL()
# 		RL[SM_0X0001 << 11] <= mant() & GL()
# 		GL[~(SM_0XFFFF << 8)] <= RL()

# 	with apl_commands():
# 		RL[SM_0X0101 << 4] <= mant() & NRL()
# 		RL[SM_0X0001 << 8] <= mant() & GL()
# 		GL[~(SM_0XFFFF << 7)] <= RL()

# 	with apl_commands():
# 		RL[SM_0X1111 << 1] <= mant() & NRL()
# 		RL[SM_0X0001 << 7] <= mant() & GL()
# 		GL[SM_0X0001 << 15] <= RL()

# 	with apl_commands():
# 		RL[SM_0X1111 << 2] <= mant() & NRL()

# 	with apl_commands():
# 		RL[~SM_0X0001] <= mant() ^ NRL()
# 		RL[SM_0X0001] <= mant() ^ GL()

# 	with apl_commands():
# 		mant[SM_0XFFFF] <= SRL()
# 		RL[SM_0XFFFF] <= exp()

# @belex_apl
# def round_mant_and_inc_exp_01(Belex, sgn_res: VR, mant: VR, exp: VR):
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")
# 	with apl_commands(" increment exp 6 bits"):
# 		RL[SM_0XFFFF << 10] <= exp()
# 		GL[SM_0X0001 << 10] <= RL()
# 		GL[SM_0X0001 << 11] <= RL()
# 		GL[SM_0X0001 << 12] <= RL()

# 	with apl_commands():
# 		RL[SM_0X0001 << 11] <= exp() & NRL()
# 		RL[SM_0X0001 << 13] <= exp() & GL()

# 	with apl_commands():
# 		RL[SM_0X0001 << 12] <= exp() & NRL()
# 		RL[SM_0X0001 << 14] <= exp() & NRL()

# 	with apl_commands():
# 		RL[SM_0XFFFF << 11] <= exp() ^ NRL()
# 		RL[SM_0X0001 << 10] <= exp() ^ INV_RSP16()

# 	with apl_commands():
# 		exp[SM_0XFFFF << 10] <= RL()
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")

# @belex_apl
# def round_mant_and_inc_exp_10(Belex, sgn_res: VR, mant: VR, exp: VR):
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")
# 	with apl_commands(" if (res_mant == 0x800) {"):
# 		RL[SM_0X0001 << 11] <= mant()
# 		RL[~(SM_0X0001 << 11)] <= ~mant() & INV_RSP16()
# 		GL[SM_0XFFFF] <= RL()

# 	with apl_commands(" res_mant = 0x400;"):
# 		RL[SM_0X0001 << 10] <= mant() | GL()
# 		RL[~(SM_0X0001 << 10)] <= mant() & INV_GL()

# 	with apl_commands():
# 		mant[SM_0XFFFF] <= RL()

# @belex_apl
# def add_f16_dbg(Belex):
# 	with apl_commands():
# 		RL[SM_0XFFFF] <= RSP16()
# 	with apl_commands():		
# 		RL[SM_0X0001 << 13] <= INV_RSP16()
# 		RL[SM_0X0001 << 14] <= INV_RSP16()
# 	with apl_commands():		
# 		GGL[SM_0X0001 << 12] <= RL()
# 		GGL[SM_0X0001 << 13] <= RL()
# 	with apl_commands():		
# 		GL[SM_0X0001 << 12] <= RL()
# 		GL[SM_0X0001 << 13] <= RL()


# 	Belex.glass(RL, plats = range(0, 16, 1), order = "lsb")		
# 	Belex.glass(GGL, plats = range(0, 16, 1), order = "lsb")	
# 	Belex.glass(GL, plats = range(0, 16, 1), order = "lsb")	
# 	with apl_commands():
# 		NOOP()				

# @belex_apl
# def round_mant_and_inc_exp_11(Belex, sgn_res: VR, mant: VR, exp: VR):
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")
# 	Belex.glass(GL, plats = range(0, 16, 1), order = "lsb")
# 	with apl_commands(" res_exp++;"):
# 		RL[SM_0XFFFF << 10] <= exp() & GL()
# 		GL[SM_0X0001 << 10] <= RL()
# 		GL[SM_0X0001 << 11] <= RL()
# 		RL[SM_0X0001 << 9] <= GL()

# 	Belex.glass(RL, plats = range(0, 16, 1), order = "lsb")		
# 	with apl_commands():
# 		RL[SM_0X0001 << 11] <= exp() & NRL()
# 		RL[SM_0X0001 << 12] <= exp() & GL()
# 	# with apl_commands():		
# 	# 	GGL[SM_0X0001 << 12] <= RL()
# 	# 	GGL[SM_0X0001 << 13] <= RL()
# 	with apl_commands():		
# 		GL[SM_0X0001 << 12] <= RL()
# 		GL[SM_0X0001 << 13] <= RL()


# 	Belex.glass(RL, plats = range(0, 16, 1), order = "lsb")		
# 	Belex.glass(GGL, plats = range(0, 16, 1), order = "lsb")		
# 	# with apl_commands():
# 	# 	RL[SM_0X0001 << 13] <= exp() & NRL()
# 	# 	RL[SM_0X0001 << 14] <= exp() & GGL()
# 	with apl_commands():
# 		RL[SM_0X0001 << 13] <= exp() & NRL()
# 		RL[SM_0X0001 << 14] <= exp() & GL()	

# 	Belex.glass(RL, plats = range(0, 16, 1), order = "lsb")		
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")		
# 	with apl_commands():
# 		RL[SM_0XFFFF << 10] <= exp() ^ NRL()
# 		RL[SM_0X0001 << 9] <= RSP16()
# 	Belex.glass(RL, plats = range(0, 16, 1), order = "lsb")				
# 	#Debug		
# 	with apl_commands():
# 		exp[SM_0XFFFF << 10] <= RL()
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")	
# 	#Belex.glass(RL, plats = range(0, 16, 1), order = "lsb")	
# 	#Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")	

# @belex_apl
# def round_mant_and_inc_exp_20(Belex, sgn_res: VR, mant: VR, exp: VR):
# 	with apl_commands(" }"):
# 		exp[SM_0XFFFF << 10] <= RL()
# 		#if (!(res_mant & (1 << 10)))
# # res_exp = 0;
# 		RL[SM_0X0001 << 10] <= mant()
# 		GL[SM_0X0001 << 10] <= RL()
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")	
# 	with apl_commands():
# 		RL[SM_0X001F << 10] <= exp() & GL()
# 		RL[SM_0X0001 << 15] <= sgn_res()

# 	with apl_commands():
# 		exp[SM_0X001F << 10] <= RL()
# 		#build result
# 		mant[SM_0X001F << 10] <= RL()
# 		mant[SM_0X0001 << 15] <= RL()
# 	#For debug
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")		
# 	Belex.glass(sgn_res, plats = range(0, 16, 1), order = "lsb")		
# 	with apl_commands():
# 		NOOP()
# 	Belex.glass(exp, plats = range(0, 16, 1), order = "lsb")
# 	Belex.glass(sgn_res, plats = range(0, 16, 1), order = "lsb")				

########################################################################	

@belex_apl
def build_res(Belex, res: VR, f16_x: VR, f16_y: VR, mant: VR, exp: VR):
	
	with apl_commands():
		RL[~(SM_0XFFFF << 15)] <= f16_x() ^ INV_RSP16()
		RL[SM_0XFFFF << 15] <= f16_x() ^ RSP16()

	with apl_commands():
		RL[SM_0XFFFF] ^= f16_y()
		GL[SM_0XFFFF] <= RL()

	with apl_commands():
		RL[SM_0XFFFF << 15] <= mant() & INV_GL()

	with apl_commands():
		mant[SM_0XFFFF << 15] <= RL()
		RL[SM_0XFFFF << 10] <= f16_x()
		RL[~(SM_0XFFFF << 10)] <= ~f16_x() & INV_RSP16()
		GL[~(SM_0XFFFF << 10)] <= RL()

	with apl_commands():
		RN_REG_T1[SM_0X0001] <= INV_GL()
		GL[SM_0X001F << 10] <= RL()
		RL[~(SM_0XFFFF << 10)] <= 1

	with apl_commands():
		RN_REG_T0[SM_0XFFFF] <= GL()
		RL[SM_0XFFFF << 10] <= f16_y()
		RL[~(SM_0XFFFF << 10)] ^= f16_y()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		RN_REG_T0[SM_0X0001 << 1] <= GL()
		RN_REG_T1[SM_0X0001 << 15] <= GL()
		GL[~(SM_0XFFFF << 10)] <= RL()

	with apl_commands():
		RN_REG_T1[SM_0X0001 << 1] <= INV_GL()
		#if ((res_exp >= 0x1f) || (x.exponent == 0X1f) || (y.exponent == 0X1f))
		RL[SM_0XFFFF << 10] <= exp()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 15] <= ~exp() & INV_GL()
		GL[SM_0X0001 << 15] <= RL()
		RL[~(SM_0XFFFF << 2)] <= ~RN_REG_T0() & INV_RSP16()
		GL[~(SM_0XFFFF << 2)] <= RL()

	with apl_commands():
		RL[SM_0X001F << 10] <= mant() | INV_GL()
		RL[~(SM_0XFFFF << 10)] <= mant() & GL()
		RL[SM_0X0001 << 15] <= f16_x() & RN_REG_T0() & RN_REG_T1()

	with apl_commands():
		mant[~(SM_0XFFFF << 15)] <= RL()
		#if (((x.exponent == 0X1f) && x.mantissa) || x is nan
#     ((y.exponent == 0X1f) && y.mantissa) || y is nan
#     ((x.exponent == 0X1f) && y.exponent == 0X1f && (x.sign ^ y.sign)) inf + -inf (or -inf + inf)
		RL[SM_0X0001 << 15] ^= f16_y() & RN_REG_T0() & RN_REG_T1()
		RL[~(SM_0XFFFF << 2)] <= RN_REG_T0()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[~(SM_0XFFFF << 2)] <= ~(RN_REG_T0() & RN_REG_T1()) & INV_GL()
		GL[~(SM_0XFFFF << 2)] <= RL()

	with apl_commands():
		RL[~(SM_0X0001 << 15)] <= mant() | INV_GL()
		RL[SM_0X0001 << 15] <= mant()

	with apl_commands():
		res[SM_0XFFFF] <= RL()

##################################################

@belex_apl
def addsub_frag_add_no_co_u16(Belex, x: VR, y: VR, res: VR, x_xor_y: VR, cout1: VR):

	with apl_commands():
		RL[SM_0XFFFF] <= x()

	with apl_commands():
		RL[SM_0XFFFF] ^= y()
		GGL[SM_0X3333 << 4] <= RL()

	with apl_commands():
		x_xor_y[~SM_0X1111] <= RL()
		SM_0X1111[x_xor_y,cout1] <= RL()
		RL[SM_0X3333 << 4] <= x() & y()
		RL[SM_0X000F] <= x() & y()

	with apl_commands():
		RL[SM_0X1111 << 6] <= x_xor_y() & GGL()
		RL[SM_0X1111 << 1] |= x_xor_y() & NRL()

	with apl_commands():
		cout1[SM_0X1111 << 6] <= RL()
		RL[SM_0X1111 << 7] <= x_xor_y() & NRL()
		RL[SM_0X1111 << 6] <= x() & y()
		RL[SM_0X0001 << 2] |= x_xor_y() & NRL()

	with apl_commands():
		cout1[SM_0X1111 << 7] <= RL()
		RL[SM_0X1111 << 7] <= x() & y()
		RL[SM_0X1111 << 6] |= x_xor_y() & NRL()
		RL[SM_0X0001 << 3] |= x_xor_y() & NRL()
		GL[SM_0X0001 << 3] <= RL()

	with apl_commands():
		cout1[SM_0X1111 << 5] <= GGL()
		RL[SM_0X1111 << 7] |= x_xor_y() & NRL()

	with apl_commands():
		RL[SM_0X000F << 4] |= cout1() & GL()
		GL[SM_0X0001 << 7] <= RL()

	with apl_commands():
		RL[SM_0X000F << 8] |= cout1() & GL()
		GL[SM_0X0001 << 11] <= RL()

	with apl_commands():
		RL[SM_0X000F << 12] |= cout1() & GL()

	with apl_commands():
		RL[SM_0XFFFF] <= x_xor_y() ^ NRL()

	with apl_commands():
		res[SM_0XFFFF] <= RL()


##################################################
# inl_sub_f16_t7 fragments

@belex_apl
def negate_y0(Belex, y: VR):

	with apl_commands():
		RL[SM_0XFFFF << 15] <= y() ^ INV_RSP16()

	with apl_commands():
		y[SM_0XFFFF << 15] <= RL()

@belex_apl
def negate_y1(Belex, y: VR):

	with apl_commands():
		RL[SM_0XFFFF << 15] <= y() ^ INV_RSP16()

	with apl_commands():
		y[SM_0XFFFF << 15] <= RL()

@belex_apl
def handle_x_minus_x(Belex, x: VR, res: VR):

	with apl_commands():
		RL[SM_0X001F << 10] <= x()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		res[~(SM_0XFFFF << 15)] <= GL()
		res[SM_0XFFFF << 15] <= RSP16()
##################################################
# inl_mul_f16_t6  fragments

@belex_apl
def is_small_f16(Belex, f16: VR):

	with apl_commands():
		RL[SM_0X001F << 10] <= ~f16() & INV_RSP16()
		GL[SM_0X001F << 10] <= RL()

@belex_apl
def swap_and_set_mrk_t1(Belex, x: VR, y: VR, mrk: Mask):

	with apl_commands():
		RL[SM_0XFFFF] <= y() & GL()
		RN_REG_FLAGS[mrk] <= GL()

	with apl_commands():
		RL[SM_0XFFFF] ^= x() & GL()

	with apl_commands():
		RN_REG_T0[SM_0XFFFF] <= RL()
		RL[SM_0XFFFF] ^= x()

	with apl_commands():
		x[SM_0XFFFF] <= RL()
		RL[SM_0XFFFF] <= RN_REG_T0()

	with apl_commands():
		RL[SM_0XFFFF] ^= y()

	with apl_commands():
		y[SM_0XFFFF] <= RL()

@belex_apl
def create_and_shift_left_tmpx_t1(Belex, f16_x: VR, mant_x_and_shift: VR):

	with apl_commands():
		RL[~(SM_0X0001 << 15)] <= ~f16_x() & INV_RSP16()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		mant_x_and_shift[~(SM_0XFFFF << 10)] <= RL()
		mant_x_and_shift[SM_0XFFFF << 10] <= GL()
		RL[SM_0X0001 << 10] <= GL()
		#GL = 1 (shift 8 needed)
		GL[SM_0X00FF << 3] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 11)] <= INV_GL()
		RL[~(SM_0XFFFF << 3)] &= ~INV_GL()
		mant_x_and_shift[SM_0X0001 << 15] <= GL()
		GL[SM_0X0001] <= RL()

	with apl_commands():
		RL[SM_0X0001] <= ~RN_REG_T0() & INV_NRL()
		GL[SM_0X0001 << 1] <= RL()
		RL[SM_0X0001 << 8] <= (mant_x_and_shift() & RN_REG_T0()) | GL()

	with apl_commands():
		RL[SM_0X0001 << 1] <= NRL()
		GL[SM_0X0001 << 2] <= RL()
		RL[SM_0X0001 << 9] <= (mant_x_and_shift() & RN_REG_T0()) | GL()

	with apl_commands():
		RL[SM_0X0001 << 2] <= (mant_x_and_shift() & RN_REG_T0()) | NRL()
		RL[SM_0X0001 << 10] <= (mant_x_and_shift() & RN_REG_T0()) | GL()
		RL[~(SM_0XFFFF << 2)] <= (mant_x_and_shift() & RN_REG_T0()) | RL()
		#GL = 1 (shift 4 needed)
		GL[SM_0X000F << 7] <= RL()

	with apl_commands():
		mant_x_and_shift[~(SM_0XFFFF << 11)] <= RL()
		mant_x_and_shift[SM_0X0001 << 14] <= GL()
		mant_x_and_shift[SM_0X0001 << 11] <= RSP16()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 11)] <= INV_GL()
		RL[~(SM_0XFFFF << 11)] &= ~INV_GL()

	with apl_commands():
		RL[SM_0X00FF << 1] <= NRL()
		RL[SM_0X0001] <= GL()

	with apl_commands():
		RL[SM_0X00FF << 1] <= NRL()

	with apl_commands():
		RL[SM_0X00FF << 2] <= NRL()

	with apl_commands():
		RL[~SM_0X0001] <= (mant_x_and_shift() & RN_REG_T0()) | NRL()
		RL[SM_0X0001] <= (mant_x_and_shift() & RN_REG_T0()) | GL()
		#GL = 1 (shift 2 needed)
		GL[SM_0X0001 << 9] <= RL()
		GL[SM_0X0001 << 10] <= RL()

	with apl_commands():
		mant_x_and_shift[~(SM_0XFFFF << 11)] <= RL()
		mant_x_and_shift[SM_0X0001 << 13] <= GL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 11)] <= INV_GL()
		RL[~(SM_0XFFFF << 11)] &= ~INV_GL()

	with apl_commands():
		RL[~SM_0X0001] <= NRL()
		RL[SM_0X0001] <= GL()

	with apl_commands():
		RL[~SM_0X0001] <= (mant_x_and_shift() & RN_REG_T0()) | NRL()
		RL[SM_0X0001] <= (mant_x_and_shift() & RN_REG_T0()) | GL()
		#GL = 1 (shift 1 needed)
		GL[SM_0X0001 << 10] <= RL()

	with apl_commands():
		mant_x_and_shift[~(SM_0XFFFF << 11)] <= RL()
		mant_x_and_shift[SM_0X0001 << 12] <= GL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 11)] <= INV_GL()
		RL[~(SM_0XFFFF << 11)] &= ~INV_GL()

	with apl_commands():
		RL[~SM_0X0001] <= (mant_x_and_shift() & RN_REG_T0()) | NRL()
		RL[SM_0X0001] <= (mant_x_and_shift() & RN_REG_T0()) | GL()

	with apl_commands():
		mant_x_and_shift[~(SM_0XFFFF << 11)] <= INV_RL()

@belex_apl
def init_mul_11_from_f16(Belex, f16_y: VR, mnt_x: VR, s0: VR, s1: VR, _2x_sticky: VR, m0: VR, c_xor_s: VR):

	with apl_commands(" input: mant_x, f16_y  output:s0 = (y[0])(x >> 1) RL = (y[1])(x) _2x = (x << 1) m0 = (y[2])(0xffff)"):
		RL[SM_0XFFFF] <= mnt_x()
		SM_0XFFFF[s0,s1,c_xor_s] <= RSP16()
		_2x_sticky[SM_0XFFFF << 12] <= RSP16()

	with apl_commands():
		_2x_sticky[~(SM_0XFFFF << 12)] <= NRL()
		RL[SM_0X0001] <= f16_y()
		GL[SM_0X0001] <= RL()
		c_xor_s[SM_0XFFFF << 12] <= RL()

	with apl_commands():
		RL[~SM_0X0001] <= mnt_x() & GL()
		RL[SM_0X0001] <= mnt_x() & f16_y()

	with apl_commands():
		s0[~(SM_0XFFFF << 10)] <= SRL()
		RL[SM_0X0001 << 2] <= f16_y()
		GL[SM_0X0001 << 2] <= RL()

	with apl_commands():
		_2x_sticky[SM_0X0001] <= RL()
		m0[SM_0XFFFF << 1] <= GL()
		RL[SM_0X0001 << 1] <= f16_y()
		GL[SM_0X0001 << 1] <= RL()

	with apl_commands():
		RL[~(SM_0XFFFF << 11)] <= mnt_x() & GL()
		RL[SM_0XFFFF << 11] <= RSP16()

@belex_apl
def _3to2_mul_f16(Belex, c: VR, s0: VR, s1: VR, _2x_sticky: VR, m0: VR, c_xor_s: VR, f16_y: VR, i_msk: Mask):

	with apl_commands():
		c[~(SM_0XFFFF << 12)] <= RL()
		RL[~(SM_0XFFFF << 11)] ^= s0()

	with apl_commands():
		c_xor_s[~(SM_0XFFFF << 11)] <= RL()
		RL[~SM_0X0001] ^= _2x_sticky() & m0()
		RL[SM_0X0001] |= _2x_sticky()

	with apl_commands():
		s1[~(SM_0XFFFF << 11)] <= SRL()
		RL[i_msk << 2] <= f16_y()
		GL[i_msk << 2] <= RL()

	with apl_commands():
		_2x_sticky[SM_0X0001] <= RL()
		RL[SM_0X0001 << 15] <= GL()
		#~(SM_0XFFFF << 12):SB[m1] = GL;
		RL[SM_0X00FF << 1] <= _2x_sticky() & m0() & c_xor_s()
		RL[SM_0X000F << 8] <= _2x_sticky() & m0() & c_xor_s()

	with apl_commands():
		RL[SM_0X0001] <= c() & s0()
		RL[SM_0X00FF << 1] |= c() & s0()
		RL[SM_0X000F << 8] |= c() & s0()
		m0[~(SM_0XFFFF << 12)] <= GL()

@belex_apl
def _3to2_mul_f16_before_last(Belex, c: VR, s0: VR, s1: VR, _2x_sticky: VR, m0: VR, c_xor_s: VR, f16_y: VR):

	with apl_commands():
		c[~(SM_0XFFFF << 12)] <= RL()
		RL[~(SM_0XFFFF << 11)] ^= s0()

	with apl_commands():
		c_xor_s[~(SM_0XFFFF << 11)] <= RL()
		RL[~SM_0X0001] ^= _2x_sticky() & m0()
		RL[SM_0X0001] |= _2x_sticky()

	with apl_commands():
		s1[~(SM_0XFFFF << 11)] <= SRL()

	with apl_commands():
		RL[SM_0X001F << 10] <= ~f16_y() & INV_RSP16()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		_2x_sticky[SM_0X0001] <= RL()
		RL[SM_0X0001 << 15] <= GL()
		#~(SM_0XFFFF << 12):SB[m1] = GL;
		RL[SM_0X00FF << 1] <= _2x_sticky() & m0() & c_xor_s()
		RL[SM_0X000F << 8] <= _2x_sticky() & m0() & c_xor_s()

	with apl_commands():
		RL[SM_0X0001] <= c() & s0()
		RL[SM_0X00FF << 1] |= c() & s0()
		RL[SM_0X000F << 8] |= c() & s0()
		m0[~(SM_0XFFFF << 12)] <= INV_GL()

@belex_apl
def _3to2_mul_f16_last(Belex, c: VR, s0: VR, s1: VR, _2x_sticky: VR, m0: VR, c_xor_s: VR):

	with apl_commands():
		c[~(SM_0XFFFF << 12)] <= RL()
		RL[~(SM_0XFFFF << 11)] ^= s0()

	with apl_commands():
		c_xor_s[~(SM_0XFFFF << 11)] <= RL()
		RL[~SM_0X0001] ^= _2x_sticky() & m0()
		RL[SM_0X0001] &= _2x_sticky()

	with apl_commands():
		s1[~(SM_0XFFFF << 11)] <= SRL()
		RL[~SM_0X0001] <= c() & s0()
		RL[SM_0X0001] |= c_xor_s() & SRL()
		GL[SM_0X0001] <= RL()

	with apl_commands():
		RN_REG_FLAGS[SM_0X0001 << C_FLAG] <= GL()
		RL[SM_0X0001] <= c() & s0()
		RL[SM_0X00FF << 1] |= _2x_sticky() & m0() & c_xor_s()
		RL[SM_0X000F << 8] |= _2x_sticky() & m0() & c_xor_s()

	with apl_commands():
		c[~(SM_0XFFFF << 12)] <= RL()
		s1[SM_0XFFFF << 12] <= RSP16()
		c[SM_0XFFFF << 12] <= RSP16()

@belex_apl
def add_f16exp(Belex, res: VR, f16_x: VR, f16_y: VR, x: VR, t_xory: VR):

	with apl_commands():
		RL[SM_0XFFFF << 10] <= ~f16_x() & INV_RSP16()
		GL[SM_0X001F << 10] <= RL()
		res[~(SM_0XFFFF << 10)] <= RSP16()

	with apl_commands():
		x[SM_0X000F << 11] <= INV_RL()
		x[SM_0X0001 << 15] <= SRL()
		RL[SM_0X000F << 11] <= f16_y() | INV_RL()
		RL[SM_0X0001 << 10] <= f16_x() | GL()

	with apl_commands():
		t_xory[SM_0X000F << 11] <= RL()
		x[SM_0X0001 << 10] <= RL()
		RL[SM_0X0001 << 10] &= f16_y()
		RL[SM_0XFFFF << 11] <= x() & f16_y()

	with apl_commands():
		RL[SM_0X0001 << 11] |= t_xory() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 12] |= t_xory() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 13] |= t_xory() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 14] |= t_xory() & NRL()

	with apl_commands():
		RL[SM_0XFFFF << 11] <= x() ^ NRL()
		RL[SM_0X0001 << 10] <= x() ^ RSP16()

	with apl_commands():
		RL[SM_0X001F << 10] ^= f16_y()

	with apl_commands(" shift right 1"):
		res[SM_0XFFFF << 9] <= SRL()

@belex_apl
def add_f16biad_shift(Belex, res: VR, shift: VR):

	with apl_commands(" Add 16 and shift 2"):
		RL[SM_0XFFFF << 12] <= shift()

	with apl_commands():
		RL[SM_0X000F << 11] <= SRL()
		RL[SM_0X0001 << 15] <= INV_SRL()

	with apl_commands():
		res[SM_0XFFFF << 10] <= SRL()
		RL[SM_0X0001 << 10] <= SRL()

	with apl_commands(" dec 1 and shift 1"):
		RL[SM_0XFFFF << 12] <= ~res() & INV_RSP16()
		RL[SM_0X0001 << 11] <= ~res() & INV_NRL()
		GL[SM_0X0001 << 11] <= RL()
		GL[SM_0X0001 << 12] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 12] <= ~res() & NRL()
		RL[SM_0X0001 << 13] <= ~res() & GL()
		RL[SM_0X0001 << 10] <= INV_RL()

	with apl_commands():
		RL[SM_0XFFFF << 11] <= res() ^ NRL()

	with apl_commands():
		res[SM_0XFFFF << 9] <= SRL()

@belex_apl
def simple_sub_7msb_frag_t2(Belex, res: VR, x: VR, y: VR):

	with apl_commands():
		RL[SM_0XFFFF << 9] <= x()

	with apl_commands():
		RL[SM_0XFFFF << 9] ^= y()
		RN_REG_T0[SM_0XFFFF << 9] <= INV_RL()

	with apl_commands():
		RN_REG_T1[SM_0XFFFF << 9] <= INV_RL()
		RL[SM_0XFFFF << 9] <= RN_REG_T0() & y()

	with apl_commands():
		RL[SM_0X0001 << 10] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 11] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 12] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 13] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0X0001 << 14] |= RN_REG_T1() & NRL()

	with apl_commands():
		RL[SM_0XFFFF << 10] <= RN_REG_T1() ^ INV_NRL()
		RL[SM_0X0001 << 9] <= RN_REG_T1() ^ INV_RSP16()

	with apl_commands():
		res[SM_0XFFFF << 9] <= RL()

@belex_apl
def simple_dec_7msb_frag(Belex, res: VR, x: VR):

	with apl_commands():
		RL[SM_0XFFFF << 9] <= ~x() & INV_RSP16()
		GL[SM_0X000F << 9] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 10] <= ~x() & NRL()
		RL[SM_0X0001 << 13] <= ~x() & GL()
		GL[SM_0X0001 << 10] <= RL()
		GL[SM_0X0001 << 11] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 11] <= ~x() & NRL()
		RL[SM_0X0001 << 14] <= ~x() & NRL()
		RL[SM_0X0001 << 12] <= ~x() & GL()

	with apl_commands():
		RL[SM_0XFFFF << 10] <= x() ^ NRL()

	with apl_commands():
		res[SM_0XFFFF << 9] <= RL()

@belex_apl
def sr_16_by_vec_t1(Belex, in_out: VR, shift: VR):

	with apl_commands():
		RL[~(SM_0XFFFF << 12)] <= in_out()
		in_out[SM_0XFFFF << 12] <= RSP16()
		#GL = 1 (shift 8 needed)
		RL[SM_0X000F << 12] <= shift()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 12)] <= INV_GL()
		RL[~(SM_0XFFFF << 12)] &= ~INV_GL()
		GL[SM_0X0001 << 11] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 3] <= (in_out() & RN_REG_T0()) | GL()
		GL[SM_0X0001 << 10] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 2] <= (in_out() & RN_REG_T0()) | GL()
		GL[SM_0X0001 << 9] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 1] <= (in_out() & RN_REG_T0()) | GL()
		GL[SM_0X0001 << 8] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 0] <= (in_out() & RN_REG_T0()) | GL()
		RL[SM_0X00FF << 4] <= in_out() & RN_REG_T0()
		#GL = 1 (shift 4 needed)
		GL[SM_0X0001 << 14] <= RL()

	with apl_commands():
		in_out[~(SM_0XFFFF << 12)] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 12)] <= INV_GL()
		RL[~(SM_0XFFFF << 12)] &= ~INV_GL()

	with apl_commands():
		RL[~(SM_0XFFFF << 11)] <= SRL()

	with apl_commands():
		RL[~(SM_0XFFFF << 10)] <= SRL()

	with apl_commands():
		RL[~(SM_0XFFFF << 9)] <= SRL()

	with apl_commands():
		RL[SM_0X00FF] <= (in_out() & RN_REG_T0()) | SRL()
		RL[SM_0X000F << 8] <= in_out() & RN_REG_T0()
		#GL = 1 (shift 2 needed)
		GL[SM_0X0001 << 13] <= RL()

	with apl_commands():
		in_out[~(SM_0XFFFF << 12)] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 12)] <= INV_GL()
		RL[~(SM_0XFFFF << 12)] &= ~INV_GL()

	with apl_commands():
		RL[~(SM_0XFFFF << 11)] <= SRL()
		RL[SM_0X0001 << 11] <= in_out() & RN_REG_T0()

	with apl_commands():
		RL[~(SM_0XFFFF << 10)] <= (in_out() & RN_REG_T0()) | SRL()
		RL[SM_0X0001 << 10] <= in_out() & RN_REG_T0()
		#GL = 1 (shift 1 needed)
		GL[SM_0X0001 << 12] <= RL()

	with apl_commands():
		in_out[~(SM_0XFFFF << 12)] <= RL()

	with apl_commands():
		RN_REG_T0[~(SM_0XFFFF << 12)] <= INV_GL()
		RL[~(SM_0XFFFF << 12)] &= ~INV_GL()

	with apl_commands():
		RL[~(SM_0XFFFF << 11)] <= (in_out() & RN_REG_T0()) | SRL()
		RL[SM_0X0001 << 11] <= in_out() & RN_REG_T0()

	with apl_commands():
		in_out[~(SM_0XFFFF << 12)] <= RL()

@belex_apl
def is_mrk(Belex, mrk: Mask):

	with apl_commands():
		RL[mrk] <= RN_REG_FLAGS()
		GL[mrk] <= RL()

@belex_apl
def min_zero_and_negate(Belex, dst: VR, src: VR, tmp: VR):

	with apl_commands():
		RL[SM_0XFFFF] <= src()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		RL[SM_0XFFFF] &= GL()

	with apl_commands():
		tmp[SM_0XFFFF] <= INV_RL()

	with apl_commands(" negate  increment exp 7 bits"):
		RL[SM_0XFFFF << 9] <= tmp()
		#FIXME: BUG
		GGL[SM_0X0001 << 9] <= RL()
		GGL[SM_0X0001 << 10] <= RL()
		GL[SM_0X000F << 9] <= RL()

	#FIXME: BUG - Multiply the code is work around for the bug ! ! ! 
	with apl_commands():
		GGL[SM_0X0001 << 9] <= RL()
		GGL[SM_0X0001 << 10] <= RL()		

	with apl_commands():
		RL[SM_0X0001 << 10] <= tmp() & NRL()
		RL[SM_0X0001 << 11] <= tmp() & GGL()
		RL[SM_0X0001 << 13] <= tmp() & GL()
	
	with apl_commands():
		RL[SM_0X0001 << 12] <= tmp() & NRL()
		RL[SM_0X0001 << 14] <= tmp() & NRL()

	with apl_commands():
		RL[SM_0XFFFF << 10] <= tmp() ^ NRL()
		RL[SM_0X0001 << 9] <= tmp() ^ INV_RSP16()

	with apl_commands():
		dst[SM_0XFFFF << 9] <= RL()

@belex_apl
def max_0xF_on_7msb_and_sr3(Belex, dst: VR, src: VR):

	with apl_commands():
		RL[SM_0XFFFF] <= src()
		GL[SM_0X0001 << 13] <= RL()

	with apl_commands():
		RL[SM_0X000F << 9] |= GL()
		RL[~(SM_0X000F << 9)] <= 0

	with apl_commands():
		RL[SM_0XFFFF] <= NRL()

	with apl_commands():
		RL[SM_0XFFFF] <= NRL()

	with apl_commands():
		dst[SM_0XFFFF] <= NRL()

@belex_apl
def shift_right_one_when_mant_is_too_big(Belex, mant: VR, exp: VR, shift: VR, tmp: VR):

	with apl_commands(" shift right big-mant by one"):
		RL[SM_0XFFFF] <= mant()
		GL[SM_0X0001 << 11] <= RL()

	with apl_commands():
		tmp[SM_0XFFFF] <= INV_GL()
		RL[SM_0XFFFF] &= ~INV_GL()

	with apl_commands():
		RL[SM_0XFFFF] <= (mant() & tmp()) | SRL()

	with apl_commands():
		mant[SM_0XFFFF] <= RL()

	with apl_commands(" increment exp 7 bits"):
		RL[SM_0XFFFF << 9] <= exp() & GL()
		GL[SM_0X000F << 9] <= RL()
		GGL[SM_0X3333 << 9] <= RL()
		RL[SM_0X0001 << 8] <= GL()

	with apl_commands():
		RL[SM_0X0001 << 10] <= exp() & NRL()
		RL[SM_0X0001 << 11] <= exp() & GGL()
		RL[SM_0X0001 << 13] <= exp() & GL()

	with apl_commands():
		RL[SM_0X0001 << 12] <= exp() & NRL()
		RL[SM_0X0001 << 14] <= exp() & NRL()

	with apl_commands():
		RL[SM_0XFFFF << 9] <= exp() ^ NRL()
		RL[SM_0X0001 << 8] <= RSP16()

	with apl_commands():
		exp[SM_0XFFFF << 9] <= RL()

	with apl_commands(" set exp to one for shifted mants due to 'exp is too small'"):
		RL[SM_0XFFFF] <= ~shift() & INV_RSP16()
		GL[SM_0XFFFF] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 9] <= INV_GL()
		RL[~(SM_0X0001 << 9)] <= exp() & GL()

	with apl_commands():
		RL[SM_0X0001 << 9] |= exp() & GL()

	with apl_commands():
		exp[SM_0XFFFF] <= NRL()

@belex_apl
def build_f16_res(Belex, res: VR, f16_x: VR, f16_y: VR):

	with apl_commands(" res.mantissa = mant_res & 0x3ff;  if (mant_res & 0x400)  res.exponent = (u16)(exp_res & 0x1f);  else  res.exponent = 0;  res.sign = x.sign ^ y.sign;"):
		RL[~(SM_0XFFFF << 11)] <= RN_REG_T5()
		GL[SM_0X0001 << 10] <= RL()
		RL[SM_0X0001 << 15] <= f16_x()

	with apl_commands():
		RN_REG_T4[~(SM_0XFFFF << 10)] <= RL()
		RL[SM_0X001F << 10] <= RN_REG_T3() & GL()
		RL[SM_0X0001 << 15] ^= f16_y()

	with apl_commands():
		RN_REG_T4[SM_0XFFFF << 10] <= RL()

	with apl_commands():
		RL[SM_0XFFFF << 10] <= f16_x()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		RN_REG_T0[SM_0X000F] <= GL()
		RL[SM_0XFFFF << 10] <= f16_y()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		RN_REG_T0[SM_0X0001 << 1] <= GL()
		RN_REG_T0[SM_0X0001 << 3] <= GL()

	with apl_commands():
		RL[~(SM_0XFFFF << 10)] <= ~f16_x() & INV_RSP16()
		GL[~(SM_0XFFFF << 10)] <= RL()

	with apl_commands():
		RN_REG_T2[SM_0X0001] <= INV_GL()
		#(SM_0X0001 << 3):SB[RN_REG_T2] = GL;  x.mant == 0
		RL[SM_0X001F << 10] <= ~f16_x() & INV_RSP16()
		GL[~(SM_0XFFFF << 15)] <= RL()

	with apl_commands():
		RN_REG_T2[SM_0X0001 << 3] <= GL()

	with apl_commands():
		RL[~(SM_0XFFFF << 10)] <= ~f16_y() & INV_RSP16()
		GL[~(SM_0XFFFF << 10)] <= RL()

	with apl_commands():
		RN_REG_T2[SM_0X0001 << 1] <= INV_GL()
		#SM_0X0001 << 2:SB[RN_REG_T2] = GL;  y.mant == 0
		RL[SM_0X001F << 10] <= ~f16_y() & INV_RSP16()
		GL[~(SM_0XFFFF << 15)] <= RL()

	with apl_commands():
		RN_REG_T2[SM_0X0001 << 2] <= GL()

	with apl_commands("if ((exp_res >= 0x1f) || (x.exponent == 0X1f) || (y.exponent == 0X1f))"):
		RL[SM_0XFFFF << 10] <= RN_REG_T3()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		RL[SM_0X0001 << 15] <= ~RN_REG_T3() & INV_GL()
		GL[SM_0X0001 << 15] <= RL()
		RL[~(SM_0XFFFF << 2)] <= ~RN_REG_T0() & INV_RSP16()
		GL[~(SM_0XFFFF << 2)] <= RL()

	with apl_commands():
		RL[SM_0X001F << 10] <= RN_REG_T4() | INV_GL()
		RL[~(SM_0XFFFF << 10)] <= RN_REG_T4() & GL()

	with apl_commands():
		RN_REG_T4[~(SM_0XFFFF << 15)] <= RL()

	with apl_commands(" if (((x.exponent == 0X1f) && x.mantissa) ||      ((y.exponent == 0X1f) && y.mantissa) ||      ((x.exponent == 0X1f) && !y.mantissa && !y.exponent) ||      ((y.exponent == 0X1f) && !x.mantissa && !x.exponent)) {  res.mantissa = 0X3ff;"):
		RL[SM_0X000F] <= ~(RN_REG_T0() & RN_REG_T2()) & INV_RSP16()
		GL[SM_0X000F] <= RL()

	with apl_commands():
		RL[~(SM_0X0001 << 15)] <= RN_REG_T4() | INV_GL()
		RL[SM_0X0001 << 15] <= RN_REG_T4()

	with apl_commands():
		res[SM_0XFFFF] <= RL()

@belex_apl
def addsub_carry_prefix(Belex, x: VR, y: VR, t_xory_ci: VR, t_ci0: VR):

	with apl_commands():
		RL[SM_0X0001] <= x() & GL()
		t_xory_ci[SM_0X0001] <= GL()
		RL[~SM_0X0001] <= x()

	with apl_commands():
		RL[SM_0X0001] |= y() & t_xory_ci()
		RL[~SM_0X0001] |= y()

	with apl_commands():
		t_xory_ci[~SM_0X0001] <= RL()
		RL[~SM_0X0001] <= x() & y()
		RL[SM_0X0001] |= x() & y()

	with apl_commands():
		t_ci0[SM_0X1111 << 1] <= NRL()
		RL[SM_0X1111 << 1] |= t_xory_ci() & NRL()
		#5,9,13:RL = Cout0[5,9,13] = x&y | ci(x|y)
		RL[SM_0X1111 << 4] |= t_xory_ci()

	with apl_commands():
		t_ci0[SM_0X1111 << 2] <= NRL()
		RL[SM_0X1111 << 2] |= t_xory_ci() & NRL()
		RL[SM_0X1111 << 5] |= t_xory_ci() & NRL()

	with apl_commands():
		t_ci0[SM_0X1111 << 3] <= NRL()
		RL[SM_0X1111 << 3] |= t_xory_ci() & NRL()
		RL[SM_0X1111 << 6] |= t_xory_ci() & NRL()
		GL[SM_0X0001 << 15] <= RL()

	with apl_commands():
		t_ci0[SM_0X1111 << 4] <= NRL()
		t_ci0[SM_0X0001] <= GL()
		RL[SM_0X1111 << 7] |= t_xory_ci() & NRL()
		GL[SM_0X0001 << 15] <= RL()

@belex_apl
def addsub_carry_in_out_u12_suffix(Belex, res: VR, x: VR, y: VR, t_xory_ci: VR, t_ci0: VR, t_ci1: VR):

	with apl_commands():
		t_ci1[SM_0X0001] <= GL()
		t_ci1[SM_0XFFFF << 5] <= NRL()
		RL[SM_0XFFFF] <= t_ci0()
		GL[SM_0X0001 << 4] <= RL()

	with apl_commands():
		RL[SM_0X000F << 5] |= t_ci1() & GL()
		GL[SM_0X0001 << 8] <= RL()

	with apl_commands():
		RL[SM_0X000F << 9] |= t_ci1() & GL()
		GL[SM_0X0001 << 12] <= RL()
		RL[SM_0X0001] <= t_xory_ci()

	with apl_commands():
		RN_REG_FLAGS[SM_0X0001 << C_FLAG] <= GL()
		RL[SM_0XFFFF] ^= x()

	with apl_commands():
		RL[SM_0XFFFF] ^= y()

	with apl_commands():
		res[~(SM_0XFFFF << 12)] <= RL()	

##################################################

@belex_apl
def monotonic_transformation_f16_u16_nan2max_frag(Belex, u16_out: VR, f16_in: VR, tmp_exp_non_zero: VR):

	with apl_commands():
		RL[SM_0XFFFF] <= ~f16_in() & INV_RSP16()
		GL[~(SM_0XFFFF << 15)] <= RL()

	with apl_commands():
		tmp_exp_non_zero[SM_0XFFFF] <= INV_GL()
		GL[~(SM_0XFFFF << 10)] <= RL()

	with apl_commands():
		RL[SM_0X001F << 10] <= f16_in() & INV_GL()
		GL[SM_0X001F << 10] <= RL()

	with apl_commands():
		RL[~(SM_0XFFFF << 15)] <= (f16_in() & tmp_exp_non_zero()) | GL()
		RL[SM_0XFFFF << 15] <= (f16_in() & tmp_exp_non_zero()) & INV_GL()
		GL[SM_0XFFFF << 15] <= RL()

	with apl_commands():
		RL[~(SM_0XFFFF << 15)] ^= GL()
		RL[SM_0XFFFF << 15] <= INV_RL()

	with apl_commands():
		u16_out[SM_0XFFFF] <= RL()
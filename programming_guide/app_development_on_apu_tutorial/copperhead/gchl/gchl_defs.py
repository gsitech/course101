from lpython import (
    i8, u16, i32, i64,
    ccall, ccallback, overload,
    CPtr, c_p_pointer, Pointer,
    packed, dataclass, sizeof,
)

######### SYSTEM REGISTERS #########
SB_0: u16 = u16(0)
SB_1: u16 = u16(1)
SB_2: u16 = u16(2)
SB_3: u16 = u16(3)
SB_4: u16 = u16(4)
SB_5: u16 = u16(5)
SB_6: u16 = u16(6)
SB_7: u16 = u16(7)
SB_8: u16 = u16(8)
SB_9: u16 = u16(9)
SB_10: u16 = u16(10)
SB_11: u16 = u16(11)
SB_12: u16 = u16(12)
SB_13: u16 = u16(13)
SB_14: u16 = u16(14)
SB_15: u16 = u16(15)
SB_16: u16 = u16(16)
SB_17: u16 = u16(17)
SB_18: u16 = u16(18)
SB_19: u16 = u16(19)
SB_20: u16 = u16(20)
SB_21: u16 = u16(21)
SB_22: u16 = u16(22)
SB_23: u16 = u16(23)

SM_BIT_0: u16 = u16(0)
SM_BIT_1: u16 = u16(1)
SM_BIT_2: u16 = u16(2)
SM_BIT_3: u16 = u16(3)
SM_BIT_4: u16 = u16(4)
SM_BIT_5: u16 = u16(5)
SM_BIT_6: u16 = u16(6)
SM_BIT_7: u16 = u16(7)
SM_BIT_8: u16 = u16(8)
SM_BIT_9: u16 = u16(9)
SM_BIT_10: u16 = u16(10)
SM_BIT_11: u16 = u16(11)
SM_BIT_12: u16 = u16(12)
SM_BIT_13: u16 = u16(13)
SM_BIT_14: u16 = u16(14)
SM_BIT_15: u16 = u16(15)


# ####################################
# TODO: FIX THIS
# generated code is wrong is not passing the val for the next var:
# uint16_t VR16_G_FIRST;

# VR16_G0: u16 = SB_0
# VR16_G1: u16 = SB_1
# VR16_G2: u16 = SB_2
# VR16_G3: u16 = SB_3
# VR16_G4: u16 = SB_4
# VR16_G5: u16 = SB_5
# VR16_G6: u16 = SB_6
# VR16_G7: u16 = SB_7
# VR16_G8: u16 = SB_8
# VR16_G9: u16 = SB_9
# VR16_G10: u16 = SB_10
# VR16_G11: u16 = SB_11
# VR16_G12: u16 = SB_12
# VR16_G13: u16 = SB_13
# VR16_G14: u16 = SB_14
# VR16_M4_IDX: u16 = SB_15
# VR16_T0: u16 = SB_16
# VR16_T1: u16 = SB_17
# VR16_T2: u16 = SB_18
# VR16_T3: u16 = SB_19
# VR16_T4: u16 = SB_20
# VR16_T5: u16 = SB_21
# VR16_T6: u16 = SB_22
# VR16_FLAGS: u16 = SB_23
#
# VR16_G_FIRST: u16 = VR16_G0
# VR16_G_LAST: u16 = VR16_G14
# VR16_RO_LAST: u16 = VR16_M4_IDX
# VR16_T_FIRST: u16 = VR16_T0
# VR16_T_LAST: u16 = VR16_T6
#####################################
############### TEMPORARY FIX ###############
VR16_G0: u16 = u16(0)
VR16_G1: u16 = u16(1)
VR16_G2: u16 = u16(2)
VR16_G3: u16 = u16(3)
VR16_G4: u16 = u16(4)
VR16_G5: u16 = u16(5)
VR16_G6: u16 = u16(6)
VR16_G7: u16 = u16(7)
VR16_G8: u16 = u16(8)
VR16_G9: u16 = u16(9)
VR16_G10: u16 = u16(10)
VR16_G11: u16 = u16(11)
VR16_G12: u16 = u16(12)
VR16_G13: u16 = u16(13)
VR16_G14: u16 = u16(14)
VR16_M4_IDX: u16 = u16(15)
VR16_T0: u16 = u16(16)
VR16_T1: u16 = u16(17)
VR16_T2: u16 = u16(18)
VR16_T3: u16 = u16(19)
VR16_T4: u16 = u16(20)
VR16_T5: u16 = u16(21)
VR16_T6: u16 = u16(22)
VR16_FLAGS: u16 = u16(23)

VR16_G_FIRST: u16 = u16(0)
VR16_G_LAST: u16 = u16(14)
VR16_RO_LAST: u16 = u16(15)
VR16_T_FIRST: u16 = u16(16)
VR16_T_LAST: u16 = u16(22)
#############################################

#MMB vectors
GCHL_VR16_0:    u16 = u16(0)
GCHL_VR16_1:	u16 = u16(1)
GCHL_VR16_2:	u16 = u16(2)
GCHL_VR16_3:	u16 = u16(3)
GCHL_VR16_4:	u16 = u16(4)
GCHL_VR16_5:	u16 = u16(5)
GCHL_VR16_6:	u16 = u16(6)
GCHL_VR16_7:	u16 = u16(7)
GCHL_VR16_8:	u16 = u16(8)
GCHL_VR16_9:	u16 = u16(9)
GCHL_VR16_10:	u16 = u16(10)
GCHL_VR16_11:	u16 = u16(11)
GCHL_VR16_12:	u16 = u16(12)
GCHL_VR16_13:	u16 = u16(13)
GCHL_VR16_14:	u16 = u16(14)
GCHL_VR16_IDX:	u16 = u16(15)
GCHL_VR16_FLAGS:  u16 = u16(23)

#L1 vectors memory registers
GCHL_VM_0:	u16 = u16(0)
GCHL_VM_1:	u16 = u16(1)
GCHL_VM_2:	u16 = u16(2)
GCHL_VM_3:	u16 = u16(3)
GCHL_VM_4:	u16 = u16(4)
GCHL_VM_5:	u16 = u16(5)
GCHL_VM_6:	u16 = u16(6)
GCHL_VM_7:	u16 = u16(7)
GCHL_VM_8:	u16 = u16(8)
GCHL_VM_9:	u16 = u16(9)
GCHL_VM_10:	u16 = u16(10)
GCHL_VM_11:	u16 = u16(11)
GCHL_VM_12:	u16 = u16(12)
GCHL_VM_13:	u16 = u16(13)
GCHL_VM_14:	u16 = u16(14)
GCHL_VM_15:	u16 = u16(15)
GCHL_VM_16:	u16 = u16(16)
GCHL_VM_17:	u16 = u16(17)
GCHL_VM_18:	u16 = u16(18)
GCHL_VM_19:	u16 = u16(19)
GCHL_VM_20:	u16 = u16(20)
GCHL_VM_21:	u16 = u16(21)
GCHL_VM_22:	u16 = u16(22)
GCHL_VM_23:	u16 = u16(23)
GCHL_VM_24:	u16 = u16(24)
GCHL_VM_25:	u16 = u16(25)
GCHL_VM_26:	u16 = u16(26)
GCHL_VM_27:	u16 = u16(27)
GCHL_VM_28:	u16 = u16(28)
GCHL_VM_29:	u16 = u16(29)
GCHL_VM_30:	u16 = u16(30)
GCHL_VM_31:	u16 = u16(31)
GCHL_VM_32:	u16 = u16(32)
GCHL_VM_33:	u16 = u16(33)
GCHL_VM_34:	u16 = u16(34)
GCHL_VM_35:	u16 = u16(35)
GCHL_VM_36:	u16 = u16(36)
GCHL_VM_37:	u16 = u16(37)
GCHL_VM_38:	u16 = u16(38)
GCHL_VM_39:	u16 = u16(39)
GCHL_VM_40:	u16 = u16(40)
GCHL_VM_41:	u16 = u16(41)
GCHL_VM_42:	u16 = u16(42)
GCHL_VM_43:	u16 = u16(43)
GCHL_VM_44:	u16 = u16(44)
GCHL_VM_45:	u16 = u16(45)
GCHL_VM_46:	u16 = u16(46)
GCHL_VM_47:	u16 = u16(47)

#############################################
L1_ADDR_REG0: u16 = u16(0) 
L1_ADDR_REG1: u16 = u16(1) 
L1_ADDR_REG2: u16 = u16(2) 
L1_ADDR_REG3: u16 = u16(3) 
#############################################

L2_ADDR_REG0: u16 = u16(0) 



APL_INVAL_ROWNUM: u16 = u16(0xff)

_2VR16_G0_1: u16 = VR16_G0
_2VR16_G1_2: u16 = VR16_G1
_2VR16_G2_3: u16 = VR16_G2
_2VR16_G3_4: u16 = VR16_G3
_2VR16_G4_5: u16 = VR16_G4
_2VR16_G5_6: u16 = VR16_G5
_2VR16_G6_7: u16 = VR16_G6
_2VR16_G7_8: u16 = VR16_G7
_2VR16_G8_9: u16 = VR16_G8
_2VR16_G9_10: u16 = VR16_G9
_2VR16_G10_11: u16 = VR16_G10
_2VR16_G11_12: u16 = VR16_G11
_2VR16_G12_13: u16 = VR16_G12
_2VR16_G13_14: u16 = VR16_G13
_2VR16_T0_1: u16 = VR16_T0
_2VR16_T1_2: u16 = VR16_T1
_2VR16_T2_3: u16 = VR16_T2
_2VR16_T3_4: u16 = VR16_T3
_2VR16_T4_5: u16 = VR16_T4
_2VR16_T5_6: u16 = VR16_T5

_2VR16_G_FIRST: u16 = _2VR16_G0_1
_2VR16_G_LAST: u16 = _2VR16_G13_14
_2VR16_T_FIRST: u16 = _2VR16_T0_1
_2VR16_T_LAST: u16 = _2VR16_T5_6

# # /* MMB Flag IDs (in flags Vector Register) */


C_FLAG: u16 = SM_BIT_0  # /* Carry in/out flag */
B_FLAG: u16 = SM_BIT_1  # /* Borrow in/out flag */
OF_FLAG: u16 = SM_BIT_2  # /* Overflow flag */
PE_FLAG: u16 = SM_BIT_3  # /* Parity error */

# # /* Markers for general purpose usage Callee must preserve */
GP0_MRK: u16 = SM_BIT_4
GP1_MRK: u16 = SM_BIT_5
GP2_MRK: u16 = SM_BIT_6
GP3_MRK: u16 = SM_BIT_7
GP4_MRK: u16 = SM_BIT_8
GP5_MRK: u16 = SM_BIT_9
GP6_MRK: u16 = SM_BIT_10
GP7_MRK: u16 = SM_BIT_11

# #	/* Markers for general purpose usage Callee may overwrite preserve */
TMP0_MRK: u16 = SM_BIT_12
TMP1_MRK: u16 = SM_BIT_13
TMP2_MRK: u16 = SM_BIT_14
TMP3_MRK: u16 = SM_BIT_15

# #	/* General purpose vector-markers */
FIRST_GP0_MRK: u16 = GP0_MRK
LAST_GP7_MRK: u16 = GP7_MRK

# #	/* Temporary vector-markers */
FIRST_TMP0_MRK: u16 = TMP0_MRK
LAST_TMP3_MRK: u16 = TMP3_MRK

# # /* MMB Flag bitmasks (correspond to enum gvml_mrks_n_flgs flag IDs) */
# #num mmb_mrks_n_flgs {
MMB_C_FLAG: u16 = u16(u16(1) << C_FLAG)  # /* Carry in/out flag */
MMB_B_FLAG: u16 = u16(u16(1) << B_FLAG)  # /* Borrow in/out flag */
MMB_OF_FLAG: u16 = u16(u16(1) << OF_FLAG)  # /* Overflow flag */
MMB_PE_FLAG: u16 = u16(u16(1) << PE_FLAG)  # /* Parity error flag */

# #	/* Markers for general purpose usage Callee must preserve */
MMB_MRK0: u16 = u16(u16(1) << GP0_MRK)  # /* General purpose marker */
MMB_MRK1: u16 = u16(u16(1) << GP1_MRK)  # /* General purpose marker */
MMB_MRK2: u16 = u16(u16(1) << GP2_MRK)  # /* General purpose marker */
MMB_MRK3: u16 = u16(u16(1) << GP3_MRK)  # /* General purpose marker */
MMB_MRK4: u16 = u16(u16(1) << GP4_MRK)  # /* General purpose marker */
MMB_MRK5: u16 = u16(u16(1) << GP5_MRK)  # /* General purpose marker */
MMB_MRK6: u16 = u16(u16(1) << GP6_MRK)  # /* General purpose marker */
MMB_MRK7: u16 = u16(u16(1) << GP7_MRK)  # /* General purpose marker */

# #	/* Markers for general purpose usage Callee may overwrite preserve */
MMB_TMP0_MRK: u16 = u16(u16(1) << TMP0_MRK)
MMB_TMP1_MRK: u16 = u16(u16(1) << TMP1_MRK)
MMB_TMP2_MRK: u16 = u16(u16(1) << TMP2_MRK)
MMB_TMP3_MRK: u16 = u16(u16(1) << TMP3_MRK)

# #enum smaps_vals {
# #	/* pre-defined section maps vector-registers */
SM_0XFFFF_VAL: u16 = u16(0xFFFF)
SM_0X0001_VAL: u16 = u16(0x0001)
SM_0X1111_VAL: u16 = u16(0x1111)
SM_0X0101_VAL: u16 = u16(0x0101)
SM_0X000F_VAL: u16 = u16(0x000F)
SM_0X0F0F_VAL: u16 = u16(0x0F0F)
SM_0X0707_VAL: u16 = u16(0x0707)
SM_0X5555_VAL: u16 = u16(0x5555)
SM_0X3333_VAL: u16 = u16(0x3333)
SM_0X00FF_VAL: u16 = u16(0x00FF)
SM_0X001F_VAL: u16 = u16(0x001F)
SM_0X003F_VAL: u16 = u16(0x0003)

# # bool is_gvml_init(void);

# #endif //GVML_APL_H

GSI_APC_NUM_HALF_BANKS: u16 = u16(2)
GSI_MMB_NUM_BANKS: u16 = u16(4)
GSI_APUC_NUM_APCS: u16 = u16(2)

RN_REG_G0: u16 = u16(0)
RN_REG_G1: u16 = u16(1)
RN_REG_G2: u16 = u16(2)
RN_REG_G3: u16 = u16(3)
RN_REG_G4: u16 = u16(4)
RN_REG_G5: u16 = u16(5)
RN_REG_G6: u16 = u16(6)
RN_REG_G7: u16 = u16(7)
RN_REG_T0: u16 = u16(8)
RN_REG_T1: u16 = u16(9)
RN_REG_T2: u16 = u16(10)
RN_REG_T3: u16 = u16(11)
RN_REG_T4: u16 = u16(12)
RN_REG_T5: u16 = u16(13)
RN_REG_T6: u16 = u16(14)
RN_REG_FLAGS: u16 = u16(15)

# /* general purpose smaps registers */
SM_REG0: u16 = u16(0)
SM_REG1: u16 = u16(1)
SM_REG2: u16 = u16(2)
SM_REG3: u16 = u16(3)

SM_0XFFFF: u16 = u16(4)
SM_0X0001: u16 = u16(5)
SM_0X1111: u16 = u16(6)
SM_0X0101: u16 = u16(7)
SM_0X000F: u16 = u16(8)
SM_0X0F0F: u16 = u16(9)
SM_0X0707: u16 = u16(10)
SM_0X5555: u16 = u16(11)
SM_0X3333: u16 = u16(12)
SM_0X00FF: u16 = u16(13)
SM_0X001F: u16 = u16(14)
SM_0X003F: u16 = u16(15)

# SM_0XFFFF_VAL: u16 = u16(0xFFFF)
# SM_0X0001_VAL: u16 = u16(0x0001)
# SM_0X1111_VAL: u16 = u16(0x1111)
# SM_0X0101_VAL: u16 = u16(0x0101)
# SM_0X000F_VAL: u16 = u16(0x000F)
# SM_0X0F0F_VAL: u16 = u16(0x0F0F)
# SM_0X0707_VAL: u16 = u16(0x0707)
# SM_0X5555_VAL: u16 = u16(0x5555)
# SM_0X3333_VAL: u16 = u16(0x3333)
# SM_0X00FF_VAL: u16 = u16(0x00FF)
# SM_0X001F_VAL: u16 = u16(0x001F)
# SM_0X003F_VAL: u16 = u16(0x003F)

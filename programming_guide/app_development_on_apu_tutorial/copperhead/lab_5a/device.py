from lpython import (
    u16, i32, u32, u16,
    ccallback, CPtr, c_p_pointer, Pointer, InOut,
    dataclass, sizeof, empty_c_void_p
)

from common import GD_LAB_5_MAX_NUM_FEATURES, GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK, GD_LAB_5_MAX_K, GD_LAB_5_CMD_LOAD_DB, GD_LAB_5_CMD_SEARCH, gd_lab_5_load_db, gd_lab_5_search, gd_lab_5_cmd
from gvml_defs import GVML_MRK0, GVML_MRK1, GVML_MRK2, GVML_MRK3, GVML_VR16_0, GVML_VR16_1, GVML_VR16_2, GVML_VR16_3, GVML_VR16_4, GVML_VR16_5, GVML_VR16_6
from gchl.lib import gchl_init_once, gchl_cpy_16, gchl_reset_16, gchl_reset_16_msk, gchl_load_16, gchl_add_f16, gchl_sub_f16, gchl_cpy_imm_16

from gsi.arc_utils import increment_ptr, dereference_u16_ptr, direct_dma_l4_to_l1_32k
from gsi.gal import gal_mem_handle_to_apu_ptr
from gsi.gvml import (gvml_create_index_16,
                      gvml_mark_kmin_idxval_u16_mrk_g32k, gvml_set_m,
                      gvml_cpy_m, gvml_cpy_from_mrk_16_msk, gvml_merge_v15mrk1_g32k,
                      gvml_cpy_to_mrk_16_msk, gvml_xor_m, gvml_and_m, gvml_cpy_16_mrk,
                      gvml_cpy_imm_16_msk_mrk, gvml_cpy_imm_16_mrk, gvml_get_marked_data)

@dataclass
class db_data:
    num_32k_chunks_records : u32 = u32(0)
    num_features : u32 = u32(0)
    p_data : CPtr = empty_c_void_p()

g_db_data : db_data = db_data()

# workaround to write to a global user type
def init_db_data(d : InOut[db_data], num_features : u32, num_32k_chunks_records : u32, p_data : CPtr) -> None:
    d.num_features = num_features
    d.num_32k_chunks_records = num_32k_chunks_records
    d.p_data = p_data

def load_db(load_db_data : gd_lab_5_load_db) -> i32:
    global g_db_data

    if (load_db_data.num_32k_chunks_records < u32(2)):
        print('number of chunks,', load_db_data.num_32k_chunks_records, ', must be greater than 1')
        return -1
    
    if load_db_data.num_features > GD_LAB_5_MAX_NUM_FEATURES:
        print('number of features,', load_db_data.num_features, ', must not exceed', GD_LAB_5_MAX_NUM_FEATURES)
        return -1
    
    init_db_data(g_db_data, load_db_data.num_features, load_db_data.num_32k_chunks_records, gal_mem_handle_to_apu_ptr(load_db_data.db))

    # Load the first chunk
    f : i32
    for f in range(i32(g_db_data.num_features)):
        direct_dma_l4_to_l1_32k(u16(f), increment_ptr(g_db_data.p_data, f * i32(GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK) * i32(sizeof(u16))))

    return 0

def abs_f16(vr : u16) -> None:
    gchl_reset_16_msk(vr, u16(0x8000))

def do_search(search_data : gd_lab_5_search) -> i32:
    global g_db_data

    # TODO: implement this function (remove this line when you're done)
    return -1

    if u32(search_data.k) > GD_LAB_5_MAX_K:
        print('k,', search_data.k, ', must not exceed', GD_LAB_5_MAX_K)
        return -1
    
    vr_merged_idx_lsb : u16 = GVML_VR16_0
    vr_merged_idx_msb : u16 = GVML_VR16_1
    vr_merged : u16 = GVML_VR16_2
    vr_idx : u16 = GVML_VR16_3
    vr_distances : u16 = GVML_VR16_4
    vr_records : u16 = GVML_VR16_5
    vr_query : u16 = GVML_VR16_6
    mrk_merged : u16 = GVML_MRK0
    mrk_in : u16 = GVML_MRK1
    mrk_kmin : u16 = GVML_MRK2
    mrk_added_entries : u16 = GVML_MRK3

    gvml_create_index_16(vr_merged_idx_lsb)
    gchl_reset_16(vr_merged_idx_msb)
    gvml_create_index_16(vr_idx)
    gvml_set_m(mrk_in)

    p_q : CPtr = gal_mem_handle_to_apu_ptr(search_data.query)
    p_iv : CPtr = gal_mem_handle_to_apu_ptr(search_data.output)
    p_next_chunk : CPtr = increment_ptr(g_db_data.p_data, i32(GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK) * i32(g_db_data.num_features) * i32(sizeof(u16)))

    c : i32
    for c in range(i32(g_db_data.num_32k_chunks_records)):
        gchl_reset_16(vr_distances)
        f : i32
        for f in range(i32(g_db_data.num_features)):
            f_val : u16 = dereference_u16_ptr(increment_ptr(p_q, f * i32(sizeof(u16))))
            gchl_cpy_imm_16(vr_query, f_val)
            gchl_load_16(vr_records, u16(f))
            gchl_sub_f16(vr_records, vr_records, vr_query)
            abs_f16(vr_records)
            gchl_add_f16(vr_distances, vr_distances, vr_records)

        gvml_mark_kmin_idxval_u16_mrk_g32k(mrk_kmin, vr_distances, vr_idx, search_data.k, mrk_in, vr_query)

        if c == 0:
            gvml_cpy_m(mrk_merged, mrk_kmin)
            gvml_cpy_from_mrk_16_msk(vr_distances, mrk_merged, u16(0x8000))
            gchl_cpy_16(vr_merged, vr_distances)
        else:
            # merge results
            # TODO: use @mrk_merged and its updated version residing in @vr_merged to calculate @mrk_added_entries (don't forget to update @mrk_merged too)

            # determine the added entries
            # TODO: use @mrk_merged and its updated version residing in @vr_merged to calculate @mrk_added_entries (don't forget to update @mrk_merged too)

            # TODO: determine the k minimum values in @vr_merged (use @mrk_merged as both input and output and use @vr_idx to break ties)

            # determine the added entries that survived the cut
            # TODO: use @mrk_added_entries and @mrk_merged to determine which of the added entries are relevant (store the result in @mrk_added_entries)

            # update the indices of the added entries
            # TODO: use @mrk_added_entries and @vr_records (which contains the m4 index of the merged results) to update @vr_merged_idx_lsb and @vr_merged_idx_msb
            # TODO: don't forget: if @c is odd, you need to update the most significant bit of @vr_merged_idx_lsb
            pass

        # Load next chunk (or the 1st chunk if we're done)
        if c == i32(g_db_data.num_32k_chunks_records) - 1:
            p_next_chunk = g_db_data.p_data

        for f in range(i32(g_db_data.num_features)):
            direct_dma_l4_to_l1_32k(u16(f), increment_ptr(p_next_chunk, f * i32(GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK) * i32(sizeof(u16))))

        p_next_chunk = increment_ptr(p_next_chunk, i32(GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK) * i32(g_db_data.num_features) * i32(sizeof(u16))) 

    gchl_reset_16_msk(vr_merged, u16(0x8000))
    gvml_get_marked_data(p_iv, vr_merged_idx_lsb, u32(3), mrk_merged, search_data.k)       

    return 0

@ccallback
def task(in_: CPtr, out: CPtr) -> i32:
    cmd: Pointer[gd_lab_5_cmd] = c_p_pointer(in_, gd_lab_5_cmd)

    if cmd.cmd == GD_LAB_5_CMD_LOAD_DB:
        gchl_init_once()
        return load_db(cmd.load_db_data )
    elif cmd.cmd == GD_LAB_5_CMD_SEARCH:
        return do_search(cmd.search_data)
    else:
        return -1
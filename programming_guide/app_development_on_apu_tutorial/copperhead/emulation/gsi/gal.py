from lpython import CPtr, u64, u32, i32, i64
import ctypes
import numpy as np

global_arrays = []
my_list = []

def gal_mem_handle_to_apu_ptr(hndl: u64) -> CPtr:
    return ctypes.cast(int(hndl), ctypes.c_void_p)

def gal_fast_malloc_cache_aligned(size: u32, invalidate: bool) -> CPtr:
    xs = np.empty(size, dtype=np.uint8)
    p = ctypes.c_void_p(xs.ctypes.data)
    global_arrays.append(p.value)
    my_list.append(xs)
    return p

def gal_fast_free_cache_aligned(algn_p: CPtr) -> None:
    global_arrays.remove(algn_p.value)

def gal_fast_l2dma_async_memcpy_init(apuc_id: u32) -> None:
    pass

def gal_fast_cache_dcache_flush_mlines_wrapper(start_addr: CPtr, size: u32) -> i32:
    pass

def gal_fast_l2dma_mem_to_mem_512_wrapper(dst: CPtr, src: CPtr, apc_od: u32) -> None:
    new_dest = ctypes.cast(dst, ctypes.c_void_p)
    new_src = ctypes.cast(src, ctypes.c_void_p)
    ctypes.memmove(new_dest, new_src, 512)

def gal_fast_l2dma_async_memcpy_end(apuc_id: u32) -> None:
    pass

def gal_init() -> None:
    pass

def gal_pm_start() -> None:
    pass

def gal_pm_stop() -> None:
    pass

def gal_get_pm_cycle_count(a: bool) -> i64:
    return 0
from lpython import i32, i64, CPtr, u64
import numpy as np
import ctypes

gdl_global_arrays = []
gdl_my_list = []

def gdl_init_wrapper() -> None:
    pass

def gdl_exit_wrapper() -> None:
    pass

def find_and_allocate_context() -> i64:
    return 0

def gdl_context_free_wrapper(ctx_id: i64) -> None:
    pass

def gdl_mem_alloc_aligned_wrapper(ctx_id : i64, size: i64, pool : i32, alignment : i32) -> u64:
    xs = np.empty(size, dtype=np.uint8)
    p = ctypes.c_void_p(xs.ctypes.data)
    gdl_global_arrays.append(p.value)
    gdl_my_list.append(xs)
    return p.value

def gdl_mem_free_wrapper(buffer : u64) -> None:
    gdl_global_arrays.remove(buffer)

def gdl_mem_handle_is_null_wrapper(hndl : u64) -> bool:
    return hndl == u64(0)

def gdl_mem_cpy_to_dev_wrapper(
        dest_handle: u64,
        src_pointer: CPtr,
        size_in_bytes: i64) -> i32:
    new_dest = ctypes.cast(dest_handle, ctypes.c_void_p)
    new_src = ctypes.cast(src_pointer, ctypes.c_void_p)
    ctypes.memmove(new_dest, new_src, size_in_bytes)
    return 0

def gdl_add_to_mem_handle_wrapper(inp : u64, addition : i64) -> u64:
    return ctypes.c_void_p(inp).value + addition

def gdl_mem_cpy_from_dev_wrapper(dest_pointer: CPtr, src_handle: u64, size_in_bytes: i64) -> i32:
    return gdl_mem_cpy_to_dev_wrapper(dest_pointer, src_handle, size_in_bytes)

def gdl_run_task_timeout_wrapper(
        ctx_handle: i64,
        task_number_from_the_C_side: i32,
        in_handle: u64,
        out_handle: u64
) -> i32:
    from device import task
    ret = task(in_handle, out_handle)
    return ret

def gdl_mem_handle_to_host_ptr_wrapper(handle : u64) -> CPtr:
    return ctypes.c_void_p(handle)

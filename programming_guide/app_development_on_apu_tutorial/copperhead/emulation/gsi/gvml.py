from lpython import u16, i32, u32, CPtr, Pointer, Callable, CPtr
import refgvml
import numpy as np
import ctypes

from belex_libs.memory import load_16, store_16
from arc_utils import vr_to_np, np_to_vr, np_to_vmr

VR_IDX = 15
VR_MARKERS_AND_FLAGS = 23

def _mrks_set_to_GL(mrk_src : u16) -> np.ndarray:
    u16_mrk_src = (vr_to_np(VR_MARKERS_AND_FLAGS) & np.uint16(mrk_src)).astype(np.uint16)
    return u16_mrk_src == mrk_src

def gvml_init_once() -> None:
    idx = np.arange(2**15, dtype=np.uint16)
    np_to_vr(VR_IDX, idx)

def gvml_load_16(dest_vr: u16, source_vm_reg: u16) -> None:
    load_16(int(dest_vr), int(source_vm_reg))

def gvml_add_f16(vdst : u16, vsrc1 : u16, vsrc2 : u16):
    src1 = vr_to_np(vsrc1)
    src2 = vr_to_np(vsrc2)
    dst = np.empty_like(src1)
    refgvml.add_f16(dst, src1, src2)
    np_to_vr(vdst, dst)

def gvml_sub_f16(vdst : u16, vsrc1 : u16, vsrc2 : u16):
    src1 = vr_to_np(vsrc1)
    src2 = vr_to_np(vsrc2)
    dst = np.empty_like(src1)
    refgvml.sub_f16(dst, src1, src2)
    np_to_vr(vdst, dst)

def gvml_mul_f16(vdst : u16, vsrc1 : u16, vsrc2 : u16):
    src1 = vr_to_np(vsrc1)
    src2 = vr_to_np(vsrc2)
    dst = np.empty_like(src1)
    refgvml.mul_f16(dst, src1, src2)
    np_to_vr(vdst, dst)    

def gvml_store_16(dest_vm_reg: u16, source_vr: u16) -> None:
    store_16(int(dest_vm_reg), int(source_vr))

def gvml_create_index_16(vdst : u16) -> None:
    idx = np.arange(2**15, dtype=np.uint16)
    np_to_vr(vdst, idx)

def gvml_lt_imm_u16(mdst : u16, vsrc : u16, val : u16) -> None:
    bool_mrk = vr_to_np(vsrc) < np.uint16(val)
    u16_mrk = (bool_mrk * np.uint16(mdst)).astype(np.uint16)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mdst)) | u16_mrk)

def gvml_reset_16(vreg : u16) -> None:
    np_to_vr(vreg, np.zeros(2**15, dtype=np.uint16))

def gvml_reset_16_msk(vreg : u16, msk : u16) -> None:
    v = vr_to_np(vreg)
    np_to_vr(vreg, v & ~np.uint16(msk))

def gvml_cpy_imm_16(vr_src : u16, val : u16)-> None:
    np_to_vr(vr_src, np.full(2**15, val, dtype=np.uint16))

def gvml_monotonic_transformation_nan2max_u16_f16(vr_dst : u16, vr_src : u16)-> None:
    dst = np.empty(2**15, dtype=np.uint16)
    refgvml.monotonic_transformation_nan2max_u16_f16(dst, vr_to_np(vr_src))
    np_to_vr(vr_dst, dst)

def gvml_mark_kmin_idxval_u16_mrk_g32k(mrk_dst : u16, vr_src : u16, vr_idx : u16, k : u32, mrk_src : u16, _2vtmp : u16)-> None:
    # write to temporaries to mimic GVML
    tmp = np.full(2**15, 0xbeef, dtype=np.uint16)
    np_to_vr(_2vtmp, tmp)
    np_to_vr(_2vtmp + u16(1), tmp)

    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    u8_mrk_src = np.packbits(bool_mrk_src, bitorder='little')

    u8_mrk_dst = np.empty_like(u8_mrk_src)

    refgvml.mark_kmin_idxval_u16_mrk_g32k(u8_mrk_dst, vr_to_np(vr_src), vr_to_np(vr_idx), k, u8_mrk_src)

    u16_mrk_dst = np.unpackbits(u8_mrk_dst, bitorder='little').astype(np.uint16) * np.uint16(mrk_dst)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mrk_dst)) | u16_mrk_dst)

def gvml_get_marked_data_xv(output : CPtr, vr_idx : u16, vr_data : u16, mrk_src : u16, num_ent: u32) -> i32:
    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    u8_mrk_src = np.packbits(bool_mrk_src, bitorder='little')

    return refgvml.get_marked_data_xv(output.value, vr_to_np(vr_idx), vr_to_np(vr_data), u8_mrk_src, num_ent)

def gvml_get_marked_data_sxvv_interval_wrapper(
    output: CPtr, 
    vr_merged_idx_lsb: u16,
    vr_merged_idx_msb: u16,
    vr_merged: u16,     
    val: u32,    
    mrk_src: u16,   
    num_ent: u32,
    interval_ent: u32,
    interval_func: Callable[[CPtr], None],
    interval_params: CPtr) -> i32:
    
    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    u8_mrk_src = np.packbits(bool_mrk_src, bitorder='little')

    num_extracted = refgvml.get_marked_data_sxvv_interval(
        output.value, 
        vr_to_np(vr_merged_idx_lsb), 
        vr_to_np(vr_merged_idx_msb), 
        vr_to_np(vr_merged), 
        val,
        u8_mrk_src,
        num_ent)

    num_intervals = u32(num_extracted) // interval_ent
    for _ in range(num_intervals):
        interval_func(interval_params)
    return i32(num_extracted)

def gvml_create_subgrp_index_u16(vr_dst : u16, grp_log : u16, sgrp_log : u16)-> None:
    dst = vr_to_np(vr_dst)
    refgvml.create_subgrp_index_u16(dst, grp_log, sgrp_log)
    np_to_vr(vr_dst, dst)

def gvml_eq_imm_16(mrk_dst : u16, vr_src : u16, val : u16)-> None:
    bool_vr = vr_to_np(vr_src) == val
    u16_mrk = bool_vr.astype(np.uint16) * np.uint16(mrk_dst)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mrk_dst)) | u16_mrk)

def gvml_create_index_u16_grp_sgrp(vr_dst : u16, grp_log : u16, sgrp_log : u16, valid_subgrp_size : u32, vr_tmp : u16)-> None:
    # write to temporary to mimic GVML
    tmp = np.full(2**15, 0xbeef, dtype=np.uint16)
    np_to_vr(vr_tmp, tmp)
    dst = vr_to_np(vr_dst)
    refgvml.create_index_u16_grp_sgrp(dst, grp_log, sgrp_log, valid_subgrp_size)
    np_to_vr(vr_dst, dst)

def gvml_cpy_imm_subgrp_16_grp(vr_dst : u16, grp_log : u16, sgrp_log : u16, imm_data : CPtr, data_size : u32)-> None:
    dst = vr_to_np(vr_dst)
    refgvml.cpy_imm_subgrp_16_grp(dst, grp_log, sgrp_log, imm_data.value, data_size)    
    np_to_vr(vr_dst, dst)

def gvml_add_subgrps_f16_grp(vr_dst : u16, vr_src : u16, grp_log : u16, sgrp_log : u16, dst_subgrp_index : u32, vmtmp : u16, vtmp : u16)-> None:
    # write to temporaries to mimic GVML
    tmp = np.full(2**15, 0xbeef, dtype=np.uint16)
    if sgrp_log < u16(refgvml.REFGVML_P2_4):
        np_to_vmr(vmtmp, tmp)
    np_to_vr(vtmp, tmp)
    dst = vr_to_np(vr_dst)
    src = vr_to_np(vr_src)
    refgvml.add_subgrps_f16_grp(dst, src, grp_log, sgrp_log, dst_subgrp_index)
    np_to_vr(vr_dst, dst)

def gvml_set_m(mrk_src : u16) -> None:
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, markers_and_flags | np.full(2**15, mrk_src, dtype=np.uint16))

def gvml_cpy_m(mrk_dst : u16, mrk_src : u16) -> None:
    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    u16_mrk = bool_mrk_src.astype(np.uint16) * np.uint16(mrk_dst)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mrk_dst)) | u16_mrk)

def gvml_cpy_from_mrk_16_msk(vr_dst : u16, mrk_src : u16, msk : u16)-> None:
    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    unmasked = vr_to_np(vr_dst) & ~np.uint16(msk)
    np_to_vr(vr_dst, np.where(bool_mrk_src, unmasked | np.uint16(msk), unmasked))

def gvml_cpy_16(vr_dst : u16, vr_src : u16)-> None:
    src = vr_to_np(vr_src)
    np_to_vr(vr_dst, src)

def gvml_merge_v15mrk1_g32k(v15mrk1_dst : u16, vidx_dst : u16, v15mrk1_src : u16)-> None:
    dst = vr_to_np(v15mrk1_dst)
    idx_dst = vr_to_np(vidx_dst)
    refgvml.merge_v15mrk1_g32k(dst, idx_dst, vr_to_np(v15mrk1_src), refgvml.REFGVML_MERGE_FEW)
    np_to_vr(v15mrk1_dst, dst)
    np_to_vr(vidx_dst, idx_dst)

def gvml_cpy_to_mrk_16_msk(mrk_dst : u16, vr_src : u16, msk : u16)-> None:
    vr_src_msk = vr_to_np(vr_src) & np.uint16(msk)
    u16_mrk_dst = np.where(vr_src_msk == np.uint16(msk), np.uint16(mrk_dst), 0)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mrk_dst)) | u16_mrk_dst.astype(np.uint16))

def gvml_xor_m(mrk_dst : u16, mrk_src1 : u16, mrk_src2 : u16)-> None:
    bool_mrk_src1 = _mrks_set_to_GL(mrk_src1)
    bool_mrk_src2 = _mrks_set_to_GL(mrk_src2)
    u16_mrk_dst = (bool_mrk_src1 ^ bool_mrk_src2).astype(np.uint16) * np.uint16(mrk_dst)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mrk_dst)) | u16_mrk_dst)

def gvml_and_m(mrk_dst : u16, mrk_src1 : u16, mrk_src2 : u16)-> None:
    bool_mrk_src1 = _mrks_set_to_GL(mrk_src1)
    bool_mrk_src2 = _mrks_set_to_GL(mrk_src2)
    u16_mrk_dst = (bool_mrk_src1 & bool_mrk_src2).astype(np.uint16) * np.uint16(mrk_dst)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mrk_dst)) | u16_mrk_dst)

def gvml_or_m(mrk_dst : u16, mrk_src1 : u16, mrk_src2 : u16)-> None:
    bool_mrk_src1 = _mrks_set_to_GL(mrk_src1)
    bool_mrk_src2 = _mrks_set_to_GL(mrk_src2)
    u16_mrk_dst = (bool_mrk_src1 | bool_mrk_src2).astype(np.uint16) * np.uint16(mrk_dst)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mrk_dst)) | u16_mrk_dst)    

def gvml_cpy_16_mrk(vr_dst : u16, vr_src : u16, mrk_src : u16)-> None:
    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    dst = np.where(bool_mrk_src, vr_to_np(vr_src), vr_to_np(vr_dst))
    np_to_vr(vr_dst, dst)

def gvml_cpy_imm_16_msk_mrk(vr_dst : u16, val : u16, msk : u16, mrk_src : u16)-> None:
    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    msk_u16 = np.uint16(msk)
    dst = vr_to_np(vr_dst)
    dst = np.where(bool_mrk_src, (dst & ~msk_u16) | (np.uint16(val) & msk_u16), dst)
    np_to_vr(vr_dst, dst)

def gvml_cpy_imm_16_mrk(vr_dst : u16, val : u16, mrk_src : u16)-> None:
    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    dst = np.where(bool_mrk_src, np.uint16(val), vr_to_np(vr_dst))
    np_to_vr(vr_dst, dst)

def gvml_get_marked_data(output : CPtr, start_vsrc : u16, num_vsrcs : u32, mrk_src : u16, num_ent: u32)-> u32:
    bool_mrk_src = _mrks_set_to_GL(mrk_src)
    u8_mrk_src = np.packbits(bool_mrk_src, bitorder='little')

    vrs = np.empty((num_vsrcs, 2**15), dtype=np.uint16)
    for i in range(num_vsrcs):
        vrs[i] = vr_to_np(start_vsrc + u16(i))

    return u32(refgvml.get_marked_data(output.value, vrs, u8_mrk_src, int(num_ent)))

def gvml_mod_u16(vdst: u16, vsrc1: u16, vsrc2: u16) -> None:
    src1 = vr_to_np(vsrc1)  
    src2 = vr_to_np(vsrc2)
    np_to_vr(vdst, src1 % src2)

def _convert_cptr_to_np_array(cptr: CPtr, size: u32) -> CPtr:
    return np.ctypeslib.as_array(ctypes.cast(cptr, ctypes.POINTER(ctypes.c_uint16)), shape=(size,))

def gvml_lookup_16(vdst: u16, vsrc: u16, lut_addr: Pointer[u16], lut_size: u32) -> None:
    src = vr_to_np(vsrc)
    dst = vr_to_np(vdst)
    lut_arr = _convert_cptr_to_np_array(lut_addr, lut_size)
    res = np.take(lut_arr, src, mode='clip')
    dst = np.where(src < len(lut_arr), res, dst)
    np_to_vr(vdst, dst)

def gvml_eq_imm_16_msk(mrk_dst: u16, vsrc: u16, val: u16, msk: u16) -> None:
    bool_vr = (vr_to_np(vsrc) & np.uint16(msk)) == np.uint16(val)
    u16_mrk = bool_vr.astype(np.uint16) * np.uint16(mrk_dst)
    markers_and_flags = vr_to_np(VR_MARKERS_AND_FLAGS)
    np_to_vr(VR_MARKERS_AND_FLAGS, (markers_and_flags & ~np.uint16(mrk_dst)) | u16_mrk)

def gvml_count_m_g32k_wrapper(mrk: u16) -> u32:
    bool_mrk = _mrks_set_to_GL(mrk)
    return u32(int(np.sum(bool_mrk)))

import numpy as np
import ctypes

from lpython import i8, u16, i32, u32, u64, CPtr, Pointer, Const
from belex.diri.half_bank import DIRI
from system_emulation.gsi_libs import getfullargspec, reg_mapping_from_kind, apl_set_rn_reg, apl_set_sm_reg, apl_set_l1_reg, apl_set_l1_reg_ext, apl_set_l2_reg
from gchl.fragments import (chf_cmn_cpy_16_msk, chf_csrc_reset_16_msk, load_16_gchl,
                            my_cpy_imm_16, store_16_gchl, swap_if_x_bigger_than_y, get_exp_and_shifted3_mant,
                            simple_sub_6msb_frag_calc_sgn_data_t2, move_small_shift_to_5msb_and_limit_it_to_16, sr_sticky_by_vec_t2,
                            negate_mant_16_msb_sign_t1, addsub_frag_add_no_co_u16, calc_res_sgn, negate_mant_16_t1,
                            _5b_on_10_to_14b_left_mask_t3, add_mask_to_mant, shift_left_data_size15_count_lsb10_t1,
                            simple_sub_6msb_frag_t2, remove_mask_from_mant, round_mant_and_inc_exp, build_res,
                            negate_y0, negate_y1, sr_16_t0_imm_frag, monotonic_transformation_f16_u16_nan2max_frag,
                            is_small_f16, swap_and_set_mrk_t1, create_and_shift_left_tmpx_t1, init_mul_11_from_f16,
                            _3to2_mul_f16, _3to2_mul_f16_before_last, _3to2_mul_f16_last, addsub_carry_prefix,
                            addsub_carry_in_out_u12_suffix, add_f16exp, add_f16biad_shift, simple_sub_7msb_frag_t2,
                            simple_dec_7msb_frag, min_zero_and_negate, max_0xF_on_7msb_and_sr3, sr_16_by_vec_t1,
                            shift_right_one_when_mant_is_too_big, is_mrk, build_f16_res)
import sys

_32K: u32 = u32(32*1024)
BYTES_IN_U16: u32 = u32(2)

l2_dma = np.empty(_32K, dtype=np.uint16)

def vr_to_np(vr, dtype=None):
    if None == dtype:
        dtype = np.uint16
    diri = DIRI.context()
    return np.packbits(diri.hb[int(vr)], bitorder='little').view(dtype)
   
def vmr_to_np(vmr, dtype=None):
    if None == dtype:
        dtype = np.uint16
    diri = DIRI.context()
    return np.packbits(diri.vmr[int(vmr)], bitorder='little').view(dtype)
   
def np_to_vr(vr, array):
    diri = DIRI.context()
    diri.hb[int(vr)] = np.unpackbits(array.view(np.uint8), bitorder='little').astype(bool).reshape(32768, 16)
   
def np_to_vmr(vmr, array):
    bool_array = np.unpackbits(array.view(np.uint8), bitorder='little').astype(bool).reshape(32768, 16)
    diri = DIRI.context()
    from belex.utils.memory_utils import vmr_to_row
    lower_l1_addr = vmr_to_row(int(vmr))
    upper_l1_addr = lower_l1_addr + 4
    vmr_sections = np.array([0, 4, 8, 12])
    for group, l1_addr in enumerate(range(lower_l1_addr, upper_l1_addr)):
        diri.L1[l1_addr] = bool_array[::, group + vmr_sections]

def increment_ptr(p : CPtr, offset : i32) -> CPtr:
    if type(p) is ctypes.c_void_p:
        return ctypes.c_void_p(p.value + offset)
    if type(p) is int:
        return p + offset
    raise ValueError(f'type {type(p)} is not supported')

def dereference_u16_ptr(p : CPtr) -> u16:
    p_u16 = ctypes.cast(p, ctypes.POINTER(ctypes.c_uint16))
    return p_u16[0]

def direct_dma_l4_to_l1_32k(vmr_dst : u16, l4_src : CPtr) -> None:
    l4_addr = ctypes.cast(l4_src, ctypes.POINTER(ctypes.c_uint16))
    src_array = np.ctypeslib.as_array(l4_addr, shape=(2**15,))
    np_to_vmr(vmr_dst, src_array)

def direct_dma_l1_to_l4_32k(l4_dst : CPtr, vmr_src : u16) -> None:
    l4_addr = ctypes.cast(l4_dst, ctypes.POINTER(ctypes.c_uint16))
    dst_array = np.ctypeslib.as_array(l4_addr, shape=(2**15,))
    dst_array[:] = vmr_to_np(vmr_src)

def direct_dma_l4_to_l2_32k_start(l4_src: CPtr) -> None:
    l4_addr = ctypes.cast(l4_src, ctypes.POINTER(ctypes.c_uint16)) 
    l4_array = np.ctypeslib.as_array(l4_addr, shape=(2**15,))
    global l2_dma
    l2_dma = l4_array

def dma_l2_sync() -> None:
    pass

def direct_dma_l2_to_l1_32k(vmr_dst: u16) -> None:
    global l2_dma
    np_to_vmr(vmr_dst, l2_dma)

def c_strcpy(dst : Pointer[i8], src : Const[str])-> None:
    for i, c in enumerate(src):
        dst[i] = i8(c)
    dst[i + 1] = i8(0)

def i8_array_to_str(x : Pointer[i8]) -> str:
    i = 0
    b = bytearray()
    while x[i] != 0:
        b.append(x[i])
        i += 1
    b.append(0)
    ret = b.decode()
    return ret

def GSI_IS_ERR_PTR_OR_NULL(e : CPtr) -> bool:
    c_u64 = e.value
    return 0 == c_u64 or (c_u64 < 0 and -c_u64 < u64(4096))

def CH_RUN_FRAG_ASYNC(fn_nym: bytes, *arg_vals) -> None:
    wrapper_fn = sys.modules[__name__].__dict__[fn_nym.decode()]
    llb_fn = wrapper_fn.__wrapped__

    reg_mappings_by_param = {}
    spec = getfullargspec(llb_fn)
    for name, kind in spec.annotations.items():
        if name != "return":
            reg_mapping = reg_mapping_from_kind[kind]
            reg_mappings_by_param[name] = reg_mapping

    kwargs = {}

    for index in range(0, len(arg_vals), 2):
        param_nym = arg_vals[index].decode()
        param_reg = arg_vals[index + 1]
        kwargs[param_nym] = reg_mappings_by_param[param_nym][param_reg]

    wrapper_fn(**kwargs)

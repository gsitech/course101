#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_iv.h>              /* gvml_create_index_16() */
#include <gsi/libgvml_element_wise.h>    /* gvml_add_f16() */
#include <gsi/libgvml_min_max.h>         /* gvml_mark_kmin_idxval_u16_mrk_g32k() */
#include <gsi/libgvml_get_marked_data.h> /* gvml_get_marked_data_xv() */
#include <gsi/libgvml_debug.h>

#include "gsi_device_lab_3.h"
#include "gsi_device_lab_3_dma.h"

static struct {
	uint32_t num_records;
	uint32_t num_features;
	enum gvml_mrks_n_flgs mrk_num_records;
	enum gvml_vr16 vr_idx;
} g_db_data = {
	.num_records = 0,
	.num_features = 0,
	.mrk_num_records = GVML_MRK0,
	.vr_idx = GVML_VR16_0,
};

static int load_db(struct gd_lab_3_load_db *load_db_data)
{
	if (load_db_data->num_features > GD_LAB_3_MAX_NUM_FEATURES) {
		gsi_error("number of features (%u) must not exceed %d", (unsigned )load_db_data->num_features, GD_LAB_3_MAX_NUM_FEATURES);
		return gsi_status(EINVAL);
	}

	if (load_db_data->num_records > GD_LAB_3_MAX_NUM_RECORDS_IN_DB) {
		gsi_error("number of records (%u) must not exceed %d", (unsigned )load_db_data->num_records, GD_LAB_3_MAX_NUM_RECORDS_IN_DB);
		return gsi_status(EINVAL);
	}

	g_db_data.num_features = load_db_data->num_features;
	g_db_data.num_records = load_db_data->num_records;
	gvml_create_index_16(g_db_data.vr_idx);
	gvml_lt_imm_u16(g_db_data.mrk_num_records, g_db_data.vr_idx, (uint16_t)g_db_data.num_records);

	uint16_t *ptr_in = gal_mem_handle_to_apu_ptr(load_db_data->db);

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);

	for (uint32_t f = 0; f < load_db_data->num_features; ++f) {
		direct_dma_l4_to_l1_32k(f, &ptr_in[f * GD_LAB_3_MAX_NUM_RECORDS_IN_DB]);
	}

	return 0;
}

static void abs_f16(enum gvml_vr16 vr)
{
	gvml_reset_16_msk(vr, 0x8000);
}

static int do_search(struct gd_lab_3_search *search_data)
{
	if (search_data->k > g_db_data.num_records) {
		gsi_error("k (%u) must not exceed number of records (%u)", (unsigned )search_data->k, (unsigned )g_db_data.num_records);
		return gsi_status(EINVAL);
	}

	enum gvml_vr16 vr_distances = GVML_VR16_1;
	enum gvml_vr16 vr_records = GVML_VR16_2;
	enum gvml_vr16 vr_query = GVML_VR16_3;
	enum gvml_mrks_n_flgs mrk_kmin = GVML_MRK1;

	const uint16_t *p_q = gal_mem_handle_to_apu_ptr(search_data->queries);
	struct gd_lab_3_idx_val *p_iv = gal_mem_handle_to_apu_ptr(search_data->output);

	for (uint32_t q = 0; q < search_data->num_queries; ++q, p_q += g_db_data.num_features, p_iv += search_data->k) {
		gvml_reset_16(vr_distances);
		for (uint32_t f = 0; f < g_db_data.num_features; ++f) {
			gvml_cpy_imm_16(vr_query, p_q[f]);
			gvml_load_16(vr_records, f);
			gvml_sub_f16(vr_records, vr_records, vr_query);
			abs_f16(vr_records);
			gvml_add_f16(vr_distances, vr_distances, vr_records);
		}
		gvml_monotonic_transformation_nan2max_u16_f16(vr_records, vr_distances);
		gvml_mark_kmin_idxval_u16_mrk_g32k(
			mrk_kmin,         /* enum gvml_mrks_n_flgs mdst */
			vr_records,       /* enum gvml_vr16 vsrc_val */
			g_db_data.vr_idx, /* enum gvml_vr16 vsrc_idx */
			search_data->k,   /* unsigned int k */
			g_db_data.mrk_num_records, /* enum gvml_mrks_n_flgs msrc */
			vr_query);        /* enum gvml_vr16 _2vtmp */
		gvml_get_marked_data_xv(
			p_iv,             /* void *buff */
			g_db_data.vr_idx, /* enum gvml_vr16 v16src */
			vr_distances,     /* enum gvml_vr16 v15src */
			mrk_kmin,         /* enum gvml_mrks_n_flgs msrc */
			search_data->k);  /* unsigned int num_ent */
	}

	return 0;
}


GAL_TASK_ENTRY_POINT(gd_lab_3, in, out)
{
	struct gd_lab_3_cmd *cmd = (struct gd_lab_3_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_3_CMD_LOAD_DB:
		gvml_init_once();
		ret = load_db(&cmd->load_db_data);
		break;
	case GD_LAB_3_CMD_SEARCH:
		ret = do_search(&cmd->search_data);
		break;
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, (int )cmd->cmd); /* aborts execution */
		break;
	}

	return ret;
}

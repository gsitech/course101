#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <refgvml.h>
#include <refgvml_element_wise.h>

#include <gsi/gsi_sim_config.h>

GDL_TASK_DECLARE(gd_lab_3);
#include "gsi_device_lab_3.h"

static int load_db(
	gdl_context_handle_t ctx_id,
	uint16_t records[],
	uint32_t num_records,
	uint32_t num_features)
{
	int ret;
	gdl_mem_handle_t dev_cmd_buf = GDL_MEM_HANDLE_NULL, dev_records_buf = GDL_MEM_HANDLE_NULL;
	uint16_t *pre_processed_records = NULL;
	uint64_t pre_processed_records_size = gd_lab_3_get_preprocessed_db_size(num_features);

	pre_processed_records = malloc(pre_processed_records_size);
	if (NULL == pre_processed_records) {
		gsi_error("malloc() failed to allocate %lu bytes", pre_processed_records_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	gd_lab_3_preprocess_db(pre_processed_records, records, num_features, num_records);

	dev_records_buf = gdl_mem_alloc_aligned(ctx_id, pre_processed_records_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(dev_records_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", pre_processed_records_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(dev_records_buf, pre_processed_records, pre_processed_records_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	struct gd_lab_3_cmd cmd = {
		.cmd = GD_LAB_3_CMD_LOAD_DB,
		.load_db_data = {
			.db = dev_records_buf,
			.num_records = num_records,
			.num_features = num_features
		}
	};

	uint64_t cmd_buf_size = sizeof(cmd);
	dev_cmd_buf = gdl_mem_alloc_aligned(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(dev_cmd_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", cmd_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(dev_cmd_buf, &cmd, cmd_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_run_task_timeout(
			ctx_id,              /* @ctx_handler - the id of a hardware context previously allocated */
			GDL_TASK(gd_lab_3),  /* @code_offset - the code offset of the function that the task should execute */
			dev_cmd_buf,         /* @inp - input memory handle */
			GDL_MEM_HANDLE_NULL, /* @outp - output memory handle */
			GDL_TEMPORARY_DEFAULT_MEM_BUF,      /* @mem_buf - an array of previously allocated memory handles and their sizes */
			GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE, /* @buf_size - the length of the mem_buf array */
			GDL_TEMPORARY_DEFAULT_CORE_INDEX,   /* @apuc_idx - the apuc that the task should be executed on */
			NULL,              /* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
			0,                 /* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
			GDL_USER_MAPPING); /* @map_type - determine the mapping type for the specific task */

	if (ret) {
		gsi_error("gdl_run_task_timeout() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

CLEAN_UP:
	gdl_mem_free(dev_cmd_buf);
	gdl_mem_free(dev_records_buf);
	free(pre_processed_records);

	return ret;
}

static int do_search(
	gdl_context_handle_t ctx_id,
	struct gd_lab_3_idx_val iv_dst[],
	const uint16_t queries[],
	uint32_t num_queries,
	uint32_t num_features,
	uint32_t k)
{
	int ret;
	gdl_mem_handle_t dev_cmd_buf = GDL_MEM_HANDLE_NULL, io_dev_bufs = GDL_MEM_HANDLE_NULL;

	uint64_t queries_size = sizeof(*queries) * num_queries * num_features;
	uint64_t output_size = sizeof(*iv_dst) * num_queries * k;
	uint64_t io_dev_buf_size = queries_size + output_size;

	io_dev_bufs = gdl_mem_alloc_aligned(ctx_id, io_dev_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(io_dev_bufs)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", io_dev_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(io_dev_bufs, queries, queries_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	struct gd_lab_3_cmd cmd = {
		.cmd = GD_LAB_3_CMD_SEARCH,
		.search_data = {
			.queries = io_dev_bufs,
			.num_queries = num_queries,
			.k = k
		}
	};

	ret = gdl_add_to_mem_handle(&cmd.search_data.output, cmd.search_data.queries, queries_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	uint64_t cmd_buf_size = sizeof(cmd);
	dev_cmd_buf = gdl_mem_alloc_aligned(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(dev_cmd_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", cmd_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(dev_cmd_buf, &cmd, cmd_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_run_task_timeout(
			ctx_id,              /* @ctx_handler - the id of a hardware context previously allocated */
			GDL_TASK(gd_lab_3),  /* @code_offset - the code offset of the function that the task should execute */
			dev_cmd_buf,         /* @inp - input memory handle */
			GDL_MEM_HANDLE_NULL, /* @outp - output memory handle */
			GDL_TEMPORARY_DEFAULT_MEM_BUF,      /* @mem_buf - an array of previously allocated memory handles and their sizes */
			GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE, /* @buf_size - the length of the mem_buf array */
			GDL_TEMPORARY_DEFAULT_CORE_INDEX,   /* @apuc_idx - the apuc that the task should be executed on */
			NULL,              /* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
			0,                 /* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
			GDL_USER_MAPPING); /* @map_type - determine the mapping type for the specific task */

	if (ret) {
		gsi_error("gdl_run_task_timeout() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_from_dev(iv_dst, cmd.search_data.output, output_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_from_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

CLEAN_UP:
	gdl_mem_free(dev_cmd_buf);
	gdl_mem_free(io_dev_bufs);

	return ret;
}

static float random_float_0_to_1(void)
{
	return (float)rand() / (float)(RAND_MAX);
}

static void init_random_f16_array(uint16_t *dst, uint32_t num_rows, uint32_t num_cols)
{
	for (uint32_t r = 0; r < num_rows; ++r) {
		for (uint32_t f = 0; f < num_cols; ++f) {
			*dst++ = refgvml_float_2_f16(random_float_0_to_1());
		}
	}
}

static uint16_t refgvml_abs_f16(uint16_t x)
{
	return x & 0x7FFF;
}

static uint16_t calc_l1_distance(const uint16_t record[], const uint16_t query[], uint32_t num_features)
{
	uint16_t ret = 0;

	for (uint32_t f = 0; f < num_features; ++f) {
		uint16_t diff = refgvml_sub_f16(record[f], query[f]);
		uint16_t abs_diff = refgvml_abs_f16(diff);
		ret = refgvml_add_f16(ret, abs_diff);
	}

	return ret;
}

static int f16_is_lhs_lt_rhs(uint16_t lhs, uint16_t rhs)
{
	return refgvml_monotonic_transformation_nan2max_u16_f16(lhs) < refgvml_monotonic_transformation_nan2max_u16_f16(rhs);
}

static int f16_idx_val_cmp_ascending(const void *lhs, const void *rhs)
{
	const struct gd_lab_3_idx_val *iv_lhs = (struct gd_lab_3_idx_val *)lhs;
	const struct gd_lab_3_idx_val *iv_rhs = (struct gd_lab_3_idx_val *)rhs;
	return (f16_is_lhs_lt_rhs(iv_lhs->val, iv_rhs->val) || (iv_lhs->val == iv_rhs->val && iv_lhs->idx < iv_rhs->idx)) ? -1 : 1;
}

static void sort_f16_idx_val_ascending(struct gd_lab_3_idx_val vi[], uint32_t num_elements)
{
	qsort(vi, num_elements, sizeof(*vi), f16_idx_val_cmp_ascending);
}

static void calc_ref_iv(struct gd_lab_3_idx_val vi[], const uint16_t records[], uint32_t num_records, uint32_t num_features, const uint16_t queries[], uint32_t num_queries)
{
	for (uint32_t q = 0; q < num_queries; ++q) {
		for (uint32_t r = 0; r < num_records; ++r) {
			vi[q * num_records + r].val = calc_l1_distance(&records[r * num_features], &queries[q * num_features], num_features);
			vi[q * num_records + r].idx = (uint16_t)r;
		}
	}
}

static int check_results(struct gd_lab_3_idx_val iv_res[], struct gd_lab_3_idx_val iv_ref[], uint32_t num_records, uint32_t num_queries, uint32_t k)
{
	/* Sanity check */
	for (uint32_t q = 0; q < num_queries; ++q) {
		for (uint32_t e = 0; e < k; ++e) {
			struct gd_lab_3_idx_val iv_res_element = iv_res[q * k + e];
			if (iv_res_element.idx >= num_records) {
				gsi_error("q = %u, e = %u: result has index >= num_records (val = 0x%x, idx = %d), num_records = %u",
					q, e, iv_res_element.val, iv_res_element.idx, num_records);
				return -1;
			}
			struct gd_lab_3_idx_val iv_ref_element = iv_ref[q * num_records + iv_res_element.idx];
			if (iv_res_element.val != iv_ref_element.val) {
				gsi_error("q = %u, e = %u: mismatch in distances idx = %d (res val = 0x%x, ref val = 0x%x)",
					q, e, iv_res_element.idx, iv_res_element.val, iv_ref_element.val);
				return -1;
			}
		}
	}

	/* Check best k */
	for (uint32_t q = 0; q < num_queries; ++q) {
		sort_f16_idx_val_ascending(&iv_ref[q * num_records], num_records);
		sort_f16_idx_val_ascending(&iv_res[q * k], k);
		for (uint32_t e = 0; e < k; ++e) {
			struct gd_lab_3_idx_val iv_res_element = iv_res[q * k + e];
			struct gd_lab_3_idx_val iv_ref_element = iv_ref[q * num_records + e];
			if (iv_res_element.val != iv_ref_element.val || iv_res_element.idx != iv_ref_element.idx) {
				gsi_error("q = %u, e = %u: mismatch in results res: (val = 0x%x, idx = %d), ref: (val = 0x%x, idx = %d)\n",
					q, e, iv_res_element.val, iv_res_element.idx, iv_ref_element.val, iv_ref_element.idx);
				return -1;
			}
		}
	}

	return 0;
}

struct lab_3_args {
	uint32_t num_records;
	uint32_t num_features;
	uint32_t num_queries;
	uint32_t k;
	uint32_t num_searches;
};

static int parse_args(struct lab_3_args *args, int argc, char *argv[])
{
	if (6 != argc) {
		gsi_error("usage: %s num_records num_features num_queries k num_searches", argv[0]);
		return gsi_status(EINVAL);
	}

	args->num_records = atoi(argv[1]);
	args->num_features = atoi(argv[2]);
	args->num_queries = atoi(argv[3]);
	args->k = atoi(argv[4]);
	args->num_searches = atoi(argv[5]);

	printf("****************** ARGS ******************\n");
	printf("num_records = %u\n", args->num_records);
	printf("num_features = %u\n", args->num_features);
	printf("num_queries = %u\n", args->num_queries);
	printf("k = %u\n", args->k);
	printf("num_searches = %u\n", args->num_searches);
	printf("******************************************\n");

	return 0;
}


// For Simulator:
enum { NUM_CTXS = 1 };
static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};


int main(int argc, char *argv[])
{
	gsi_libsys_init(
		argv[0], /* program name */
		true);   /* log_to_screen */

	struct lab_3_args args;
	int ret = parse_args(&args, argc, argv);
	if (ret) {
		gsi_fatal("parse_args() failed");
	}

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	uint32_t num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	printf("Num Contexts = %u\n", num_ctxs);
	/* Use first available context */
	gdl_context_handle_t valid_ctx_id;
	uint32_t ctx;
	for (ctx = 0; ctx < num_ctxs; ++ctx) {
		if (contexts_desc[ctx].status == GDL_CONTEXT_READY) {
			valid_ctx_id = contexts_desc[ctx].ctx_id;
			printf("Memory Size = %0.1fG\n", (float)contexts_desc[ctx].mem_size / 1024L / 1024L / 1024L);
			printf("Num Apucs = %d\n", contexts_desc[ctx].num_apucs);
			break;
		}
	}

	if (ctx == num_ctxs) {
		gsi_fatal("Failed to find valid context");
	}

	const long long unsigned int const_mapped_size_req = 3L * 1024L * 1024L * 1024L;
	long long unsigned int const_mapped_size_recv, dynamic_mapped_size_recv;

	ret = gdl_context_alloc(valid_ctx_id, const_mapped_size_req, &const_mapped_size_recv, &dynamic_mapped_size_recv);
	if (ret) {
		gsi_fatal("gdl_context_alloc failed: %s", gsi_status_errorstr(ret));
	}
	printf("Constantly mapped memory = %0.1fG\n", (float)const_mapped_size_recv / 1024L / 1024L / 1024L);
	printf("Dynamically mapped memory = %0.1fG\n", (float)dynamic_mapped_size_recv / 1024L / 1024L / 1024L);

	uint16_t *records = NULL, *queries = NULL;
	struct gd_lab_3_idx_val *iv_ref = NULL, *iv_res = NULL;

	records = malloc(sizeof(uint16_t) * args.num_records * args.num_features);
	queries = malloc(sizeof(uint16_t) * args.num_queries * args.num_features);
	iv_ref = malloc(sizeof(struct gd_lab_3_idx_val) * args.num_records * args.num_queries);
	iv_res = malloc(sizeof(struct gd_lab_3_idx_val) * args.k * args.num_queries);

	if (NULL == records || NULL == queries || NULL == iv_ref || NULL == iv_res) {
		gsi_error("malloc failed");
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	init_random_f16_array(records, args.num_records, args.num_features);

	printf("Loading database ...\n");
	ret = load_db(valid_ctx_id, records, args.num_records, args.num_features);
	if (ret) {
		gsi_error("load_db() failed with %d", ret);
		goto CLEAN_UP;
	}
	printf("Finished loading database\n");

	for (uint32_t s = 0; s < args.num_searches; ++s) {
		init_random_f16_array(queries, args.num_queries, args.num_features);
		printf("Performing search %u of %u ...\n", s + 1, args.num_searches);
		ret = do_search(valid_ctx_id, iv_res, queries, args.num_queries, args.num_features, args.k);
		if (ret) {
			gsi_error("do_search() failed with %d", ret);
			goto CLEAN_UP;
		}
		printf("Finished searching\n");

		printf("Checking results ...\n");
		calc_ref_iv(iv_ref, records, args.num_records, args.num_features, queries, args.num_queries);
		ret = check_results(iv_res, iv_ref, args.num_records, args.num_queries, args.k);
		if (ret) {
			goto CLEAN_UP;
		}
		printf("Finished checking results\n");
	}

CLEAN_UP:
	free(iv_res);
	free(iv_ref);
	free(queries);
	free(records);

	gdl_context_free(valid_ctx_id);
	gdl_exit();

	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nFailure\n");
	} else {
		printf("\nSuccess\n");
	}
	return ret;
}

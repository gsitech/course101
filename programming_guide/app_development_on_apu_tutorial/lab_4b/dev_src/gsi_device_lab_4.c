#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_iv.h>              /* gvml_create_index_16() */
#include <gsi/libgvml_element_wise.h>    /* gvml_add_f16() */
#include <gsi/libgvml_min_max.h>         /* gvml_mark_kmin_idxval_u16_mrk_g32k() */
#include <gsi/libgvml_get_marked_data.h> /* gvml_get_marked_data_xv() */
#include <gsi/libgvml_debug.h>

#include "gsi_device_lab_4.h"
#include "gsi_device_lab_4_dma.h"

static struct {
	uint32_t num_records;
	uint32_t num_features;
	uint32_t num_cols_per_rec_pow_2;
	enum gvml_mrks_n_flgs mrk_valid_records;
	enum gvml_vr16 vr_idx;
} g_db_data = {
	.num_records = 0,
	.num_features = 0,
	.mrk_valid_records = GVML_MRK0,
	.vr_idx = GVML_VR16_0,
};

static void create_mrk(
	enum gvml_mrks_n_flgs mrk_dst,
	uint32_t num_cols_per_rec_pow_2,
	enum gvml_vr16 vr_tmp)
{
	gvml_create_subgrp_index_u16(vr_tmp, num_cols_per_rec_pow_2 + GVML_P2_4, GVML_P2_4);
	gvml_eq_imm_16(mrk_dst, vr_tmp, 0);
}

static int load_db(struct gd_lab_4_load_db *load_db_data)
{
	if ((load_db_data->num_features >> load_db_data->num_cols_per_rec_pow_2) > GD_LAB_4_MAX_NUM_FEATURES) {
		gsi_error("The combination of num_features = %u and num_cols_per_rec_pow_2 = %u exceeds available L1 memory", (unsigned )load_db_data->num_features, (unsigned )load_db_data->num_cols_per_rec_pow_2);
		return gsi_status(EINVAL);
	}

	if ((load_db_data->num_records << load_db_data->num_cols_per_rec_pow_2) > GD_LAB_4_MAX_NUM_RECORDS_IN_DB) {
		gsi_error("The combination of num_records = %u and num_cols_per_rec_pow_2 = %u exceeds available L1 memory", (unsigned )load_db_data->num_records, (unsigned )load_db_data->num_cols_per_rec_pow_2);
		return gsi_status(EINVAL);
	}

	g_db_data.num_features = load_db_data->num_features;
	g_db_data.num_records = load_db_data->num_records;
	g_db_data.num_cols_per_rec_pow_2 = load_db_data->num_cols_per_rec_pow_2;
	gvml_create_index_u16_grp_sgrp(g_db_data.vr_idx, g_db_data.num_cols_per_rec_pow_2 + GVML_P2_4, GVML_P2_4, 1 << GVML_P2_4, GVML_VR16_1);
	create_mrk(g_db_data.mrk_valid_records, g_db_data.num_cols_per_rec_pow_2, GVML_VR16_1);

	uint16_t *ptr_in = gal_mem_handle_to_apu_ptr(load_db_data->db);

	uint32_t num_feature_grps = g_db_data.num_features >> g_db_data.num_cols_per_rec_pow_2;
	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);
	for (uint32_t fg = 0; fg < num_feature_grps; ++fg) {
		direct_dma_l4_to_l1_32k(fg, &ptr_in[fg * GD_LAB_4_MAX_NUM_RECORDS_IN_DB]);
	}

	return 0;
}

static void abs_f16(enum gvml_vr16 vr)
{
	gvml_reset_16_msk(vr, 0x8000);
}

static int do_search(struct gd_lab_4_search *search_data)
{
	if (search_data->k > g_db_data.num_records) {
		gsi_error("k (%u) must not exceed number of records (%u)", (unsigned )search_data->k, (unsigned )g_db_data.num_records);
		return gsi_status(EINVAL);
	}

	enum gvml_vr16 vr_distances = GVML_VR16_1;
	enum gvml_vr16 vr_records = GVML_VR16_2;
	enum gvml_vr16 vr_query = GVML_VR16_3;
	enum gvml_mrks_n_flgs mrk_kmin = GVML_MRK1;

	const uint16_t *p_q = gal_mem_handle_to_apu_ptr(search_data->queries);
	struct gd_lab_4_idx_val *p_iv = gal_mem_handle_to_apu_ptr(search_data->output);
	uint32_t num_feature_grps = g_db_data.num_features >> g_db_data.num_cols_per_rec_pow_2;

	for (uint32_t q = 0; q < search_data->num_queries; ++q, p_q += g_db_data.num_features, p_iv += search_data->k) {
		gvml_reset_16(vr_distances);
		for (uint32_t fg = 0; fg < num_feature_grps; ++fg) {
			gvml_cpy_imm_subgrp_16_grp(
				vr_query,                                     /* enum gvml_vr16 vdst */
				g_db_data.num_cols_per_rec_pow_2 + GVML_P2_4, /* enum gvml_power2_sizes grp */
				GVML_P2_4,                                    /* enum gvml_power2_sizes sgrp */
				&p_q[fg << g_db_data.num_cols_per_rec_pow_2], /* const uint16_t *data */
				1 << g_db_data.num_cols_per_rec_pow_2);       /* unsigned int data_size */
			gvml_load_16(vr_records, fg);
			gvml_sub_f16(vr_records, vr_records, vr_query);
			abs_f16(vr_records);
			gvml_add_f16(vr_distances, vr_distances, vr_records);
		}
		gvml_add_subgrps_f16_grp(
			vr_distances,                   /* enum gvml_vr16 vdst */
			vr_distances,                   /* enum gvml_vr16 vsrc */
			g_db_data.num_cols_per_rec_pow_2 + GVML_P2_4,   /* enum gvml_power2_sizes grp */
			GVML_P2_4,                      /* enum gvml_power2_sizes sgrp */
			0,                              /* unsigned int dst_sgrp_idx */
			GVML_VM_0,                      /* enum gvml_vm_reg vmtmp  - in practice not used */
			vr_query);                      /* enum gvml_vr16 vtmp */

		gvml_monotonic_transformation_nan2max_u16_f16(vr_records, vr_distances);
		gvml_mark_kmin_idxval_u16_mrk_g32k(
			mrk_kmin,           /* enum gvml_mrks_n_flgs mdst */
			vr_records,         /* enum gvml_vr16 vsrc_val */
			g_db_data.vr_idx,   /* enum gvml_vr16 vsrc_idx */
			search_data->k,     /* unsigned int k */
			g_db_data.mrk_valid_records,    /* enum gvml_mrks_n_flgs msrc */
			vr_query);          /* enum gvml_vr16 _2vtmp */
		gvml_get_marked_data_xv(
			p_iv,               /* void *buff */
			g_db_data.vr_idx,   /* enum gvml_vr16 v16src */
			vr_distances,       /* enum gvml_vr16 v15src */
			mrk_kmin,           /* enum gvml_mrks_n_flgs msrc */
			search_data->k);    /* unsigned int num_ent */
	}

	return 0;
}


GAL_TASK_ENTRY_POINT(gd_lab_4, in, out)
{
	struct gd_lab_4_cmd *cmd = (struct gd_lab_4_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_4_CMD_LOAD_DB:
		gvml_init_once();
		ret = load_db(&cmd->load_db_data);
		break;
	case GD_LAB_4_CMD_SEARCH:
		ret = do_search(&cmd->search_data);
		break;
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, (int )cmd->cmd); /* aborts execution */
		break;
	}

	return ret;
}

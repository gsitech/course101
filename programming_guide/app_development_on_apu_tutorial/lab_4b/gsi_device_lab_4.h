#ifndef GSI_DEVICE_LAB_4_H
#define GSI_DEVICE_LAB_4_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

enum {
	GD_LAB_4_MAX_NUM_RECORDS_IN_DB = 32 * 1024,
	GD_LAB_4_MAX_NUM_FEATURES = 48,
};

enum gd_lab_4_cmd_type {
	GD_LAB_4_CMD_LOAD_DB,
	GD_LAB_4_CMD_SEARCH,
	GD_LAB_4_NUM_CMDS
};

static inline uint64_t gd_lab_4_get_preprocessed_db_size(uint32_t num_features, uint32_t num_cols_per_rec_pow_2)
{
	const uint32_t _32k = 32 * 1024;
	return sizeof(uint16_t) * (num_features >> num_cols_per_rec_pow_2) * _32k;
}

static inline int gd_lab_4_preprocess_db(uint16_t dst[], const uint16_t src[], uint32_t num_records, uint32_t num_features, uint32_t num_cols_per_rec_pow_2)
{
	if (num_cols_per_rec_pow_2 > 3) {
		gsi_error("num_cols_per_rec_pow_2 (%u) must be <= 3", (unsigned )num_cols_per_rec_pow_2);
		return gsi_status(EINVAL);
	}
	if (num_records & 3) {
		gsi_error("num_records (%u) must be a multiple of 4", (unsigned )num_records);
		return gsi_status(EINVAL);
	}
	uint32_t num_feature_grps = num_features >> num_cols_per_rec_pow_2;
	if (num_features != num_feature_grps << num_cols_per_rec_pow_2) {
		gsi_error("num_features (%u) must be a multiple of (2 ^ num_cols_per_rec_pow_2), (num_cols_per_rec_pow_2 = %u)", (unsigned )num_features, (unsigned )num_cols_per_rec_pow_2);
		return gsi_status(EINVAL);
	}
	if ((num_features >> num_cols_per_rec_pow_2) > GD_LAB_4_MAX_NUM_FEATURES) {
		gsi_error("The combination of num_features = %u and num_cols_per_rec_pow_2 = %u exceeds available L1 memory", (unsigned )num_features, (unsigned )num_cols_per_rec_pow_2);
		return gsi_status(EINVAL);
	}
	if ((num_records << num_cols_per_rec_pow_2) > GD_LAB_4_MAX_NUM_RECORDS_IN_DB) {
		gsi_error("The combination of num_records = %u and num_cols_per_rec_pow_2 = %u exceeds available L1 memory", (unsigned )num_records, (unsigned )num_cols_per_rec_pow_2);
		return gsi_status(EINVAL);
	}

	uint32_t num_quad_recs = num_records / 4;
	uint64_t _32k = 32 * 1024;
	uint64_t num_cols_per_rec = 1 << num_cols_per_rec_pow_2;

	for (uint32_t qr = 0; qr < num_quad_recs; ++qr) {
		for (uint32_t fg = 0; fg < num_feature_grps; ++fg) {
			for (uint32_t c = 0; c < num_cols_per_rec; ++c) {
				for (uint32_t i = 0; i < 4; ++i) {
					dst[_32k * fg + qr * num_cols_per_rec * 4 + c * 4 + i] = src[(qr * 4  + i) * num_features + fg * num_cols_per_rec + c];
				}
			}
		}
	}

	return 0;
}

struct gd_lab_4_idx_val {
	uint16_t idx;
	uint16_t val;
} __attribute__((packed));

struct gd_lab_4_load_db {
	uint64_t db;		/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint32_t num_features;
	uint32_t num_records;
	uint32_t num_cols_per_rec_pow_2;
} __attribute__((packed));

struct gd_lab_4_search {
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint64_t queries;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint32_t num_queries;
	uint32_t k;
} __attribute__((packed));

struct gd_lab_4_cmd {
	uint32_t cmd;
	uint32_t pad_for_64bit_alignment;
	union 	{
		struct gd_lab_4_load_db	load_db_data;
		struct gd_lab_4_search	search_data;
	} __attribute__((packed));
} __attribute__((packed));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GSI_DEVICE_LAB_4_H */

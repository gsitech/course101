#ifndef GSI_DEVICE_LAB_0_H
#define GSI_DEVICE_LAB_0_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

enum gd_lab_0_cmd_type {
	GD_LAB_0_CMD_HELLO_WORLD,
	GD_LAB_0_NUM_CMDS
};

struct gd_lab_0_hello_world_data {
	int32_t an_int;
	char a_char_array[64];
	uint64_t a_mem_hndl;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
} __attribute__((packed));

struct gd_lab_0_cmd {
	uint32_t	cmd;
	union 	{
		struct gd_lab_0_hello_world_data hello_world_data;
	} __attribute__((packed));
} __attribute__((packed));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GSI_DEVICE_LAB_0_H */

----------------------------------------------------------------------------
 LAB 2 README
-----------------------------------------------------------------------------

This lab requires the following prerequisites:
- Device drivers, fw and libs are properly installed
- GVML libs are properly installed

This lab covers the following topics:
- GVML floating point (FP) types
- Introduction to GVML Reference Library
- Exercise: Order of mathematical operations

File structure:
lab_2
├── arc_module_example.lcf
├── arc.tcf
├── dev_src
│   ├── gsi_device_lab_2.c
│   ├── gsi_device_lab_2_dma.c
│   └── include
│       └── gsi_device_lab_2_dma.h
├── gsi_device_lab_2_app.c
├── gsi_device_lab_2.h
├── Makefile
└── README

This lab contains a Makefile to build gsi_device_lab_2. You can choose to build in debug (make mode=debug) or release mode (make mode=release).
Calling make mode=debug will create a build directory with the following structure:

build
└── debug
    ├── gsi_device_lab_2
    ├── gsi_device_lab_2.a
    ├── gsi_device_lab_2_apuc_code.o
    ├── gsi_device_lab_2.bin
    ├── gsi_device_lab_2_defs.c
    ├── gsi_device_lab_2_defs.h
    ├── gsi_device_lab_2_dma_module.o
    ├── gsi_device_lab_2_dma.s
    ├── gsi_device_lab_2.mod
    ├── gsi_device_lab_2_module.o
    └── gsi_device_lab_2.

To run this lab on the host machine:
The program accepts one argument (a number between 0 and 3 corresponding to the tasks):
0 - add_f16 demo
1 - Exercise 0
2 - Exercise 1

This lab is accompanied by an online recording / presentation.
The slide show used in this presentation can be found in lab_2_slideshow.pdf.

This lab's relevant git tags are:
Directory	ANNOTATION
lab_2a          Lab 2: GVML Floating Point ops + GVML Ref + exercises
lab_2b          Lab 2: solutions to exercises
#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_element_wise.h> /* gvml_add_f16() */
#include <gsi/libgvml_debug.h>

#include "gsi_device_lab_2.h"
#include "gsi_device_lab_2_dma.h"

static int add_f16_demo(struct gd_lab_2_add_f16_data *data)
{
	void *ptr_in_a = gal_mem_handle_to_apu_ptr(data->input_a);
	void *ptr_in_b = gal_mem_handle_to_apu_ptr(data->input_b);
	void *ptr_out = gal_mem_handle_to_apu_ptr(data->output);
	enum gvml_vr16 vr_a = GVML_VR16_0;
	enum gvml_vr16 vr_b = GVML_VR16_1;

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);
	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_a);
	gvml_load_16(vr_a, GVML_VM_0);
	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_b);
	gvml_load_16(vr_b, GVML_VM_0);
	gvml_add_f16(vr_a, vr_a, vr_b);
	gvml_store_16(GVML_VM_0, vr_a);
	direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0);

	return 0;
}

static int exercise_0(struct gd_lab_2_exercise_data *data)
{
	void *ptr_in_a = gal_mem_handle_to_apu_ptr(data->input_a);
	void *ptr_in_b = gal_mem_handle_to_apu_ptr(data->input_b);
	void *ptr_in_c = gal_mem_handle_to_apu_ptr(data->input_c);
	void *ptr_out = gal_mem_handle_to_apu_ptr(data->output);
	enum gvml_vr16 vr_0 = GVML_VR16_0;
	enum gvml_vr16 vr_1 = GVML_VR16_1;

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);
	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_b);
	gvml_load_16(vr_0, GVML_VM_0);
	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_c);
	gvml_load_16(vr_1, GVML_VM_0);
	gvml_add_f16(vr_0, vr_0, vr_1);
	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_a);
	gvml_load_16(vr_1, GVML_VM_0);
	gvml_mul_f16(vr_0, vr_0, vr_1);
	gvml_store_16(GVML_VM_0, vr_0);
	direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0);

	return 0;
}

static int exercise_1(struct gd_lab_2_exercise_data *data)
{
	void *ptr_in_a = gal_mem_handle_to_apu_ptr(data->input_a);
	void *ptr_in_b = gal_mem_handle_to_apu_ptr(data->input_b);
	void *ptr_in_c = gal_mem_handle_to_apu_ptr(data->input_c);
	void *ptr_out = gal_mem_handle_to_apu_ptr(data->output);
	enum gvml_vr16 vr_0 = GVML_VR16_0;
	enum gvml_vr16 vr_1 = GVML_VR16_1;
	enum gvml_vr16 vr_2 = GVML_VR16_2;

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);
	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_a);
	gvml_load_16(vr_0, GVML_VM_0);
	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_b);
	gvml_load_16(vr_1, GVML_VM_0);
	gvml_mul_f16(vr_1, vr_0, vr_1);

	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in_c);
	gvml_load_16(vr_2, GVML_VM_0);
	gvml_mul_f16(vr_2, vr_0, vr_2);

	gvml_add_f16(vr_0, vr_1, vr_2);

	gvml_store_16(GVML_VM_0, vr_0);
	direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0);

	return 0;
}

GAL_TASK_ENTRY_POINT(gd_lab_2, in, out)
{
	struct gd_lab_2_cmd *cmd = (struct gd_lab_2_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_2_CMD_ADD_F16:
		gvml_init_once();
		ret = add_f16_demo(&cmd->add_f16_data);
		break;
	case GD_LAB_2_CMD_EXERCISE_0:
		gvml_init_once();
		ret = exercise_0(&cmd->exercise_data);
		break;
	case GD_LAB_2_CMD_EXERCISE_1:
		gvml_init_once();
		ret = exercise_1(&cmd->exercise_data);
		break;
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, (int )cmd->cmd); /* aborts execution */
		break;
	}

	return ret;
}

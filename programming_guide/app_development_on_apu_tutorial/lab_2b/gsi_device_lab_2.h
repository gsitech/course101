#ifndef GSI_DEVICE_LAB_2_H
#define GSI_DEVICE_LAB_2_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

enum gd_lab_2_cmd_type {
	GD_LAB_2_CMD_ADD_F16,
	GD_LAB_2_CMD_EXERCISE_0,
	GD_LAB_2_CMD_EXERCISE_1,
	GD_LAB_2_NUM_CMDS
};

struct gd_lab_2_add_f16_data {
	uint64_t input_a;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k f16 elements */
	uint64_t input_b;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k f16 elements */
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k f16 elements */
} __attribute__((packed));

/* GD_LAB_2_CMD_EXERCISE_0: @output = a * (b + c) */
/* GD_LAB_2_CMD_EXERCISE_1: @output = a * b + a * c */
struct gd_lab_2_exercise_data {
	uint64_t input_a;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k f16 elements */
	uint64_t input_b;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k f16 elements */
	uint64_t input_c;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k f16 elements */
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k f16 elements */
} __attribute__((packed));

struct gd_lab_2_cmd {
	uint32_t cmd;
	uint32_t pad_for_64bit_alignment;
	union 	{
		struct gd_lab_2_add_f16_data add_f16_data;
		struct gd_lab_2_exercise_data exercise_data;
	} __attribute__((packed));
} __attribute__((packed));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GSI_DEVICE_LAB_2_H */

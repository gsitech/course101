#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <gsi/libgdl.h>
#include <gsi/libsys.h>

#include <gsi/gsi_sim_config.h>

GDL_TASK_DECLARE(gd_lab_1);
#include "gsi_device_lab_1.h"

enum {
	_32K = 32 * 1024,
};

static int run_lab_1_dma_demo_cmd(gdl_context_handle_t ctx_id, uint16_t _32k_u16_elements_dst[], const uint16_t _32k_u16_elements_src[])
{
	int ret;
	gdl_mem_handle_t dev_cmd_buf = GDL_MEM_HANDLE_NULL, io_dev_bufs = GDL_MEM_HANDLE_NULL;

	uint64_t io_dev_buf_size = sizeof(uint16_t) * _32K;
	uint64_t io_dev_buf_total_size = io_dev_buf_size * 2;

	io_dev_bufs = gdl_mem_alloc_aligned(ctx_id, io_dev_buf_total_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(io_dev_bufs)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes\n", io_dev_buf_total_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(io_dev_bufs, _32k_u16_elements_src, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s\n", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	struct gd_lab_1_cmd cmd = {
		.cmd = GD_LAB_1_CMD_DIRECT_DMA_DEMO,
		.dma_demo_data.input = io_dev_bufs,
	};

	ret = gdl_add_to_mem_handle(&cmd.dma_demo_data.output, cmd.dma_demo_data.input, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s\n", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	uint64_t cmd_buf_size = sizeof(cmd);
	dev_cmd_buf = gdl_mem_alloc_aligned(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(dev_cmd_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes\n", cmd_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(dev_cmd_buf, &cmd, cmd_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s\n", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_run_task_timeout(
		ctx_id,					/* @ctx_handler - the id of a hardware context previously allocated */
	        GDL_TASK(gd_lab_1),			/* @code_offset - the code offset of the function that the task should execute */
	        dev_cmd_buf,				/* @inp - input memory handle */
	        GDL_MEM_HANDLE_NULL,			/* @outp - output memory handle */
	        GDL_TEMPORARY_DEFAULT_MEM_BUF,		/* @mem_buf - an array of previously allocated memory handles and their sizes */
	        GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,	/* @buf_size - the length of the mem_buf array */
	        GDL_TEMPORARY_DEFAULT_CORE_INDEX,	/* @apuc_idx - the apuc that the task should be executed on */
	        NULL,					/* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
	        0,					/* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
	        GDL_USER_MAPPING);			/* @map_type - determine the mapping type for the specific task */

	if (ret) {
		gsi_error("gdl_run_task_timeout() failed: %s\n", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_from_dev(_32k_u16_elements_dst, cmd.dma_demo_data.output, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_from_dev() failed: %s\n", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

CLEAN_UP:
	gdl_mem_free(dev_cmd_buf);
	gdl_mem_free(io_dev_bufs);

	return ret;
}

static int run_lab_1_dma_demo_cmd_and_check(gdl_context_handle_t ctx_id)
{
	uint16_t _32k_u16_elements_dst[_32K], _32k_u16_elements_src[_32K];

	for (uint32_t i = 0; i < _32K; ++i)
		_32k_u16_elements_src[i] = rand() & 0xffff;

	int ret = run_lab_1_dma_demo_cmd(ctx_id, _32k_u16_elements_dst, _32k_u16_elements_src);

	if (ret)
		return ret;

	if (memcmp(_32k_u16_elements_dst, _32k_u16_elements_src, sizeof(_32k_u16_elements_dst)) != 0) {
		gsi_error("dst is not identical to src");
		ret = gsi_status(EINVAL);
	}

	return ret;
}

static int run_lab_1_lookup_demo_cmd(
	gdl_context_handle_t ctx_id, 
	uint16_t _32k_u16_elements_dst[],
	const uint16_t lut_16b_src[],
	uint64_t lut_len)
{
	int ret;
	gdl_mem_handle_t dev_cmd_buf = GDL_MEM_HANDLE_NULL, output_dev_buf = GDL_MEM_HANDLE_NULL;

	uint64_t output_dev_buf_size = sizeof(uint16_t) * _32K;

	output_dev_buf = gdl_mem_alloc_aligned(ctx_id, output_dev_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(output_dev_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", output_dev_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	struct gd_lab_1_cmd cmd = {
		.cmd = GD_LAB_1_CMD_LOOKUP_DEMO,
		.lookup_demo_data = {
			.input_len = lut_len,
			.output = output_dev_buf
		}
	};

	memcpy(cmd.lookup_demo_data.input, lut_16b_src, sizeof(*lut_16b_src) * lut_len);

	uint64_t cmd_buf_size = sizeof(cmd);
	dev_cmd_buf = gdl_mem_alloc_aligned(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(dev_cmd_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", cmd_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(dev_cmd_buf, &cmd, cmd_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_run_task_timeout(
		ctx_id,					/* @ctx_handler - the id of a hardware context previously allocated */
	        GDL_TASK(gd_lab_1),			/* @code_offset - the code offset of the function that the task should execute */
	        dev_cmd_buf,				/* @inp - input memory handle */
	        GDL_MEM_HANDLE_NULL,			/* @outp - output memory handle */
	        GDL_TEMPORARY_DEFAULT_MEM_BUF,		/* @mem_buf - an array of previously allocated memory handles and their sizes */
	        GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,	/* @buf_size - the length of the mem_buf array */
	        GDL_TEMPORARY_DEFAULT_CORE_INDEX,	/* @apuc_idx - the apuc that the task should be executed on */
	        NULL,					/* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
	        0,					/* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
	        GDL_USER_MAPPING);			/* @map_type - determine the mapping type for the specific task */

	if (ret) {
		gsi_error("gdl_run_task_timeout() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_from_dev(_32k_u16_elements_dst, cmd.lookup_demo_data.output, output_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_from_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

CLEAN_UP:
	gdl_mem_free(dev_cmd_buf);
	gdl_mem_free(output_dev_buf);

	return ret;
}

static int run_lab_1_lookup_demo_cmd_and_check(gdl_context_handle_t ctx_id)
{
	uint16_t _32k_16b_elements[_32K];
	const uint16_t lut[] = { 18, 5, 7 };
	int ret = run_lab_1_lookup_demo_cmd(ctx_id, _32k_16b_elements, lut, 3);
	if (ret)
		return ret;

	for (uint32_t i = 0; i < _32K; ++i) {
		if (_32k_16b_elements[i] != lut[i % 3]) {
			gsi_error("Mismatch for element %u: result = %d, expected value = %d", i, _32k_16b_elements[i], lut[i % 3]);
			return gsi_status(EINVAL);
		}
	}

	return ret;
}

static int run_lab_1_gmd_demo_cmd(gdl_context_handle_t ctx_id, uint16_t _512_u16_elements_dst[])
{
	int ret;
	gdl_mem_handle_t dev_cmd_buf = GDL_MEM_HANDLE_NULL, output_dev_buf = GDL_MEM_HANDLE_NULL;

	uint64_t dev_buf_size = sizeof(uint16_t) * 512;

	output_dev_buf = gdl_mem_alloc_aligned(ctx_id, dev_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(output_dev_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes\n", dev_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	struct gd_lab_1_cmd cmd = {
		.cmd = GD_LAB_1_CMD_GET_MARKED_DATA_DEMO,
		.gmd_demo_data.output = output_dev_buf,
	};

	uint64_t cmd_buf_size = sizeof(cmd);
	dev_cmd_buf = gdl_mem_alloc_aligned(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(dev_cmd_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes\n", cmd_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(dev_cmd_buf, &cmd, cmd_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s\n", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_run_task_timeout(
		ctx_id,					/* @ctx_handler - the id of a hardware context previously allocated */
	        GDL_TASK(gd_lab_1),			/* @code_offset - the code offset of the function that the task should execute */
	        dev_cmd_buf,				/* @inp - input memory handle */
	        GDL_MEM_HANDLE_NULL,			/* @outp - output memory handle */
	        GDL_TEMPORARY_DEFAULT_MEM_BUF,		/* @mem_buf - an array of previously allocated memory handles and their sizes */
	        GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,	/* @buf_size - the length of the mem_buf array */
	        GDL_TEMPORARY_DEFAULT_CORE_INDEX,	/* @apuc_idx - the apuc that the task should be executed on */
	        NULL,					/* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
	        0,					/* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
	        GDL_USER_MAPPING);			/* @map_type - determine the mapping type for the specific task */

	if (ret) {
		gsi_error("gdl_run_task_timeout() failed: %s\n", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_from_dev(_512_u16_elements_dst, cmd.gmd_demo_data.output, dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_from_dev() failed: %s\n", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

CLEAN_UP:
	gdl_mem_free(dev_cmd_buf);
	gdl_mem_free(output_dev_buf);

	return ret;
}

static int32_t cmp_u16_ascending(const void *lhs, const void *rhs)
{
	uint16_t u16_lhs = *(uint16_t *)lhs;
	uint16_t u16_rhs = *(uint16_t *)rhs;
	return (int32_t)u16_lhs - (int32_t)u16_rhs;
}

static void sort_u16_ascending(uint16_t data[], uint32_t num_elements)
{
	qsort(data, num_elements, sizeof(*data), cmp_u16_ascending);
}

static int run_lab_1_gmd_demo_cmd_and_check(gdl_context_handle_t ctx_id)
{
	uint16_t _512_u16_elements[512];
	int ret = run_lab_1_gmd_demo_cmd(ctx_id, _512_u16_elements);
	if (ret)
		return ret;

	sort_u16_ascending(_512_u16_elements, 512);

	for (uint16_t i = 0; i < _32K; i += 64) {
		if (_512_u16_elements[i / 64] != i) {
			gsi_error("Mismatch for element %d: result = %d, expected value = %d", i / 64, _512_u16_elements[i], i);
			return gsi_status(EINVAL);
		}
	}

	return ret;
}

static int run_lab_1_fizzbuzz_cmd(
	gdl_context_handle_t GSI_UNUSED(ctx_id), 
	struct gd_lab_1_idx_val GSI_UNUSED(_32k_iv_elements_dst[]),
	uint32_t GSI_UNUSED(*dst_len),
	const uint16_t GSI_UNUSED(_32k_u16_elements_src[]))
{
	/* TODO: send a GD_LAB_1_CMD_FIZZBUZZ_EXERCISE command to the device */
	/* TODO: return a meaningful return value when the function is implemented */
	return gsi_status(ENOSYS);
}

static int32_t cmp_iv_ascending(const void *lhs, const void *rhs)
{
	struct gd_lab_1_idx_val *iv_lhs = (struct gd_lab_1_idx_val *)lhs;
	struct gd_lab_1_idx_val *iv_rhs = (struct gd_lab_1_idx_val *)rhs;
	return (int32_t)iv_lhs->idx - (int32_t)iv_rhs->idx;
}

static void sort_iv_ascending(struct gd_lab_1_idx_val data[], uint32_t num_elements)
{
	qsort(data, num_elements, sizeof(*data), cmp_iv_ascending);
}

static int run_lab_1_fizzbuzz_cmd_and_check(gdl_context_handle_t ctx_id)
{
	struct gd_lab_1_idx_val _32k_iv_elements_dst[_32K];
	uint32_t dst_len;
	uint16_t _32k_u16_elements_src[_32K];

	for (uint32_t i = 0; i < _32K; ++i)
		_32k_u16_elements_src[i] = rand() & 0xffff;

	int ret = run_lab_1_fizzbuzz_cmd(ctx_id, _32k_iv_elements_dst, &dst_len, _32k_u16_elements_src);
	if (ret)
		return ret;

	sort_iv_ascending(_32k_iv_elements_dst, dst_len);

	for (uint32_t i = 0, dst_ctr = 0; i < _32K; ++i) {
		if ((_32k_u16_elements_src[i] % 15) == 0) {
			if (dst_ctr == dst_len) {
				gsi_error("FIZZBUZZ missing: i = %u: val = %d", i, _32k_u16_elements_src[i]);
				return gsi_status(EINVAL);
			}
			struct gd_lab_1_idx_val iv = _32k_iv_elements_dst[dst_ctr];
			if (iv.idx != i || iv.val != GD_LAB_1_FIZZBUZZ) {
				gsi_error("FIZZBUZZ mismatch: i = %u: val = %d, dev result = 0x%x", i, _32k_u16_elements_src[i], iv.val);
				return gsi_status(EINVAL);
			}
			++dst_ctr;
		} else if ((_32k_u16_elements_src[i] % 3) == 0) {
			if (dst_ctr == dst_len) {
				gsi_error("FIZZ missing: i = %u: val = %d", i, _32k_u16_elements_src[i]);
				return gsi_status(EINVAL);
			}
			struct gd_lab_1_idx_val iv = _32k_iv_elements_dst[dst_ctr];
			if (iv.idx != i || iv.val != GD_LAB_1_FIZZ) {
				gsi_error("FIZZ mismatch: i = %u: val = %d, dev result = 0x%x", i, _32k_u16_elements_src[i], iv.val);
				return gsi_status(EINVAL);
			}
			++dst_ctr;
		} else if ((_32k_u16_elements_src[i] % 5) == 0) {
			if (dst_ctr == dst_len) {
				gsi_error("BUZZ missing: i = %u: val = %d", i, _32k_u16_elements_src[i]);
				return gsi_status(EINVAL);
			}
			struct gd_lab_1_idx_val iv = _32k_iv_elements_dst[dst_ctr];
			if (iv.idx != i || iv.val != GD_LAB_1_BUZZ) {
				gsi_error("BUZZ mismatch: i = %u: val = %d, dev result = 0x%x", i, _32k_u16_elements_src[i], iv.val);
				return gsi_status(EINVAL);
			}
			++dst_ctr;
		}
	}

	return ret;
}

static int parse_args(int *args, int argc, char *argv[])
{
	if (2 != argc) {
		gsi_error("usage: %s [0 | 1 | 2 | 3] (dma_demo, lookup_demo, gmd_demo, fizzbuzz_exercise)", argv[0]);
		return gsi_status(EINVAL);
	}

	int choice = atoi(argv[1]);
	if (choice < 0 || choice > 3) {
		gsi_error("usage: %s [0 | 1 | 2 | 3] (dma_demo, lookup_demo, gmd_demo, fizzbuzz_exercise)", argv[0]);
		return gsi_status(EINVAL);
	}

	*args = choice;

	return 0;
}


// For Simulator:
enum { NUM_CTXS = 1 };
static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};


int main(int argc, char *argv[])
{
	uint32_t num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];

	gsi_libsys_init(
		argv[0],	/* program name */
		true);		/* log_to_screen */

	int args;
	int ret = parse_args(&args, argc, argv);
	if (ret)
		gsi_fatal("parse_args failed: %s", gsi_status_errorstr(ret));

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	printf("Num Contexts = %u\n", num_ctxs);
	/* Use first available context */
	gdl_context_handle_t valid_ctx_id;
	uint32_t ctx;
	for (ctx = 0; ctx < num_ctxs; ++ctx) {
		if (contexts_desc[ctx].status == GDL_CONTEXT_READY) {
			valid_ctx_id = contexts_desc[ctx].ctx_id;
			printf("Memory Size = %0.1fG\n", (float)contexts_desc[ctx].mem_size / 1024L / 1024L / 1024L);
			printf("Num Apucs = %d\n", contexts_desc[ctx].num_apucs);
			break;
		}
	}

	if (ctx == num_ctxs) {
		gsi_fatal("Failed to find valid context");
	}

	const long long unsigned int const_mapped_size_req = 3L * 1024L * 1024L * 1024L;
	long long unsigned int const_mapped_size_recv, dynamic_mapped_size_recv;

	ret = gdl_context_alloc(valid_ctx_id, const_mapped_size_req, &const_mapped_size_recv, &dynamic_mapped_size_recv);
	if (ret) {
		gsi_fatal("gdl_context_alloc failed: %s", gsi_status_errorstr(ret));
	}

	switch (args)
	{
	case GD_LAB_1_CMD_DIRECT_DMA_DEMO:
		printf("\nDirect DMA demo:\n");
		ret = run_lab_1_dma_demo_cmd_and_check(valid_ctx_id);
		break;

	case GD_LAB_1_CMD_LOOKUP_DEMO:
		printf("\nLookup demo:\n");
		ret = run_lab_1_lookup_demo_cmd_and_check(valid_ctx_id);
		break;

	case GD_LAB_1_CMD_GET_MARKED_DATA_DEMO:
		printf("\nGet Marked Data demo:\n");
		ret = run_lab_1_gmd_demo_cmd_and_check(valid_ctx_id);
		break;

	case GD_LAB_1_CMD_FIZZBUZZ_EXERCISE:
		printf("\nFizzbuzz Exercise:\n");
		ret = run_lab_1_fizzbuzz_cmd_and_check(valid_ctx_id);
		break;
	
	default:
		break;
	}

	// CLEAN_UP:
	gdl_context_free(valid_ctx_id);
	gdl_exit();

	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nFailure\n");
	} else {
		printf("\nSuccess\n");
	}
	return ret;
}

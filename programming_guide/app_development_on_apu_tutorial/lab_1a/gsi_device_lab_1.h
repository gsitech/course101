#ifndef GSI_DEVICE_LAB_1_H
#define GSI_DEVICE_LAB_1_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

enum gd_lab_1_cmd_type {
	GD_LAB_1_CMD_DIRECT_DMA_DEMO,
	GD_LAB_1_CMD_LOOKUP_DEMO,
	GD_LAB_1_CMD_GET_MARKED_DATA_DEMO,
	GD_LAB_1_CMD_FIZZBUZZ_EXERCISE,
	GD_LAB_1_NUM_CMDS
};

/* input and output are both arrays of 32K 16-byte elements */
struct gd_lab_1_dma_demo_data {
	uint64_t input;		/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
} __attribute__((packed));

struct gd_lab_1_lookup_demo_data {
	uint16_t input[8];	/* These elements will be spread over 32k elements */
	uint16_t input_len;	/* <= 8 */
	uint16_t pad_for_32bit_alignment;
	uint32_t pad_for_64bit_alignment;
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k uint16_t elements */
} __attribute__((packed));

struct gd_lab_1_gmd_demo_data {
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 512 uint16_t elements */
} __attribute__((packed));

enum {
	GD_LAB_1_FIZZ = 0xf,
	GD_LAB_1_BUZZ = 0xb,
	GD_LAB_1_FIZZBUZZ = 0xfb,
};

struct gd_lab_1_idx_val {
	uint16_t idx;
	uint16_t val;
} __attribute__((packed));

struct gd_lab_1_fizzbuzz_exercise_data {
	uint64_t input;		/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* 32k uint16_t elements */
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */	/* at most 32k struct gd_lab_1_idx_val elements */
	uint32_t output_len;	/* <= 32k */
} __attribute__((packed));

struct gd_lab_1_cmd {
	uint32_t cmd;
	uint32_t pad_for_64bit_alignment;
	union 	{
		struct gd_lab_1_dma_demo_data dma_demo_data;
		struct gd_lab_1_lookup_demo_data lookup_demo_data;
		struct gd_lab_1_gmd_demo_data gmd_demo_data;
		struct gd_lab_1_fizzbuzz_exercise_data fizzbuzz_exercise_data;
	} __attribute__((packed));
} __attribute__((packed));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GSI_DEVICE_LAB_1_H */

#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_iv.h>			/* gvml_create_index_16 */
#include <gsi/libgvml_element_wise.h>		/* gvml_cpy_imm_16(), gvml_mod_u16() */
#include <gsi/libgvml_get_marked_data.h>	/* gvml_get_marked_data() */

#include "gsi_device_lab_1.h"
#include "gsi_device_lab_1_dma.h"

static int direct_dma_demo(struct gd_lab_1_dma_demo_data *demo_data)
{
	void *ptr_in = gal_mem_handle_to_apu_ptr(demo_data->input);
	void *ptr_out = gal_mem_handle_to_apu_ptr(demo_data->output);
	
	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);

	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in);
	direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0);

	return 0;
}

static int lookup_demo(struct gd_lab_1_lookup_demo_data *demo_data)
{
	void *ptr_out = gal_mem_handle_to_apu_ptr(demo_data->output);

	enum gvml_vr16 vr_idx = GVML_VR16_0;
	enum gvml_vr16 vr_input = GVML_VR16_1;

	/* Prepare index */
	gvml_create_index_16(vr_idx);
	gvml_cpy_imm_16(vr_input, demo_data->input_len);
	gvml_mod_u16(vr_idx, vr_idx, vr_input);
	
	/* Need extra (void *) casting to ignore alignment warning */
	gvml_lookup_16(vr_input, vr_idx, (void *)demo_data->input, demo_data->input_len);

	gvml_store_16(GVML_VM_0, vr_input);

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);
	direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0);

	return 0;
}

static int get_marked_data_demo(struct gd_lab_1_gmd_demo_data *demo_data)
{
	uint16_t *ptr_out = gal_mem_handle_to_apu_ptr(demo_data->output);

	enum gvml_vr16 vr_idx = GVML_VR16_0;
	enum gvml_mrks_n_flgs mrk_multiple_of_64 = GVML_MRK0;

	/* Prepare index */
	gvml_create_index_16(vr_idx);
	gvml_eq_imm_16_msk(mrk_multiple_of_64, vr_idx, 0, 0x3f);

	gvml_get_marked_data(
		ptr_out,		/* uint16_t *buff */
                vr_idx,			/* enum gvml_vr16 start_vsrc */
		1,			/* unsigned int num_vsrcs */
		mrk_multiple_of_64,	/* enum gvml_mrks_n_flgs msrc */
		512);			/* unsigned int num_ent */	

	return 0;
}

static int fizzbuzz(struct gd_lab_1_fizzbuzz_exercise_data *fizzbuzz_data)
{
	enum gvml_vr16 GSI_UNUSED(vr_input) = GVML_VR16_0;
	enum gvml_vr16 GSI_UNUSED(vr_divisor) = GVML_VR16_1;
	enum gvml_vr16 GSI_UNUSED(vr_output) = GVML_VR16_2;
	enum gvml_mrks_n_flgs GSI_UNUSED(mrk_fizz) = GVML_MRK0;
	enum gvml_mrks_n_flgs GSI_UNUSED(mrk_buzz) = GVML_MRK1;
	enum gvml_mrks_n_flgs GSI_UNUSED(mrk_output) = GVML_MRK2;

	void GSI_UNUSED(*ptr_in) = gal_mem_handle_to_apu_ptr(fizzbuzz_data->input);
	void GSI_UNUSED(*ptr_out) = gal_mem_handle_to_apu_ptr(fizzbuzz_data->output);
	
	/* FIZZ */
	/* TODO: load the data in @ptr_in into @vr_input */
	/* TODO: use gvml_cpy_imm_16(), gvml_mod_u16() and gvml_eq_imm_16() to mark all GD_LAB_1_FIZZ entries in @mrk_fizz */
	/* TODO: use @mrk_fizz and gvml_cpy_imm_16_mrk() to inititalize the GD_LAB_1_FIZZ entries in @vr_output */
	/* TODO: copy @mrk_fizz to @mrk_output */

	/* BUZZ */
	/* TODO: do the same thing for buzz with @mrk_buzz */
	/* TODO: use gvml_or_m() to add the results in @mrk_buzz to @mrk_output */

	/* FIZZBUZZ */
	/* TODO: use gvml_and_m() with mrk_fizz() and mrk_buzz() to find the GD_LAB_1_FIZZBUZZ entries (reuse @mrk_fizz to store the result) */
	/* TODO: use @mrk_fizz and gvml_cpy_imm_16_mrk() to inititalize the GD_LAB_1_FIZZBUZZ entries in @vr_output */

	/* TODO: use gvml_count_m_g32k() to count the number of valid entries in @mrk_output */
	/* TODO: reuse @vr_divisor and create an index in it (@vr_divisor is immediately before @vr_output) */
	/* TODO: use gvml_get_marked_data() on both @vr_divisor and @vr_output to extract the results */
	/* TODO: don't forget to assign fizzbuzz_data->output_len */

	/* TODO: return 0 when the function is implemented */
	return gsi_status(ENOSYS);
}

GAL_TASK_ENTRY_POINT(gd_lab_1, in, out)
{
	struct gd_lab_1_cmd *cmd = (struct gd_lab_1_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_1_CMD_DIRECT_DMA_DEMO:
		gvml_init_once();
		ret = direct_dma_demo(&cmd->dma_demo_data);
		break;

	case GD_LAB_1_CMD_LOOKUP_DEMO:
		gvml_init_once();
		ret = lookup_demo(&cmd->lookup_demo_data);
		break;

	case GD_LAB_1_CMD_GET_MARKED_DATA_DEMO:
		gvml_init_once();
		ret = get_marked_data_demo(&cmd->gmd_demo_data);
		break;

	case GD_LAB_1_CMD_FIZZBUZZ_EXERCISE:
		gvml_init_once();
		ret = fizzbuzz(&cmd->fizzbuzz_exercise_data);
		break;			
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, cmd->cmd);	/* aborts execution */
		break;
	}

	return ret;
}

#!/bin/bash

# Un-comment the following line to enable debug
# set -x

echo Info: PWD=${PWD}
echo Info: $0 "$@"

err_report() {
    echo "Error on line $1 at file $0"
    echo exit 1
}

trap 'err_report $LINENO' ERR

echo Info: example for running all the lab tests

# Default build mode is release. To change build mode to debug, do: mode=debug $0 ...
mode=${mode:=debug}

echo Info: run labs that where build with mode=${mode}

ERRORS=
echo '############################################# '
CMD="./lab_0/build/${mode}/gsi_device_lab_0"
echo Info: run lab 0 - ${CMD}
${CMD} || ERRORS="${ERRORS} lab_0"

echo '############################################# '
for i in 0 1 2 3
do
  CMD="./lab_1b/build/${mode}/gsi_device_lab_1 $i"
  echo Info: run lab 1 - ${CMD}
  ${CMD} || ERRORS="${ERRORS} lab_1b $i"
done

echo '############################################# '
for i in 0 1 2
do
	CMD="./lab_2b/build/${mode}/gsi_device_lab_2 $i"
	echo Info: run lab 2 - ${CMD}
	${CMD} || ERRORS="${ERRORS} lab_2b $i"
done


echo '############################################# '
CMD="./lab_3b/build/${mode}/gsi_device_lab_3 20000 33 5 10 3"
echo Info: run lab 3 - ${CMD}
${CMD} || ERRORS="${ERRORS} lab_3b"

echo '############################################# '
CMD="./lab_4b/build/${mode}/gsi_device_lab_4 8000 192 2 5 1004 3"
echo Info: run lab 4 - ${CMD}
${CMD} || ERRORS="${ERRORS} lab_4b"

echo '############################################# '
CMD="./lab_5b/build/${mode}/gsi_device_lab_5 5 48 10 3"
echo Info: run lab 5 - ${CMD}
${CMD} || ERRORS="${ERRORS} lab_5b"

echo '############################################# '
CMD="./lab_6g/build/${mode}/gsi_device_lab_6 10 48 6000 5"
echo Info: run lab 6 - ${CMD}
${CMD} || ERRORS="${ERRORS} lab_6g"

echo '############################################# '
CMD="./lab_7/build/${mode}/gsi_device_lab_7 32000 40 5 100 3"
echo Info: run lab 7 - ${CMD}
${CMD} || ERRORS="${ERRORS} lab_7"

echo '############################################# '

if [ ! -z "${ERRORS}" ]; then
	echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
	echo Errors: the following tests had failed: ${ERRORS}
	exit 1
else
	echo Info: all tests are O.K.
	exit 0
fi

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <refgvml.h>
#include <refgvml_element_wise.h>

#include <gsi/gsi_sim_config.h>

GDL_TASK_DECLARE(gd_lab_2);
#include "gsi_device_lab_2.h"

enum {
	_32K = 32 * 1024,
};

static int run_lab_2_cmd(
	gdl_context_handle_t ctx_id,
	uint16_t _32k_f16_elements_dst[],
	const uint16_t _32k_f16_elements_src_a[],
	const uint16_t _32k_f16_elements_src_b[])
{
	int ret;
	gdl_mem_handle_t dev_cmd_buf = GDL_MEM_HANDLE_NULL, io_dev_bufs = GDL_MEM_HANDLE_NULL;

	uint64_t io_dev_buf_size = sizeof(uint16_t) * _32K;
	uint64_t io_dev_bus_total_size = io_dev_buf_size * 3;

	io_dev_bufs = gdl_mem_alloc_aligned(ctx_id, io_dev_bus_total_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(io_dev_bufs)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", io_dev_bus_total_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(io_dev_bufs, _32k_f16_elements_src_a, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	struct gd_lab_2_cmd cmd = {
		.cmd = GD_LAB_2_CMD_ADD_F16,
		.add_f16_data.input_a = io_dev_bufs,
	};

	ret = gdl_add_to_mem_handle(&cmd.add_f16_data.input_b, cmd.add_f16_data.input_a, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(cmd.add_f16_data.input_b, _32k_f16_elements_src_b, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_add_to_mem_handle(&cmd.add_f16_data.output, cmd.add_f16_data.input_b, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	uint64_t cmd_buf_size = sizeof(cmd);
	dev_cmd_buf = gdl_mem_alloc_aligned(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(dev_cmd_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", cmd_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(dev_cmd_buf, &cmd, cmd_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_run_task_timeout(
			ctx_id,              /* @ctx_handler - the id of a hardware context previously allocated */
			GDL_TASK(gd_lab_2),  /* @code_offset - the code offset of the function that the task should execute */
			dev_cmd_buf,         /* @inp - input memory handle */
			GDL_MEM_HANDLE_NULL, /* @outp - output memory handle */
			GDL_TEMPORARY_DEFAULT_MEM_BUF,      /* @mem_buf - an array of previously allocated memory handles and their sizes */
			GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE, /* @buf_size - the length of the mem_buf array */
			GDL_TEMPORARY_DEFAULT_CORE_INDEX,   /* @apuc_idx - the apuc that the task should be executed on */
			NULL,              /* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
			0,                 /* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
			GDL_USER_MAPPING); /* @map_type - determine the mapping type for the specific task */

	if (ret) {
		gsi_error("gdl_run_task_timeout() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_from_dev(_32k_f16_elements_dst, cmd.add_f16_data.output, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_from_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

CLEAN_UP:
	gdl_mem_free(dev_cmd_buf);
	gdl_mem_free(io_dev_bufs);

	return ret;
}

static float random_float_0_to_1(void)
{
	return (float)rand() / (float)(RAND_MAX);
}

static int run_lab_2_cmd_and_check(gdl_context_handle_t ctx_id)
{
	uint16_t a[_32K], b[_32K], dst[_32K];

	for (uint64_t i = 0; i < _32K; ++i) {
		a[i] = refgvml_float_2_f16(random_float_0_to_1());
		b[i] = refgvml_float_2_f16(random_float_0_to_1());
	}

	int ret = run_lab_2_cmd(ctx_id, dst, a, b);
	if (ret)
		return ret;

	for (uint32_t i = 0; i < _32K; ++i) {
		uint16_t ref_val = refgvml_add_f16(a[i], b[i]);
		if (ref_val != dst[i]) {
			gsi_error("Mismatch for element %u: result = 0x%x, expected value = 0x%x", i, dst[i], ref_val);
			return gsi_status(EINVAL);
		}
	}

	return ret;
}

static int run_lab_2_exercise_cmd(
	gdl_context_handle_t ctx_id,
	uint16_t _32k_f16_elements_dst[],
	const uint16_t _32k_f16_elements_src_a[],
	const uint16_t _32k_f16_elements_src_b[],
	const uint16_t _32k_f16_elements_src_c[],
	enum gd_lab_2_cmd_type exercise_type)
{
	int ret;
	gdl_mem_handle_t dev_cmd_buf = GDL_MEM_HANDLE_NULL, io_dev_bufs = GDL_MEM_HANDLE_NULL;

	uint64_t io_dev_buf_size = sizeof(uint16_t) * _32K;
	uint64_t io_dev_bus_total_size = io_dev_buf_size * 4;

	io_dev_bufs = gdl_mem_alloc_aligned(ctx_id, io_dev_bus_total_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(io_dev_bufs)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", io_dev_bus_total_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	struct gd_lab_2_cmd cmd = {
		.cmd = exercise_type,
		.exercise_data.input_a = io_dev_bufs,
	};

	ret = gdl_add_to_mem_handle(&cmd.exercise_data.input_b, cmd.exercise_data.input_a, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_add_to_mem_handle(&cmd.exercise_data.input_c, cmd.exercise_data.input_b, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_add_to_mem_handle(&cmd.exercise_data.output, cmd.exercise_data.input_c, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(cmd.exercise_data.input_a, _32k_f16_elements_src_a, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(cmd.exercise_data.input_b, _32k_f16_elements_src_b, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(cmd.exercise_data.input_c, _32k_f16_elements_src_c, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	uint64_t cmd_buf_size = sizeof(cmd);
	dev_cmd_buf = gdl_mem_alloc_aligned(ctx_id, cmd_buf_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(dev_cmd_buf)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", cmd_buf_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(dev_cmd_buf, &cmd, cmd_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_run_task_timeout(
			ctx_id,              /* @ctx_handler - the id of a hardware context previously allocated */
			GDL_TASK(gd_lab_2),  /* @code_offset - the code offset of the function that the task should execute */
			dev_cmd_buf,         /* @inp - input memory handle */
			GDL_MEM_HANDLE_NULL, /* @outp - output memory handle */
			GDL_TEMPORARY_DEFAULT_MEM_BUF,      /* @mem_buf - an array of previously allocated memory handles and their sizes */
			GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE, /* @buf_size - the length of the mem_buf array */
			GDL_TEMPORARY_DEFAULT_CORE_INDEX,   /* @apuc_idx - the apuc that the task should be executed on */
			NULL,              /* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
			0,                 /* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
			GDL_USER_MAPPING); /* @map_type - determine the mapping type for the specific task */

	if (ret) {
		gsi_error("gdl_run_task_timeout() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_from_dev(_32k_f16_elements_dst, cmd.exercise_data.output, io_dev_buf_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_from_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

CLEAN_UP:
	gdl_mem_free(dev_cmd_buf);
	gdl_mem_free(io_dev_bufs);

	return ret;
}

static int run_lab_2_exercise_0_cmd_and_check(gdl_context_handle_t ctx_id)
{
	uint16_t a[_32K], b[_32K], c[_32K], dst[_32K];

	for (uint64_t i = 0; i < _32K; ++i) {
		a[i] = refgvml_float_2_f16(random_float_0_to_1());
		b[i] = refgvml_float_2_f16(random_float_0_to_1());
		c[i] = refgvml_float_2_f16(random_float_0_to_1());
	}

	int ret = run_lab_2_exercise_cmd(ctx_id, dst, a, b, c, GD_LAB_2_CMD_EXERCISE_0);
	if (ret)
		return ret;

	for (uint32_t i = 0; i < _32K; ++i) {
		/* TODO: implement the reference calculation */
		uint16_t ref_val = 0;
		if (ref_val != dst[i]) {
			gsi_error("Mismatch for element %u: result = 0x%x, expected value = 0x%x", i, dst[i], ref_val);
			return gsi_status(EINVAL);
		}
	}

	return ret;
}

static int run_lab_2_exercise_1_cmd_and_check(gdl_context_handle_t ctx_id)
{
	uint16_t a[_32K], b[_32K], c[_32K], dst[_32K];

	for (uint64_t i = 0; i < _32K; ++i) {
		a[i] = refgvml_float_2_f16(random_float_0_to_1());
		b[i] = refgvml_float_2_f16(random_float_0_to_1());
		c[i] = refgvml_float_2_f16(random_float_0_to_1());
	}

	int ret = run_lab_2_exercise_cmd(ctx_id, dst, a, b, c, GD_LAB_2_CMD_EXERCISE_1);
	if (ret)
		return ret;

	for (uint32_t i = 0; i < _32K; ++i) {
		/* TODO: implement the reference calculation */
		uint16_t ref_val = 0;
		if (ref_val != dst[i]) {
			gsi_error("Mismatch for element %u: result = 0x%x, expected value = 0x%x", i, dst[i], ref_val);
			return gsi_status(EINVAL);
		}
	}

	return ret;
}

static int parse_args(int *args, int argc, char *argv[])
{
	if (2 != argc) {
		gsi_error("usage: %s [0 | 1 | 2] (add_f16_demo, exercise_0, exercise_1)", argv[0]);
		return gsi_status(EINVAL);
	}

	int choice = atoi(argv[1]);
	if (choice < 0 || choice > 2) {
		gsi_error("usage: %s [0 | 1 | 2] (add_f16_demo, exercise_0, exercise_1)", argv[0]);
		return gsi_status(EINVAL);
	}

	*args = choice;

	return 0;
}


// For Simulator:
enum { NUM_CTXS = 1 };
static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};


int main(int argc, char *argv[])
{
	uint32_t num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];

	gsi_libsys_init(
		argv[0], /* program name */
		true);   /* log_to_screen */

	int args;
	int ret = parse_args(&args, argc, argv);
	if (ret)
		gsi_fatal("parse_args failed: %s", gsi_status_errorstr(ret));

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	printf("Num Contexts = %u\n", num_ctxs);
	/* Use first available context */
	gdl_context_handle_t valid_ctx_id;
	uint32_t ctx;
	for (ctx = 0; ctx < num_ctxs; ++ctx) {
		if (contexts_desc[ctx].status == GDL_CONTEXT_READY) {
			valid_ctx_id = contexts_desc[ctx].ctx_id;
			printf("Memory Size = %0.1fG\n", (float)contexts_desc[ctx].mem_size / 1024L / 1024L / 1024L);
			printf("Num Apucs = %d\n", contexts_desc[ctx].num_apucs);
			break;
		}
	}

	if (ctx == num_ctxs) {
		gsi_fatal("Failed to find valid context");
	}

	const long long unsigned int const_mapped_size_req = 3L * 1024L * 1024L * 1024L;
	long long unsigned int const_mapped_size_recv, dynamic_mapped_size_recv;

	ret = gdl_context_alloc(valid_ctx_id, const_mapped_size_req, &const_mapped_size_recv, &dynamic_mapped_size_recv);
	if (ret) {
		gsi_fatal("gdl_context_alloc failed: %s", gsi_status_errorstr(ret));
	}

	switch (args) {
	case GD_LAB_2_CMD_ADD_F16:
		printf("\nadd_f16 demo:\n");
		ret = run_lab_2_cmd_and_check(valid_ctx_id);
		break;
	case GD_LAB_2_CMD_EXERCISE_0:
		printf("\nExercise 0, a * (b + c):\n");
		ret = run_lab_2_exercise_0_cmd_and_check(valid_ctx_id);
		break;
	case GD_LAB_2_CMD_EXERCISE_1:
		printf("\nExercise 1, a * b + a * c:\n");
		ret = run_lab_2_exercise_1_cmd_and_check(valid_ctx_id);
		break;
	default:
		break;
	}

	gdl_context_free(valid_ctx_id);
	gdl_exit();

	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nFailure\n");
	} else {
		printf("\nSuccess\n");
	}
	return ret;
}

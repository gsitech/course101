#ifndef GSI_DEVICE_LAB_3_H
#define GSI_DEVICE_LAB_3_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

enum {
	GD_LAB_3_MAX_NUM_RECORDS_IN_DB = 32 * 1024,
	GD_LAB_3_MAX_NUM_FEATURES = 48,
};

enum gd_lab_3_cmd_type {
	GD_LAB_3_CMD_LOAD_DB,
	GD_LAB_3_CMD_SEARCH,
	GD_LAB_3_NUM_CMDS
};

static inline uint64_t gd_lab_3_get_preprocessed_db_size(uint32_t num_features)
{
	return sizeof(uint16_t) * num_features * GD_LAB_3_MAX_NUM_RECORDS_IN_DB;
}

static inline void gd_lab_3_preprocess_db(uint16_t dst_f16[], const uint16_t src_f16[], uint32_t num_features, uint32_t num_records)
{
	for (uint32_t f = 0; f < num_features; ++f) {
		for (uint32_t r = 0; r < num_records; ++r) {
			dst_f16[GD_LAB_3_MAX_NUM_RECORDS_IN_DB * f + r] = src_f16[r * num_features + f];
		}
		/* For valgrind */
		for (uint32_t r = num_records; r < GD_LAB_3_MAX_NUM_RECORDS_IN_DB; ++r) {
			dst_f16[GD_LAB_3_MAX_NUM_RECORDS_IN_DB * f + r] = 0;
		}
	}
}

struct gd_lab_3_idx_val {
	uint16_t idx;
	uint16_t val;
} __attribute__((packed));

struct gd_lab_3_load_db {
	uint64_t db;		/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint32_t num_features;
	uint32_t num_records;
} __attribute__((packed));

struct gd_lab_3_search {
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint64_t queries;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint32_t num_queries;
	uint32_t k;
} __attribute__((packed));

struct gd_lab_3_cmd {
	uint32_t cmd;
	int32_t  pad_for_64bit_alignment;
	union 	{
		struct gd_lab_3_load_db	load_db_data;
		struct gd_lab_3_search	search_data;
	} __attribute__((packed));
} __attribute__((packed));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GSI_DEVICE_LAB_3_H */

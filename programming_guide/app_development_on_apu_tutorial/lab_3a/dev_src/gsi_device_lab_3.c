#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_iv.h>              /* gvml_create_index_16() */
#include <gsi/libgvml_element_wise.h>    /* gvml_add_f16() */
#include <gsi/libgvml_min_max.h>         /* gvml_mark_kmin_idxval_u16_mrk_g32k() */
#include <gsi/libgvml_get_marked_data.h> /* gvml_get_marked_data_xv() */
#include <gsi/libgvml_debug.h>

#include "gsi_device_lab_3.h"
#include "gsi_device_lab_3_dma.h"

static struct {
	uint32_t num_records;
	uint32_t num_features;
	enum gvml_mrks_n_flgs mrk_num_records;
	enum gvml_vr16 vr_idx;
} g_db_data = {
	.num_records = 0,
	.num_features = 0,
	.mrk_num_records = GVML_MRK0,
	.vr_idx = GVML_VR16_0,
};

static int load_db(struct gd_lab_3_load_db *load_db_data)
{
	if (load_db_data->num_features > GD_LAB_3_MAX_NUM_FEATURES) {
		gsi_error("number of features (%u) must not exceed %d", load_db_data->num_features, GD_LAB_3_MAX_NUM_FEATURES);
		return gsi_status(EINVAL);
	}

	if (load_db_data->num_records > GD_LAB_3_MAX_NUM_RECORDS_IN_DB) {
		gsi_error("number of records (%u) must not exceed %d", load_db_data->num_records, GD_LAB_3_MAX_NUM_RECORDS_IN_DB);
		return gsi_status(EINVAL);
	}

	g_db_data.num_features = load_db_data->num_features;
	g_db_data.num_records = load_db_data->num_records;
	gvml_create_index_16(g_db_data.vr_idx);
	/* TODO: initialize @g_db_data.mrk_num_records */

	uint16_t GSI_UNUSED(*ptr_in) = gal_mem_handle_to_apu_ptr(load_db_data->db);

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);

	/* TODO: load the data in @ptr_in into L1: */
	/* Load 32K f16 elements for each feature in the database */

	return gsi_status(ENOSYS);
}

static void abs_f16(enum gvml_vr16 vr)
{
	gvml_reset_16_msk(vr, 0x8000);
}

static int do_search(struct gd_lab_3_search *search_data)
{
	if (search_data->k > g_db_data.num_records) {
		gsi_error("k (%u) must not exceed number of records (%u)", search_data->k, g_db_data.num_records);
		return gsi_status(EINVAL);
	}

	/* TODO: the following vectors / markers should be enough to implement the function but feel free to use more. */	
	enum gvml_vr16 GSI_UNUSED(vr_distances) = GVML_VR16_1;
	enum gvml_vr16 GSI_UNUSED(vr_records) = GVML_VR16_2;
	enum gvml_vr16 GSI_UNUSED(vr_query) = GVML_VR16_3;
	enum gvml_mrks_n_flgs GSI_UNUSED(mrk_kmin) = GVML_MRK1;

	const uint16_t *p_q = gal_mem_handle_to_apu_ptr(search_data->queries);
	struct gd_lab_3_idx_val *p_iv = gal_mem_handle_to_apu_ptr(search_data->output);

	for (uint32_t q = 0; q < search_data->num_queries; ++q, p_q += g_db_data.num_features, p_iv += search_data->k) {
		/* TODO: initialize @vr_distances to zero */

		for (uint32_t f = 0; f < g_db_data.num_features; ++f) {
			/* TODO: broadcast @p_q[f] onto @vr_query */
			/* TODO: load feature @f of the database (in L1) into @vr_records*/
			/* TODO: subtract @vr_query from @vr_records (reuse @vr_records to store the result) */
			/* Take the absolute value of @vr_records */
			abs_f16(vr_records);
			/* TODO: add the results for this feature to @vr_distances */
		}
		/* convert the the f16 distances in @vr_distances into monotonically equivalent u16 values */
		/* store the results in @vr_records */
		gvml_monotonic_transformation_nan2max_u16_f16(vr_records, vr_distances);
		/* TODO: initialize @mrk_kmin by calling gvml_mark_kmin_idxval_u16_mrk_g32k() with @vr_records */
		/* TODO: extract @k index-value pairs from @vr_distances and @vr_idx into @p_vi, using gvml_get_marked_data_xv() */
	}

	return gsi_status(ENOSYS);
}


GAL_TASK_ENTRY_POINT(gd_lab_3, in, out)
{
	struct gd_lab_3_cmd *cmd = (struct gd_lab_3_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_3_CMD_LOAD_DB:
		gvml_init_once();
		ret = load_db(&cmd->load_db_data);
		break;
	case GD_LAB_3_CMD_SEARCH:
		ret = do_search(&cmd->search_data);
		break;
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, cmd->cmd); /* aborts execution */
		break;
	}

	return ret;
}

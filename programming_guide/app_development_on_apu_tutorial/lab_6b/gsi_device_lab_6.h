#ifndef GSI_DEVICE_LAB_6_H
#define GSI_DEVICE_LAB_6_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

enum {
	GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK = 32 * 1024,
	GD_LAB_6_MAX_NUM_FEATURES = 48,
	GD_LAB_6_MAX_K = GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK / 2,
};

enum gd_lab_6_cmd_type {
	GD_LAB_6_CMD_LOAD_DB,
	GD_LAB_6_CMD_SEARCH,
	GD_LAB_6_NUM_CMDS
};

static inline uint64_t gd_lab_6_get_preprocessed_db_size(uint32_t num_features, uint32_t num_chunks)
{
	return sizeof(uint16_t) * num_features * num_chunks * GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK;
}

static inline void gd_lab_6_preprocess_db(uint16_t dst_f16[], const uint16_t src_f16[], uint32_t num_features, uint32_t num_chunks)
{
	for (uint32_t c = 0; c < num_chunks; ++c, dst_f16 += GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK * num_features, src_f16 += GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK * num_features) {
		for (uint32_t f = 0; f < num_features; ++f) {
			for (uint32_t r = 0; r < GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK; ++r) {
				dst_f16[GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK * f + r] = src_f16[r * num_features + f];
			}
		}
	}
}

struct gd_lab_6_idx_val {
	uint32_t idx;
	uint16_t val;
} __attribute__((packed));

struct gd_lab_6_load_db {
	uint64_t db;		/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint32_t num_features;
	uint32_t num_32k_chunks_records;
} __attribute__((packed));

struct gd_lab_6_search {
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint64_t query;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint32_t k;
} __attribute__((packed));

struct gd_lab_6_cmd {
	uint32_t cmd;
	union 	{
		struct gd_lab_6_load_db	load_db_data;
		struct gd_lab_6_search	search_data;
	} __attribute__((packed));
} __attribute__((packed));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GSI_DEVICE_LAB_6_H */

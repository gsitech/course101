#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_iv.h>			/* gvml_create_index_16 */
#include <gsi/libgvml_element_wise.h>		/* gvml_cpy_imm_16(), gvml_mod_u16() */
#include <gsi/libgvml_get_marked_data.h>	/* gvml_get_marked_data() */

#include "gsi_device_lab_1.h"
#include "gsi_device_lab_1_dma.h"

static int direct_dma_demo(struct gd_lab_1_dma_demo_data *demo_data)
{
	void *ptr_in = gal_mem_handle_to_apu_ptr(demo_data->input);
	void *ptr_out = gal_mem_handle_to_apu_ptr(demo_data->output);
	
	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);

	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in);
	direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0);

	return 0;
}

static int lookup_demo(struct gd_lab_1_lookup_demo_data *demo_data)
{
	void *ptr_out = gal_mem_handle_to_apu_ptr(demo_data->output);

	enum gvml_vr16 vr_idx = GVML_VR16_0;
	enum gvml_vr16 vr_input = GVML_VR16_1;

	/* Prepare index */
	gvml_create_index_16(vr_idx);
	gvml_cpy_imm_16(vr_input, demo_data->input_len);
	gvml_mod_u16(vr_idx, vr_idx, vr_input);
	
	/* Need extra (void *) casting to ignore alignment warning */
	gvml_lookup_16(vr_input, vr_idx, (const uint16_t *)demo_data->input, demo_data->input_len);

	gvml_store_16(GVML_VM_0, vr_input);

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);
	direct_dma_l1_to_l4_32k(ptr_out, GVML_VM_0);

	return 0;
}

static int get_marked_data_demo(struct gd_lab_1_gmd_demo_data *demo_data)
{
	uint16_t *ptr_out = gal_mem_handle_to_apu_ptr(demo_data->output);

	enum gvml_vr16 vr_idx = GVML_VR16_0;
	enum gvml_mrks_n_flgs mrk_multiple_of_64 = GVML_MRK0;

	/* Prepare index */
	gvml_create_index_16(vr_idx);
	gvml_eq_imm_16_msk(mrk_multiple_of_64, vr_idx, 0, 0x3f);

	gvml_get_marked_data(
		ptr_out,		/* uint16_t *buff */
                vr_idx,			/* enum gvml_vr16 start_vsrc */
		1,			/* unsigned int num_vsrcs */
		mrk_multiple_of_64,	/* enum gvml_mrks_n_flgs msrc */
		512);			/* unsigned int num_ent */	

	return 0;
}

static int fizzbuzz(struct gd_lab_1_fizzbuzz_exercise_data *fizzbuzz_data)
{
	enum gvml_vr16 vr_input = GVML_VR16_0;
	enum gvml_vr16 vr_divisor = GVML_VR16_1;
	enum gvml_vr16 vr_output = GVML_VR16_2;
	enum gvml_mrks_n_flgs mrk_fizz = GVML_MRK0;
	enum gvml_mrks_n_flgs mrk_buzz = GVML_MRK1;
	enum gvml_mrks_n_flgs mrk_output = GVML_MRK2;

	void *ptr_in = gal_mem_handle_to_apu_ptr(fizzbuzz_data->input);
	void *ptr_out = gal_mem_handle_to_apu_ptr(fizzbuzz_data->output);
	
	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);

	direct_dma_l4_to_l1_32k(GVML_VM_0, ptr_in);
	gvml_load_16(vr_input, GVML_VM_0);

	gvml_cpy_imm_16(vr_divisor, 3);
	gvml_mod_u16(vr_divisor, vr_input, vr_divisor);
	gvml_eq_imm_16(mrk_fizz, vr_divisor, 0);
	gvml_cpy_imm_16_mrk(vr_output, GD_LAB_1_FIZZ, mrk_fizz);
	gvml_cpy_m(mrk_output, mrk_fizz);

	gvml_cpy_imm_16(vr_divisor, 5);
	gvml_mod_u16(vr_divisor, vr_input, vr_divisor);
	gvml_eq_imm_16(mrk_buzz, vr_divisor, 0);
	gvml_cpy_imm_16_mrk(vr_output, GD_LAB_1_BUZZ, mrk_buzz);
	gvml_or_m(mrk_output, mrk_output, mrk_buzz);

	gvml_and_m(mrk_fizz, mrk_fizz, mrk_buzz);
	gvml_cpy_imm_16_mrk(vr_output, GD_LAB_1_FIZZBUZZ, mrk_fizz);

	uint32_t count;
	gvml_count_m_g32k(&count, mrk_output);
	gvml_create_index_16(vr_divisor);

	gvml_get_marked_data(
		ptr_out,		/* uint16_t *buff */
                vr_divisor,		/* enum gvml_vr16 start_vsrc */
		2,			/* unsigned int num_vsrcs */
		mrk_output,		/* enum gvml_mrks_n_flgs msrc */
		count);			/* unsigned int num_ent */

	fizzbuzz_data->output_len = count;

	return 0;
}

GAL_TASK_ENTRY_POINT(gd_lab_1, in, out)
{
	struct gd_lab_1_cmd *cmd = (struct gd_lab_1_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_1_CMD_DIRECT_DMA_DEMO:
		gvml_init_once();
		ret = direct_dma_demo(&cmd->dma_demo_data);
		break;

	case GD_LAB_1_CMD_LOOKUP_DEMO:
		gvml_init_once();
		ret = lookup_demo(&cmd->lookup_demo_data);
		break;

	case GD_LAB_1_CMD_GET_MARKED_DATA_DEMO:
		gvml_init_once();
		ret = get_marked_data_demo(&cmd->gmd_demo_data);
		break;

	case GD_LAB_1_CMD_FIZZBUZZ_EXERCISE:
		gvml_init_once();
		ret = fizzbuzz(&cmd->fizzbuzz_exercise_data);
		break;			
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, (int )cmd->cmd);	/* aborts execution */
		break;
	}

	return ret;
}

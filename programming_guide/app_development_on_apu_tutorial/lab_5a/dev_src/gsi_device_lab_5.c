#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_iv.h>               /* gvml_create_index_16() */
#include <gsi/libgvml_element_wise.h>     /* gvml_add_f16() */
#include <gsi/libgvml_min_max.h>          /* gvml_mark_kmin_idxval_u16_mrk_g32k() */
#include <gsi/libgvml_get_marked_data.h>  /* gvml_get_marked_data_xv() */
#include <gsi/libgvml_debug.h>

#include "gsi_device_lab_5.h"
#include "gsi_device_lab_5_dma.h"

static struct {
	uint32_t num_32k_chunks_records;
	uint32_t num_features;
	uint16_t *p_data;
} g_db_data = {
	.num_32k_chunks_records = 0,
	.num_features = 0,
	.p_data = NULL,
};

static int load_db(struct gd_lab_5_load_db *load_db_data)
{
	if (load_db_data->num_32k_chunks_records < 2) {
		gsi_info("number of chunks (%u) must be graeter than 1", load_db_data->num_32k_chunks_records);
		return gsi_status(EINVAL);
	}

	if (load_db_data->num_features > GD_LAB_5_MAX_NUM_FEATURES) {
		gsi_info("number of features (%u) must not exceed %d", load_db_data->num_features, GD_LAB_5_MAX_NUM_FEATURES);
		return gsi_status(EINVAL);
	}

	g_db_data.num_features = load_db_data->num_features;
	g_db_data.num_32k_chunks_records = load_db_data->num_32k_chunks_records;
	g_db_data.p_data = gal_mem_handle_to_apu_ptr(load_db_data->db);

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);

	/* Load the 1st chunk */
	for (uint32_t f = 0; f < load_db_data->num_features; ++f) {
		direct_dma_l4_to_l1_32k(f, &g_db_data.p_data[f * GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK]);
	}

	return 0;
}

static void abs_f16(enum gvml_vr16 vr)
{
	gvml_reset_16_msk(vr, 0x8000);
}

static int do_search(struct gd_lab_5_search *search_data)
{
	/* TODO: implement this function */
	return gsi_status(ENOSYS);
	if (search_data->k > GD_LAB_5_MAX_K) {
		gsi_info("k (%u) must not exceed %d", search_data->k, GD_LAB_5_MAX_K);
		return gsi_status(EINVAL);
	}

	enum gvml_vr16 vr_merged_idx_lsb = GVML_VR16_0;
	enum gvml_vr16 vr_merged_idx_msb = GVML_VR16_1;
	enum gvml_vr16 vr_merged = GVML_VR16_2;
	enum gvml_vr16 vr_idx = GVML_VR16_3;
	enum gvml_vr16 vr_distances = GVML_VR16_4;
	enum gvml_vr16 vr_records = GVML_VR16_5;
	enum gvml_vr16 vr_query = GVML_VR16_6;
	enum gvml_mrks_n_flgs mrk_merged = GVML_MRK0;
	enum gvml_mrks_n_flgs mrk_in = GVML_MRK1;
	enum gvml_mrks_n_flgs mrk_kmin = GVML_MRK2;
	enum gvml_mrks_n_flgs GSI_UNUSED(mrk_added_entries) = GVML_MRK3;

	gvml_create_index_16(vr_merged_idx_lsb);
	gvml_reset_16(vr_merged_idx_msb);
	gvml_create_index_16(vr_idx);
	gvml_set_m(mrk_in);

	const uint16_t *p_q = gal_mem_handle_to_apu_ptr(search_data->query);
	struct gd_lab_5_idx_val *p_iv = gal_mem_handle_to_apu_ptr(search_data->output);
	const uint16_t *p_next_chunk = g_db_data.p_data + GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK * g_db_data.num_features;

	for (uint32_t c = 0; c < g_db_data.num_32k_chunks_records; ++c, p_next_chunk += GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK * g_db_data.num_features) {
		gvml_reset_16(vr_distances);
		for (uint32_t f = 0; f < g_db_data.num_features; ++f) {
			gvml_cpy_imm_16(vr_query, p_q[f]);
			gvml_load_16(vr_records, f);
			gvml_sub_f16(vr_records, vr_records, vr_query);
			abs_f16(vr_records);
			gvml_add_f16(vr_distances, vr_distances, vr_records);
		}

		gvml_mark_kmin_idxval_u16_mrk_g32k(
			mrk_kmin,       /* enum gvml_mrks_n_flgs mdst */
			vr_distances,   /* enum gvml_vr16 vsrc_val */
			vr_idx,         /* enum gvml_vr16 vsrc_idx */
			search_data->k, /* unsigned int k */
			mrk_in,         /* enum gvml_mrks_n_flgs msrc */
			vr_query);      /* enum gvml_vr16 _2vtmp */

		if (c == 0) {
			gvml_cpy_m(mrk_merged, mrk_kmin);
			gvml_cpy_from_mrk_16_msk(vr_distances, mrk_merged, 0x8000);
			gvml_cpy_16(vr_merged, vr_distances);
		} else {
			/* merge results */
			/* TODO: prepare @vr_distances and @vr_merged for merging */
			/* TODO: merge @vr_distances into @vr_merged using gvml_merge_v15mrk1_g32k() (reuse @vr_records as the destination index (@vidx_dst)) */

			/* determine the added entries */
			/* TODO: use @mrk_merged and its updated version residing in @vr_merged to calculate @mrk_added_entries (don't forget to update @mrk_merged too) */

			/* TODO: determine the k minimum values in @vr_merged (use @mrk_merged as both input and output and use @vr_idx to break ties) */

			/* determine the added entries that survived the cut */
			/* TODO: use @mrk_added_entries and @mrk_merged to determine which of the added entries are relevant (store the result in @mrk_added_entries) */

			/* update the indices of the added entries */
			/* TODO: use @mrk_added_entries and @vr_records (which contains the m4 index of the merged results) to update @vr_merged_idx_lsb and @vr_merged_idx_msb */
			/* TODO: don't forget: if @c is odd, you need to update the most significant bit of @vr_merged_idx_lsb */
		}

		/* Load next chunk (or the 1st chunk if we're done) */
		if (c == g_db_data.num_32k_chunks_records - 1) {
			p_next_chunk = g_db_data.p_data;
		}
		for (uint32_t f = 0; f < g_db_data.num_features; ++f) {
			direct_dma_l4_to_l1_32k(f, &p_next_chunk[f * GD_LAB_5_NUM_RECORDS_IN_DB_CHUNK]);
		}
	}

	gvml_reset_16_msk(vr_merged, 0x8000);
	gvml_get_marked_data(
		(uint16_t *)(void *)p_iv, /* uint16_t *buff */
		vr_merged_idx_lsb,        /* enum gvml_vr16 start_vsrc */
		3,                        /* unsigned int num_vsrcs */
		mrk_merged,               /* enum gvml_mrks_n_flgs msrc */
		search_data->k);          /* unsigned int num_ent */

	return 0;
}


GAL_TASK_ENTRY_POINT(gd_lab_5, in, out)
{
	struct gd_lab_5_cmd *cmd = (struct gd_lab_5_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_5_CMD_LOAD_DB:
		gvml_init_once();
		ret = load_db(&cmd->load_db_data);
		break;
	case GD_LAB_5_CMD_SEARCH:
		ret = do_search(&cmd->search_data);
		break;
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, cmd->cmd); /* aborts execution */
		break;
	}

	return ret;
}

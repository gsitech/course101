#ifndef GSI_DEVICE_LAB_7_H
#define GSI_DEVICE_LAB_7_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

enum {
	GD_LAB_7_MAX_NUM_RECORDS_IN_DB = 32 * 1024,
	GD_LAB_7_MAX_NUM_FEATURES = 48,
};

enum gd_lab_7_cmd_type {
	GD_LAB_7_CMD_SIF,
	GD_LAB_7_NUM_CMDS
};

static inline uint64_t gd_lab_7_get_preprocessed_db_size(uint32_t num_features)
{
	return sizeof(uint16_t) * num_features * GD_LAB_7_MAX_NUM_RECORDS_IN_DB;
}

static inline void gd_lab_7_preprocess_db(uint16_t dst_f16[], const uint16_t src_f16[], uint32_t num_features, uint32_t num_records)
{
	for (uint32_t f = 0; f < num_features; ++f) {
		for (uint32_t r = 0; r < num_records; ++r) {
			dst_f16[GD_LAB_7_MAX_NUM_RECORDS_IN_DB * f + r] = src_f16[r * num_features + f];
		}
		/* For valgrind */
		for (uint32_t r = num_records; r < GD_LAB_7_MAX_NUM_RECORDS_IN_DB; ++r) {
			dst_f16[GD_LAB_7_MAX_NUM_RECORDS_IN_DB * f + r] = 0;
		}
	}
}

struct gd_lab_7_idx_val {
	uint16_t idx;
	uint16_t val;
} __attribute__((packed));

struct gd_lab_7_search_in_focus_search_session {
	int32_t	sem;
	int32_t	ret_val;
	uint16_t k;
	uint16_t num_queries;
} __attribute__((packed));

#define GD_LAB_7_INITIAL_SEARCH_SESSION { .sem = 1, .ret_val = 1 }

struct gd_lab_7_search_in_focus {
	uint64_t db;		/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint32_t num_features;
	uint32_t num_records;
	uint64_t queries;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
	uint64_t output;	/* gdl_mem_handle_t(host) / gal_mem_handle_t(dev) */
} __attribute__((packed));

struct gd_lab_7_search_in_focus_session {
	uint64_t			search_session;	/* pointer to constantly mapped address in L4, (gdl_mem_handle_t(host) / gal_mem_handle_t(dev)), aligned to GSLD_MEM_ALIGNMENT*/
	struct gd_lab_7_search_in_focus	search_in_focus;
}__attribute__((packed));

/* Is the device ready to report the start-up result? */
static inline __attribute__((always_inline)) int gd_lab_7_search_in_focus_is_ready(volatile struct gd_lab_7_search_in_focus_search_session *ss)
{
	return ss->sem == 0;
}

static inline __attribute__((always_inline)) int gd_lab_7_search_in_focus_get_ret_val(volatile struct gd_lab_7_search_in_focus_search_session *ss)
{
	return ss->ret_val;
}

static inline __attribute__((always_inline)) int gd_lab_7_search_is_ready(volatile struct gd_lab_7_search_in_focus_search_session *ss)
{
	return ss->sem == 0;
}

static inline __attribute__((always_inline)) void gd_lab_7_search_post(volatile struct gd_lab_7_search_in_focus_search_session *ss, uint16_t k, uint16_t num_queries)
{
	GSI_DEBUG_ASSERT(gd_lab_7_search_is_ready(ss));
	ss->k = k;
	ss->num_queries = num_queries;
	ss->sem = 1;
}

static inline __attribute__((always_inline)) void gd_lab_7_search_stop(volatile struct gd_lab_7_search_in_focus_search_session *ss)
{
	GSI_DEBUG_ASSERT(gd_lab_7_search_is_ready(ss));
	ss->k = 0;
	ss->sem = 1;
}

struct gd_lab_7_cmd {
	uint32_t cmd;
	union 	{
		struct gd_lab_7_search_in_focus_session	sif_session;
	} __attribute__((packed));
} __attribute__((packed));

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GSI_DEVICE_LAB_7_H */

#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_iv.h>              /* gvml_create_index_16() */
#include <gsi/libgvml_element_wise.h>    /* gvml_add_f16() */
#include <gsi/libgvml_min_max.h>         /* gvml_mark_kmin_idxval_u16_mrk_g32k() */
#include <gsi/libgvml_get_marked_data.h> /* gvml_get_marked_data_xv() */
#include <gsi/libgvml_debug.h>

#include "gsi_device_lab_7.h"
#include "gsi_device_lab_7_dma.h"

struct sif_data {
	uint32_t num_records;
	uint32_t num_features;
	enum gvml_mrks_n_flgs mrk_num_records;
	enum gvml_vr16 vr_idx;
	uint16_t *p_q;
	struct gd_lab_7_idx_val *p_iv;
};

static int init_sif(struct sif_data *data, struct gd_lab_7_search_in_focus *sif_input_data)
{
	if (sif_input_data->num_features > GD_LAB_7_MAX_NUM_FEATURES) {
		gsi_error("number of features (%u) must not exceed %d", (unsigned )sif_input_data->num_features, GD_LAB_7_MAX_NUM_FEATURES);
		return gsi_status(EINVAL);
	}

	if (sif_input_data->num_records > GD_LAB_7_MAX_NUM_RECORDS_IN_DB) {
		gsi_error("number of records (%u) must not exceed %d", (unsigned )sif_input_data->num_records, GD_LAB_7_MAX_NUM_RECORDS_IN_DB);
		return gsi_status(EINVAL);
	}

	data->num_records = sif_input_data->num_records;
	data->num_features = sif_input_data->num_features;
	data->vr_idx = GVML_VR16_0;
	gvml_create_index_16(data->vr_idx);
	data->mrk_num_records = GVML_MRK0;
	gvml_lt_imm_u16(data->mrk_num_records, data->vr_idx, (uint16_t)data->num_records);
	data->p_q = gal_mem_handle_to_apu_ptr(sif_input_data->queries);
	data->p_iv = gal_mem_handle_to_apu_ptr(sif_input_data->output);

	uint16_t *ptr_in = gal_mem_handle_to_apu_ptr(sif_input_data->db);

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);

	for (uint32_t f = 0; f < data->num_features; ++f) {
		direct_dma_l4_to_l1_32k(f, &ptr_in[f * GD_LAB_7_MAX_NUM_RECORDS_IN_DB]);
	}

	return 0;
}

static void abs_f16(enum gvml_vr16 vr)
{
	gvml_reset_16_msk(vr, 0x8000);
}

static int do_search(struct sif_data *data, uint32_t num_queries, uint32_t k)
{
	if (k > data->num_records) {
		gsi_error("k (%u) must not exceed number of records (%u)", (unsigned )k, (unsigned )data->num_records);
		return gsi_status(EINVAL);
	}

	enum gvml_vr16 vr_distances = GVML_VR16_1;
	enum gvml_vr16 vr_records = GVML_VR16_2;
	enum gvml_vr16 vr_query = GVML_VR16_3;
	enum gvml_mrks_n_flgs mrk_kmin = GVML_MRK1;

	const uint16_t *p_q = data->p_q;
	struct gd_lab_7_idx_val *p_iv = data->p_iv;
	gal_fast_cache_dcache_invalidate_mlines((uintptr_t)p_q, sizeof(*p_q) * data->num_features * num_queries);

	for (uint32_t q = 0; q < num_queries; ++q, p_q += data->num_features, p_iv += k) {
		gvml_reset_16(vr_distances);
		for (uint32_t f = 0; f < data->num_features; ++f) {
			gvml_cpy_imm_16(vr_query, p_q[f]);
			gvml_load_16(vr_records, f);
			gvml_sub_f16(vr_records, vr_records, vr_query);
			abs_f16(vr_records);
			gvml_add_f16(vr_distances, vr_distances, vr_records);
		}
		gvml_monotonic_transformation_nan2max_u16_f16(vr_records, vr_distances);
		gvml_mark_kmin_idxval_u16_mrk_g32k(
			mrk_kmin,     /* enum gvml_mrks_n_flgs mdst */
			vr_records,   /* enum gvml_vr16 vsrc_val */
			data->vr_idx, /* enum gvml_vr16 vsrc_idx */
			k,            /* unsigned int k */
			data->mrk_num_records, /* enum gvml_mrks_n_flgs msrc */
			vr_query);    /* enum gvml_vr16 _2vtmp */
		gvml_get_marked_data_xv(
			p_iv,         /* void *buff */
			data->vr_idx, /* enum gvml_vr16 v16src */
			vr_distances, /* enum gvml_vr16 v15src */
			mrk_kmin,     /* enum gvml_mrks_n_flgs msrc */
			k);           /* unsigned int num_ent */
	}
	gal_fast_cache_dcache_flush_mlines((uintptr_t)data->p_iv, sizeof(*p_iv) * num_queries * k);

	return 0;
}

static void sif_wait(volatile struct gd_lab_7_search_in_focus_search_session *ss)
{
	while (gal_fast_cache_uc_u32_rd((uintptr_t)&ss->sem) == 0) {
		; // Empty
	}
}

static void sif_post(volatile struct gd_lab_7_search_in_focus_search_session *ss, int32_t ret)
{
	gal_fast_cache_uc_u32_wr((uintptr_t)&ss->ret_val, ret);
	gal_fast_cache_uc_u32_wr((uintptr_t)&ss->sem, 0);
}

static int sif(struct gd_lab_7_search_in_focus_session *sif_session)
{
	struct sif_data data;
	int ret = init_sif(&data, &sif_session->search_in_focus);
	volatile struct gd_lab_7_search_in_focus_search_session *ss = gal_mem_handle_to_apu_ptr(sif_session->search_session);
	sif_post(ss, ret);
	if (ret)
		return ret;

	for (;;) {
		sif_wait(ss);

		uint32_t k_num_queries = gal_fast_cache_uc_u32_rd((uintptr_t)&ss->k);
		uint16_t k = k_num_queries & 0xffff;
		if (k == 0) {
			break;
		}
		uint16_t num_queries = (uint16_t)(k_num_queries >> 16);

		int32_t ret = do_search(&data, num_queries, k);
		sif_post(ss, ret);
	}
	return 0;
}


GAL_TASK_ENTRY_POINT(gd_lab_7, in, out)
{
	struct gd_lab_7_cmd *cmd = (struct gd_lab_7_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_7_CMD_SIF:
		gvml_init_once();
		ret = sif(&cmd->sif_session);
		break;
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, (int )cmd->cmd); /* aborts execution */
		break;
	}

	return ret;
}

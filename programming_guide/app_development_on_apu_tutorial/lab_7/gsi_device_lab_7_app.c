#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>

#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <refgvml.h>
#include <refgvml_element_wise.h>

#include <gsi/gsi_sim_config.h>

GDL_TASK_DECLARE(gd_lab_7);
#include "gsi_device_lab_7.h"

#define DIV_ROUND_UP(x, n) (((x) + (n)-1) / (n))

static uint64_t align_size(uint64_t size, uint64_t alignment)
{
	return DIV_ROUND_UP(size, alignment) * alignment;
}

static void search_in_focus_wait(volatile struct gd_lab_7_search_in_focus_search_session *ss)
{
	while (!gd_lab_7_search_in_focus_is_ready(ss)) {
		/* Empty */
	}
}

static void search_wait(volatile struct gd_lab_7_search_in_focus_search_session *ss)
{
	while (!gd_lab_7_search_is_ready(ss)) {
		/* Empty */
	}
}

struct sif_mem_hndls {
	gdl_mem_handle_t output;
	gdl_mem_handle_t input;
	gdl_mem_handle_t cmd;
	gdl_mem_handle_t search_session;
	gdl_mem_handle_t db;
};

static void destroy_sif_mem_hndls(struct sif_mem_hndls *hndls)
{
	gdl_mem_free(hndls->output);
}

static int init_sif_mem_hndls(
	struct sif_mem_hndls *hndls,
	gdl_context_handle_t ctx_id,
	const uint16_t records[],
	uint32_t num_records,
	uint32_t num_features,
	uint32_t max_num_queries,
	uint32_t max_k)
{
	int ret;
	gdl_mem_handle_t mem_hndl = GDL_MEM_HANDLE_NULL;
	uint16_t *pre_processed_records = NULL;
	uint64_t pre_processed_records_size = gd_lab_7_get_preprocessed_db_size(num_features);

	pre_processed_records = malloc(pre_processed_records_size);
	if (NULL == pre_processed_records) {
		gsi_error("malloc() failed to allocate %lu bytes", pre_processed_records_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}
	gd_lab_7_preprocess_db(pre_processed_records, records, num_features, num_records);

	uint64_t aligned_output_buffer_size = align_size(sizeof(struct gd_lab_7_idx_val) * max_num_queries * max_k, 32);
	uint64_t aligned_queries_buffer_size = align_size(sizeof(uint16_t) * max_num_queries * num_features, 32);
	uint64_t aligned_cmd_buffer_size = align_size(sizeof(struct gd_lab_7_cmd), 32);
	uint64_t aligned_search_session_buffer_size = align_size(sizeof(struct gd_lab_7_search_in_focus_search_session), 32);
	uint64_t total_buffer_size =
		aligned_output_buffer_size + 
		aligned_queries_buffer_size +
		aligned_cmd_buffer_size +
		aligned_search_session_buffer_size +
		pre_processed_records_size;

	mem_hndl = gdl_mem_alloc_aligned(ctx_id, total_buffer_size, GDL_CONST_MAPPED_POOL, GDL_ALIGN_32);
	if (gdl_mem_handle_is_null(mem_hndl)) {
		gsi_error("gdl_mem_alloc() failed to allocate %lu bytes", total_buffer_size);
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	hndls->output = mem_hndl;
	ret = gdl_add_to_mem_handle(&hndls->input, hndls->output, aligned_output_buffer_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_add_to_mem_handle(&hndls->cmd, hndls->input, aligned_queries_buffer_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_add_to_mem_handle(&hndls->search_session, hndls->cmd, aligned_cmd_buffer_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	struct gd_lab_7_search_in_focus_search_session initial_ss = GD_LAB_7_INITIAL_SEARCH_SESSION;
	ret = gdl_mem_cpy_to_dev(hndls->search_session, &initial_ss, sizeof(initial_ss));
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_add_to_mem_handle(&hndls->db, hndls->search_session, aligned_search_session_buffer_size);
	if (ret) {
		gsi_error("gdl_add_to_mem_handle() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	ret = gdl_mem_cpy_to_dev(hndls->db, pre_processed_records, pre_processed_records_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

	struct gd_lab_7_cmd cmd = {
		.cmd = GD_LAB_7_CMD_SIF,
		.sif_session = {
			.search_in_focus = {
				.db = hndls->db,
				.num_features = num_features,
				.num_records = num_records,
				.output = hndls->output,
				.queries = hndls->input
			},
			.search_session = hndls->search_session
		}
	};

	ret = gdl_mem_cpy_to_dev(hndls->cmd, &cmd, sizeof(cmd));
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		goto CLEAN_UP;
	}

CLEAN_UP:
	if (ret)
		gdl_mem_free(mem_hndl);
	free(pre_processed_records);

	return 0;
}

struct run_task_args {
	gdl_context_handle_t ctx_id;
	gdl_mem_handle_t cmd_mem_hndl;
};

static void *run_task(void *ptr)
{
	struct run_task_args *args = (struct run_task_args *)ptr;
	return (void *)(uintptr_t)gdl_run_task_timeout(
		args->ctx_id,                       /* @ctx_handler - the id of a hardware context previously allocated */
		GDL_TASK(gd_lab_7),                 /* @code_offset - the code offset of the function that the task should execute */
		args->cmd_mem_hndl,                 /* @inp - input memory handle */
		GDL_MEM_HANDLE_NULL,                /* @outp - output memory handle */
		GDL_TEMPORARY_DEFAULT_MEM_BUF,      /* @mem_buf - an array of previously allocated memory handles and their sizes */
		GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE, /* @buf_size - the length of the mem_buf array */
		GDL_TEMPORARY_DEFAULT_CORE_INDEX,   /* @apuc_idx - the apuc that the task should be executed on */
		NULL,                               /* @comp - if task was successfully scheduled, and @comp is provided, the task completion status, or any error is returned in comp. */
		0,                                  /* @ms_timeout - the time in mili-seconds a task should wait for completion before aborting (0 indicates waiting indefinitely) */
		GDL_USER_MAPPING);                  /* @map_type - determine the mapping type for the specific task */
}

static int run_task_async(pthread_t *thread, gdl_context_handle_t ctx_id, gdl_mem_handle_t cmd_mem_hndl)
{
	static struct run_task_args args;
	args.ctx_id = ctx_id;
	args.cmd_mem_hndl = cmd_mem_hndl;
	int thread_ret = pthread_create(thread, NULL, run_task, &args);
	if (thread_ret) {
		gsi_error("pthread_create() failed with %d", thread_ret);
		return thread_ret;
	}
	return 0;
}

static int search_in_focus(
	pthread_t *thread,
	struct sif_mem_hndls *hndls,
	gdl_context_handle_t ctx_id,
	const uint16_t records[],
	uint32_t num_records,
	uint32_t num_features,
	uint32_t max_num_queries,
	uint32_t max_k)
{
	int ret = init_sif_mem_hndls(
		hndls,
		ctx_id,
		records,
		num_records,
		num_features,
		max_num_queries,
		max_k);
	if (ret) {
		return ret;
	}

	ret = run_task_async(thread, ctx_id, hndls->cmd);
	if (ret) {
		goto CLEAN_UP;
	}

	/* Verify that the cmd was accepted by the device */
	volatile struct gd_lab_7_search_in_focus_search_session *ss =
		(struct gd_lab_7_search_in_focus_search_session *)gdl_mem_handle_to_host_ptr(hndls->search_session);
	search_in_focus_wait(ss);
	ret = gd_lab_7_search_in_focus_get_ret_val(ss);
	if (ret) {
		gsi_error("Search in Focus failed with %d", ret);
		int join_ret = pthread_join(*thread, NULL);
		if (join_ret) {
			gsi_error("pthread_join() failed with %d", join_ret);
			ret = join_ret;
			goto CLEAN_UP;
		}
	}

CLEAN_UP:
	if (ret) {
		destroy_sif_mem_hndls(hndls);
	}

	return ret;
}

static int stop_focus(pthread_t thread, volatile struct gd_lab_7_search_in_focus_search_session *ss)
{
	search_wait(ss);
	gd_lab_7_search_stop(ss);
	int ret = pthread_join(thread, NULL);
	if (ret) {
		gsi_error("pthread_join() failed with %d", ret);
	}

	return ret;
}

static int do_search(
	struct sif_mem_hndls *hndls,
	struct gd_lab_7_idx_val iv_dst[],
	const uint16_t queries[],
	uint32_t num_queries,
	uint32_t num_features,
	uint32_t k)
{
	volatile struct gd_lab_7_search_in_focus_search_session *ss =
		(struct gd_lab_7_search_in_focus_search_session *)gdl_mem_handle_to_host_ptr(hndls->search_session);
	search_wait(ss);

	int ret;
	uint64_t queries_size = sizeof(*queries) * num_queries * num_features;
	uint64_t output_size = sizeof(*iv_dst) * num_queries * k;

	ret = gdl_mem_cpy_to_dev(hndls->input, queries, queries_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_to_dev() failed: %s", gsi_status_errorstr(ret));
		return ret;
	}

	gd_lab_7_search_post(ss, k, num_queries);
	search_wait(ss);

	ret = gd_lab_7_search_in_focus_get_ret_val(ss);
	if (ret) {
		gsi_error("search failed: %s", gsi_status_errorstr(ret));
		return ret;
	}

	ret = gdl_mem_cpy_from_dev(iv_dst, hndls->output, output_size);
	if (ret) {
		gsi_error("gdl_mem_cpy_from_dev() failed: %s", gsi_status_errorstr(ret));
		return ret;
	}

	return ret;
}

static float random_float_0_to_1(void)
{
	return (float)rand() / (float)(RAND_MAX);
}

static void init_random_f16_array(uint16_t *dst, uint32_t num_rows, uint32_t num_cols)
{
	for (uint32_t r = 0; r < num_rows; ++r) {
		for (uint32_t f = 0; f < num_cols; ++f) {
			*dst++ = refgvml_float_2_f16(random_float_0_to_1());
		}
	}
}

static uint16_t refgvml_abs_f16(uint16_t x)
{
	return x & 0x7FFF;
}

static uint16_t calc_l1_distance(const uint16_t record[], const uint16_t query[], uint32_t num_features)
{
	uint16_t ret = 0;

	for (uint32_t f = 0; f < num_features; ++f) {
		uint16_t diff = refgvml_sub_f16(record[f], query[f]);
		uint16_t abs_diff = refgvml_abs_f16(diff);
		ret = refgvml_add_f16(ret, abs_diff);
	}

	return ret;
}

static int f16_is_lhs_lt_rhs(uint16_t lhs, uint16_t rhs)
{
	return refgvml_monotonic_transformation_nan2max_u16_f16(lhs) < refgvml_monotonic_transformation_nan2max_u16_f16(rhs);
}

static int f16_idx_val_cmp_ascending(const void *lhs, const void *rhs)
{
	const struct gd_lab_7_idx_val *iv_lhs = (struct gd_lab_7_idx_val *)lhs;
	const struct gd_lab_7_idx_val *iv_rhs = (struct gd_lab_7_idx_val *)rhs;
	return (f16_is_lhs_lt_rhs(iv_lhs->val, iv_rhs->val) || (iv_lhs->val == iv_rhs->val && iv_lhs->idx < iv_rhs->idx)) ? -1 : 1;
}

static void sort_f16_idx_val_ascending(struct gd_lab_7_idx_val iv[], uint32_t num_elements)
{
	qsort(iv, num_elements, sizeof(*iv), f16_idx_val_cmp_ascending);
}

static void calc_ref_iv(struct gd_lab_7_idx_val iv[], const uint16_t records[], uint32_t num_records, uint32_t num_features, const uint16_t queries[], uint32_t num_queries)
{
	for (uint32_t q = 0; q < num_queries; ++q) {
		for (uint32_t r = 0; r < num_records; ++r) {
			iv[q * num_records + r].val = calc_l1_distance(&records[r * num_features], &queries[q * num_features], num_features);
			iv[q * num_records + r].idx = (uint16_t)r;
		}
	}
}

static int check_results(struct gd_lab_7_idx_val iv_res[], struct gd_lab_7_idx_val iv_ref[], uint32_t num_records, uint32_t num_queries, uint32_t k)
{
	/* Sanity check */
	for (uint32_t q = 0; q < num_queries; ++q) {
		for (uint32_t e = 0; e < k; ++e) {
			struct gd_lab_7_idx_val iv_res_element = iv_res[q * k + e];
			if (iv_res_element.idx >= num_records) {
				gsi_error("q = %u, e = %u: result has index >= num_records (val = 0x%x, idx = %d), num_records = %u",
						  q, e, iv_res_element.val, iv_res_element.idx, num_records);
				return -1;
			}
			struct gd_lab_7_idx_val iv_ref_element = iv_ref[q * num_records + iv_res_element.idx];
			if (iv_res_element.val != iv_ref_element.val) {
				gsi_error("q = %u, e = %u: mismatch in distances idx = %d (res val = 0x%x, ref val = 0x%x)",
						  q, e, iv_res_element.idx, iv_res_element.val, iv_ref_element.val);
				return -1;
			}
		}
	}

	/* Check best k */
	for (uint32_t q = 0; q < num_queries; ++q) {
		sort_f16_idx_val_ascending(&iv_ref[q * num_records], num_records);
		sort_f16_idx_val_ascending(&iv_res[q * k], k);
		for (uint32_t e = 0; e < k; ++e) {
			struct gd_lab_7_idx_val iv_res_element = iv_res[q * k + e];
			struct gd_lab_7_idx_val iv_ref_element = iv_ref[q * num_records + e];
			if (iv_res_element.val != iv_ref_element.val || iv_res_element.idx != iv_ref_element.idx) {
				gsi_error("q = %u, e = %u: mismatch in results res: (val = 0x%x, idx = %d), ref: (val = 0x%x, idx = %d)\n",
						  q, e, iv_res_element.val, iv_res_element.idx, iv_ref_element.val, iv_ref_element.idx);
				return -1;
			}
		}
	}

	return 0;
}

struct lab_7_args {
	uint32_t num_records;
	uint32_t num_features;
	uint32_t max_num_queries;
	uint32_t max_k;
	uint32_t num_searches;
};

static int parse_args(struct lab_7_args *args, int argc, char *argv[])
{
	if (6 != argc) {
		gsi_error("usage: %s num_records num_features max_num_queries max_k num_searches", argv[0]);
		return gsi_status(EINVAL);
	}

	args->num_records = atoi(argv[1]);
	args->num_features = atoi(argv[2]);
	args->max_num_queries = atoi(argv[3]);
	args->max_k = atoi(argv[4]);
	args->num_searches = atoi(argv[5]);

	printf("****************** ARGS ******************\n");
	printf("num_records = %u\n", args->num_records);
	printf("num_features = %u\n", args->num_features);
	printf("max_num_queries = %u\n", args->max_num_queries);
	printf("max_k = %u\n", args->max_k);
	printf("num_searches = %u\n", args->num_searches);
	printf("******************************************\n");

	return 0;
}

static uint32_t rand_int_range_inclusive(uint32_t lb, uint32_t ub)
{
	return rand() % (ub - lb + 1) + lb;
}


// For Simulator:
enum { NUM_CTXS = 1 };
static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};


int main(int argc, char *argv[])
{
	gsi_libsys_init(
		argv[0],    /* program name */
		true);      /* log_to_screen */

	printf("If you are using valgrind, run with --fair-sched=yes. See https://valgrind.org/docs/manual/manual-core.html#manual-core.pthreads_perf_sched\n");

	struct lab_7_args args;
	int ret = parse_args(&args, argc, argv);
	if (ret)
		gsi_fatal("parse_args() failed");

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	uint32_t num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	/* Use first available context */
	gdl_context_handle_t valid_ctx_id;
	for (uint32_t ctx = 0; ctx < num_ctxs; ++ctx) {
		if (contexts_desc[ctx].status == GDL_CONTEXT_READY) {
			valid_ctx_id = contexts_desc[ctx].ctx_id;
			printf("Memory Size = %0.1fG\n", (float)contexts_desc[ctx].mem_size / 1024L / 1024L / 1024L);
			printf("Num Apucs = %d\n", contexts_desc[ctx].num_apucs);
			break;
		}
	}

	const long long unsigned int const_mapped_size_req = 3L * 1024L * 1024L * 1024L;
	long long unsigned int const_mapped_size_recv, dynamic_mapped_size_recv;

	ret = gdl_context_alloc(valid_ctx_id, const_mapped_size_req, &const_mapped_size_recv, &dynamic_mapped_size_recv);
	if (ret) {
		gsi_error("gdl_context_alloc failed: %s", gsi_status_errorstr(ret));
		return ret;
	}
	printf("Constantly mapped memory = %0.1fG\n", (float)const_mapped_size_recv / 1024L / 1024L / 1024L);
	printf("Dynamically mapped memory = %0.1fG\n", (float)dynamic_mapped_size_recv / 1024L / 1024L / 1024L);

	uint16_t *records = NULL, *queries = NULL;
	struct gd_lab_7_idx_val *iv_ref = NULL, *iv_res = NULL;
	struct sif_mem_hndls sif_hndls = {
		.output = GDL_MEM_HANDLE_NULL,
		.input = GDL_MEM_HANDLE_NULL,
		.cmd = GDL_MEM_HANDLE_NULL,
		.search_session = GDL_MEM_HANDLE_NULL,
		.db = GDL_MEM_HANDLE_NULL,
	};
	pthread_t sif_thread;

	records = malloc(sizeof(uint16_t) * args.num_records * args.num_features);
	queries = malloc(sizeof(uint16_t) * args.max_num_queries * args.num_features);
	iv_ref = malloc(sizeof(struct gd_lab_7_idx_val) * args.num_records * args.max_num_queries);
	iv_res = malloc(sizeof(struct gd_lab_7_idx_val) * args.max_k * args.max_num_queries);

	if (NULL == records || NULL == queries || NULL == iv_ref || NULL == iv_res) {
		gsi_error("malloc failed");
		ret = gsi_status(ENOMEM);
		goto CLEAN_UP;
	}

	init_random_f16_array(records, args.num_records, args.num_features);

	printf("Search in Focus ...\n");
	ret = search_in_focus(
		&sif_thread,
		&sif_hndls,
		valid_ctx_id,
		records,
		args.num_records,
		args.num_features,
		args.max_num_queries,
		args.max_k);
	if (ret) {
		gsi_error("search_in_focus() failed with %d", ret);
		goto CLEAN_UP;
	}
	printf("search_in_focus() completed successfully\n");

	for (uint32_t s = 0; s < args.num_searches; ++s) {
		uint32_t num_queries = rand_int_range_inclusive(1, args.max_num_queries);
		uint32_t k = rand_int_range_inclusive(1, args.max_k);
		init_random_f16_array(queries, num_queries, args.num_features);
		printf("Performing search %u of %u (num_queries = %u, k = %u)...\n", s + 1, args.num_searches, num_queries, k);
		ret = do_search(&sif_hndls, iv_res, queries, num_queries, args.num_features, k);
		if (ret) {
			gsi_error("do_search() failed with %d", ret);
			goto CLEAN_UP;
		}
		printf("Finished searching\n");

		printf("Checking results ...\n");
		calc_ref_iv(iv_ref, records, args.num_records, args.num_features, queries, num_queries);
		ret = check_results(iv_res, iv_ref, args.num_records, num_queries, k);
		if (ret) {
			goto CLEAN_UP;
		}
		printf("Finished checking results\n");
	}

CLEAN_UP:
	stop_focus(sif_thread, gdl_mem_handle_to_host_ptr(sif_hndls.search_session));
	free(iv_res);
	free(iv_ref);
	free(queries);
	free(records);

	gdl_context_free(valid_ctx_id);
	gdl_exit();

	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nFailure\n");
	} else {
		printf("\nSuccess\n");
	}
	return ret;
}

#include <gsi/libsys/assert.h>
#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#include <gsi/libgvml_iv.h>              /* gvml_create_index_16() */
#include <gsi/libgvml_element_wise.h>    /* gvml_add_f16() */
#include <gsi/libgvml_min_max.h>         /* gvml_mark_kmin_idxval_u16_mrk_g32k() */
#include <gsi/libgvml_get_marked_data.h> /* gvml_get_marked_data_xv() */
#include <gsi/libgvml_debug.h>

#include <gsi_device_lab_6.h>
#include <gsi_device_lab_6_dma.h>
#include <gsi_device_lab_6_profiling.h>

#define DIV_ROUND_UP(x, n) (((x) + (n)-1) / (n))
#define NUM_64_BIT_OUTPUT_ELEMENTS_IN_512B (512 / sizeof(struct gd_lab_6_idx_val))

PROF_VAR(search);
PROF_VAR(l4_to_l1);
PROF_VAR(calc);
PROF_VAR(extract);

static struct {
	uint32_t num_32k_chunks_records;
	uint32_t num_features;
	uint16_t *p_data;
} g_db_data = {
	.num_32k_chunks_records = 0,
	.num_features = 0,
	.p_data = NULL,
};

static void prof_init(void)
{
	arc_counters_init();
	PROF_INIT(search);
	PROF_INIT(l4_to_l1);
	PROF_INIT(calc);
	PROF_INIT(extract);
}

static void prof_print(void)
{
	PROF_PRINT(search);
	PROF_PRINT(l4_to_l1);
	PROF_PRINT(calc);
	PROF_PRINT(extract);
}

static int load_db(struct gd_lab_6_load_db *load_db_data)
{
	if (load_db_data->num_32k_chunks_records < 2) {
		gsi_info("number of chunks (%u) must be graeter than 1", load_db_data->num_32k_chunks_records);
		return gsi_status(EINVAL);
	}

	if (load_db_data->num_features > GD_LAB_6_MAX_NUM_FEATURES) {
		gsi_info("number of features (%u) must not exceed %d", load_db_data->num_features, GD_LAB_6_MAX_NUM_FEATURES);
		return gsi_status(EINVAL);
	}

	g_db_data.num_features = load_db_data->num_features;
	g_db_data.num_32k_chunks_records = load_db_data->num_32k_chunks_records;
	g_db_data.p_data = gal_mem_handle_to_apu_ptr(load_db_data->db);

	gal_set_l2dma_dma_mode(GAL_L2DMA_MODE_DIRECT);

	/* Load the 1st chunk */
	for (uint32_t f = 0; f < load_db_data->num_features; ++f) {
		direct_dma_l4_to_l1_32k(f, &g_db_data.p_data[f * GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK]);
	}

	prof_init();

	return 0;
}

static void abs_f16(enum gvml_vr16 vr)
{
	gvml_reset_16_msk(vr, 0x8000);
}

struct _64_bit_output_out_data {
	uint64_t *l3;
	uint64_t *l4;
};

/* TODO: implement this function */
static void _64_bit_output_copy0_512_bytes_from_l3_to_l4(void *params)
{
	struct _64_bit_output_out_data GSI_UNUSED(*idx_val) = (struct _64_bit_output_out_data *)params;
	/* TODO: flush 512b from @idx_val->l3 */
	/* TODO: copy 512 from idx_val->l3 to idx_val->l4 */
	/* TODO: increment idx_val->l3 and idx_val->l4 by 512 bytes*/
}

static int do_search(struct gd_lab_6_search *search_data)
{
	PROF_START(search);
	if (search_data->k > GD_LAB_6_MAX_K) {
		gsi_info("k (%u) must not exceed %d", search_data->k, GD_LAB_6_MAX_K);
		return gsi_status(EINVAL);
	}

	uint32_t num_512b_txns = DIV_ROUND_UP(sizeof(struct gd_lab_6_idx_val) * search_data->k, 512);
	const uint64_t l3_buff_size = num_512b_txns * 512;
	void *l3_buff = gal_fast_malloc_cache_aligned(l3_buff_size, false);
	if (GSI_IS_ERR_PTR_OR_NULL(l3_buff)) {
		gsi_info("gal_fast_malloc_cache_aligned() failed to allocate %llu bytes", l3_buff_size);
		return gsi_status(ENOMEM);
	}

	enum gvml_vr16 vr_merged_idx_lsb = GVML_VR16_0;
	enum gvml_vr16 vr_merged_idx_msb = GVML_VR16_1;
	enum gvml_vr16 vr_merged = GVML_VR16_2;
	enum gvml_vr16 vr_idx = GVML_VR16_3;
	enum gvml_vr16 vr_distances = GVML_VR16_4;
	enum gvml_vr16 vr_records = GVML_VR16_5;
	enum gvml_vr16 vr_query = GVML_VR16_6;
	enum gvml_mrks_n_flgs mrk_merged = GVML_MRK0;
	enum gvml_mrks_n_flgs mrk_in = GVML_MRK1;
	enum gvml_mrks_n_flgs mrk_kmin = GVML_MRK2;
	enum gvml_mrks_n_flgs mrk_added_entries = GVML_MRK3;

	gvml_create_index_16(vr_merged_idx_lsb);
	gvml_reset_16(vr_merged_idx_msb);
	gvml_create_index_16(vr_idx);
	gvml_set_m(mrk_in);

	const uint16_t *p_q = gal_mem_handle_to_apu_ptr(search_data->query);
	void *p_iv = gal_mem_handle_to_apu_ptr(search_data->output);
	const uint16_t *p_next_chunk = g_db_data.p_data + GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK * g_db_data.num_features;

	for (uint32_t c = 0; c < g_db_data.num_32k_chunks_records; ++c, p_next_chunk += GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK * g_db_data.num_features) {
		if (c == g_db_data.num_32k_chunks_records - 1) {
			p_next_chunk = g_db_data.p_data;
		}
		direct_dma_l4_to_l2_32k_start(p_next_chunk);

		PROF_START(calc);
		gvml_reset_16(vr_distances);
		for (uint32_t f = 0; f < g_db_data.num_features; ++f) {
			gvml_cpy_imm_16(vr_query, p_q[f]);
			gvml_load_16(vr_records, f);
			gvml_sub_f16(vr_records, vr_records, vr_query);
			abs_f16(vr_records);
			gvml_add_f16(vr_distances, vr_distances, vr_records);
		}

		gvml_mark_kmin_idxval_u16_mrk_g32k(
			mrk_kmin,       /* enum gvml_mrks_n_flgs mdst */
			vr_distances,   /* enum gvml_vr16 vsrc_val */
			vr_idx,         /* enum gvml_vr16 vsrc_idx */
			search_data->k, /* unsigned int k */
			mrk_in,         /* enum gvml_mrks_n_flgs msrc */
			vr_query);      /* enum gvml_vr16 _2vtmp */

		if (c == 0) {
			gvml_cpy_m(mrk_merged, mrk_kmin);
			gvml_cpy_from_mrk_16_msk(vr_distances, mrk_merged, 0x8000);
			gvml_cpy_16(vr_merged, vr_distances);
		} else {
			/* merge results */
			gvml_cpy_from_mrk_16_msk(vr_distances, mrk_kmin, 0x8000);
			gvml_cpy_from_mrk_16_msk(vr_merged, mrk_merged, 0x8000);
			gvml_merge_v15mrk1_g32k(vr_merged, vr_records, vr_distances);

			/* determine the added entries */
			gvml_cpy_m(mrk_added_entries, mrk_merged);
			gvml_cpy_to_mrk_16_msk(mrk_merged, vr_merged, 0x8000);
			gvml_xor_m(mrk_added_entries, mrk_added_entries, mrk_merged);

			gvml_mark_kmin_idxval_u16_mrk_g32k(
				mrk_merged,     /* enum gvml_mrks_n_flgs mdst */
				vr_merged,      /* enum gvml_vr16 vsrc_val */
				vr_idx,         /* enum gvml_vr16 vsrc_idx */
				search_data->k, /* unsigned int k */
				mrk_merged,     /* enum gvml_mrks_n_flgs msrc */
				vr_query);      /* enum gvml_vr16 _2vtmp */

			/* determine the added entries that survived the cut */
			gvml_and_m(mrk_added_entries, mrk_added_entries, mrk_merged);

			/* update the indices of the added entries */
			gvml_cpy_16_mrk(vr_merged_idx_lsb, vr_records, mrk_added_entries);
			if (c & 1)
				gvml_cpy_imm_16_msk_mrk(vr_merged_idx_lsb, 0x8000, 0x8000, mrk_added_entries);
			gvml_cpy_imm_16_mrk(vr_merged_idx_msb, c >> 1, mrk_added_entries);
		}
		PROF_END(calc);

		/* Load next chunk (or the 1st chunk if we're done) */

		PROF_START(l4_to_l1);
		dma_l2_sync();
		direct_dma_l2_to_l1_32k(0);
		for (uint32_t f = 1; f < g_db_data.num_features; ++f) {
			direct_dma_l4_to_l1_32k(f, &p_next_chunk[f * GD_LAB_6_NUM_RECORDS_IN_DB_CHUNK]);
		}
		PROF_END(l4_to_l1);
	}

	PROF_START(extract);
	gvml_reset_16_msk(vr_merged, 0x8000);

	struct _64_bit_output_out_data out_data = {
		.l3 = (uint64_t *)l3_buff,
		.l4 = (uint64_t *)p_iv,
	};

	gal_fast_l2dma_async_memcpy_init(GAL_L2DMA_APC_ID_0);
	gvml_get_marked_data_sxvv_interval(
		l3_buff,                    /* void *buff */
		vr_merged_idx_lsb,          /* enum gvml_vr16 v16src1 */
		vr_merged_idx_msb,          /* enum gvml_vr16 v16src2 */
		vr_merged,                  /* enum gvml_vr16 v15src */
		0,                          /* val (not relevant in this lab) */
		mrk_merged,                 /* enum gvml_mrks_n_flgs msrc */
		search_data->k,             /* unsigned int num_ent */
		NUM_64_BIT_OUTPUT_ELEMENTS_IN_512B,           /* unsigned int interval_ent */
		_64_bit_output_copy0_512_bytes_from_l3_to_l4, /* interval_func_t interval_func */
		&out_data);                                   /* void *interval_params */
	/* TODO: if @search_data->k is not divisible by 8, you need to flush + copy one last time */
	gal_fast_l2dma_async_memcpy_end(GAL_L2DMA_APC_ID_0);
	gal_fast_free_cache_aligned(l3_buff);
	PROF_END(extract);
	PROF_END(search);

	prof_print();

	return gsi_status(ENOSYS);
}


GAL_TASK_ENTRY_POINT(gd_lab_6, in, out)
{
	struct gd_lab_6_cmd *cmd = (struct gd_lab_6_cmd *)in;
	int ret;

	switch (cmd->cmd) {
	case GD_LAB_6_CMD_LOAD_DB:
		gvml_init_once();
		ret = load_db(&cmd->load_db_data);
		break;
	case GD_LAB_6_CMD_SEARCH:
		ret = do_search(&cmd->search_data);
		break;
	default:
		gsi_fatal("%s: unknown command %d\n", __func__, cmd->cmd); /* aborts execution */
		break;
	}

	return ret;
}

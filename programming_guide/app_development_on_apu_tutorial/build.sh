#!/bin/bash

echo Info: set path to ARC compilers

# Example for default product x86_64 - when Synopsys ccac compiler is used:
source /efs/data/public/synopsis/ARC-2018.06/env_set.env

# Example for product gnu-ccac - when GNU toolchain for ARC is used:
# Note: GNU toolchain default build (without no-small-data) is not supported
# Note: GNU toolchain from 2022.09 (or later) is not supported
GNU_TOOLCHAIN_FOR_ARC_BASE=/efs/data/public/synopsis/GNU/arc_gnu_2023.03-release_elf32_le_linux_no_sdata/arc-snps-elf
export PATH=${GNU_TOOLCHAIN_FOR_ARC_BASE}/bin:${PATH}

errors=
for i in lab_0 lab_1b lab_2b lab_3b lab_4b lab_5b lab_6g lab_7
do
	echo '###################### ' $i
	make -C $i "$@" || errors="${errors} $i"
done

echo '###################### '
echo Info: the executables are:
find . -type f -executable \! -name '*\.mod*' \! -name '*\.bin*' \! -name '*\.a' \! -name '*\.sh' \! -name '*\.so' | grep /build/ | sort

if [ ! -z "${errors}" ]; then
	echo '!!!!!!!!!!!!!!!!!!!!!!'
	echo Error: fail to build labs:
	echo ${errors}
	exit 1
fi
exit 0

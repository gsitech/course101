/*
 * Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#ifndef GSI_HELLO_WORLD_MODULE_H
#define GSI_HELLO_WORLD_MODULE_H

#define EXPECTED_RESPONSE_MSG "Hello World from the APU!"

#endif /* GSI_HELLO_WORLD_MODULE_H */

/*
 * Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gsi/libgdl.h>

#include "hello-world-module.h"

GDL_TASK_DECLARE(hello_world);

int main(void)
{
	gdl_init();

	gdl_context_handle_t board_ctx = (gdl_context_handle_t)NULL;
	unsigned long long constant_memory_pool_size = 0;
	gdl_mem_handle_t apu_response = GDL_MEM_HANDLE_NULL;
	int error = 0;

	/* Your code goes here */

	int8_t *msg = (int8_t *)gdl_mem_handle_to_host_ptr(apu_response);
	printf("Got from APU: \"%s\", %s\n",
		"",
		strcmp("", EXPECTED_RESPONSE_MSG) == 0 ? "as expected" : "but expected \"" EXPECTED_RESPONSE_MSG "\" :(");

	gdl_exit();

	return error;
}

/*
 * Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#include <string.h>

#include <gsi/libgal.h>

#include "hello-world-module.h"


GAL_TASK_ENTRY_POINT(hello_world, buffer, out)
{
	gal_init();

	strcpy(buffer, EXPECTED_RESPONSE_MSG);

	gal_exit();

	return 0;
}

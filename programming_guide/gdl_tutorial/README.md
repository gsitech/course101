# GDL Training Material

This directory contains code for the host development labs, as well
links to labs' handouts, video labs, and related material.

## Labs

* lab1 - Write a simple "Hello World" application using the APU
    * [Video for lab1](https://web.microsoftstream.com/video/10a30a2e-56d9-4f6f-a856-31eee4d708ce)

* lab2 - Fix coding errors to learn about dynamic memory mapping in GDL

### Note

Each lab has a branch with the solution of the lab, the branch name is: ``feature/<lab-name>-solution``

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gsi/libgdl.h>

#include "dynamic-memory-module.h"

#define MB (1LLU << 20)
#define GB (1LLU << 30)

#define DATASET1_VECTOR_LENGTH 256
#define DATASET1_SIZE (2 * GB)
#define DATASET1_VECTOR_COUNT (DATASET1_SIZE/(DATASET1_VECTOR_LENGTH * sizeof(float)))

#define DATASET2_VECTOR_LENGTH 192
#define DATASET2_SIZE (3 * GB)
#define DATASET2_VECTOR_COUNT (DATASET2_SIZE/(DATASET2_VECTOR_LENGTH * sizeof(float)))

GDL_TASK_DECLARE(dynamic_memory);

struct task_desc {
	struct gsi_task_desc gdl_task;
	gdl_mem_handle_t command_mem_handle;
	unsigned int ms_timeout;
};

const char *gdl_error_to_str(int gdl_status_retval)
{
	return strerror(-gdl_status_retval);
}

int send_command_to_apu(struct task_desc *task,
                        struct apu_command *command,
                        struct gdl_mem_buffer *buffers,
                        int buffers_count)
{
	int error = gdl_mem_cpy_to_dev(task->command_mem_handle, command, sizeof(*command));
	if (error) {
		return error;
	}


	/* send command */
	return gdl_schedule_task_timeout(&task->gdl_task,
	                                 buffers,
	                                 buffers_count,
	                                 NULL,
	                                 task->ms_timeout,
	                                 GDL_USER_MAPPING);
}

/*
 * fill a buffer with random floating-point numbers, avoid NANs, subnormal
 * numbers and infinite.
 */
void fill_float_buffer_with_random_numbers(float *buffer, int count)
{
	for (int i = 0; i < count; ++i) {
		uint32_t exponent = 1 + rand() % 254;
		uint32_t mantissa = rand() % (1 << 22);
		uint32_t number = (exponent << 23) | mantissa;
		if (exponent > 199) {
			number |= 0x80000000;
		}
		buffer[i] = *(float *)&number;

		// printf("exponent=0x%X, mantissa=0x%X, number=0x%X, buffer[%d]=%E\n", exponent, mantissa, number, i, buffer[i]);
	}
}

int calculate_digests(gdl_context_handle_t board_ctx, struct task_desc *task, struct apu_command *command)
{
	int error = 0;
	char *digest1 = NULL;
	float *data1 = NULL;
	char *digest2 = NULL;
	float *data2 = NULL;
	gdl_mem_handle_t dataset1_digest_mem_handle = GDL_MEM_HANDLE_NULL;
	gdl_mem_handle_t dataset1_vectors_mem_handle = GDL_MEM_HANDLE_NULL;
	gdl_mem_handle_t dataset2_digest_mem_handle = GDL_MEM_HANDLE_NULL;
	gdl_mem_handle_t dataset2_vectors_mem_handle = GDL_MEM_HANDLE_NULL;
	struct apu_dataset_desc dataset1 = {0};
	struct apu_dataset_desc dataset2 = {0};

	/* allocate memory on the host */
	digest1 = malloc(APU_DIGEST_SIZE);
	if (NULL == digest1) {
		error = ENOMEM;
		fprintf(stderr, "%s: failed to allocate memory for dataset1's digest\n", strerror(error));
		goto FREE_HOST_BUFFERS;
	}
	data1 = malloc(DATASET1_SIZE);
	if (NULL == data1) {
		error = ENOMEM;
		fprintf(stderr, "%s: failed to allocate memory for dataset1 vectors\n", strerror(error));
		goto FREE_HOST_BUFFERS;
	}
	digest2 = malloc(APU_DIGEST_SIZE);
	if (NULL == digest2) {
		error = ENOMEM;
		fprintf(stderr, "%s: failed to allocate memory for dataset2's digest\n", strerror(error));
		goto FREE_HOST_BUFFERS;
	}
	data2 = malloc(DATASET2_SIZE);
	if (NULL == data2) {
		error = ENOMEM;
		fprintf(stderr, "%s: failed to allocate memory for dataset2 vectors\n", strerror(error));
		goto FREE_HOST_BUFFERS;
	}

	printf("initializing datasets - "); fflush(stdout);
	fill_float_buffer_with_random_numbers(data1, DATASET1_VECTOR_LENGTH * DATASET1_VECTOR_COUNT);
	fill_float_buffer_with_random_numbers(data2, DATASET2_VECTOR_LENGTH * DATASET2_VECTOR_COUNT);
	printf("DONE\n"); fflush(stdout);

	printf("allocating dataset1 buffers on the L4 memory - "); fflush(stdout);
	dataset1_digest_mem_handle = gdl_mem_alloc_nonull(board_ctx, APU_DIGEST_SIZE, GDL_CONST_MAPPED_POOL);
	dataset1_vectors_mem_handle = gdl_mem_alloc_nonull(board_ctx, DATASET1_SIZE, GDL_CONST_MAPPED_POOL);
	printf("DONE\n"); fflush(stdout);

	printf("allocating dataset2 buffers on the L4 memory - "); fflush(stdout);
	dataset2_digest_mem_handle = gdl_mem_alloc_nonull(board_ctx, APU_DIGEST_SIZE, GDL_CONST_MAPPED_POOL);
	dataset2_vectors_mem_handle = gdl_mem_alloc_nonull(board_ctx, DATASET2_SIZE, GDL_DYNAMIC_MAPPED_POOL);
	printf("DONE\n"); fflush(stdout);



	printf("copying vectors data to the L4 memory - "); fflush(stdout);
	error = gdl_mem_cpy_to_dev(dataset1_vectors_mem_handle, data1, DATASET1_SIZE);
	if (error) {
		fprintf(stderr, "%s: failed to copy dataset1's buffer to the L4 memory\n", gdl_error_to_str(error));
		goto FREE_L4_BUFFERS;
	}

	error = gdl_mem_cpy_to_dev(dataset2_vectors_mem_handle, data2, DATASET2_SIZE);
	if (error) {
		fprintf(stderr, "%s: failed to copy dataset2's buffer to the L4 memory\n", gdl_error_to_str(error));
		goto FREE_L4_BUFFERS;
	}
	printf("DONE\n"); fflush(stdout);

	/* send the digest command for dataset1 */
	printf("sending the digest command for the first dataset - "); fflush(stdout);
	dataset1 = (struct apu_dataset_desc) {
		.vector_length = DATASET1_VECTOR_LENGTH,
		.vector_count = DATASET1_VECTOR_COUNT,
		.vectors = (uint64_t)dataset1_vectors_mem_handle,
	};
	command->name = APU_COMMAND_NAME_DIGEST;
	command->digest.output = dataset1_digest_mem_handle;
	command->digest.dataset = dataset1;
	error = send_command_to_apu(task, command, GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE);
	if (error) {
		fprintf(stderr, "%s: failed to calculate a digest for dataset1\n", gdl_error_to_str(error));
		goto FREE_L4_BUFFERS;

	}
	printf("DONE\n"); fflush(stdout);

	printf("sending the digest command for the second dataset - "); fflush(stdout);
	dataset2 = (struct apu_dataset_desc) {
		.vector_length = DATASET2_VECTOR_LENGTH,
		.vector_count = DATASET2_VECTOR_COUNT,
		.vectors = (uint64_t)dataset2_vectors_mem_handle,
	};
	command->name = APU_COMMAND_NAME_DIGEST;
	command->digest.output = dataset2_digest_mem_handle;
	command->digest.dataset = dataset2;
	error = send_command_to_apu(task, command, GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE);
	if (error) {
		fprintf(stderr, "%s: failed to calculate a digest for dataset2\n", gdl_error_to_str(error));
		goto FREE_L4_BUFFERS;

	}
	printf("DONE\n"); fflush(stdout);

	/* get the digest and print it */
	error = gdl_mem_cpy_from_dev(digest1, dataset1_digest_mem_handle, APU_DIGEST_SIZE);
	if (error) {
		fprintf(stderr, "%s: failed to copy digest for dataset1 from L4 memory\n", gdl_error_to_str(error));
		goto FREE_L4_BUFFERS;
	}

	error = gdl_mem_cpy_from_dev(digest2, dataset2_digest_mem_handle, APU_DIGEST_SIZE);
	if (error) {
		fprintf(stderr, "%s: failed to copy digest for dataset2 from L4 memory\n", gdl_error_to_str(error));
		goto FREE_L4_BUFFERS;
	}

	printf("** Dataset1's Digest *******************************************************************************************************\n"); fflush(stdout);
	printf("%s", digest1); fflush(stdout);
	printf("----------------------------------------------------------------------------------------------------------------------------\n"); fflush(stdout);

	printf("** Dataset2's Digest *******************************************************************************************************\n"); fflush(stdout);
	printf("%s", digest2); fflush(stdout);
	printf("----------------------------------------------------------------------------------------------------------------------------\n"); fflush(stdout);

FREE_L4_BUFFERS:
	gdl_mem_free(dataset2_vectors_mem_handle);
	gdl_mem_free(dataset2_digest_mem_handle);
	gdl_mem_free(dataset1_vectors_mem_handle);
	gdl_mem_free(dataset1_digest_mem_handle);
FREE_HOST_BUFFERS:
	free(data2);
	free(digest2);
	free(data1);
	free(digest1);
	return error;
}

int init_task(gdl_context_handle_t board_ctx, struct task_desc *task)
{
	/* prepare buffer for command on the device */
	task->command_mem_handle = gdl_mem_alloc_nonull(board_ctx,
	                                                sizeof(struct apu_command),
	                                                GDL_CONST_MAPPED_POOL);
	task->ms_timeout = 1000;
	struct gsi_task_desc *res = gdl_task_desc_init(board_ctx,
	                                               &task->gdl_task,
	                                               GDL_TASK(dynamic_memory),
	                                               task->command_mem_handle,
	                                               GDL_MEM_HANDLE_NULL,
	                                               0,
	                                               0);
	return NULL == res;
}

void deinit_task(struct task_desc *task)
{
	gdl_mem_free(task->command_mem_handle);
}

int main(void)
{
	struct task_desc task = {0};
	struct apu_command command = {0};

	srand(time(NULL));

	int error = gdl_init();
	if (error) {
		fprintf(stderr, "%s: failed to initialize GDL\n", gdl_error_to_str(error));
		abort();
	}

	gdl_context_handle_t board_ctx = (gdl_context_handle_t)NULL;
	unsigned long long constant_memory_pool_size = 0;
	unsigned long long dynamic_memory_pool_size = 0;

	error = gdl_context_find_and_alloc(4,                          /* number of APUCs requested */
	                                   6 * GB,                     /* minimal total memory size */
	                                   0,                          /* size of the constant memory pool */
	                                   NULL,                       /* received size of constant memory pool */
	                                   &dynamic_memory_pool_size,  /* received size of dynamic memory pool */
	                                   &board_ctx);                /* output, the APU handle */
	if (error) {
		fprintf(stderr, "%s: failed to allocate a GDL context\n", gdl_error_to_str(error));
		goto EXIT;
	}
	printf("constant memory pool size=%llu dynamic memory pool size=%llu\n",
	       constant_memory_pool_size,
	       dynamic_memory_pool_size);

	error = init_task(board_ctx, &task);
	if (error) {
		fprintf(stderr, "failed to initialize task\n");
		goto FREE_GDL_CONTEXT;
	}

	printf("sending INIT command to the APU - ");

	command.name = APU_COMMAND_NAME_INIT;
	error = send_command_to_apu(&task, &command, GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE);
	if (error) {
		fprintf(stderr, "%s: failed to initialize APUC\n", gdl_error_to_str(error));
		goto FREE_TASK;
	}
	printf("DONE\n");

	error = calculate_digests(board_ctx, &task, &command);
	if (error) {
		fprintf(stderr, "failed to calculate digests\n");
		goto EXIT_CMD;
	}

	printf("sending EXIT command to the APU - ");

EXIT_CMD:
	command.name = APU_COMMAND_NAME_EXIT;
	error = send_command_to_apu(&task, &command, GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE);
	if (error) {
		fprintf(stderr, "%s: failed to exit the APUC\n", gdl_error_to_str(error));
	}
	printf("DONE\n");

FREE_TASK:
	deinit_task(&task);

FREE_GDL_CONTEXT:
	(void)gdl_context_free(board_ctx);
EXIT:
	error = gdl_exit();
	if (error) {
		fprintf(stderr, "%s: failed to exit GDL\n", strerror(error));
		abort();
	}

	return error;
}

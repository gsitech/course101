#!/bin/bash

# Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
#
# This software source code is the sole property of GSI Technology, Inc.
# and is proprietary and confidential.
#

rm -rf ./build
mkdir ./build

app_name=dynamic-memory
apu_file=./$app_name-module.c
host_app_file=./$app_name.c

gdl_a=/usr/lib/gsi/libgsidevice.a
gal_a=/usr/lib/gsi/libgsiapu_arc.a
libsys_a=/usr/lib/gsi/libgsisys_archs36.a
apu_cc=/efs/data/public/synopsis/ARC-2018.06/MetaWare/arc/bin/ccac
apu_cc_args="-tcf=/efs/data/public/synopsis/arch/arc.tcf -tcf_apex -tcf_core_config /usr/share/gsi/wnc-l4/arc_module.lcf -Hnocopyr -Hnosdata -Hpictable"
apu_link_args=""
apu_syslib_args="-DGSI_LIBSYS_ARCHS36"
elf2bin="/efs/data/public/synopsis/ARC-2018.06/MetaWare/arc/bin/elf2bin  -Pload -q"

# Get licence
source /efs/data/public/synopsis/ARC-2018.06/env_set.env

# Compile application module task
$apu_cc $apu_cc_args $apu_syslib_args \
        -I. -I /usr/include \
        -I /usr/include/gsi/aarch64 \
        -I /usr/include/gsi/archs36 \
        -I /usr/include/gsi/x86_32 \
        -I /usr/include/gsi/x86_64 -c $apu_file \
        -o ./build/$app_name-module.o

# Build application module
$apu_cc $apu_cc_args $apu_link_args\
        ./build/$app_name-module.o \
        $gal_a \
        $libsys_a \
        -o ./build/$app_name.mod

# Use modtool to get module info
/usr/bin/modtool -c ./build/$app_name-defs.c -i ./build/$app_name-defs.h -b ./build/$app_name.mod

# Convert module to bin
$elf2bin ./build/$app_name.mod ./build/$app_name.bin

# Create object file from module bin
obj_name=$(echo ./build/$app_name.bin | sed 's@[/.-]@\_@g')
objcopy -I binary -O elf64-x86-64 -B i386 \
        --redefine-sym _binary_${obj_name}_start=gsi_module_dump_start \
        --redefine-sym _binary_${obj_name}_end=gsi_module_dump_end \
        --redefine-sym _binary_${obj_name}_size=gsi_module_dump_size \
        ./build/$app_name.bin \
        ./build/$app_name-apuc-code.o

# Build host application from module-object, gdl-libs and user application
gcc -DGSI_LIBSYS_X86_64 \
        -I /usr/include \
        -I /usr/include/gsi/aarch64 \
        -I /usr/include/gsi/archs36 \
        -I /usr/include/gsi/x86_32 \
        -I /usr/include/gsi/x86_64 \
        -I ./build \
        -pthread \
        $host_app_file \
        $gdl_a \
        /usr/lib/gsi/libgsisys_x86_64.a \
        ./build/$app_name-defs.c \
        ./build/$app_name-apuc-code.o \
        -o ./build/$app_name \
        -ldl -lm

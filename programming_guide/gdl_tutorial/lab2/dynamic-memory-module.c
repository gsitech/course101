#include <string.h>
#include <stdint.h>

#include <gsi/libgal.h>

#include "dynamic-memory-module.h"

const char *opening_sentence[] = {
	/* The Wonderful Wizard of Oz */
	"Dorothy lived in the midst of the great Kansas prairies, with Uncle Henry, who was a farmer, and Aunt Em, who was the farmer’s wife.\n"
	"Their house was small, for the lumber to build it had to be carried by wagon many miles.\n",

	/* The Picture of Dorian Gray */
	"The studio was filled with the rich odour of roses, and when the light summer wind stirred amidst the trees of the garden,\n"
	"there came through the open door the heavy scent of the lilac, or the more delicate perfume of the pink-flowering thorn.\n"
};

GAL_TASK_ENTRY_POINT(dynamic_memory, cmd, out)
{
	struct apu_command *command = cmd;

	switch (command->name) {
	case APU_COMMAND_NAME_INIT: {
		gal_init();
	} break;
	case APU_COMMAND_NAME_EXIT: {
		gal_exit();
	} break;
	case APU_COMMAND_NAME_DIGEST: {
		char *digest = gal_mem_handle_to_apu_ptr(command->digest.output);
		float *vectors = gal_mem_handle_to_apu_ptr(command->digest.dataset.vectors);
		unsigned int length = command->digest.dataset.vector_length;
		unsigned int count = command->digest.dataset.vector_count;
		unsigned int last = length * count - 1;
		if (vectors[last] > 1.0) {
			strcpy(digest, opening_sentence[0]);
		} else {
			strcpy(digest, opening_sentence[1]);
		}
	} break;
	default:
		return -1;
	}


	return 0;
}

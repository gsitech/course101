#ifndef DYNAMIC_MEMORY_MODULE_H
#define DYNAMIC_MEMORY_MODULE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>


#define APU_PACKED __attribute__((packed))
#define APU_DIGEST_SIZE 256

enum apu_command_name {
	APU_COMMAND_NAME_INIT,
	APU_COMMAND_NAME_EXIT,
	APU_COMMAND_NAME_DIGEST,
};

/* a dataset is a collection of single-precision floating-point vectors */
struct apu_dataset_desc {
	uint32_t vector_length;           /* length of every vector in the dataset */
	uint32_t vector_count;            /* the total number of vectors in the dataset */
	uint64_t vectors;                 /* memory_handle to the vectors */
} APU_PACKED;

struct apu_digest_command_desc {
	uint64_t output;                  /* memory_handle to an array of APU_DIGEST_SIZE bytes to hold the digest */
	struct apu_dataset_desc dataset;
} APU_PACKED;

struct apu_command {
	uint32_t name;                    /* use enum apu_command_name */
	union {
		struct apu_digest_command_desc digest;
	};
} APU_PACKED;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* DYNAMIC_MEMORY_MODULE_H */
